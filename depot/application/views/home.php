<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">หน้าแรก</strong>
</div>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 16px;">ข้อมูลภาพรวม</strong>
    <table class="table table-bordered">
        <tbody>
        <tr>
            <td>จำนวนผู้ใช้ทั้งหมด</td>
            <td style="width: 100px;" class="text-right"> 999 คน</td>
        </tr>
        <tr>
            <td>จำนวนผู้ประกอบการทั้งหมด</td>
            <td style="width: 100px;" class="text-right"> 35,678 ราย</td>
        </tr>
        <tr>
            <td>คำร้องทั้งหมดที่ยังไม่ได้รับการยืนยัน</td>
            <td style="width: 100px;" class="text-right"> 9 รายการ</td>
        </tr>
        </tbody>
    </table>
</div>