<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Car Servicing</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.structure.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap.min.css');?>">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap-theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('fonts/stylesheet.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/style.css');?>">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo site_assets_url('js/jquery-3.1.1.min.js');?>"></script>
    <script src="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrap.min.js');?>"></script>
</head>
<body>
<div class="container">
    <div class="row" style="background-color: #666;">
        <div class="col-md-6">
            <h3 style="color: #fff;">Back Office System</h3>
        </div>
        <div class="col-md-6" style="background-color: #666;">
            <div class="text-right" style="line-height: 48px;">
                <span></span>
                <a href="index.html" style="color: #FFF;"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top: 4px;">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked row">
                <li role="presentation" class="active"><a href="#">
                        <span class="glyphicon glyphicon-home"></span> หน้แรก
                    </a></li>
                <li role="presentation" class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-file"></span> จัการข้อมูลประชาสัมพันธ์ <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li role="presentation"><a href="article.html">บทความ</a></li>
                        <li role="presentation"><a href="promotion.html">โปรโมชั้น</a></li>
                        <li role="presentation"><a href="advertise.html">โฆษณา</a></li>
                        <li role="presentation"><a href="emergency.html">การติดต่อฉุกเฉิน</a></li>
                    </ul>
                </li>
                <li role="presentation"><a href="user.html">
                        <span class="glyphicon glyphicon-user"></span> จัดการข้อมูลผู้ใช้งาน
                    </a></li>
                <li role="presentation"><a href="business.html">
                        <span class="glyphicon glyphicon-flag"></span> จัดการข้อมูลผู้ประกอบการ
                    </a></li>
                <li role="presentation"><a href="staff.php">
                        <span class="glyphicon glyphicon-eye-open"></span> จัดการข้อมูลผู้ดูแลระบบ
                    </a></li>
                <li role="presentation"><a href="index.html">
                        <span class="glyphicon glyphicon-log-out"></span> ออกจากระบบ
                    </a></li>
            </ul>
        </div>
        <div class="col-md-9">
            <div class="row">