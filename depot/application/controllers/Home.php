<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
	    header('Content-type:Application/json');
        $post = array();
//        $post['$_POST'] = $_POST;
//        $post['$_FILES'] = $_FILES;
//        echo json_encode($_FILES, true); die;
        if (!empty($_FILES)) {
            $post['result'] = $this->do_upload();
        }else{
            $data = $post['$_POST'] = $_POST;
            $data['$_FILES'] = $_FILES;
            $post['result'] = array('status'=>'ERROR','code'=>'501', 'send_data' =>$data);
        }
        echo json_encode($post, true);
    }

    public function do_upload()
    {
        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024*4;
        $config['max_width'] = 2048;
        $config['max_height'] = 2048;
        $img_list = array();
        $result = array();

        $this->load->library('upload', $config);
        $img_num = (int)$this->input->post('img_num');
        $img_type = $this->input->post('img_type');
        $ref_id = $this->input->post('ref_id');
        if ($img_num > 0) {
            for ($i = 0; $i < $img_num; $i++) {
                $file_name = $_FILES['upload_img_' . $i]['name'];
                $file_name = explode('.',$file_name);
                $file_name = $file_name[count($file_name)-1];
                $file_name = $img_type . '_' . $ref_id . '_' . date('YmdHis_').$i.".".$file_name;
                $config['file_name'] = $file_name;
                $img_list[] = $file_name;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('upload_img_' . $i)) {

                    $error_msg = $this->upload->display_errors();
                    if(strpos($error_msg,'filetype')>0){
                        $error_msg.=' (yours filetype is '. $_FILES['upload_img_' . $i]['type'].')';
                    }
                    $result[] = array('error' => $error_msg);

                } else {
                    $result[] = $this->add_to_DB($img_type, $file_name, $ref_id);
                }
            }
        }else{
            array('status'=>'ERROR','code'=>'502', 'detail' =>'No Photo to upload.');
        }
        return $result;
    }

    public function upload_image()
    {
        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 1024*4;
        $config['max_width'] = 2048;
        $config['max_height'] = 2048;
        $img_list = array();
        $result = array();

        $this->load->library('upload', $config);
        $img_num = (int)$this->input->post('img_num');
        $img_type = $this->input->post('img_type');
        $ref_id = $this->input->post('ref_id');
        if ($img_num > 0) {
            $file_name = $_FILES['upload_img']['name'];
            $file_name = explode('.',$file_name);
            $file_name = $file_name[1];
            $file_name = $img_type . '_' . $ref_id . '_' . date('YmdHis.').$file_name;
            $config['file_name'] = $file_name;
            $img_list[] = $file_name;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('upload_img')) {

                $error_msg = $this->upload->display_errors();
                if(strpos($error_msg,'filetype')>0){
                    $error_msg.=' (yours filetype is '. $_FILES['upload_img']['type'].')';
                }
                $result = array('error' => $error_msg);

            } else {
                $result = $this->add_to_DB($img_type, $file_name, $ref_id);
            }
        }else{
            array('status'=>'ERROR','code'=>'502', 'detail' =>'No Photo to upload.');
        }
        echo json_encode(array('result'=>$result),true) ;
    }

    private function add_to_DB($img_type, $img_name, $ref_id)
    {
        $img_src = site_url('images/' . $img_name);
        $now_time = date('Y-m-d H:i:s');
        $data = array(
            'type' => $img_type,
            'ref_id' => $ref_id,
            'src' => $img_src,
            'name' => $img_name,
            'updated_time' => $now_time,
            'created_time' => $now_time
        );
        if ($this->db->insert('images', $data)) {
            return  array('src' => $img_src, 'name' => $img_name, 'id'=>$this->db->insert_id());
        } else {
            return array('error' => $this->db->_error_message());
        }
    }

    public function clean_deleted_file(){
        $this->db->where('status','deleted');
        $query = $this->db->get('images');
        console_log($query->num_rows());
        if($query->num_rows()>0){
            $result_data = $query->result();
            foreach ($result_data as $item){
                $file = __DIR__."/../../images/".$item->name;
                console_log($file);
                var_dump(unlink($file));
            }
            die;
        }
//        unlink('path_to_filename');
    }

    public function move_file(){
        $this->db->where('status !=','deleted');
        $query = $this->db->get('images');
        console_log($query->num_rows());
        if($query->num_rows()>0){
            $result_data = $query->result();
            foreach ($result_data as $item){
                $file_1 = __DIR__."/../../images/".$item->name;
                $file_2 = __DIR__."/../../images_2/".$item->name;
                var_dump(copy($file_1, $file_2));
            }
            die;
        }
//        unlink('path_to_filename');
    }
}
