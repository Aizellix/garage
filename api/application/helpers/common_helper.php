<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Site Assets URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('print_json'))
{
	function print_json($data = array())
	{
	    header('Content-type:Application/json');
		echo json_encode($data,true);
	}
}

if ( ! function_exists('site_assets_url'))
{
	function site_assets_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/'.$uri);
	}
}

if ( ! function_exists('music_site_url'))
{
    function music_site_url($uri = '')
    {
        $CI =& get_instance();
        $CI->config->load('api_config');
        $music_site = $CI->config->item('music_site');
        return $music_site.$uri;
    }
}
// ------------------------------------------------------------------------

/**
 * Site Root URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('console_log'))
{
    function console_log($data = '')
    {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
    }
}


if ( ! function_exists('site_root_url'))
{
	function site_root_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url($uri);
	}
}

if ( ! function_exists('time_ago') ) {
  function time_ago( $date, $granularity = 2 ) {
    $date = strtotime($date);
    $difference = time() - $date;
    $periods = array(
      'decade' => 315360000,
      'year'   => 31536000,
      'month'  => 2628000,
      'week'   => 604800,
      'day'    => 86400,
      'hour'   => 3600,
      'minute' => 60,
      'second' => 1
    );
    $retval = '';
    foreach ($periods as $key => $value) {
      if ($difference >= $value) {
        $time = floor($difference/$value);
        $difference %= $value;
        $retval .= ($retval ? ' ' : '').$time.' ';
        $retval .= (($time > 1) ? $key.'s' : $key);
        $granularity--;
      }
      if ($granularity == '0') { break; }
    }
    if ( $retval == '' ) { $retval = 'a few second';}
    return ' posted '.$retval.' ago';
  }
}


if ( ! function_exists('viewed_increment'))
{
    function viewed_increment($item_id = '', $item_type = '')
    {
        $CI =& get_instance();
        $CI->db->select('id,viewed');
        $CI->db->where(array('id'=>$item_id,'status !='=>'deleted'));
        $query = $CI->db->get($item_type);
        if($query->num_rows()>0){

            $business_data = $query->result();
            $business_data = $business_data[0];

            $new_viewed = (int)$business_data->viewed;

            $update_data = array(
                'viewed' => $new_viewed+1
            );
            $CI->db->where('id', $item_id);
            if ($CI->db->update($item_type, $update_data)) {
                $CI->config->load('config');
                $api_url = $CI->config->item('api_url');
                $api_param = array(
                    'type'=>$item_type,
                    'id'=>$item_id
                );
                $CI->curl->simple_post($api_url.'stat/logger',$api_param);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

if ( ! function_exists('update_review_score'))
{
    function update_review_score($business_id = '',$score = 0,$operation = '',$old_score=0)
    {
        $CI =& get_instance();
        $CI->db->select('id,score,reviewed');
        $CI->db->where(array('id'=>$business_id));
        $query = $CI->db->get('business');
        if($query->num_rows()>0){
            $business_data = $query->result();
            $business_data = $business_data[0];
            $new_score = (int)$business_data->score;
            $new_reviewed = (int)$business_data->reviewed;
            if($operation=="add"){
                $new_score += $score;
                $new_reviewed +=1;
            }else if($operation=="edit"){
                $new_score -= $old_score;
                $new_score += $score;
                $new_reviewed += 0;
            }else{
                $new_score -= $score;
                $new_reviewed -=1;
            }
            if($new_score<0) $new_score = 0;
            if($new_reviewed<0) $new_reviewed = 0;
            $update_data = array(
                'score' => $new_score,
                'reviewed' => $new_reviewed
            );
            $CI->db->where('id', $business_id);
            if ($CI->db->update('business', $update_data)) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

if ( ! function_exists('update_review_user'))
{
    function update_review_user($user_id = '',$operation = '')
    {
        $CI =& get_instance();
        $CI->db->select('id,reviewed');
        $CI->db->where(array('id'=>$user_id));
        $query = $CI->db->get('user');
        if($query->num_rows()>0){
            $user_data = $query->result();
            $user_data = $user_data[0];
            $new_reviewed = (int)$user_data->reviewed;
            if($operation=="add"){
                $new_reviewed +=1;
            }else if($operation=="edit"){
                $new_reviewed += 0;
            }else{
                $new_reviewed -=1;
            }
            if($new_reviewed<0) $new_reviewed = 0;
            $update_data = array(
                'reviewed' => $new_reviewed
            );
            $CI->db->where('id', $user_id);
            if ($CI->db->update('user', $update_data)) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

if ( ! function_exists('cal_distance'))
{
    function cal_distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $unit='M')
    {
        $theta = $longitudeFrom - $longitudeTo;
        $dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        $decimal = 2;
        if ($unit == "KM") {
            $distance = ($miles * 1.609344);
        } else if ($unit == "M") {
            $decimal = 0;
            $distance = ($miles * 1.609344 * 1000);
        }else if ($unit == "N") {
            $distance = ($miles * 0.8684);
        } else {
            $distance = $miles;
        }
        return round($distance, $decimal);
    }
}

if ( ! function_exists('using_deal'))
{
    function using_deal($deal_data, $business_data, $user_data, $user_point, $latitude, $longitude)
    {
        $CI =& get_instance();
        $deal_id = $deal_data->id;
        $business_id = $business_data->id;
        $user_id = $user_data->id;
        $point_type = $deal_data->point_type;
        $quota_left = $deal_data->quota_left;
        $deal_point = (int)$deal_data->point;
        $usage = (int)$deal_data->usage;
        $deal_type = $deal_data->type;
        point_log($user_id, $deal_point, "use_deal", null, $point_type, $deal_id);

        if($point_type=="use"){
            if($user_point<$deal_point){
                $output = array(
                    'status' => 'USER_POINT_IS_NOT_ENOUGH',
                    'code'=> 200
                );
                print_json($output); die;
            }
            $user_point_updated = $user_point - $deal_point;
        }else{
            $user_point_updated = $user_point + $deal_point;
        }
        $CI->db->where('id', $user_id);
        $CI->db->where('status !=', "deleted");
        $CI->db->update('user', array("point"=>$user_point_updated,'updated_time' => date("Y-m-d H:i:s")));

        $quota_left-=1;
        $usage+=1;
        $deal_data_updated = array(
            "quota_left"=>$quota_left,
            "usage"=>$usage,
            'updated_time' => date("Y-m-d H:i:s")
        );
        $CI->db->where('id', $deal_id);
        $CI->db->where('status !=', "deleted");
        $CI->db->update('deal', $deal_data_updated);

        $deal_long_data = array(
            "user_id"=>$user_id,
            "deal_id"=>$deal_id,
            "business_id"=>$business_id,
            "business_name"=>$business_data->name,
            "user_name"=>$user_data->name,
            "deal_name"=>$deal_data->title,
            "deal_type"=>$deal_type,
            "point"=>$deal_point,
            "point_type"=>$point_type,
            "user_old_point"=>$user_point,
            "user_new_point"=>$user_point_updated,
            "lat"=>$latitude,
            "lng"=>$longitude,
            'created_time' => date("Y-m-d H:i:s")
        );
        $CI->db->insert('deal_log', $deal_long_data);
        return $deal_long_data;
    }
}

if ( ! function_exists('history_log'))
{
    function history_log($user_id = '',$user_name = '',$content_id = '', $status='', $type='')
    {
        $CI =& get_instance();
        $query = $CI->db->get_where('history_log',array('content_id'=>$content_id,'status'=>$status,'type'=>$type));
        $created_time = date('Y-m-d H:i:s');
        if($query->num_rows()>0){
            $update_data = array(
                'user_id' => $user_id,
                'user_name' => $user_name,
                'created_time' => $created_time
            );
            $CI->db->where(array('content_id'=>$content_id,'status'=>$status));
            if ($CI->db->update('history_log', $update_data)) {
                return true;
            }else{
                return false;
            }
        }else{
            $update_data = array(
                'user_id' => $user_id,
                'user_name' => $user_name,
                'content_id' => $content_id,
                'status' => $status,
                'created_time' => $created_time,
                'type' => $type
            );
            if ($CI->db->insert('history_log', $update_data)) {
                return true;
            }else{
                return false;
            }
        }
    }
}


if ( ! function_exists('create_business_code'))
{
    function create_business_code($business_id){
        $max_length = 7;
//        echo "<per>";
        $business_id_base20 = base_convert($business_id, 10, 20);
        $decimal_num = array("U","K","L","M","H","I","J","R","S","T","N","O","E","F","G","P","Q","B","C","D"); //0-19
        $separate_num = array("X","Y","Z"); //0-2
        $random_num = array("A","B","C","D","E","F","G","R","S","T","U","V","W","H","I","J","K","L","M","N","O","P","Q"); //0-22
        $business_code = array();

        $business_id_arr = str_split($business_id_base20, 1);

        $pre_code_length = $max_length-count($business_id_arr);
        if($pre_code_length-1<=0) $pre_code_length+=2;
        for($i=0;$i<$pre_code_length-1;$i++){
            $business_code[] = $random_num[rand(0,22)];
        }
        $business_code[] = $separate_num[rand(0,2)];
        foreach ($business_id_arr as $item){
            $key = base_convert($item, 20, 10);
            $business_code[] = $decimal_num[$key];
        }
        $business_code = implode($business_code);
        //var_dump($business_id,$business_code);
        return strtoupper($business_code);
    }
}

if ( ! function_exists('add_user_point'))
{
    function add_user_point($user_id, $point, $type, $ref_id, $ref_id2 = 0, $delay_day = null){
        $CI =& get_instance();
        $CI->db->where('id', $user_id);
        $query = $CI->db->get('user');
        $user_data = $query->result();
        $current_point = 0;
        foreach ($user_data as $item){
            $current_point = (int)$item->point;
            $current_point+=$point;
        }
        $CI->db->where('id', $user_id);
        $CI->db->update('user', array("point"=>$current_point));
        point_log($user_id, $point, $type, $delay_day, 'get', $ref_id, $ref_id2);
        $point_data = array(
            "message" => "คุณได้รับ ".$point." แต้ม",
            "point" => $point
        );
        return $point_data;
    }
}


if ( ! function_exists('point_log'))
{
    function point_log($user_id, $point, $type, $delay_day, $usage, $ref_id, $ref_id2){
        $CI =& get_instance();
        $data = array(
            'user_id'=>$user_id,
            'ref_id'=>$ref_id,
            'ref_id2'=>$ref_id2,
            'point'=>$point,
            'type'=>$type,
            'usage'=>$usage,
            'delay'=>$delay_day,
            'created_time'=>date("Y-m-d H:i:s"),
        );
        $CI->db->insert('point_log', $data);
    }
}

if ( ! function_exists('gen_business_code'))
{
    function gen_business_code($business_id){
        $CI =& get_instance();
        $business_code = create_business_code($business_id);
        $CI->db->where('id', $business_id);
        $CI->db->where('status !=', "deleted");
        $CI->db->update('business', array("business_code"=>$business_code));
    }
}

if ( ! function_exists('decode_business_code'))
{
    function decode_business_code($business_code){
        $business_code_arr = str_split($business_code,1);
        //echo "<pre>";
        $decimal_num = array("U","K","L","M","H","I","J","R","S","T","N","O","E","F","G","P","Q","B","C","D"); //0-19
        $separate_num = array("X","Y","Z"); //0-2
        $separate_key = -1;
        foreach ($separate_num as $item){
            $result = array_keys($business_code_arr,$item);
            if(count($result)>0){
                //var_dump($result);
                $separate_key = $result[0];
            }
        }
        $business_id_base20_encoded_arr = array_slice($business_code_arr,$separate_key+1);
        $business_id_base20_arr = array();
        foreach ($business_id_base20_encoded_arr as $item){
            $result = array_keys($decimal_num,$item);
            if(count($result)>0){
                //var_dump($result);
                $key = $result[0];
                $business_id_base20_arr[] = base_convert($key, 10, 20);
            }
        }
        $business_id_base20 = implode($business_id_base20_arr);
        $business_id = base_convert($business_id_base20, 20, 10);
        //var_dump($business_code,$business_id_base20_encoded_arr,$business_id_base20,$business_id);
        return $business_id;
    }
}