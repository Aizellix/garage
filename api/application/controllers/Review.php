<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller
{
    private $error_post_data = array();
    private $api_key = '';

    public function __construct()
    {
        parent::__construct();
        $this->api_key = $this->config->item('api_key');
    }


    public function index()
    {
        echo json_encode('Review');
    }
    public function get_list($business_id){
        $offset = ($this->input->get('offset'))?$this->input->get('offset'):0;
        $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;
        $page = (int)($this->input->get('page'))?$this->input->get('page'):1;
        $skip = (int)($this->input->get('skip'))?$this->input->get('skip'):false;
        $id_list = ($this->input->get('id_list'))?$this->input->get('id_list'):'';
        if($id_list!=''){
            $id_list = explode('|',$id_list);
            $id_list = array_diff($id_list,array(''));
        }else{
            $id_list = array();
        }
        $this->db->from('review');
        $this->db->where(array('business_id'=>$business_id,'status !='=> 'deleted'));
        if(count($id_list)>0){
            $this->db->where_in('id',$id_list);
        }
        $this->db->where_not_in('id',$skip);
        $total_num = $this->db->count_all_results();

        $this->db->order_by('id','DESC');
        $this->db->where_not_in('id', $skip);
        if(count($id_list)>0){
            $this->db->where_in('id',$id_list);
        }
        $query = $this->db->get_where('review',array('business_id'=>$business_id,'status !='=> 'deleted'),$limit,$offset);

        $review_list = $query->result();

        $user_id_list = array();
        foreach ($review_list as $list){
            $user_id_list[] = $list->user_id;
        }
        $user_ist = array();
        $offset = $limit+$offset;
        if(count($user_id_list)>0){
            $this->db->select('id,point,fb_id,email,name,birthday,gender,profile_image,created_time,updated_time');
            $this->db->where_in('id',$user_id_list);
            $query = $this->db->get_where('user',array('status !='=> 'deleted'));
            $user_data = $query->result();
            foreach ($user_data as $list){
                $user_ist[$list->id] = $list;
            }
        }

        foreach ($review_list as $key => $list){
            $user_id = $list->user_id;
            if(!isset($user_ist[$user_id])){
                $this->delete_review_user_not_found($business_id,$user_id,$list->id);
            }else{
                if($user_id == $user_ist[$user_id]->id){
                    $review_list[$key]->user_data = $user_ist[$user_id];
                }
            }
        }
//        var_dump($review_list); die;
        foreach ($review_list as $key => $list){
            $query = $this->db->get_where('images',array('ref_id'=>$list->id,"type"=>"review",'status !='=> 'deleted'));
            $img_data = array();
            foreach ($query->result() as $img_list){
                $img_data[] = array("id"=>$img_list->id,"url"=>$img_list->src);
            }
            $review_list[$key]->images = $img_data;
        }
        $output = array();
        $total_page = ceil($total_num / $limit);
        $next_url = '';
        if($offset<$total_num){
            $url_param = array(
                'limit'=>$limit,
                'offset'=>$offset
            );
            if(is_array($skip)&&count($skip)>0){
                $url_param['skip'] = $skip;
            }
            $next_url = site_url('review/get_list/'.$business_id.'?'.http_build_query($url_param));
        }

        $output['total_page'] = $total_page;
        $output['total_num'] = $total_num;
        $output['limit'] = $limit;
        $output['page'] = $page;
        $output['next'] = $next_url;
        $output['data'] = $review_list;
        $output['status'] = 'SUCCESS';
        $output['code'] = 200;
        print_json($output);
    }

    private function del_all_photo($id){
        $this->db->where('ref_id', $id);
        $this->db->where('type', "review");
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }else{
            return true;
        }
    }

    public function add_review(){
        $api_key = $this->input->post('api_key');
        $business_id = $this->input->post('business_id');
        $detail = $this->input->post('detail');
        $score = (int)$this->input->post('score');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $device_id = $this->input->post('device_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id');
                $this->db->where(array('id'=>$business_id,'status !='=>'deleted'));
                $review_query = $this->db->get('business');
                if($review_query->num_rows()>0){

                    $this->db->where(array('user_id'=>$user_id,'business_id'=>$business_id,'status !='=>'deleted'));
                    $query = $this->db->get('review');
                    if($query->num_rows()>0){
                        $review_data = $query->result();
                        $score = $review_data[0]->score;
                        if(update_review_score($business_id,$score,'edit')){
                            if(update_review_user($user_id,'edit')){
                                $output = array(
                                    'status' => "ALREADY_REVIEWED",
                                    'code' => 200,
                                    'data' => $review_data
                                );
                                print_json($output); die;
                            }
                        }else{
                            var_dump($business_id,$score);die;
                        }
                        die;
                    }
                    $created_time = date('Y-m-d H:i:s');
                    $update_data = array(
                        'business_id' => $business_id,
                        'user_id' => $user_id,
                        'detail' => $detail,
                        'score' => $score,
                        'created_time' => $created_time,
                        'updated_time' => $created_time,
                        'status' => 'active'
                    );
                    if ($this->db->insert('review', $update_data)) {
                        $review_id = $this->db->insert_id();
                        $update_data['id'] = $review_id;
                        if(isset($_FILES['image'])){
                            if (is_array($_FILES['image']['name'])) {
                                $img_data = $this->update_photo($review_id, 'review','image');
                                $update_data['img'] = $img_data;
                            }
                        }
                        $this->config->load('config');
                        $api_url = $this->config->item('api_url');
                        $api_param = array(
                            'type'=>'business',
                            'id'=>$business_id,
                            'review'=>$this->db->insert_id(),
                            'score'=>$score
                        );
                        $this->curl->simple_post($api_url.'stat/logger',$api_param);
//                    var_dump($api_url.'stat/logger',$api_param); die;
                        if(update_review_score($business_id,$score,'add')){
                            if(update_review_user($user_id,'add')){
                                $this->db->where(array('type'=>"add_review",'business_id'=>$business_id,'status !='=>'deleted'));
                                $query = $this->db->get('review');
                                if($query->num_rows()<=0){
                                    $point_data = add_user_point($user_id,POINT_ADD_REVIEW, 'add_review', $review_id, $business_id);
                                }
                                $output = array(
                                    'status' => "SUCCESS",
                                    'code' => 200,
                                    'point_data' => $point_data,
                                    'data' => $update_data
                                );
                                print_json($output);
                            }
                        }
                    }else{
                        $output = array(
                            'status' => "CAN_NOT_CREATE_REVIEW",
                            'code' => 200
                        );
                        print_json($output);
                    }
                }else{
                    $output = array(
                        'status' => "BUSINESS_NOT_FOUND",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }
    private function update_photo($ref_id, $img_type,$file_label)
    {
        if (isset($_FILES)) {
            $POST_DATA = array();
            $img_num = count($_FILES[$file_label]['tmp_name']);
            if (is_array($_FILES[$file_label]['tmp_name'])) {
               // $POST_DATA["x"] = 1;
                for ($i = 0; $i < $img_num; $i++) {
                    $tmp_file = $_FILES[$file_label]['tmp_name'][$i];
                    $file_name = basename($_FILES[$file_label]['name'][$i]);
                    $file_type = $_FILES[$file_label]['type'][$i];
                    $POST_DATA['upload_img_' . $i] = curl_file_create($tmp_file, $file_type, $file_name);
                }
            } else {
                //$POST_DATA["x"] = 2;
                $tmp_file = $_FILES[$file_label]['tmp_name'];
                $file_name = basename($_FILES[$file_label]['name']);
                $file_type = $_FILES[$file_label]['type'];
                $POST_DATA['upload_img_0'] = curl_file_create($tmp_file, $file_type, $file_name);
            }
            $POST_DATA['img_num'] = $img_num;
            $POST_DATA['depot_key'] = $this->config->item('depot_key');
            $POST_DATA['img_type'] = $img_type;
            $POST_DATA['ref_id'] = $ref_id;
            $curl_handle = curl_init($this->config->item('depot_url'));
            curl_setopt($curl_handle, CURLOPT_POST, 1);
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

            $returned_data = curl_exec($curl_handle);
            curl_close($curl_handle);

            $returned_data = json_decode($returned_data, true);
            return $returned_data['result'];
        } else {
            return "1";
        }
    }

    public function edit_review(){
        $api_key = $this->input->post('api_key');
        $business_id = $this->input->post('business_id');
        $detail = $this->input->post('detail');
        $score = (int)$this->input->post('score');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $device_id = $this->input->post('device_id');
        $review_id = $this->input->post('review_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id,score');
                $this->db->where(array('id'=>$review_id,'business_id'=>$business_id, 'user_id' => $user_id,'status !='=>'deleted'));
                $query = $this->db->get('review');
                $review_data = $query->result();
                $old_score = $review_data[0]->score;
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'REVIEW_IS_NOT_FOUND',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $updated_time = date('Y-m-d H:i:s');
                $update_data = array(
                    'detail' => $detail,
                    'score' => $score,
                    'updated_time' => $updated_time
                );
                $this->db->where(array('id'=>$review_id,'business_id'=>$business_id, 'user_id' => $user_id,'status !='=>'deleted'));
                if ($this->db->update('review', $update_data)) {
                    update_review_score($business_id,$score,'edit',$old_score);
                    update_review_user($user_id,'edit');
                    $output = array(
                        'status' => "SUCCESS",
                        'code' => 200
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_EDIT_REVIEW",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }
    public function remove_photo(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $device_id = $this->input->post('device_id');
        $review_id = $this->input->post('review_id');
        $image_id = $this->input->post('image_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->where('ref_id', $review_id);
                $this->db->where('type', "review");
                $this->db->where_in('id', $image_id);
                if ($this->db->update('images', array('status'=>'deleted'))) {
                    $output = array(
                        'status' => "SUCCESS",
                        'code' => 200
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_EDIT_REVIEW",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    public function add_photo(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $device_id = $this->input->post('device_id');
        $review_id = $this->input->post('review_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id');
                $this->db->where(array('user_id'=>$user_id,'id'=>$review_id,'status !='=>'deleted'));
                $review_query = $this->db->get('review');
                if($review_query->num_rows()>0){
                    if (is_array($_FILES['image']['name'])) {
                        $img_data = $this->update_photo($review_id, 'review','image');
                    }
                    if (is_array($img_data)&&count($img_data>0)) {
                        $output = array(
                            'status' => "SUCCESS",
                            'data' => $img_data,
                            'code' => 200
                        );
                        print_json($output); die;
                    }else{
                        $output = array(
                            'status' => "CAN_NOT_EDIT_REVIEW",
                            'code' => 200
                        );
                        print_json($output); die;
                    }
                }else{
                    $output = array(
                        'status' => "REVIEW_OWNER_IS_WRONG",
                        'code' => 200
                    );
                    print_json($output); die;
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    public function delete_review(){
        $api_key = $this->input->post('api_key');
        $business_id = $this->input->post('business_id');
        $user_id = $this->input->post('user_id');
        $review_id = $this->input->post('review_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id,score');
            $this->db->where(array('id'=>$review_id,'business_id'=>$business_id, 'user_id' => $user_id));
            $query = $this->db->get('review');
            if($query->num_rows()>0){
                $review_data = $query->result();
                $review_data = $review_data[0];
                $score = (int)$review_data->score;
                $updated_time = date('Y-m-d H:i:s');
                $update_data = array(
                    'status' => 'deleted',
                    'updated_time' => $updated_time
                );
                $this->db->where('id',$review_id);
                if ($this->db->update('review', $update_data)) {
                    update_review_score($business_id,$score,'delete');
                    update_review_user($user_id,'delete');
                    $this->del_all_photo($review_id);
                    $output = array(
                        'status' => "SUCCESS",
                        'code' => 200
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_DELETE_REVIEW",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'REVIEW_IS_NOT_FOUND',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    private function delete_review_user_not_found($business_id,$user_id,$review_id){
        $this->db->select('id,score');
        $this->db->where(array('id'=>$review_id,'business_id'=>$business_id, 'user_id' => $user_id));
        $query = $this->db->get('review');
        if($query->num_rows()>0){
            $review_data = $query->result();
            $review_data = $review_data[0];
            $score = (int)$review_data->score;
            $updated_time = date('Y-m-d H:i:s');
            $update_data = array(
                'status' => 'deleted',
                'updated_time' => $updated_time
            );
            $this->db->where('id',$review_id);
            if ($this->db->update('review', $update_data)) {
                update_review_score($business_id,$score,'delete');
                update_review_user($user_id,'delete');
                $output = array(
                    'status' => "SUCCESS",
                    'code' => 200
                );
                return $output;
            }else{
                $output = array(
                    'status' => "CAN_NOT_DELETE_REVIEW",
                    'code' => 200
                );
                return $output;
            }
        }else{
            $output = array(
                'status' => 'REVIEW_IS_NOT_FOUND',
                'code'=> 200
            );
            return $output;
        }
    }

}