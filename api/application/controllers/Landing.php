<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landing extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }


    public function index()
	{
        echo json_encode('Landing Page');
	}

	public function home(){
        $output = array(
            'status' => "SUCCESS",
            'code' => 200,
            'data' =>array(
                'promotion'=>$this->gen_promotion(),
                'advertise'=>$this->gen_advertise(),
                'article'=>$this->gen_article()
            )
        );
        print_json($output);
    }

    private function gen_article(){
        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('article',array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted'),4,0);

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){

                $business_list = $this->get_business_list($list->business_list);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        return $out_put;
    }

    private function gen_advertise(){
        $query = $this->db->get_where('advertise',array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'), 'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){
                $business_list = $this->get_business_list($list->business_id);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        return $out_put;
    }

    private function gen_promotion(){
        $query = $this->db->get_where('promotion',array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){

                $business_list = $this->get_business_list($list->business_list);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        return $out_put;
    }

    private function get_business_list($business_list_id){
        $business_list_id = explode(',',$business_list_id);
//        $this->db->select('name,cover_image_src,id,address_province,address_district');
        $this->db->where_in('id',$business_list_id);
        $query = $this->db->get('business');

        $business_list = $query->result();

        $current_lat = $this->input->get('lat');
        $current_lon = $this->input->get('lon');
        $unit = ($this->input->get('unit'))?$this->input->get('unit'):'M';
        if($current_lat&&$current_lon){
            $new_data = array();
            $i = 0;
            foreach ($business_list as $item){
                $item->distance_unit = strtoupper($unit);
                $item->distance = cal_distance($current_lat,$current_lon,$item->lat,$item->lon, $unit);
                $new_data[($item->distance*100).".".$i] = $item;
                $i++;
//                echo '<pre>';
//                var_dump($current_lat,$current_lon,$item->lat,$item->lon,$item->distance); die;
            }
//            ksort($new_data);
//            $business_list = $new_data;
            ksort($new_data);
            $business_list = array();
            foreach ($new_data as $item) {
                $business_list[] = $item;
            }
        }
        return $business_list;
    }
}
