<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emergency extends CI_Controller
{
    private $error_post_data = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
//        $this->load->helper(array('form', 'url'));
//        $data = array();
//        $returned_data = $this->update_photo(1,'business');
//        $data['error'] = "";
//        $data['ok'] = $returned_data;
//        $this->load->view('header');
//        $this->load->view('home',$data);
//        $this->load->view('footer');
        echo json_encode('Emergency');
    }

    public function get_list(){
        if($this->input->get('type')=='insurance'||$this->input->get('type')=='emergency'){
            $type = $this->input->get('type');
            $this->db->where('type',$type);
        }
        $this->db->order_by('type','ASC');
        $this->db->order_by('order_number','ASC');
        $this->db->order_by('title','ASC');
        $query = $this->db->get_where('emergency',array('status !='=> 'deleted'));

        if($query->num_rows()>0){
            $cate_list = $query->result();
        }else{
            $cate_list = array();
        }
        $out_put = array(
            'list' => $cate_list,
            'total' => count($cate_list)
        );
        print_json($out_put);
    }
}