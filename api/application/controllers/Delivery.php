<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller
{
    private $error_post_data = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        echo json_encode('Promotion');
    }

    public function get_list(){
        $type = $this->input->get('type');
        $this->db->order_by('order_number','ASC');
        if($type=="normal"){
            $where = array('type'=> 'normal','status !='=> 'deleted');
        }elseif($type=="lady"){
            $where = array('type'=> 'lady','status !='=> 'deleted');
        }else{
            $where = array('status !='=> 'deleted');
        }
        $query = $this->db->get_where('delivery',$where);

        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }
    public function get_data($id){
        if($this->input->get('view')>=1){
            viewed_increment($id,'delivery');
        }
        if(!isset($id)){
            $out_put = array(
                'list' => array(),
                'total' => 0
            );
            print_json($out_put); die;
        }
        $query = $this->db->get_where('delivery',array('id'=>$id,'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            $data_list = array();
        }
        $out_put = array(
            'data' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

}