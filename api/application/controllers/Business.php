<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller
{
    private $error_post_data = array();
    private $user_data;
    private $api_key;

    public function __construct()
    {
        parent::__construct();
        $this->api_key = $this->config->item('api_key');
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
        }
    }


    public function index()
    {
//        $this->load->helper(array('form', 'url'));
//        $data = array();
//        $returned_data = $this->update_photo(1,'business');
//        $data['error'] = "";
//        $data['ok'] = $returned_data;
//        $this->load->view('header');
//        $this->load->view('home',$data);
//        $this->load->view('footer');
        echo json_encode('Business');
    }



    public function add_business()
    {
//        echo '<pre>';
//        var_dump($_FILES);
//        var_dump($_POST); die;
        if ($this->input->post('do') == 'post') {
            $this->error_post_data = array();
            $time_now = date('Y-m-d H:i:s');
            $cate_list = $this->get_cate_list();
            $cate_list2 = $this->get_cate_list2();

            if($this->get_post_data('service_type') && is_array($this->get_post_data('service_type'))){
                $service_type = json_encode($this->get_post_data('service_type'));
                $service_type = json_decode($service_type);
                if(!is_array($service_type)) $service_type = array();
                $new_service_type = array();
                foreach ($service_type as $_type){
                    if(isset($cate_list[$_type])){
                        $new_service_type[] = $cate_list[$_type]['id'];
                    }else if(isset($cate_list2[$_type])){
                        $new_service_type[] = $cate_list2[$_type]['id'];
                    }else{
                        $new_service_type = $service_type;
                    }
                }
                $service_type = json_encode($new_service_type);
            }else{
                $service_type = $this->get_post_data('service_type', "[]");
            }
            if($this->get_post_data('facilities',"[]") && is_array($this->get_post_data('facilities',"[]"))){
                $facilities = json_encode($this->get_post_data('facilities',"[]"));
                $facilities = json_decode($facilities);
                if(!is_array($facilities)) $facilities = array();
                $new_facilities = array();
                foreach ($facilities as $_type){
                    if(isset($cate_list[$_type])){
                        if(isset($cate_list[$_type]['id'])){
                            $new_facilities[] = $cate_list[$_type]['id'];
                        }else{
                            $new_facilities[] = $cate_list[$_type]->id;
                        }
                    }else if(isset($cate_list2[$_type])){
                        if(isset($cate_list2[$_type]['id'])){
                            $new_facilities[] = $cate_list2[$_type]['id'];
                        }else{
                            $new_facilities[] = $cate_list2[$_type]->id;
                        }
                    }else{
                        $new_facilities = $service_type;
                    }
                }
                $facilities = json_encode($new_facilities);
            }else{
                $facilities = $this->get_post_data('facilities', "[]");
            }
            if($this->get_post_data('payment',"[]") && is_array($this->get_post_data('payment',"[]"))){
                $payment = json_encode($this->get_post_data('payment',"[]"));
            }else{
                $payment = $this->get_post_data('payment', "[]");
            }

            $type = $this->get_post_data('type');
            if(isset($cate_list[$type])){
                $type = $cate_list[$type];
            }else if(isset($cate_list2[$type])){
                $type = $cate_list2[$type];
            }else{
                $type = $this->get_post_data('type');;
            }
            if(isset($type->id)){
                $type_id = $type->id;
            }else if($type['id']){
                $type_id = $type['id'];
            }
            $new_business_data = array(
                "name" => $this->get_post_data('name'),
                "owner_name" => $this->get_post_data('owner_name'),
                "type" => $type_id,
                "service_type" => $service_type,
                "address_no" => $this->get_post_data('address_no', ""),
                "address_moo" => $this->get_post_data('address_moo', ""),
                "address_village" => $this->get_post_data('address_village', ""),
                "address_soi" => $this->get_post_data('address_soi', ""),
                "address_road" => $this->get_post_data('address_road', ""),
                "address_tambol" => $this->get_post_data('address_tambol', 'test'),
                "address_district" => $this->get_post_data('address_district'),
                "address_province" => $this->get_post_data('address_province'),
                "address_postcode" => $this->get_post_data('address_postcode'),
                "lat" => $this->get_post_data('lat'),
                "lon" => $this->get_post_data('lon'),
                "country" => $this->get_post_data('country'),
                "country_code" => $this->get_post_data('country_code'),
                "tel" => $this->get_post_data('tel'),
                "facebook" => $this->get_post_data('facebook', ""),
                "line" => $this->get_post_data('line', ""),
                "email" => $this->get_post_data('email', ""),
                "work_day_sun" => $this->get_post_data('work_day_sun', 0),
                "work_day_mon" => $this->get_post_data('work_day_mon', 0),
                "work_day_tue" => $this->get_post_data('work_day_tue', 0),
                "work_day_wed" => $this->get_post_data('work_day_wed', 0),
                "work_day_thu" => $this->get_post_data('work_day_thu', 0),
                "work_day_fri" => $this->get_post_data('work_day_fri', 0),
                "work_day_sat" => $this->get_post_data('work_day_sat', 0),
                "work_time_start" => $this->get_post_data('work_time_start', '00:00:00'),
                "work_time_close" => $this->get_post_data('work_time_close', '00:00:00'),
                "work_time_start_2" => $this->get_post_data('work_time_start_2', '00:00:00'),
                "work_time_close_2" => $this->get_post_data('work_time_close_2', '00:00:00'),
                "more_work_time" => $this->get_post_data('more_work_time', ""),
                "work_time_remark" => $this->get_post_data('work_time_remark', ""),
                "detail" => $this->get_post_data('detail', ""),
                "slogan" => $this->get_post_data('slogan', ""),
                "facilities" => $facilities,
                "payment" => $payment,
                "award_certification" => $this->get_post_data('award_certification', ""),
                "privilege" => $this->get_post_data('privilege', ""),
                "promoted_time_start" => $this->get_post_data('promoted_time_start', "2016-01-01 00:00:00"),
                "promoted_time_end" => $this->get_post_data('promoted_time_end', "2016-01-01 00:00:00"),
                "created_time" => $time_now,
                "updated_time" => $time_now,
                "created_user_type" => $this->get_post_data('created_user_type', "0"),
                "created_user_id" => $this->get_post_data('created_user_id', "0"),
                "created_user_name" => $this->get_post_data('created_user_name', "FirstGathering"),
                'viewed'=> 0,
                'liked'=> 0,
                'shared'=> 0,
                "status" => $this->get_post_data('status', "pending"),);
//                    echo '<pre>';
//        var_dump($new_business_data); die;
            if (count($this->error_post_data) > 0) {
                $result = array(
                    'result' => 'ERROR',
                    'message' => 'ข้อมูลจำเป็นที่ต้องการไม่ครบ',
                    'detail' => array('lost_data' => $this->error_post_data)
                );
                print_json($result);
            } else {
                if ($this->db->insert('business', $new_business_data)) {
                    $business_id = $this->db->insert_id();
                    gen_business_code($business_id);
                    $log_user_id = $this->user_data['id'];
                    $log_user_name = $this->user_data['name'];
                    history_log($log_user_id,$log_user_name,$business_id,'add','business');
                    $result = array(
                        'result' => 'SUCCESS',
                        'message' => 'สถานที่ได้ถูกเพิ่มแล้ว',
                        'data' => array(
                            'id' => $business_id
                        )
                    );
                    if ($_FILES['cover']['name']!='') {
                        $img_data = $this->update_photo($business_id, 'business','cover');
                        $this->update_cover_image($business_id,$img_data[0]['id'],$img_data[0]['src']);
                        $result['data']['img'] = $img_data;
                    }
                    if($this->input->post('client')=='cms'){
                        redirect($this->input->post('callback'));
                    }else{
                        print_json($result);
                    }

                } else {
                    $result = array(
                        'result' => 'ERROR',
                        'message' => 'ไม่สามารถเพิ่มสถานที่ได้',
                        'detail' => array('db_error' => $this->db->_error_message())
                    );
                    if($this->input->post('client')=='cms'){
                        redirect($this->input->post('callback').'?error=1');
                    }else{
                        print_json($result);
                    }
                }
            }
        } else {
            print_json(array());
        }
    }

    public function user_add_business()
    {
//        echo '<pre>';
//        var_dump($_FILES);
//        var_dump($_POST); die;
        if ($this->input->post('do') == 'post') {
            $this->error_post_data = array();
            $time_now = date('Y-m-d H:i:s');
            $cate_list = $this->get_cate_list();
            $cate_list2 = $this->get_cate_list2();

            $type = $this->get_post_data('type');
            if(isset($cate_list[$type])){
                $type = $cate_list[$type];
            }else if(isset($cate_list2[$type])){
                $type = $cate_list2[$type];
            }else{
                $type = $this->get_post_data('type');;
            }
            if(isset($type->id)){
                $type_id = $type->id;
            }else{
                $type_id = $type['id'];
            }
            $new_business_data = array(
                "name" => $this->get_post_data('name'),
                "owner_name" => $this->get_post_data('owner_name',''),
                "type" => $type_id,
                "service_type" => "[]",
                "address_no" => $this->get_post_data('address_no', ""),
                "address_moo" => $this->get_post_data('address_moo', ""),
                "address_village" => $this->get_post_data('address_village', ""),
                "address_soi" => $this->get_post_data('address_soi', ""),
                "address_road" => $this->get_post_data('address_road', ""),
                "address_tambol" => $this->get_post_data('address_tambol', 'test'),
                "address_district" => $this->get_post_data('address_district',''),
                "address_province" => $this->get_post_data('address_province',''),
                "address_postcode" => $this->get_post_data('address_postcode',''),
                "lat" => $this->get_post_data('lat'),
                "lon" => $this->get_post_data('lon'),
                "country" => $this->get_post_data('country',''),
                "country_code" => $this->get_post_data('country_code',''),
                "tel" => $this->get_post_data('tel'),
                "facebook" => $this->get_post_data('facebook', ""),
                "line" => $this->get_post_data('line', ""),
                "email" => $this->get_post_data('email', ""),
                "work_day_sun" => $this->get_post_data('work_day_sun', 0),
                "work_day_mon" => $this->get_post_data('work_day_mon', 0),
                "work_day_tue" => $this->get_post_data('work_day_tue', 0),
                "work_day_wed" => $this->get_post_data('work_day_wed', 0),
                "work_day_thu" => $this->get_post_data('work_day_thu', 0),
                "work_day_fri" => $this->get_post_data('work_day_fri', 0),
                "work_day_sat" => $this->get_post_data('work_day_sat', 0),
                "work_time_start" => $this->get_post_data('work_time_start', '00:00:00'),
                "work_time_close" => $this->get_post_data('work_time_close', '00:00:00'),
                "work_time_start_2" => $this->get_post_data('work_time_start_2', '00:00:00'),
                "work_time_close_2" => $this->get_post_data('work_time_close_2', '00:00:00'),
                "more_work_time" => $this->get_post_data('more_work_time', ""),
                "work_time_remark" => $this->get_post_data('work_time_remark', ""),
                "detail" => $this->get_post_data('detail', ""),
                "slogan" => $this->get_post_data('slogan', ""),
                "facilities" => "[]",
                "payment" => "[]",
                "award_certification" => $this->get_post_data('award_certification', ""),
                "privilege" => $this->get_post_data('privilege', ""),
                "promoted_time_start" => $this->get_post_data('promoted_time_start', "2016-01-01 00:00:00"),
                "promoted_time_end" => $this->get_post_data('promoted_time_end', "2016-01-01 00:00:00"),
                "created_time" => $time_now,
                "updated_time" => $time_now,
                "created_user_type" => $this->get_post_data('created_user_type', "0"),
                "created_user_id" => $this->get_post_data('created_user_id', "0"),
                "created_user_name" => $this->get_post_data('created_user_name', "FirstGathering"),
                'viewed'=> 0,
                'liked'=> 0,
                'shared'=> 0,
                "status" => 'pending');
//                    echo '<pre>';
//        var_dump($new_business_data); die;
            if (count($this->error_post_data) > 0) {
                $result = array(
                    'result' => 'ERROR',
                    'message' => 'ข้อมูลจำเป็นที่ต้องการไม่ครบ',
                    'detail' => array('lost_data' => $this->error_post_data)
                );
                print_json($result);
            } else {
                if ($this->db->insert('business', $new_business_data)) {
                    $business_id = $this->db->insert_id();
                    gen_business_code($business_id);
                    $user_id = (int)$this->get_post_data('created_user_id', 0);
                    $point_data = array();
                    if($user_id!=0){
                        $point_data = add_user_point($user_id,POINT_REGISTER, 'add_business', $business_id, null);
                    }
                    $log_user_id = '0';
                    $log_user_name = 'user_added';
                    history_log($log_user_id,$log_user_name,$business_id,'add','business');
                    $result = array(
                        'result' => 'SUCCESS',
                        'point_data' => $point_data,
                        'message' => 'สถานที่ได้ถูกเพิ่มแล้ว',
                        'data' => array(
                            'id' => $business_id
                        )
                    );
                    if(isset($_FILES['cover'])){
                        if ($_FILES['cover']['name']!='') {
                            $img_data = $this->update_photo($business_id, 'business','cover');
                            $this->update_cover_image($business_id,$img_data[0]['id'],$img_data[0]['src']);
                            $result['data']['img'] = $img_data;
                        }
                    }

                    if($this->input->post('client')=='cms'){
                        redirect($this->input->post('callback'));
                    }else{
                        print_json($result);
                    }

                } else {
                    $result = array(
                        'result' => 'ERROR',
                        'message' => 'ไม่สามารถเพิ่มสถานที่ได้',
                        'detail' => array('db_error' => $this->db->_error_message())
                    );
                    if($this->input->post('client')=='cms'){
                        redirect($this->input->post('callback').'?error=1');
                    }else{
                        print_json($result);
                    }
                }
            }
        } else {
            print_json(array());
        }
    }

    public function update_business()
    {
        //        var_dump($this->user_data); die;
//        echo '<pre>';
//        var_dump($_FILES);
//        var_dump($_POST); die;
        if ($this->input->post('do') == 'post') {
            $this->error_post_data = array();
            $time_now = date('Y-m-d H:i:s');
            if($this->get_post_data('service_type') && is_array($this->get_post_data('service_type'))){
                $service_type = json_encode($this->get_post_data('service_type'));
            }else{
                $service_type = $this->get_post_data('service_type', "");
            }
            if($this->get_post_data('facilities') && is_array($this->get_post_data('facilities'))){
                $facilities = json_encode($this->get_post_data('facilities'));
            }else{
                $facilities = $this->get_post_data('facilities', "");
            }
            if($this->get_post_data('payment') && is_array($this->get_post_data('payment'))){
                $payment = json_encode($this->get_post_data('payment'));
            }else{
                $payment = $this->get_post_data('payment', "");
            }
            $new_business_data = array(
                "name" => $this->get_post_data('name'),
                "owner_name" => $this->get_post_data('owner_name'),
                "type" => $this->get_post_data('type'),
                "service_type" => $service_type,
                "address_no" => $this->get_post_data('address_no', ""),
                "address_moo" => $this->get_post_data('address_moo', ""),
                "address_village" => $this->get_post_data('address_village', ""),
                "address_soi" => $this->get_post_data('address_soi', ""),
                "address_road" => $this->get_post_data('address_road', ""),
                "address_tambol" => $this->get_post_data('address_tambol', 'test'),
                "address_district" => $this->get_post_data('address_district'),
                "address_province" => $this->get_post_data('address_province'),
                "address_postcode" => $this->get_post_data('address_postcode'),
                "lat" => $this->get_post_data('lat'),
                "lon" => $this->get_post_data('lon'),
                "country" => $this->get_post_data('country'),
                "country_code" => $this->get_post_data('country_code'),
                "tel" => $this->get_post_data('tel'),
                "facebook" => $this->get_post_data('facebook', ""),
                "line" => $this->get_post_data('line', ""),
                "email" => $this->get_post_data('email', ""),
                "work_day_sun" => $this->get_post_data('work_day_sun', 0),
                "work_day_mon" => $this->get_post_data('work_day_mon', 0),
                "work_day_tue" => $this->get_post_data('work_day_tue', 0),
                "work_day_wed" => $this->get_post_data('work_day_wed', 0),
                "work_day_thu" => $this->get_post_data('work_day_thu', 0),
                "work_day_fri" => $this->get_post_data('work_day_fri', 0),
                "work_day_sat" => $this->get_post_data('work_day_sat', 0),
                "work_time_start" => $this->get_post_data('work_time_start', '00:00:00'),
                "work_time_close" => $this->get_post_data('work_time_close', '00:00:00'),
                "work_time_start_2" => $this->get_post_data('work_time_start_2', '00:00:00'),
                "work_time_close_2" => $this->get_post_data('work_time_close_2', '00:00:00'),
                "more_work_time" => $this->get_post_data('more_work_time', ""),
                "work_time_remark" => $this->get_post_data('work_time_remark', ""),
                "detail" => $this->get_post_data('detail', ""),
                "slogan" => $this->get_post_data('slogan', ""),
                "facilities" => $facilities,
                "payment" => $payment,
                "award_certification" => $this->get_post_data('award_certification', ""),
                "privilege" => $this->get_post_data('privilege', ""),
                "promoted_time_start" => $this->get_post_data('promoted_time_start', "2016-01-01 00:00:00"),
                "promoted_time_end" => $this->get_post_data('promoted_time_end', "2016-01-01 00:00:00"),
                "created_time" => $time_now,
                "updated_time" => $time_now,
                "created_user_type" => $this->get_post_data('created_user_type', "0"),
                "created_user_id" => $this->get_post_data('created_user_id', "0"),
                "created_user_name" => $this->get_post_data('created_user_name', "FirstGathering"),
                "status" => $this->get_post_data('status', "pending"));

            $business_id = $this->input->post('id');
//            echo '<pre>';
//            var_dump($new_business_data,$business_id); die;
            if (count($this->error_post_data) > 0) {
                $result = array(
                    'result' => 'ERROR',
                    'message' => 'ข้อมูลจำเป็นที่ต้องการไม่ครบ',
                    'detail' => array('lost_data' => $this->error_post_data)
                );
                print_json($result);
            } else {
                $this->db->where('id', $business_id);
                if ($this->db->update('business', $new_business_data)) {
                    $log_user_id = $this->user_data['id'];
                    $log_user_name = $this->user_data['name'];
                    history_log($log_user_id,$log_user_name,$business_id,'edit','business');
                    $result = array(
                        'result' => 'SUCCESS',
                        'message' => 'สถานที่ได้ถูกเพิ่มแล้ว',
                        'data' => array(
                            'id' => $business_id
                        )
                    );
                    if ($_FILES['cover']['name']!='') {
                        $img_data = $this->update_photo($business_id, 'business','cover');
                        $this->update_cover_image($business_id,$img_data[0]['id'],$img_data[0]['src']);
                        $result['data']['img'] = $img_data;
                    }
                    if($this->input->post('client')=='cms'){
                        redirect($this->input->post('callback'));
                    }else{
                        print_json($result);
                    }

                } else {
                    $result = array(
                        'result' => 'ERROR',
                        'message' => 'ไม่สามารถเพิ่มสถานที่ได้',
                        'detail' => array('db_error' => $this->db->_error_message())
                    );
                    if($this->input->post('client')=='cms'){
                        redirect($this->input->post('callback').'?error=1');
                    }else{
                        print_json($result);
                    }
                }
            }
        } else {
            print_json(array());
        }
    }

    private function update_photo($ref_id, $img_type,$file_label)
    {
        if ($this->input->post('do') == 'post') {
            $POST_DATA = array();
            $img_num = count($_FILES[$file_label]['tmp_name']);
            if (is_array($_FILES[$file_label]['tmp_name'])) {
                for ($i = 0; $i < $img_num; $i++) {
                    $tmp_file = $_FILES[$file_label]['tmp_name'][$i];
                    $file_name = basename($_FILES[$file_label]['name'][$i]);
                    $file_type = $_FILES[$file_label]['type'][$i];
                    $POST_DATA['upload_img_' . $i] = curl_file_create($tmp_file, $file_type, $file_name);
                }
            } else {
                $tmp_file = $_FILES[$file_label]['tmp_name'];
                $file_name = basename($_FILES[$file_label]['name']);
                $file_type = $_FILES[$file_label]['type'];
                $POST_DATA['upload_img_0'] = curl_file_create($tmp_file, $file_type, $file_name);
            }
            $POST_DATA['img_num'] = $img_num;
            $POST_DATA['depot_key'] = $this->config->item('depot_key');
            $POST_DATA['img_type'] = $img_type;
            $POST_DATA['ref_id'] = $ref_id;
            $curl_handle = curl_init($this->config->item('depot_url'));
            curl_setopt($curl_handle, CURLOPT_POST, 1);
            curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
            curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

            $returned_data = curl_exec($curl_handle);
            curl_close($curl_handle);

            $returned_data = json_decode($returned_data, true);
            return $returned_data['result'];
        } else {
            return array();
        }
    }
    public function set_cover_image(){
        $bus_id = $this->input->post('bid');
        $img_id = $this->input->post('img_id');
        $img_src = $this->input->post('img_src');
        if($bus_id&&$img_id&&$img_src){
            $this->update_cover_image($bus_id,$img_id,$img_src);
            $output = array(
                'status'=>'SUCCESS',
                'msg'=>'อัพเดตรูปปกสำเร็จ',
                'detail'=>array('bid'=>$bus_id,'img_id'=>$img_id,'img_src'=>$img_src)
        );
        }else{
            $output = array(
                'status'=>'ERROR',
                'msg'=>'อัพเดตรูปปกไม่สำเร็จ',
                'detail'=>array('bid'=>$bus_id,'img_id'=>$img_id,'img_src'=>$img_src)
            );
        }
        print_json($output);
    }

    public function set_delete_image(){
        $bus_id = $this->input->post('bid');
        $img_id = $this->input->post('img_id');
        if($bus_id&&$img_id&&is_array($img_id)&&count($img_id)>0){
            $this->update_delete_image($bus_id,$img_id);
            $output = array(
                'status'=>'SUCCESS',
                'msg'=>'ลบรูปที่เลือกสำเร็จ',
                'detail'=>array('bid'=>$bus_id,'img_id'=>$img_id)
            );
        }else{
            $output = array(
                'status'=>'ERROR',
                'msg'=>'ลบรูปที่เลือกไม่สำเร็จ',
                'detail'=>array('bid'=>$bus_id,'img_id'=>$img_id)
            );
        }
        print_json($output);
    }

    private function update_cover_image($bus_id,$img_id,$img_src){
        $data = array(
            'cover_image_id' => $img_id,
            'cover_image_src' => $img_src,
            'updated_time' => date("Y-m-d H:i:s"),

        );
        $this->db->where('id', $bus_id);
        $this->db->update('business', $data);
    }

    private function update_delete_image($bus_id,$img_id){
        $data = array(
            'status' => 'deleted',
            'updated_time' => date("Y-m-d H:i:s"),

        );
        $this->db->where_in('id',$img_id);
        $this->db->where('status', 'active');
        $this->db->where('ref_id', $bus_id);
        $this->db->where('type', 'business');
        $this->db->update('images', $data);
    }


    public function get_business_list(){
        $offset = ($this->input->get('offset'))?$this->input->get('offset'):0;
        $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;
        $next_url = '';
        $this->db->select("id");
        $this->db->where("status in ('vip','active')");
        $query = $this->db->get('business');
        $total_num =  $query->num_rows();
        $total_page = ceil($total_num/$limit);

        $this->db->select("*");
        $this->db->where("status in ('vip','active')");
        $query = $this->db->get('business',$limit, $offset);

        if($query->num_rows()>0){
            $cate_list = $this->get_cate_list();
            $cate_list2 = $this->get_cate_list2();
            $business_data = array();
            foreach ($query->result() as $item){
                if(intval($item->reviewed)>0&&intval($item->score)>0){
                    $item->score = round($item->score / $item->reviewed);
                }else{
                    $item->score = $item->score;
                }
                if(isset($cate_list[$item->type])){
                    $item->type = $cate_list[$item->type];
                }else if(isset($cate_list2[$item->type])){
                    $item->type = $cate_list2[$item->type];
                }
                $item->service_type = json_decode($item->service_type);
                $new_service_type = array();
                if(is_array($item->service_type)){
                    foreach ($item->service_type as $service_type){
                        if(isset($cate_list[$service_type])){
                            $new_service_type[] = $cate_list[$service_type];
                        }else if(isset($cate_list2[$service_type])){
                            $new_service_type[] = $cate_list2[$service_type];
                        }
                    }
                }

                $item->service_type = $new_service_type;
                $item->facilities = json_decode($item->facilities);
                $new_facilities = array();
                if(is_array($item->facilities)){
                    foreach ($item->facilities as $facilities){
                        if(isset($cate_list[$facilities])){
                            $new_facilities[] = $cate_list[$facilities];
                        }else if(isset($cate_list2[$facilities])){
                            $new_facilities[] = $cate_list2[$facilities];
                        }
                    }
                }

                $item->facilities = $new_facilities;
                $item->payment = json_decode($item->payment);
                $business_data[] = $item;
            }
            $business_list = $business_data;
        }else{
            $business_list = array();
        }
        $offset = $limit+$offset;
        $url_param = array(
            'limit'=>$limit,
            'offset'=>$offset
        );
        if($offset<$total_num){
            $next_url = site_url('business/get_business_list?'.http_build_query($url_param));
        }

        $current_lat = $this->input->get('lat');
        $current_lon = $this->input->get('lon');
        $unit = ($this->input->get('unit'))?$this->input->get('unit'):'M';
        if($current_lat&&$current_lon){
            $new_data = array();
            $i = 0;
            foreach ($business_list as $item){
                $item->distance_unit = strtoupper($unit);
                $item->distance = cal_distance($current_lat,$current_lon,$item->lat,$item->lon, $unit);
                $new_data[($item->distance*100).".".$i] = $item;
                $i++;
//                echo '<pre>';
//                var_dump($current_lat,$current_lon,$item->lat,$item->lon,$item->distance); die;
            }
//            ksort($new_data);
//            $business_list = $new_data;
            ksort($new_data);
            $business_list = array();
            foreach ($new_data as $item) {
                $business_list[] = $item;
            }
        }
        $out_put = array(
            'list' => $business_list,
            'total' => $total_num,
            'total_page' => $total_page,
            'result' => 'SUCCESS',
            'next' => $next_url,
            'code' => 200
        );
        print_json($out_put);
    }

    public function search_business()
    {
        $result = array(
            'result' => 'ERROR',
            'code' => 400,
            'message' => 'ไม่พบข้อมูล',
            'detail' => array()
        );

        $offset = ($this->input->get('offset'))?$this->input->get('offset'):0;
        $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;

        $lat = ($this->input->get('lat'))?$this->input->get('lat'):'';
        $lon = ($this->input->get('lon'))?$this->input->get('lon'):'';
        $keyword = ($this->input->get('keyword'))?$this->input->get('keyword'):false;
        $type= ($this->input->get('type'))?$this->input->get('type'):false;
        $service_type = ($this->input->get('service'))?$this->input->get('service'):false;
        $sql = "";
        if($lat!=''&&$lon!=''){
            $sql .= ", ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians( lon ) - radians($lon) ) + sin( radians($lat) ) * sin( radians( lat ) ) ) ) AS distance ";
        }

        $sql .= "FROM business WHERE ";
        if($keyword){
            $sql .= " ( `name` LIKE '%$keyword%' ";
            $sql .= " OR `address_moo` LIKE '%$keyword%' ";
            $sql .= " OR `address_village` LIKE '%$keyword%' ";
            $sql .= " OR `address_soi` LIKE '%$keyword%' ";
            $sql .= " OR `address_road` LIKE '%$keyword%' ";
            $sql .= " OR `address_tambol` LIKE '%$keyword%' ";
            $sql .= " OR `address_district` LIKE '%$keyword%' ";
            $sql .= " OR `address_province` LIKE '%$keyword%' ) AND ";
        }
        if($service_type){
            $sql .=" `service_type` LIKE '%$service_type%' AND ";
        }
        if($type){
            $sql .=" `type` = $type AND ";
        }

        $sql .=" `status` in ('vip', 'active') ";
        $distance = $this->input->get('d');
        if(floatval($distance)>5){
            $distance = floatval($distance);
            $distance = $distance*0.621371192;
            $distance = number_format($distance, 2, '.', '');
        }else{
            $distance = 3.1;
        }
        if($lat!=''&&$lon!=''){
            $sql .= "HAVING distance < ".$distance." ";
        }
        $query = $this->db->query("SELECT id ".$sql);
        $total_num =  $query->num_rows();
        if($lat!=''&&$lon!=''){
            $sql .= "ORDER BY distance ";
        }else{
            $sql .= "ORDER BY `id` DESC ";
        }


        $sql .= "LIMIT $offset, $limit ";

        $query = $this->db->query("SELECT * ".$sql);
        $data_list = array();
        $next_url = '';
        $total_page = ceil($total_num/$limit);
        $offset = $limit+$offset;
        if($query->num_rows()>0){
            $cate_list = $this->get_cate_list();
            $cate_list2 = $this->get_cate_list2();
            $business_data = array();
            foreach ($query->result() as $item){
                $item->total_score = (int)$item->score;
                if(intval($item->reviewed)>0&&intval($item->score)>0){
                    $item->score = round($item->score / $item->reviewed);
                }else{
                    $item->score = $item->score;
                }

                if(isset($cate_list[$item->type])){
                    $item->type = $cate_list[$item->type];
                }else if(isset($cate_list2[$item->type])){
                    $item->type = $cate_list2[$item->type];
                }
                $item->service_type = json_decode($item->service_type);
                if(!is_array($item->service_type)) $item->service_type = array();
                $new_service_type = array();
                foreach ($item->service_type as $service_type){
                    if(isset($cate_list[$service_type])){
                        $new_service_type[] = $cate_list[$service_type];
                    }else if(isset($cate_list2[$service_type])){
                        $new_service_type[] = $cate_list2[$service_type];
                    }
                }
                $item->service_type = $new_service_type;
                $item->facilities = json_decode($item->facilities);
                $new_facilities = array();
                foreach ($item->facilities as $facilities){
                    if(isset($cate_list[$facilities])){
                        $new_facilities[] = $cate_list[$facilities];
                    }else if(isset($cate_list2[$facilities])){
                        $new_facilities[] = $cate_list2[$facilities];
                    }
                }
                $item->facilities = $new_facilities;
                $item->payment = json_decode($item->payment);
                $business_data[] = $item;
            }
            $data_list = $business_data;


            $current_lat = $this->input->get('lat');
            $current_lon = $this->input->get('lon');
            $unit = ($this->input->get('unit'))?$this->input->get('unit'):'M';
            if($current_lat&&$current_lon){
                $new_data = array();
                $i = 0;
                foreach ($data_list as $item){
                    $item->distance_unit = strtoupper($unit);
                    $item->distance = cal_distance($current_lat,$current_lon,$item->lat,$item->lon, $unit);
                    $new_data[] = $item;
//                    $new_data[$item->distance."_".$i] = $item;
                    $i++;
//                echo '<pre>';
//                var_dump($current_lat,$current_lon,$item->lat,$item->lon,$item->distance); die;
                }
                $data_list = $new_data;
//                ksort($new_data);
//                $data_list = array();
//                foreach ($new_data as $item) {
//                    $data_list[] = $item;
//                }
            }

            $url_param = array(
                'limit'=>$limit,
                'offset'=>$offset
            );
            if($keyword){
                $url_param['keyword'] = $keyword;
            }
            if($lat!=''&&$lon!=''){
                $url_param['lat'] = $lat;
                $url_param['lon'] = $lon;
            }
            if($offset<$total_num){
                $next_url = site_url('business/search_business?'.http_build_query($url_param));
            }

            $result = array(
                'result' => 'SUCCESS',
                'code' => 200,
                'message' => 'ข้อมูลสถานที่',
                'total' => $total_num,
                'total_page' => $total_page,
                'next' => $next_url,
                'data' => $data_list
            );
        }else if($query->num_rows()==0){
            $result = array(
                'result' => 'SUCCESS',
                'code' => 200,
                'message' => 'ไม่พบข้อมูลสถานที่',
                'total' => $total_num,
                'total_page' => $total_page,
                'next' => $next_url,
                'data' => $data_list
            );
        }
        print_json($result);

    }

    public function get_data($id){

        if($this->input->get('view')>0){
            if(viewed_increment($id,'business')){
                $this->call_business_data($id);
            }else{
                $this->call_business_data($id);
            }
        }else{
            $this->call_business_data($id);
        }

    }
    private function call_business_data($id){
        $query = $this->db->get_where('business',array('id'=>$id,'status !=' => 'deleted'));
        $is_raw = $this->input->get('debug');
        if($query->num_rows()>0){
            $cate_list = $this->get_cate_list();
            $cate_list2 = $this->get_cate_list2();
            $business_data = array();
            if($is_raw=='raw_data'){
                $business_list = $query->result();
            }else{
                foreach ($query->result() as $item){
                    $item->total_score = (int)$item->score;
                    if(intval($item->reviewed)>0&&intval($item->score)>0){
                        $item->score = round($item->score / $item->reviewed);
                    }else{
                        $item->score = $item->score;
                    }
                    if(isset($cate_list[$item->type])){
                        $item->type = $cate_list[$item->type];
                    }else if(isset($cate_list2[$item->type])){
                        $item->type = $cate_list2[$item->type];
                    }
                    $item->service_type = json_decode($item->service_type);
                    if(!is_array($item->service_type)) $item->service_type = array();
                    $new_service_type = array();
                    if(is_array($item->service_type)){
                        foreach ($item->service_type as $service_type){
                            if(isset($cate_list[$service_type])){
                                $new_service_type[] = $cate_list[$service_type];
                            }else if(isset($cate_list2[$service_type])){
                                //var_dump($cate_list2);
                                $new_service_type[] = $cate_list2[$service_type];
                            }
                        }
                    }

                    $item->service_type = $new_service_type;
                    $item->facilities = json_decode($item->facilities);
                    $new_facilities = array();
                    if(is_array($item->facilities)){
                        foreach ($item->facilities as $facilities){
                            if(isset($cate_list[$facilities])){
                                $new_facilities[] = $cate_list[$facilities];
                            }else if(isset($cate_list2[$service_type])){
                                $new_facilities[] = $cate_list2[$facilities];
                            }
                        }
                    }

                    $item->facilities = $new_facilities;
                    $item->payment = json_decode($item->payment);
                    $business_data[] = $item;
                }
                $business_list = $business_data;
                if($this->input->get('no_review_photo_list')!='yes'){
                    $business_list[0]->review_data = $this->get_3lastest_review($id);
                    $business_list[0]->photo_data = $this->get_4lastest_photo($id);
                }
            }
            $current_lat = $this->input->get('lat');
            $current_lon = $this->input->get('lon');
            $unit = ($this->input->get('unit'))?$this->input->get('unit'):'M';
            if($current_lat&&$current_lon){
                $new_data = array();
                $i = 0;
                foreach ($business_list as $item){
                    $item->distance_unit = strtoupper($unit);
                    $item->distance = cal_distance($current_lat,$current_lon,$item->lat,$item->lon, $unit);
                    $new_data[($item->distance*100).".".$i] = $item;
                    $i++;
//                echo '<pre>';
//                var_dump($current_lat,$current_lon,$item->lat,$item->lon,$item->distance); die;
                }
//            ksort($new_data);
//            $business_list = $new_data;
                ksort($new_data);
                $business_list = array();
                foreach ($new_data as $item) {
                    $business_list[] = $item;
                }
            }
        }else{
            $business_list = array();
        }

        $out_put = array(
            'result' => 'SUCCESS',
            'code' => 200,
            'data' => $business_list

        );
        print_json($out_put);
    }

    private function get_post_data($key_data, $sub_data = null)
    {
        if ($this->input->post($key_data)) {
            return $this->input->post($key_data);
        } else if ($sub_data === null) {
            return $this->keep_lost_post_data($key_data);
        } else {
            return $sub_data;
        }
    }

    private function keep_lost_post_data($lost_data)
    {
        $this->error_post_data[] = $lost_data;
        return "";
    }

    public function get_photo_list()
    {
        $business_id = $this->input->get('bid');
        if($business_id!=false){
            $offset = ($this->input->get('offset'))?$this->input->get('offset'):0;
            $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;
            $skip = (int)($this->input->get('skip'))?$this->input->get('skip'):false;

            $this->db->from('images');
            $this->db->where(array('ref_id'=>$business_id,'type'=>'business','status'=>'active'));
            $this->db->where_not_in('id',$skip);
            $total_num = $this->db->count_all_results();

            $this->db->order_by('id','DESC');
            $this->db->where_not_in('id', $skip);
            $query = $this->db->get_where('images',array('ref_id'=>$business_id,'type'=>'business','status'=>'active'),$limit, $offset);
            $photo_list = array();
            $next_url = '';
            $total_page = ceil($total_num/$limit);
            $offset = $limit+$offset;
            if($query->num_rows()>0){
                foreach ($query->result() as $item) {
                    $photo_list[] = $item;
                }
                $url_param = array(
                    'bid'=>$business_id,
                    'limit'=>$limit,
                    'offset'=>$offset
                );
                if(is_array($skip)&&count($skip)>0){
                    $url_param['skip'] = $skip;
                }
                if($offset<$total_num){
                    $next_url = site_url('business/get_photo_list?'.http_build_query($url_param));
                }
            }
            $result = array(
                'result' => 'SUCCESS',
                'code' => 200,
                'message' => 'ข้อมูลรูปภาพ',
                'total' => $total_num,
                'total_page' => $total_page,
                'next' => $next_url,
                'detail' => $photo_list
            );

        }else{
            $result = array(
                'result' => 'ERROR',
                'code' => 400,
                'message' => 'ไม่พบข้อมูล',
                'detail' => array()
            );
        }
        print_json($result);
    }

    private function get_3lastest_review($business_id)
    {
        $offset = 0;
        $limit = 3;

        $this->db->from('review');
        $this->db->where(array('business_id'=>$business_id,'status !='=> 'deleted'));
        $total_num = $this->db->count_all_results();

        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('review',array('business_id'=>$business_id,'status !='=> 'deleted'),$limit,$offset);
        $review_list = $query->result();

        $user_id_list = array();
        $review_id_list = array();
        foreach ($review_list as $list){
            $review_id_list[] = $list->id;
            $user_id_list[] = $list->user_id;
        }
        $user_ist = array();

        if(count($user_id_list)>0){
            $this->db->select('id,point,fb_id,email,name,birthday,gender,profile_image,created_time,updated_time');
            $this->db->where_in('id',$user_id_list);
            $query = $this->db->get_where('user',array('status !='=> 'deleted'));
            $user_data = $query->result();

            foreach ($user_data as $list){
                $user_ist[$list->id] = $list;
            }
        }
        foreach ($review_list as $key => $list){
            $query = $this->db->get_where('images',array('ref_id'=>$list->id,"type"=>"review",'status !='=> 'deleted'));
            $img_data = array();
            foreach ($query->result() as $img_list){
                $img_data[] = array("id"=>$img_list->id,"url"=>$img_list->src);
            }
            $review_list[$key]->images = $img_data;
            $user_id = $list->user_id;
            if(isset($user_ist[$user_id])){
                if($user_id == $user_ist[$user_id]->id){
                    $review_list[$key]->user_data = $user_ist[$user_id];
                }
            }else{
                $this->delete_review($business_id,$user_id,$review_list[$key]->id);
            }

        }
        $output = array();
        $offset = 0;
        $limit = 20;
        $total_page = ceil($total_num / $limit);
        $next_url = '';
        if($total_num>1){
            $url_param = array(
                'limit'=>$limit,
                'offset'=>$offset,
                'skip'=>$review_id_list
            );
            $next_url = site_url('review/get_list/'.$business_id.'?'.http_build_query($url_param));
        }

        $output['total_page'] = $total_page;
        $output['total_num'] = $total_num;
        $output['next'] = $next_url;
        $output['data'] = $review_list;
        return $output;
    }


    private function get_4lastest_photo($business_id)
    {
        $offset = 0;
        $limit = 4;

        $this->db->from('images');
        $this->db->where(array('ref_id'=>$business_id,'type'=>'business','status !='=> 'deleted'));
        $total_num = $this->db->count_all_results();

        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('images',array('ref_id'=>$business_id,'type'=>'business','status !='=> 'deleted'),$limit,$offset);
        $photo_list = $query->result();

        $photo_id_list = array();
        foreach ($photo_list as $list){
            $photo_id_list[] = $list->id;
        }
        $output = array();
        $offset = 0;
        $limit = 20;
        $total_page = ceil($total_num / $limit);
        $next_url = '';
        if($total_num>1){
            $url_param = array(
                'bid'=>$business_id,
                'limit'=>$limit,
                'offset'=>$offset,
                'skip'=>$photo_id_list
            );
            $next_url = site_url('business/get_photo_list?'.http_build_query($url_param));
        }

        $output['total_page'] = $total_page;
        $output['total_num'] = $total_num;
        $output['next'] = $next_url;
        $output['data'] = $photo_list;
        return $output;
    }


    public function upload_image(){
//        var_dump($_FILES);
        $result = array(
            'result' => 'ERROR',
            'message' => 'พบข้อผิดพลาด',
            'code' => 400,
            'detail' => array()
        );

        if ($this->input->post('do') == 'post') {
            $business_id = $this->input->post('bid');
            if ($_FILES['photo']['name']!=''&&$business_id!=false) {
                $result = array(
                    'result' => 'SUCCESS',
                    'code' => 200,
                    'message' => 'อัพโหลดรูปสำเร็จ',
                    'data' => array('img'=>array())
                );
                $upload_respond = $this->update_photo($business_id, 'business','photo');
                if(isset($upload_respond['status'])&&$upload_respond['status']=='ERROR'){
                    $result = array(
                        'result' => $upload_respond['status'],
                        'code' => (int)$upload_respond['code'],
                        'message' => 'อัพโหลดรูปไม่สำเร็จ',
                        'detail' => $upload_respond['detail']
                    );
                }else{
                    if(isset($upload_respond[0]['error'])){
                        $result = array(
                            'result' => 'ERROR',
                            'code' => 503,
                            'message' => 'อัพโหลดรูปไม่สำเร็จ',
                            'detail' => strip_tags($upload_respond[0]['error'])
                        );
                    }else{
                        $result['data']['img'] = $upload_respond;
                    }

                }

            }else{
                $result['detail'] = 'file of photo param is empty or business is '.$business_id;
            }
            print_json($result);
        }else {
            $result['detail'] = 'value of do param is not post';
            print_json($result);
        }
    }

    private function get_cate_list()
    {
        $cate_type = $this->input->get('type');
        if (isset($cate_type) && intval($cate_type) > 0) {
            $query = $this->db->get_where('category', array('id' => $cate_type, 'status' => 'active'));
        } else {
            $query = $this->db->get_where('category', array('status' => 'active'));
        }

        if ($query->num_rows() > 0) {
            $cate_data = array();
            foreach ( $query->result() as $item){
                $cate_data[$item->id] = array('id'=>$item->id,'title'=>$item->title,'type'=>$item->type);
            }
            $cate_list = $cate_data;
        } else {
            $cate_list = array();
        }
        return $cate_list;
    }
    private function get_cate_list2()
    {
        $cate_type = $this->input->get('type');
        if (isset($cate_type) && intval($cate_type) > 0) {
            $query = $this->db->get_where('category', array('type' => $cate_type, 'status' => 'active'));
        } else {
            $query = $this->db->get_where('category', array('status' => 'active'));
        }

        if ($query->num_rows() > 0) {
            $cate_data = array();
            foreach ( $query->result() as $item){
                $cate_data[$item->title] = array('id'=>$item->id,'title'=>$item->title,'type'=>$item->type);
            }
            $cate_list = $cate_data;
        } else {
            $cate_list = array();
        }
        return $cate_list;
    }

    private function delete_review($business_id,$user_id,$review_id){
        $this->db->select('id,score');
        $this->db->where(array('id'=>$review_id,'business_id'=>$business_id, 'user_id' => $user_id));
        $query = $this->db->get('review');
        if($query->num_rows()>0){
            $review_data = $query->result();
            $review_data = $review_data[0];
            $score = (int)$review_data->score;
            $updated_time = date('Y-m-d H:i:s');
            $update_data = array(
                'status' => 'deleted',
                'updated_time' => $updated_time
            );
            $this->db->where('id',$review_id);
            if ($this->db->update('review', $update_data)) {
                update_review_score($business_id,$score,'delete');
                update_review_user($user_id,'delete');
                $output = array(
                    'status' => "SUCCESS",
                    'code' => 200
                );
                return $output;
            }else{
                $output = array(
                    'status' => "CAN_NOT_DELETE_REVIEW",
                    'code' => 200
                );
                return $output;
            }
        }else{
            $output = array(
                'status' => 'REVIEW_IS_NOT_FOUND',
                'code'=> 200
            );
            return $output;
        }
    }

    public function get_user_business_list(){
        $created_user_id = $this->input->post('user_id');
        $api_key = $this->input->post('api_key');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 400
            );
            print_json($output); die;
        }else{
            $this->db->order_by('id','DESC');
            $this->db->select('name,id,created_time,status');
            $query = $this->db->get_where('business',array('created_user_id'=>$created_user_id));
            if($query->num_rows()>0){
                $data_list = $query->result();
            }else{
                $data_list = array();
            }
            $out_put = array(
                'list' => $data_list,
                'total' => count($data_list),
                'status' => 'OK',
                'code'=> 200,
            );
            print_json($out_put); die;
        }
    }
}
