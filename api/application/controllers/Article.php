<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller
{
    private $error_post_data = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
//        $this->load->helper(array('form', 'url'));
//        $data = array();
//        $returned_data = $this->update_photo(1,'business');
//        $data['error'] = "";
//        $data['ok'] = $returned_data;
//        $this->load->view('header');
//        $this->load->view('home',$data);
//        $this->load->view('footer');
        echo json_encode('Article');
    }

    public function get_list(){
        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('article',array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){

                $business_list = $this->get_business_list($list->business_list);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

    public function get_data($id){
        if($this->input->get('view')>=1){
            viewed_increment($id,'article');
        }
        if(!isset($id)){
            $out_put = array(
                'list' => array(),
                'total' => 0
            );
            print_json($out_put); die;
        }
        $query = $this->db->get_where('article',array('id'=>$id,'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){

                $business_list = $this->get_business_list($list->business_list);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $out_put = array(
            'data' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

    private function get_business_list($business_list_id){
        $business_list_id = explode(',',$business_list_id);

        $this->db->select('name,cover_image_src,id,address_province,address_district,score,lat,lon,work_day_sun,work_day_mon,work_day_tue,work_day_wed,work_day_thu,work_day_fri,slogan,work_day_sat,work_time_start,work_time_close,work_time_start_2,work_time_close_2');
        $this->db->where_in('id',$business_list_id);
        $query = $this->db->get('business');

        $business_list = $query->result();

        $current_lat = $this->input->get('lat');
        $current_lon = $this->input->get('lon');
        $unit = ($this->input->get('unit'))?$this->input->get('unit'):'M';
        if($current_lat&&$current_lon){
            $new_data = array();
            $i = 0;
            foreach ($business_list as $item){
                $item->distance_unit = strtoupper($unit);
                $item->distance = cal_distance($current_lat,$current_lon,$item->lat,$item->lon, $unit);
                $new_data[($item->distance*100).".".$i] = $item;
                $i++;
//                echo '<pre>';
//                var_dump($current_lat,$current_lon,$item->lat,$item->lon,$item->distance); die;
            }
//            ksort($new_data);
//            $business_list = $new_data;
            ksort($new_data);
            $business_list = array();
            foreach ($new_data as $item) {
                $business_list[] = $item;
            }
        }
        return $business_list;
    }
}