<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller
{
    private $error_post_data = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
//        $this->load->helper(array('form', 'url'));
//        $data = array();
//        $returned_data = $this->update_photo(1,'business');
//        $data['error'] = "";
//        $data['ok'] = $returned_data;
//        $this->load->view('header');
//        $this->load->view('home',$data);
//        $this->load->view('footer');
        echo json_encode('Category');
    }

    public function get_list(){
        $cate_type = $this->input->get('type');
        if(isset($cate_type)&&intval($cate_type)>0){
            $query = $this->db->get_where('category',array('type'=>$cate_type,'status'=>'active'));
        }else{
            $query = $this->db->get_where('category',array('status'=>'active'));
        }

        if($query->num_rows()>0){
            $cate_list = $query->result();
        }else{
            $cate_list = array();
        }
        $out_put = array(
            'list' => $cate_list,
            'total' => count($cate_list)
        );
        print_json($out_put);
    }
}