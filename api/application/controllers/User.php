<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('FACEBOOK_SDK_V4_SRC_DIR',APPPATH.'/models/Facebook/');

require_once(APPPATH."/models/Facebook/autoload.php");
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\GraphUser;
use Facebook\FacebookRequestException;
use Facebook\FacebookRedirectLoginHelper;

class User extends CI_Controller
{
    private $error_post_data = array();
    private $appID = '';
    private $secretKey = '';
    private $api_key = '';

    public function __construct()
    {
        parent::__construct();
        $this->appID = $this->config->item('fb_appid');
        $this->secretKey = $this->config->item('fb_secretkey');
        $this->api_key = $this->config->item('api_key');
    }


    public function index()
    {
        echo json_encode('User');
    }

    public function facebookInit()
    {
        $this->fb = new Facebook\Facebook([
            'app_id'                => $this->appID ,
            'app_secret'            => $this->secretKey,
            'default_graph_version' => 'v2.5',
        ]);
        return $this->fb ;

    }

//    private function loginUrl()
//    {
//        $helper = $this->facebookInit()->getRedirectLoginHelper();
//        $permissions = ['email','user_friends']; // Optional permissions
//
//        $loginUrl = $helper->getLoginUrl(site_url()."/user/loginCallback/", $permissions);
//        echo  $loginUrl ;
//
//
//    }
//    private function loginCallback(){
//
//        $helper = $this->facebookInit()->getRedirectLoginHelper();
//        try {
//            $accessToken = $helper->getAccessToken();
//        } catch(Facebook\Exceptions\FacebookResponseException $e) {
//            // When Graph returns an error
//            echo 'Graph returned an error: ' . $e->getMessage();
//            exit;
//        } catch(Facebook\Exceptions\FacebookSDKException $e) {
//            // When validation fails or other local issues
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
//        }
//
//        if (isset($accessToken)) {
//            // Logged in!
//            var_dump('$accessToken',$accessToken);
//            $fb = $this->facebookInit();
//            $fb->setDefaultAccessToken('EAAI7nUxeqMYBALuOZBZAdMM2Lwhiqq2oQV5yWCTyjN95GyAhflZBaejcLqys3Rwf6CtjDVyADIJkIZCMJD7O7zCsefHKmxy49yJctNht3hbalLLEEAU6Us8PqHWZCuywVpyzUEKdeLgXofIEAZBRZBO8qvAauyuZANIdbpZAUNpJhLdeU1ZBFjQGdfSe3oL35uoTNXfjKuCzZCnTgZDZD');
//
//            try {
//                $response = $fb->get('/me');
//                $userNode = $response->getGraphUser();
//            } catch(Facebook\Exceptions\FacebookResponseException $e) {
//                // When Graph returns an error
//                echo 'Graph returned an error: ' . $e->getMessage();
//                exit;
//            } catch(Facebook\Exceptions\FacebookSDKException $e) {
//                // When validation fails or other local issues
//                echo 'Facebook SDK returned an error: ' . $e->getMessage();
//                exit;
//            }
//
//            echo 'Logged in as ' . $userNode->getName();
//            // Now you can redirect to another page and use the
//            // access token from $_SESSION['facebook_access_token']
//        }
//    }

    public function register(){
        $api_key = $this->input->post('api_key');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=>200
            );
            print_json($output);
            die;
        }else{
            $device_id = $this->input->post('device_id');
            $fb_token = $this->input->post('fb_token');
            $user_fb_id = $this->input->post('fb_id');
            $user_name = $this->input->post('name');
            $user_email = $this->input->post('email');
            $user_birthday = $this->input->post('birthday');
            $gender = $this->input->post('gender');
            $mobile = ($this->input->post('mobile'))?$this->input->post('mobile'):'';
            $password = $this->input->post('password');
            $password = hash("sha512", $password, false);
            $created_time = date('Y-m-d H:i:s');
            if($user_fb_id==""||$fb_token==""){
                $profile_image_url = 'http://cms.pueanrod.com/assets/images/no_profile_image.png';
            }else{
                $profile_image_url = 'http://graph.facebook.com/'.$user_fb_id.'/picture?width=500';
            }
            $this->db->where(array('email'=>$user_email,'status !='=>'deleted'));
            $query = $this->db->get('user');
            if($query->num_rows()>0){
                $output = array(
                    'status' => 'EMAIL_IS_NOT_AVAILABLE',
                    'code'=>200
                );
                print_json($output);
                die;
            }

            $update_data = array(
                'fb_id' => $user_fb_id,
                'name' => $user_name,
                'email' => $user_email,
                'gender' => $gender,
                'mobile' => $mobile,
                'password' => $password,
                'birthday' => $user_birthday,
                'profile_image' => $profile_image_url,
                'status' => 'active',
                'created_time' => $created_time,
                'updated_time' => $created_time
            );
            if ($this->db->insert('user', $update_data)) {
                $id = $this->db->insert_id();
                $token = hash("sha512", $id.$device_id.$user_fb_id, false);
                $update_data = array(
                    'token' => $token,
                    'user_id' => $id,
                    'device_id' => $device_id,
                    'fb_id' => $user_fb_id,
                    'fb_token' => $fb_token
                );
                if ($this->db->insert('session', $update_data)) {
                    $point_data = add_user_point($id,POINT_REGISTER, 'register', $id, null);
                    $output = array(
                        'status' => 'SUCCESS',
                        'code' => 200,
                        'data' => array(
                            'token' => $token,
                            'point_data' => $point_data,
                            'user_id' => $id
                        )
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_CREATE_SESSION",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => "CAN_NOT_REGISTER_USER",
                    'code' => 200
                );
                print_json($output);
            }
        }
    }

    public function update_user_info(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $user_name = $this->input->post('name');
        $user_email = $this->input->post('email');
        $user_birthday = $this->input->post('birthday');
        $gender = $this->input->post('gender');
        $mobile = $this->input->post('mobile');
        $password = $this->input->post('password');
        $device_id = $this->input->post('device_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id');
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                $query = $this->db->get('user');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'USER_IS_NOT_FOUND',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $updated_time = date('Y-m-d H:i:s');
                $update_data = array(
                    'updated_time' => $updated_time
                );
                if($user_name&&$user_name!=''){
                    $update_data['name'] = $user_name;
                }
                if($user_email&&$user_email!=''){
                    $update_data['email'] = $user_email;
                }
                if($user_birthday&&$user_birthday!=''){
                    $update_data['birthday'] = $user_birthday;
                }
                if($gender&&$gender!=''){
                    $update_data['gender'] = $gender;
                }
                if($mobile&&$mobile!=''){
                    $update_data['mobile'] = $mobile;
                }
                if($password&&$password!=''){
                    $update_data['password'] = hash("sha512", $password, false);
                }
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                if ($this->db->update('user', $update_data)) {
                    if(isset($_FILES['profile_image'])){
                        if ($_FILES['profile_image']['name']!='') {
                            $img_data = $this->update_photo($user_id, 'user','profile_image');
                            if(isset($img_data[0]['id'])&&isset($img_data[0]['src'])){
                                $this->update_profile_image($user_id,$img_data[0]['id'],$img_data[0]['src']);
                            }else{
                                $output = array(
                                    'status' => "ERROR",
                                    'detail' => $img_data,
                                    'code' => 200
                                );
                                print_json($output); die;
                            }

                        }
                    }
                    $user_data_updated = json_decode($this->curl->simple_get(site_url('user/get_data/'.$user_id)));
                    $output = array(
                        'status' => "SUCCESS",
                        'data' => $user_data_updated->data,
                        'code' => 200
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_UPDATE_USER_INFORMATION",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    public function connect_with_facebook(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $user_fb_id = $this->input->post('fb_id');
        $device_id = $this->input->post('device_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id');
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                $query = $this->db->get('user');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'USER_IS_NOT_FOUND',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $updated_time = date('Y-m-d H:i:s');
                $update_data = array(
                    'fb_id' => $user_fb_id,
                    'updated_time' => $updated_time
                );
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                if ($this->db->update('user', $update_data)) {
                    $output = array(
                        'status' => "SUCCESS",
                        'code' => 200
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_CONNECT_WITH_FACEBOOK",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    public function unlink_facebook(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $device_id = $this->input->post('device_id');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id');
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                $query = $this->db->get('user');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'USER_IS_NOT_FOUND',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $updated_time = date('Y-m-d H:i:s');
                $update_data = array(
                    'fb_id' => '',
                    'updated_time' => $updated_time
                );
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                if ($this->db->update('user', $update_data)) {
                    $output = array(
                        'status' => "SUCCESS",
                        'code' => 200
                    );
                    print_json($output);
                }else{
                    $output = array(
                        'status' => "CAN_NOT_UNLINK_WITH_FACEBOOK",
                        'code' => 200
                    );
                    print_json($output);
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    public function get_data($id){
        if(!isset($id)){
            $out_put = array(
                'list' => array(),
                'total' => 0
            );
            print_json($out_put); die;
        }
        $this->db->select('id,point,fb_id,email,name,birthday,gender,profile_image,reviewed,point,created_time,updated_time');
        $query = $this->db->get_where('user',array('id'=>$id,'status !='=> 'deleted'));
        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = $result_data[0];
        }else{
            $data_list = array();
        }
        $out_put = array(
            'data' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

    public function get_user_point($id){
        if(!isset($id)){
            $out_put = array(
                'list' => array(),
                'total' => 0
            );
            print_json($out_put); die;
        }
        $this->db->select('id,name,point,created_time,updated_time');
        $query = $this->db->get_where('user',array('id'=>$id,'status !='=> 'deleted'));
        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = $result_data[0];
        }else{
            $data_list = array();
        }
        $out_put = array(
            'data' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

    public function get_review($id){
        $offset = ($this->input->get('offset'))?$this->input->get('offset'):0;
        $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;
        $page = (int)($this->input->get('page'))?$this->input->get('page'):1;
        if($this->input->get('view')>=1){
            viewed_increment($id,'user');
        }
        if(!isset($id)){
            $out_put = array(
                'list' => array(),
                'total' => 0
            );
            print_json($out_put); die;
        }

        $this->db->from('review');
        $this->db->where(array('user_id'=>$id,'status !='=> 'deleted'));
        $total_num = $this->db->count_all_results();
        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('review',array('user_id'=>$id,'status !='=> 'deleted'),$limit,$offset);
        $offset = $limit+$offset;
        if($query->num_rows()>0){
            $result_data = $query->result();
            $business_id_list = array();
            foreach ($result_data as $list){
                $business_id_list[] = $list->business_id;
            }
            $business_ist = array();
            if(count($business_id_list)>0){
                foreach ($business_id_list as $business_id){
                    $business_data = json_decode($this->curl->simple_get(site_url('business/get_data/'.$business_id)));
                    $business_ist[$business_id] = $business_data->data[0];
                }
            }
            foreach ($result_data as $key => $list){
                $result_data[$key]->business_data = $business_ist[$list->business_id];
            }
            foreach ($result_data as $key => $list){
                $query = $this->db->get_where('images',array('ref_id'=>$list->id,"type"=>"review",'status !='=> 'deleted'));
                $img_data = array();
                foreach ($query->result() as $img_list){
                    $img_data[] = array("id"=>$img_list->id,"url"=>$img_list->src);
                }
                $result_data[$key]->images = $img_data;
            }
            $data_list = $result_data;

            $output = array();
            $total_page = ceil($total_num / $limit);
            $next_url = '';
            if($offset<$total_num){
                $url_param = array(
                    'limit'=>$limit,
                    'offset'=>$offset
                );
                $next_url = site_url('user/get_review/'.$id.'?'.http_build_query($url_param));
            }

            $output['total_page'] = $total_page;
            $output['total_num'] = $total_num;
            $output['limit'] = $limit;
            $output['page'] = $page;
            $output['next'] = $next_url;
            $output['data'] = $data_list;
            $output['status'] = 'SUCCESS';
            $output['code'] = 200;
        }else{
            $data_list = array();
            $output = array(
                'data' => $data_list,
                'total' => count($data_list)
            );

        }

        print_json($output); die;
    }

    public function login(){
        $api_key = $this->input->post('api_key');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }

        $user_fb_id = $this->input->post('fb_id');
        $device_id = $this->input->post('device_id');
        $fb_token = $this->input->post('fb_token');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $login_from = $this->input->post('login_from');

        $this->db->select('id,point,fb_id,email,mobile,name,birthday,gender,profile_image,reviewed,point,created_time,updated_time');
        if($login_from == 'facebook'){
            $this->db->where(array('fb_id'=>$user_fb_id,'status !='=>'deleted'));
            $query = $this->db->get('user');
        }else{
            $h_password = hash("sha512", $password, false);
            $this->db->where(array('email'=>$email,'password'=>$h_password,'status !='=>'deleted'));
            $query = $this->db->get('user');
        }
        if($query->num_rows()>0){
            $user_data = $query->result();
            $user_data = $user_data[0];
            $this->db->where(array('user_id'=>$user_data->id,'device_id'=>$device_id));
            $this->db->select('token,user_id');
            $session_query = $this->db->get('session');
            $session_data = $session_query->result();
            
			if(!isset($session_data[0])){
				$token = hash("sha512", $user_data->id.$device_id.$user_fb_id, false);
                $update_data = array(
                    'token' => $token,
                    'user_id' => $user_data->id,
                    'device_id' => $device_id,
                    'fb_id' => $user_fb_id,
                    'fb_token' => $fb_token
                );
                if ($this->db->insert('session', $update_data)) {
					$user_data->session = array('token'=>$token,'user_id'=>$user_data->id);
				}
			}else{
				$user_data->session = $session_data[0];
			}
            $output = array(
                'status'=> 'SUCCESS',
                'code'=> 200,
                'data'=>$user_data
            );
            print_json($output);
        }else{
            $this->db->where(array('email'=>$email,'status !='=>'deleted'));
            $query = $this->db->get('user');
            if($query->num_rows()>0){
                $output = array(
                    'status'=> 'WRONG_PASSWORD',
                    'message' => 'รหัสผ่านหรือชื่อผู้ใช้ไม่ถูกต้อง',
                    'code'=> 200
                );
            }else{
                $output = array(
                    'status'=> 'USER_NOT_FOUND',
                    'message' => 'ไม่พบผู้ใช้นี้',
                    'code'=> 200
                );
            }

            print_json($output);
        }


//        $fb = $this->facebookInit();
//        $fb->setDefaultAccessToken($fb_token);
//
//        try {
//            $response = $fb->get('/'.$user_fb_id.'?fields=email,picture.width(256),name,birthday,gender');
//            $userNode = $response->getGraphUser();
//
//
//
//        } catch(Facebook\Exceptions\FacebookResponseException $e) {
//
//            echo 'Graph returned an error: ' . $e->getMessage();
//            exit;
//        } catch(Facebook\Exceptions\FacebookSDKException $e) {
//            // When validation fails or other local issues
//            echo 'Facebook SDK returned an error: ' . $e->getMessage();
//            exit;
//        }

//        echo 'Logged in as ' . $userNode->getName();
//        var_dump($userNode);
    }

    public function check_login(){
        $api_key = $this->input->post('api_key');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }
        $user_fb_id = $this->input->post('fb_id');
        $device_id = $this->input->post('device_id');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $login_from = $this->input->post('login_from');

        $this->db->select('token,user_id');
        if($login_from == 'facebook'){
            $this->db->where(array('fb_id'=>$user_fb_id,'device_id'=>$device_id,'token'=>$token));

        }else{
            $this->db->where(array('user_id'=>$user_id,'device_id'=>$device_id,'token'=>$token));
        }
        $query = $this->db->get('session');
        if($query->num_rows()>0){
            $session_data = $query->result();
            $session_data = $session_data[0];
            $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
            $this->db->select('id,point,fb_id,email,mobile,name,birthday,gender,profile_image,reviewed,point,created_time,updated_time');
            $session_query = $this->db->get('user');
            $user_data = $session_query->result();
            $user_data = $user_data[0];
            $user_data->session = $session_data;
            $output = array(
                'status'=> 'SUCCESS',
                'code'=> 200,
                'data'=>$user_data
            );
            print_json($output);
        }else{
            $output = array(
                'status'=> 'NO_SESSION',
                'code'=> 200
            );
            print_json($output);
        }
    }

    public function request_reset_password(){
        $api_key = $this->input->post('api_key');
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }
        $email= $this->input->post('email');
        $this->db->where(array('email'=>$email,'status'=>'active'));
        $query = $this->db->get('user');
        if($query->num_rows()>0){
            $user_data = $query->result();
            $user_data = $user_data[0];
            $gen_key = $email;
            $gen_key.= date("Y-m-d H:i:s");
            $request_code = hash("sha512", $gen_key, false);
            $request_code = substr($request_code,0,50);
            $data = array(
                'request_code' => $request_code,
                'request_expired_date' => date('Y-m-d H:i:s',strtotime("+1 day")),
                'updated_time' => date("Y-m-d H:i:s")
            );
            $this->db->where(array('id' => $user_data->id, 'email' => $email, 'status' => 'active'));
            if($this->db->update('user', $data)){
                if($this->send_request_reset_password_email($email,$request_code,$user_data->id)){
                    $output = array(
                        'status'=> 'SUCCESS',
                        'code'=> 200,
                        'data'=>'ลิงค์สำหรับการเปลี่ยนรหัสผ่านได้ส่งไปยังอีเมล์ของคุณแล้ว'
                    );
                }else{
                    $output = array(
                        'status'=> 'ERROR',
                        'code'=> 200,
                        'data'=>'ไม่สามารถส่งอีเมล์ได้'
                    );
                }
                print_json($output);
            }else{
                $output = array(
                    'status'=> 'ERROR',
                    'code'=> 200,
                    'ไม่สามารถเข้าถึงข้อมูลผู้ใช้ สำหรับแก้ไขข้อมูลได้ กรุณาติดต่อเจ้าหน้าที่'
                );
                print_json($output);
            }

        }else{
            $output = array(
                'status'=> 'EMAIL_NOT_FOUND',
                'code'=> 200
            );
            print_json($output);
        }
    }


    private function update_photo($ref_id, $img_type,$file_label)
    {
        $POST_DATA = array();
        $img_num = count($_FILES[$file_label]['tmp_name']);
        if (is_array(is_array($_FILES[$file_label]['tmp_name']))) {
            for ($i = 0; $i < $img_num; $i++) {
                $tmp_file = $_FILES[$file_label]['tmp_name'][$i];
                $file_name = basename($_FILES[$file_label]['name'][$i]);
                $file_type = $_FILES[$file_label]['type'][$i];
                $POST_DATA['upload_img_' . $i] = curl_file_create($tmp_file, $file_type, $file_name);
            }
        } else {
            $tmp_file = $_FILES[$file_label]['tmp_name'];
            $file_name = basename($_FILES[$file_label]['name']);
            $file_type = $_FILES[$file_label]['type'];
            $POST_DATA['upload_img_0'] = curl_file_create($tmp_file, $file_type, $file_name);
        }
        $POST_DATA['img_num'] = $img_num;
        $POST_DATA['depot_key'] = $this->config->item('depot_key');
        $POST_DATA['img_type'] = $img_type;
        $POST_DATA['ref_id'] = $ref_id;
        $curl_handle = curl_init($this->config->item('depot_url'));
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

        $returned_data = curl_exec($curl_handle);
        curl_close($curl_handle);

        $returned_data = json_decode($returned_data, true);
        return $returned_data['result'];
    }

    private function update_profile_image($user_id,$img_id,$img_src){
        if($this->delete_profile_image($user_id,$img_id)){
            $data = array(
                'image_id' => $img_id,
                'profile_image' => $img_src,
                'updated_time' => date("Y-m-d H:i:s"),

            );
            $this->db->where('id', $user_id);
            $this->db->update('user', $data);
        }else{
            return false;
        }

    }

    private function delete_profile_image($user_id,$img_id){
        $data = array(
            'status' => 'deleted',
            'updated_time' => date("Y-m-d H:i:s")

        );
        $this->db->where(array('ref_id' => $user_id, 'type' => 'user', 'id !=' => $img_id));
        if($this->db->update('images', $data)){
            return true;
        }else{
            return false;
        }
    }

    private function send_request_reset_password_email($send_to,$request_code,$user_id){
        require 'phpmailler/PHPMailerAutoload.php';

        $mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.pueanrod.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'noreply@pueanrod.com';                 // SMTP username
        $mail->Password = 'Pue1216!';                           // SMTP password
        $mail->SMTPSecure = 'tls';
        $mail->CharSet = 'UTF-8';// Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom('noreply@pueanrod.com', 'Pueanrod');
        $mail->addAddress($send_to);

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'Request reset password - ขอรีเซ็ตรหัสผ่าน Pueanrod.com';

        $url = 'http://www.pueanrod.com/reset_password?uid='.$user_id.'&code='.$request_code;
        $msg = '<div style="padding: 20px; background-color: #eb4e25;">
                    <div><span><img src="http://www.pueanrod.com/mail_logo.png" height="45" title="เพื่อนรถ - Pueanrod.com" alt="เพื่อนรถ - Pueanrod.com"></span></div>
                    <div style="padding: 20px; background-color: #FFF;">
                    <div><p style="padding: 16px;">คุณได้ทำการขอรีเซตรหัสผ่านจากทางแอพพลิเคชั่น <strong>เพื่อนรถ</strong> หากไม่ใช่กรุณาเพิกเฉยและลบอีเมล์หรือเปลี่ยนรหัสผ่านเพื่อความปลอดภัย</p>
                    <p style="padding: 16px;">You requested to reset your password from application <strong>Pueanrod</strong>. if it is\' you please ignore and delete this email or change your password to protect your account </p>
                    <div style="padding: 14px;">เปลี่ยนรหัสผ่านได้จากลิงค์ต่อไปนี้ <strong><a target="_blank" href="'.$url.'">[ รีเซ็ตรหัสผ่าน ]</a></strong></div>
                    <div style="padding: 14px;">You can reset your password by this link <strong><a target="_blank" href="'.$url.'">[ Reset Password ]</a></strong></div>
                    </div>
                    </div>
                    <div style="text-align: center; width: 100%; font-size: 12px; color: #fff; padding-top: 20px;">&copy;2017 Pueanrod.com, Contact us: <a href="mailto:contact@pueanrod.com" style="color: #fff; font-size: 12px;">contact@pueanrod.com</a></div>
                    </div>';

        $mail->Body    = $msg;

        if(!$mail->send()) {
            return false;
        } else {
            return true;
        }
    }

}