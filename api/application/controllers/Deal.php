<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deal extends CI_Controller
{
    private $error_post_data = array();
    private $appID = '';
    private $secretKey = '';
    private $api_key = '';

    public function __construct()
    {
        parent::__construct();
        $this->appID = $this->config->item('fb_appid');
        $this->secretKey = $this->config->item('fb_secretkey');
        $this->api_key = $this->config->item('api_key');
    }


    public function index()
    {
//        $this->load->helper(array('form', 'url'));
//        $data = array();
//        $returned_data = $this->update_photo(1,'business');
//        $data['error'] = "";
//        $data['ok'] = $returned_data;
//        $this->load->view('header');
//        $this->load->view('home',$data);
//        $this->load->view('footer');
        echo json_encode('Promotion');
    }

    public function get_list(){
        $type = $this->input->get('type');
        $this->db->order_by('order_number','ASC');
        $this->db->order_by('id','DESC');
        if($type=="normal"){
            $this->db->where(array('type'=> 'normal','status !='=> 'deleted'));
        }elseif($type=="lady"){
            $this->db->where(array('type'=> 'lady','status !='=> 'deleted'));
        }else{
            $this->db->where(array('status !='=> 'deleted'));
        }
        $query = $this->db->get_where('deal',array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){

                $business_list = $this->get_business_list($list->business_list);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

    public function get_data($id){
        if($this->input->get('view')>=1){
            viewed_increment($id,'deal');
        }
        if(!isset($id)){
            $out_put = array(
                'list' => array(),
                'total' => 0
            );
            print_json($out_put); die;
        }

        $query = $this->db->get_where('deal',array('id'=>$id,'status !='=> 'deleted'));

        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list = array();
            foreach ($result_data as $list){

                $business_list = $this->get_business_list($list->business_list);
                $list->business_list = $business_list;
                $data_list[] = $list;
            }
        }else{
            $data_list = array();
        }
        $user_id = $this->input->get('user_id');
        $this->db->select('point');
        $query = $this->db->get_where('deal_log',array('user_id'=>$user_id, 'deal_id'=> $id));
        $out_put = array(
            'data' => $data_list,
            'used_number' => $query->num_rows(),
            'total' => count($data_list)
        );
        print_json($out_put);
    }

    public function check_business_code(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $deal_id = $this->input->post('deal_id');
        $business_code = $this->input->post('business_code');
        $business_code = strtoupper($business_code);
        $device_id = $this->input->post('device_id');
        if(count($_POST)<6){
            $output = array(
                'status' => 'POST_DATA_IS_INCOMPLETE',
                'code'=> 200
            );
            print_json($output); die;
        }
        $data_list = array();
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id');
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                $query = $this->db->get('user');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'USER_IS_NOT_FOUND',
                        'message' => 'ไม่พบผู้ใช้นี้',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $this->db->where('id',$deal_id);
                $this->db->where('status !=', 'deleted');
                $query = $this->db->get('deal');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'DEAL_IS_NOT_FOUND',
                        'message ' => 'ไม่พบดีลนี้แล้ว',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $deal_list = $query->result();
                $deal_data = $deal_list[0];

                $this->db->select('name,business_code,cover_image_src,id,address_province,address_district,score,lat,lon,work_day_sun,work_day_mon,work_day_tue,work_day_wed,work_day_thu,work_day_fri,slogan,work_day_sat,work_time_start,work_time_close,work_time_start_2,work_time_close_2');
                $this->db->where('business_code',$business_code);
                $this->db->where('status !=', 'deleted');
                $query = $this->db->get('business');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'BUSINESS_IS_NOT_FOUND',
                        'message' => 'คุณใช้เวลาดำเนินการนานเกินไป กรุณาทำการใหม่อีกครั้ง',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $business_list = $query->result();
                $business_data = $business_list[0];
                $business_id = $business_data->id;
                $business_list_id = explode(',',$deal_data->business_list);
                if(count($business_list_id)>0&&$deal_data->business_list!=""){
                    if(!in_array($business_id,$business_list_id)){
                        $output = array(
                            'status' => 'BUSINESS_IS_NOT_PARTICIPATE',
                            'message' => 'สถานที่นี้ไม่ร่วมรายการ',
                            'code'=> 200
                        );
                        print_json($output); die;
                    }
                }
                $out_put = array(
                    "deal_data" => $deal_data,
                    "business_data" => $business_data,
                    'message' => 'กรุณายืนยันการใช้งานดีล ภายใน 30 นาที',
                    "time_stamp" => time()+(31*60),
                    'status' => 'SUCCESS',
                    'code'=> 200
                );
                print_json($out_put);
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    public function confirm_using_deal(){
        $api_key = $this->input->post('api_key');
        $user_id = $this->input->post('user_id');
        $token = $this->input->post('token');
        $deal_id = $this->input->post('deal_id');
        $business_code = $this->input->post('business_code');
        $business_code = strtoupper($business_code);
        $business_id = $this->input->post('business_id');
        $lat = $this->input->post('lat');
        $lon = $this->input->post('lon');
        //$time_stamp = (int)$this->input->post('time_stamp');
        $time_stamp = (int)time();
        $device_id = $this->input->post('device_id');
        $data_list = array();
        if(count($_POST)<9){
            $output = array(
                'status' => 'POST_DATA_IS_INCOMPLETE',
                'code'=> 200
            );
            print_json($output); die;
        }
        if($this->api_key!=$api_key){
            $output = array(
                'status' => 'WRONG_API_KEY',
                'code'=> 200
            );
            print_json($output); die;
        }else{
            $this->db->select('id');
            $this->db->where(array('user_id'=>$user_id,'token'=>$token,'device_id'=>$device_id));
            $query = $this->db->get('session');
            if($query->num_rows()>0){
                $this->db->select('id,name,point,gender');
                $this->db->where(array('id'=>$user_id,'status !='=>'deleted'));
                $query = $this->db->get('user');
                if($query->num_rows()==0){
                    $output = array(
                        'status' => 'USER_IS_NOT_FOUND',
                        'message' => 'ไม่พบผู้ใช้นี้',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
                $user_list = $query->result();
                $user_data = $user_list[0];
                $this->db->select('name,business_code,cover_image_src,id,address_province,address_district,score,lat,lon,work_day_sun,work_day_mon,work_day_tue,work_day_wed,work_day_thu,work_day_fri,slogan,work_day_sat,work_time_start,work_time_close,work_time_start_2,work_time_close_2');
//                $this->db->where('business_code',$business_code);
                $this->db->where('id',$business_id);
                $this->db->where('status !=', 'deleted');
                $query = $this->db->get('business');
                if($query->num_rows()>0){
                    $business_list = $query->result();
                    $business_data = $business_list[0];
                    if($business_data->business_code!=$business_code){
                        $output = array(
                            'status' => 'BUSINESS_CODE_IS_NOT_CORRECT',
                            'message' => 'รหัสสถาณประกอบการไม่ถูกต้อง',
                            'code'=> 200
                        );
                        print_json($output); die;
                    }

                    $this->db->where('id',$deal_id);
                    $this->db->where('status !=', 'deleted');
                    $query = $this->db->get('deal');
                    if($query->num_rows()==0){
                        $output = array(
                            'status' => 'DEAL_IS_NOT_FOUND',
                            'message ' => 'ไม่พบดีลนี้แล้ว',
                            'code'=> 200
                        );
                        print_json($output); die;
                    }
                    $deal_list = $query->result();
                    $deal_data = $deal_list[0];
                    $deal_type = $deal_data->type;
                    if($deal_type=="lady"&&$user_data->gender!="female"){
                        $output = array(
                            'status' => 'ONLY_FOR_FEMALE_USER',
                            'message' => 'ดีลนี้ ใช้ได้เฉพาะสุภาพสตรี',
                            'code'=> 200
                        );
                        print_json($output); die;
                    }
                    $user_point = (int)$user_data->point;
                    $using_limit = (int)$deal_data->using_limit;
                    if($this->check_usage_limit($user_id,$deal_id,$using_limit)&&$using_limit>=0){
                        $current_time = time();
                        if($current_time<=$time_stamp){
                            $deal_long_data = using_deal($deal_data,$business_data,$user_data,$user_point,$lat,$lon);
                            $out_put = array(
                                'data' => $deal_long_data,
                                'status' => 'SUCCESS',
                                'code'=> 200
                            );
                            print_json($out_put);
                        }else{
                            $output = array(
                                'status' => 'ACTIVITY_IS_EXPIRED',
                                'message' => 'คุณใช้เวลาดำเนินการนานเกินไป กรุณาทำการใหม่อีกครั้ง',
                                'code'=> 200
                            );
                            print_json($output); die;
                        }
                    }else{
                        $output = array(
                            'status' => 'MAXIMUM_DEAL_LIMIT',
                            'message' => 'ดีลนี้ มีผู้ใช้เกินจำนวนแล้ว',
                            'code'=> 200
                        );
                        print_json($output); die;
                    }
                }else{
                    $output = array(
                        'status' => 'BUSINESS_IS_NOT_FOUND',
                        'message' => 'คุณใช้เวลาดำเนินการนานเกินไป กรุณาทำการใหม่อีกครั้ง',
                        'code'=> 200
                    );
                    print_json($output); die;
                }
            }else{
                $output = array(
                    'status' => 'NO_SESSION',
                    'code'=> 200
                );
                print_json($output); die;
            }
        }
    }

    private function check_usage_limit($user_id,$deal_id,$using_limit){
        $this->db->select('point');
        $query = $this->db->get_where('deal_log',array('user_id'=>$user_id, 'deal_id'=> $deal_id));
        if($query->num_rows()>=$using_limit&&$using_limit>0){
            var_dump($query->num_rows(),$using_limit);
            return false;
        }else{
            return true;
        }
    }

    public function get_deal_log($deal_id){
        $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;
        $page = (int)($this->input->get('page'))?$this->input->get('page'):1;
        $offset = ($page - 1) * $limit;

        $this->db->from('deal_log');
        $this->db->where(array('deal_id'=>$deal_id));
        $total_num = $this->db->count_all_results();

        $this->db->order_by('id','DESC');
        $query =  $this->db->get_where('deal_log',array('deal_id'=> $deal_id),$limit,$offset);
        $log_list = $query->result();

        $output = array();
        $total_page = ceil($total_num / $limit);
        $output['total_page'] = $total_page;
        $output['total_num'] = $total_num;
        $output['limit'] = $limit;
        $output['page'] = $page;
        $output['data'] = $log_list;
        $output['status'] = 'SUCCESS';
        $output['code'] = 200;
        print_json($output);
    }

    public function get_user_deal_log($user_id){
        $limit = ($this->input->get('limit'))?$this->input->get('limit'):20;
        $page = (int)($this->input->get('page'))?$this->input->get('page'):1;
        $offset = ($page - 1) * $limit;

        $this->db->from('deal_log');
        $this->db->where(array('user_id'=>$user_id));
        $total_num = $this->db->count_all_results();

        $this->db->order_by('id','DESC');
        $query =  $this->db->get_where('deal_log',array('user_id'=> $user_id),$limit,$offset);
        $log_list = $query->result();

        $output = array();
        $total_page = ceil($total_num / $limit);
        $output['total_page'] = $total_page;
        $output['total_num'] = $total_num;
        $output['limit'] = $limit;
        $output['page'] = $page;
        $output['data'] = $log_list;
        $output['status'] = 'SUCCESS';
        $output['code'] = 200;
        print_json($output);
    }

    private function get_business_list($business_list_id){
        $business_list_id = explode(',',$business_list_id);

        $this->db->select('name,business_code,cover_image_src,id,address_province,address_district,score,lat,lon,work_day_sun,work_day_mon,work_day_tue,work_day_wed,work_day_thu,work_day_fri,slogan,work_day_sat,work_time_start,work_time_close,work_time_start_2,work_time_close_2');
        $this->db->where_in('id',$business_list_id);
        $query = $this->db->get('business');

        $business_list = $query->result();

        $current_lat = $this->input->get('lat');
        $current_lon = $this->input->get('lon');
        $unit = ($this->input->get('unit'))?$this->input->get('unit'):'M';
        if($current_lat&&$current_lon){
            $new_data = array();
            $i = 0;
            foreach ($business_list as $item){
                $item->distance_unit = strtoupper($unit);
                $item->distance = cal_distance($current_lat,$current_lon,$item->lat,$item->lon, $unit);
                $new_data[($item->distance*100).".".$i] = $item;
                $i++;
            }
            ksort($new_data);
            $business_list = array();
            foreach ($new_data as $item) {
                $business_list[] = $item;
            }
        }
        return $business_list;
    }


}