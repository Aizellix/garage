<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Popup extends CI_Controller
{
    private $error_post_data = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        echo json_encode('Promotion');
    }
    public function get_list(){
        $this->db->order_by('order_number','ASC');
        $query = $this->db->get_where('popup',array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted'));
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            $data_list = array();
        }
        $out_put = array(
            'list' => $data_list,
            'total' => count($data_list)
        );
        print_json($out_put);
    }

}