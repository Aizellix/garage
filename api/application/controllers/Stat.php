<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stat extends CI_Controller
{
    private $error_post_data = array();

    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
//        $this->load->helper(array('form', 'url'));
//        $data = array();
//        $returned_data = $this->update_photo(1,'business');
//        $data['error'] = "";
//        $data['ok'] = $returned_data;
//        $this->load->view('header');
//        $this->load->view('home',$data);
//        $this->load->view('footer');
        echo json_encode('Stat');
    }

    public function logger(){
        $status = 'ERROR';
        $code = 400;
        $item_type = $this->input->post('type');
        $content_id = $this->input->post('id');
        $review_id = $this->input->post('review');
        $review_score = $this->input->post('score');
        $created_time = date('Y-m-d H:i:s');
        $title = date('Ymd',strtotime($created_time));
        $title .= '_'.$item_type.'_'.$content_id;
        $this->db->where(array('title'=>$title));
        $query = $this->db->get('transaction_log');
        $cate = '';
        $lat = '';
        $lon = '';
        $reviewed = '';
        $score = 0;
        if($query->num_rows()>0){

            $result_data = $query->result();
            $result_data = $result_data[0];

            $total_num = (int)$result_data->total;
            $this_id = $result_data->id;
            $update_data = array(
                'total' => $total_num+1
            );

            if($item_type=='business'){
                if($review_id&&$review_score){
                    $score = $result_data->score;
                    $reviewed = json_decode($result_data->reviewed,true);
                    if(!in_array($review_id,$reviewed)){
                        $reviewed[] = $review_id;
                        $score += $review_score;
                        $reviewed = json_encode($reviewed,true);
                        $update_data['reviewed'] = $reviewed;
                        $update_data['score'] = $score;
                    }

                }
            }


            $this->db->where(array('id'=>$this_id, 'title'=>$title));
            if ($this->db->update('transaction_log', $update_data)) {
                $status = 'SUCCESS';
                $code = 200;
            }
        }else{
            $data = '';
            if($item_type=='business'){
                $this->db->select('name,lat,lon,type');
                $query = $this->db->get_where($item_type,array('id'=> $content_id));
                $result_data = $query->result();
                foreach ($result_data as $item){
                    $data = $item;
                }

                $cate = $data->type;
                $lat = $data->lat;
                $lon = $data->lon;
                $data = json_encode($data,true);
                if($review_id&&$review_score){
                    $score = $review_score;
                    $reviewed = array($review_id);
                    $reviewed = json_encode($reviewed,true);
                }else{
                    $reviewed = array();
                    $reviewed = json_encode($reviewed,true);
                }

            }
            $update_data = array(
                'title'=> $title,
                'type'=> $item_type,
                'data'=> $data,
                'cate'=> $cate,
                'lat'=> $lat,
                'lon'=> $lon,
                'reviewed'=> $reviewed,
                'score'=> $score,
                'content_id'=> $content_id,
                'total'=> 1,
                'created_time'=> $created_time
            );
//            echo '<pre>';
//            var_dump($update_data); die;
            if ($this->db->insert('transaction_log', $update_data)) {
                $status = 'SUCCESS';
                $code = 200;
            }
        }

        $out_put = array(
            'status' => $status,
            'code' => $code
        );
        print_json($out_put);
    }
}