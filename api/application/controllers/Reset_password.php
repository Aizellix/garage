<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reset_password extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
    }


    public function index()
	{
        $uid = $this->input->post('uid');
        $email = $this->input->post('email');
        $code = $this->input->post('code');
        $pass1 = $this->input->post('pass1');
        $pass2 = $this->input->post('pass2');
        if(($pass1!=$pass2)||!$pass1||!$pass2){
            redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=PASSWORD_NOT_MATCHED');
        }else if(!$email||!$code||!$uid){
            redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=DATA_LOST');
        }else if($uid&&$email&&$code&&($pass1==$pass2)){
            $this->reset_password($uid,$email,$code,$pass1,$pass2);
        }else{
            redirect('http://www.pueanrod.com/');
        }
	}

    public function reset_password($uid,$email,$code,$pass1,$pass2){
        $this->db->where(array('email'=>$email,'status'=>'active')); //'id'=>$uid,'request_code'=>$code,'request_expired_date >='=>date('Y-m-d H:i:s')
        $query = $this->db->get('user');
        if($query->num_rows()>0){
            $user_data = $query->result();
            $user_data = $user_data[0];
            if($user_data->id!=$uid){
                redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=USER_NOT_FOUND');
            }else if($user_data->request_code==""){
                redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=NO_REQUEST_CODE');
            }else if($user_data->request_code!=$code){
                redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=REQUEST_CODE');
            }else if(strtotime($user_data->request_expired_date)<=strtotime(date('Y-m-d H:i:s'))){
                redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=REQUEST_CODE_EXPIRED');
            }else{
                $new_password = hash("sha512", $pass1, false);
                $data = array(
                    'password' => $new_password,
                    'request_code' => '',
                    'request_expired_date' => date('Y-m-d H:i:s',strtotime("-1 day")),
                    'updated_time' => date("Y-m-d H:i:s")
                );
                $this->db->where(array('id' => $user_data->id, 'email' => $email, 'status' => 'active'));
                if($this->db->update('user', $data)){
                    redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&status=SUCCESS');
                }else{
                    redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=NO_SERVICE');
                }
            }

        }else{
            redirect('http://www.pueanrod.com/reset_password?udi='.$uid.'&code='.$code.'&error=EMAIL_NOT_FOUND');
        }
    }
}
