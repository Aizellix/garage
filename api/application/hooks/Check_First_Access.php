<?php

class Check_First_Access
{
    function initialize()
    {
        $ci =& get_instance();
        $ci->load->helper('url');
        if (!in_array(current_url(), array(site_url(), site_url('home')))) {
            $api_key = $ci->config->item('api_key');
            $depot_key = $ci->config->item('depot_key');
            if (!in_array($api_key,array($ci->input->get('api_key'),$ci->input->post('api_key')))) {
                redirect(site_url());
                die;
            }
        }
    }
}