<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Car Servicing</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.structure.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap.min.css');?>">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap-theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('fonts/stylesheet.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/style.css');?>">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo site_assets_url('js/jquery-3.1.1.min.js');?>"></script>
    <script src="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrap.min.js');?>"></script>
</head>
<body>
<div class="container">

