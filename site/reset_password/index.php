<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pueanrod.com | เพื่อนรถ</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="./assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="./assets/fonts/stylesheet.css">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" href="./assets/css/IE10fix.css">
    <link rel="stylesheet" href="./assets/css/style.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="./assets/js/jquery-3.1.1.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <div id="header">
            <div class="col-lg-12" style="background: #eb4e25; text-align: center; padding: 15px 15px;">
                <img src="assets/images/logo.png" width="100%">
            </div>
            <div class="col-lg-12" style="background: #A4361A; text-align: center; padding: 10px 15px;">
                <div style="font-size: 2.2em; color: #fff;">
                    รีเซตรหัสผ่าน - Reset Password
                </div>
            </div>
        </div>
        <div id="main" style="background: rgba(255,255,255,0.8); ">
            <?php if(isset($_GET['error'])){
                $error = $_GET['error'];
                $error_list = array(
                    'PASSWORD_NOT_MATCHED' => "Password aren't matched, รหัสผ่านไม่ตรงกัน",
                    'DATA_LOST' => 'Some data was lost, กรอกข้อมูลไม่ครบ',
                    'USER_NOT_FOUND' => "User is not found, ไม่พบผู้ใช้งาน",
                    'NO_REQUEST_CODE' => "You don't request to reset password yet, คุณยังไม่ได้ขอคำร้องรีเซตรหัสผ่าน",
                    'REQUEST_CODE' => "Request code isn't correct, โค้ดของคำร้องไม่ถูกต้อง",
                    'REQUEST_CODE_EXPIRED' => 'Request code was expired, คำร้องหมดอายุ',
                    'NO_SERVICE' => 'No service, ระบบไม่ทำงาน',
                    'EMAIL_NOT_FOUND' => 'This email is not found ,ไม่พบอีเมล์นี้'
                );
                echo '<div style="background: rgba(0,0,0,0.15); width: 100%; padding: 10px 15px; color: #ff0000; font-size: 1.4em; line-height: 1.2em; text-align: center;}">';
                echo $error_list[$error];
                echo '</div>';
            }?>
            <div class="col-lg-12" style="text-align: center; padding: 20px 35px; font-size: 1.8em; color: #555;">
                <?php if(isset($_GET['status'])) {
                    $status = $_GET['status'];
                    if ($status == "SUCCESS") {
                        echo '<div style="width: 100%; padding: 10px 15px; font-size: 1.3em; text-align: center;}">';
                        echo '<p style="margin-top: 50px;">Your password was changed already </p><p style="margin-top: 50px;"> รหัสผ่านของคุณได้ถูกต้องใหม่แล้ว</p>';
                        echo '</div>';
                    }
                }else{
                ?>
                <form action="http://api.pueanrod.com/reset_password" method="post">
                    <div class="form-group">
                        <label for="newPassword">Email</label>
                        <input name="email" type="email" class="form-control" id="newPassword2" placeholder="อีเมล์" style="height: 50px; font-size: 0.81em;">
                    </div>
                    <div class="form-group">
                        <label for="newPassword">New password</label>
                        <input name="pass1" type="password" class="form-control" id="newPassword2" placeholder="รหัสผ่านใหม่" style="height: 50px; font-size: 0.81em;">
                    </div>
                    <div class="form-group">
                        <label for="newPassword2">Confirm new password</label>
                        <input name="pass2" type="password" class="form-control" id="newPassword2" placeholder="ยืนยันรหัสผ่านใหม่" style="height: 50px; font-size: 0.81em;">
                    </div>
                    <div class="form-group" style="margin-top: 35px;">
                        <button type="reset" class="btn btn-default" style="font-size: 1em; width: 100%; background: #999; color: #fff;" >Reset</button>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-default" style="font-size: 1em; width: 100%; background: #eb4e25; color: #fff;" >Submit</button>
                    </div>
                    <input type="hidden" name="uid" value="<?php echo (isset($_GET['uid']))? $_GET['uid'] : ''; ?>">
                    <input type="hidden" name="code" value="<?php echo (isset($_GET['code']))? $_GET['code'] : ''; ?>">

                </form>
                <?php }?>
            </div>
        </div>
        <div id="footer">
            <div class="col-lg-12" style="background: #eb4e25; text-align: center; padding: 10px 10px; color: #fff;">
                <div style="font-size: 1.3rem;">
                    &copy;2017 Pueanrod.com - เพื่อนรถ | contact: <a href="mailto:contact@pueanrod.com" style="color: #fff;">contact@pueanrod.com</a>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var doc_height = 0;
    var header_height = 0;
    var footer_height = 0;
    $(document).ready(function () {
        doc_height = $(window).height();
        header_height = $("#header").height();
        footer_height = $("#footer").height();
        $("#main").height(doc_height-(header_height+footer_height));
    })
</script>
</body>

</html>