<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pueanrod.com | เพื่อนรถ</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap.min.css'); ?>">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap-theme.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('fonts/stylesheet.css'); ?>">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/IE10fix.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/style.css'); ?>">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo site_assets_url('js/jquery-3.1.1.min.js'); ?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrap.min.js'); ?>"></script>
</head>
<body>
