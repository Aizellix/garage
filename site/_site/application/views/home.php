<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">เพื่อนรถ.คอม</h3>
                    <nav>
                        <ul class="nav masthead-nav">
                            <li class="active"><a href="#">หน้าแรก</a></li>
                            <li><a href="#">ดาวน์โหลด</a></li>
                            <li><a href="#">เกี่ยวกับ</a></li>
                            <li><a href="#">ติดต่อ</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="inner cover">
                <h1 class="cover-heading">Pueanrod | เพื่อนรถ</h1>
                <p class="lead">คุณเคยเจอปัญหารถเสียระหว่างการเดินทางหรือไม่? <br/>คุณเคยมองหาอู่รถยนต์ หรือ ศูยน์บริการ แต่ก็ไม่รู้ว่าอยู่ที่ไหนหรือไม่? <br/>เรามีทางออกให้คุณ เพื่อนรถ เพื่อนสำหรับนักเดินทาง แอพพลิเคชั่นรวมข้อมูลอู่รถยนต์และศูนย์บริการทั่วประเทศ ที่มีเครือข่ายครอบคุมที่สุด เพื่อความอุ่นใจในการเดินทาง ดาวน์โหลดแอพพลิเคชั่น เพื่อนรถ ไว้ติดมือืถอทุกคั้งก่อนเดินทาง.</p>
                <p class="lead">
                    <a href="#" class="btn btn-lg btn-default">ดาวน์โหลด</a>
                </p>
            </div>

            <div class="mastfoot">
                <div class="inner">
                    <p><a href="http://pueanrod.com">Pueanrod.com</a>, by <a href="https://twitter.com/pueanrod">@pueanrod</a>.</p>
                </div>
            </div>

        </div>

    </div>

</div>
