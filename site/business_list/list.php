<?php
$user_id = (isset($_GET['uid']))?$_GET['uid']:false;
if($user_id!==false){
    $api_key = urlencode('API*ZEKRET.PueanRod$grnjvoi5#');
    $qry_str = "user_id=$user_id&api_key=$api_key";
    $ch = curl_init();
    if(strpos($_SERVER['HTTP_HOST'],'garage.test')===false)
    {
        $api_url = "http://api.pueanrod.com/business/get_user_business_list";
    }
    else
    {
        $api_url = "http://api.garage.test/business/get_user_business_list";
    }
    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 3);

// Set request method to POST
    curl_setopt($ch, CURLOPT_POST, 1);

// Set query data here with CURLOPT_POSTFIELDS
    curl_setopt($ch, CURLOPT_POSTFIELDS, $qry_str);

    $content = trim(curl_exec($ch));
    curl_close($ch);
    $content = json_decode($content);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pueanrod.com | เพื่อนรถ</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="./assets/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="./assets/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="./assets/fonts/stylesheet.css">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" href="./assets/css/IE10fix.css">
    <link rel="stylesheet" href="./assets/css/style.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="./assets/js/jquery-3.1.1.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row text-center" style="background: rgba(255,255,255,0.5);">
        <h3 style="color: #eb4e25;">รายชื่อสถานประกอบการที่รอการยืนยันของคุณ</h3>
        <table class="table table-bordered" style="background: #fff;">
            <thead>
            <tr>
                <th>วันที่เพิ่มเข้ามา</th>
                <th>ชื่อสถานประกอบการ</th>
                <th style="text-align: center;">สถานะ</th>
            </tr>
            </thead>
            <tbody>
            <?php if($content->total>0){
                $status_list = array(
                        'active' => 'ยืนยันแล้ว',
                        'vip' => 'ยืนยันแล้ว',
                        'pending' => 'รอการยืนยัน',
                        'deleted' => 'ถูกยกเลิก'
                );
                foreach ($content->list as $item){
            ?>
            <tr>
                <td style="width: 90px; text-align: left;"><?php echo date("Y-m-d", strtotime($item->created_time)).'<br/>'.date("H:i:s", strtotime($item->created_time));?></td>
                <td style=" text-align: left;"><?php echo $item->name;?></td>
                <td style="width: 80px; text-align: center">
                    <strong class="text-capitalize status_<?php echo $item->status;?>"><?php echo $status_list[$item->status];?></strong>
                </td>
            </tr>
            <?php } }else{?>
            <tr>
                <td style="text-align: center;" colspan="3">ไม่พบข้อมูล</td>
            </tr>
            <?php }?>
            </tbody>
        </table>

    </div>
</div>
<style>
    .text-capitalize.status_active,.text-capitalize.status_vip{
        color: green;
    }
    .text-capitalize.status_pending{
        color: silver;
    }
    .text-capitalize.status_deleted{
        color: red;
    }
</style>
</body>

</html>