-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 09, 2017 at 03:25 AM
-- Server version: 5.7.18-log
-- PHP Version: 5.6.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garage`
--

-- --------------------------------------------------------

--
-- Table structure for table `advertise`
--

CREATE TABLE `advertise` (
  `id` int(11) NOT NULL,
  `business_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `cover_image_id` int(11) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT NULL,
  `viewed` int(11) UNSIGNED DEFAULT '0',
  `status` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `advertise`
--

INSERT INTO `advertise` (`id`, `business_id`, `start_time`, `end_time`, `cover_image_id`, `cover_image_src`, `viewed`, `status`, `created_time`, `updated_time`) VALUES
(1, 1, '2017-05-01 00:00:00', '2017-07-06 00:00:00', 269, 'http://depot.garage.dev/images/article_1_20170226032332.jpg', 5, 'active', '2017-03-01 22:56:24', '2017-07-05 06:08:59'),
(2, 4, '2017-03-01 00:00:00', '2017-04-30 00:00:00', 271, 'http://depot.garage.dev/images/article_2_20170301224808.jpg', 0, 'active', '2017-03-01 22:55:44', '2017-04-27 22:05:32'),
(3, 4, '2017-03-01 00:00:00', '2017-03-31 00:00:00', 272, 'http://depot.garage.dev/images/article_3_20170301225431.jpg', 1, 'active', '2017-03-01 22:56:18', '2017-03-01 22:56:18'),
(4, 4, '2017-03-03 00:43:43', '2017-03-03 00:43:43', 273, 'http://depot.garage.dev/images/article_4_20170303004343.jpg', 0, 'active', '2017-03-03 00:43:43', '2017-03-03 00:43:43'),
(5, 4, '2017-03-03 00:00:00', '2017-03-31 00:00:00', 274, 'http://depot.garage.dev/images/article_5_20170303005729.jpg', 0, 'active', '2017-03-03 00:59:03', '2017-03-03 00:59:03'),
(6, 4, '2017-03-03 00:00:00', '2017-03-31 00:00:00', 276, 'http://depot.garage.dev/images/article_6_20170303010224.jpg', 0, 'active', '2017-03-03 01:02:24', '2017-03-03 01:02:24'),
(7, 4, '2017-03-03 01:02:46', '2017-03-03 01:02:46', 277, 'http://depot.garage.dev/images/article_7_20170303010246.jpg', 0, 'active', '2017-03-03 01:02:46', '2017-03-03 01:02:46'),
(8, 4, '2017-03-03 01:06:00', '2017-03-03 01:06:00', 278, 'http://depot.garage.dev/images/article_8_20170303010600.jpg', 0, 'active', '2017-03-03 01:06:00', '2017-03-03 01:06:00'),
(9, 4, '2017-03-03 01:06:55', '2017-03-03 01:06:55', 279, 'http://depot.garage.dev/images/article_9_20170303010655.jpg', 0, 'active', '2017-03-03 01:06:55', '2017-03-03 01:06:55'),
(10, 4, '2017-03-03 01:08:36', '2017-03-03 01:08:36', 280, 'http://depot.garage.dev/images/article_10_20170303010836.jpg', 0, 'deleted', '2017-03-03 01:08:36', '2017-03-03 01:08:36'),
(11, 4, '2017-03-01 00:00:00', '2017-03-30 00:00:00', 281, 'http://depot.garage.dev/images/article_11_20170303011258.jpg', 0, 'deleted', '2017-03-03 01:00:01', '2017-03-03 01:12:58');

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` text,
  `cover_image_id` int(11) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT '',
  `detail` text,
  `notice` text,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `business_list` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `viewed` int(11) UNSIGNED DEFAULT '0',
  `liked` int(11) UNSIGNED DEFAULT '0',
  `shared` int(11) UNSIGNED DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `cover_image_id`, `cover_image_src`, `detail`, `notice`, `start_time`, `end_time`, `business_list`, `created_time`, `updated_time`, `status`, `viewed`, `liked`, `shared`) VALUES
(1, 'Test Test', 261, 'http://depot.garage.dev/images/article_1_20161231002024.jpg', '<b>wewetetwet Test</b><br>', NULL, '2017-02-01 00:00:00', '2017-04-30 00:00:00', '', '2017-04-27 21:31:50', '2017-04-27 21:31:50', 'active', 1, 0, 0),
(2, '', NULL, NULL, '<br>', NULL, '2017-02-25 13:05:42', '2017-02-25 13:05:42', NULL, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', 0, 0, 0),
(3, '', NULL, NULL, '<br>', NULL, '2017-02-25 13:05:44', '2017-02-25 13:05:44', NULL, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', 0, 0, 0),
(4, '', NULL, NULL, '<br>', NULL, '2017-02-25 13:05:47', '2017-02-25 13:05:47', NULL, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', 0, 0, 0),
(5, '', NULL, NULL, '<br>', NULL, '2017-02-25 13:05:52', '2017-02-25 13:05:52', NULL, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', 0, 0, 0),
(6, 'Test 2017', 262, 'http://depot.garage.dev/images/article_6_20170118031416.jpg', 'Test 2017<br>', NULL, '2017-02-01 00:00:00', '2017-04-30 00:00:00', '1,4,', '2017-04-27 21:31:42', '2017-04-27 21:31:42', 'active', 0, 0, 0),
(7, 'TEST_1 2017', 263, 'http://depot.garage.dev/images/article_7_20170211223803.jpg', 'TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST <br>', NULL, '2017-02-11 00:00:00', '2017-04-30 00:00:00', '1,4,', '2017-04-27 21:31:34', '2017-04-27 21:31:34', 'active', 0, 0, 0),
(8, 'Test', 284, 'http://depot.garage.dev/images/article_8_20170305171445.jpg', 'test', NULL, '2017-03-01 00:00:00', '2017-04-30 00:00:00', '4,', '2017-04-27 21:31:25', '2017-04-27 21:31:25', 'active', 0, 0, 0),
(9, 'Test', 297, 'http://depot.garage.dev/images/article_9_20170427213225.jpg', 'Test', NULL, '2017-04-01 00:00:00', '2017-05-31 00:00:00', '6,', '2017-07-05 06:55:28', '2017-07-05 06:55:28', 'active', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  `address_no` varchar(11) DEFAULT NULL,
  `address_moo` varchar(255) DEFAULT NULL,
  `address_village` varchar(255) DEFAULT NULL,
  `address_soi` varchar(255) DEFAULT NULL,
  `address_road` varchar(255) DEFAULT NULL,
  `address_tambol` varchar(255) DEFAULT NULL,
  `address_district` varchar(255) DEFAULT NULL,
  `address_province` varchar(255) DEFAULT NULL,
  `address_postcode` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_code` varchar(3) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `line` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cover_image_id` varchar(255) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT NULL,
  `work_day_sun` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_day_mon` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_day_tue` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_day_wed` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_day_thu` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_day_fri` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_day_sat` tinyint(1) UNSIGNED ZEROFILL DEFAULT NULL,
  `work_time_start` varchar(255) DEFAULT '00:00:00',
  `work_time_close` varchar(255) DEFAULT '00:00:00',
  `work_time_start_2` varchar(255) DEFAULT '00:00:00',
  `work_time_close_2` varchar(255) DEFAULT '00:00:00',
  `work_time_remark` varchar(255) DEFAULT NULL,
  `more_work_time` int(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `detail` text,
  `slogan` varchar(200) NOT NULL,
  `facilities` varchar(255) DEFAULT NULL,
  `payment` varchar(255) DEFAULT NULL,
  `award_certification` varchar(255) DEFAULT NULL,
  `privilege` varchar(255) DEFAULT NULL,
  `score` int(11) UNSIGNED DEFAULT '0',
  `total_score` int(11) NOT NULL DEFAULT '0',
  `viewed` int(11) UNSIGNED DEFAULT '0',
  `liked` int(11) UNSIGNED DEFAULT '0',
  `reviewed` int(11) UNSIGNED DEFAULT '0',
  `shared` int(11) UNSIGNED DEFAULT '0',
  `promoted_time_start` datetime DEFAULT NULL,
  `promoted_time_end` datetime DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_user_type` varchar(255) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `created_user_name` varchar(255) DEFAULT NULL,
  `status` varchar(15) DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `business`
--

INSERT INTO `business` (`id`, `name`, `owner_name`, `address_no`, `address_moo`, `address_village`, `address_soi`, `address_road`, `address_tambol`, `address_district`, `address_province`, `address_postcode`, `lat`, `lon`, `country`, `country_code`, `tel`, `facebook`, `line`, `email`, `cover_image_id`, `cover_image_src`, `work_day_sun`, `work_day_mon`, `work_day_tue`, `work_day_wed`, `work_day_thu`, `work_day_fri`, `work_day_sat`, `work_time_start`, `work_time_close`, `work_time_start_2`, `work_time_close_2`, `work_time_remark`, `more_work_time`, `type`, `service_type`, `detail`, `slogan`, `facilities`, `payment`, `award_certification`, `privilege`, `score`, `total_score`, `viewed`, `liked`, `reviewed`, `shared`, `promoted_time_start`, `promoted_time_end`, `created_time`, `updated_time`, `created_user_type`, `created_user_id`, `created_user_name`, `status`) VALUES
(1, 'Test 12345', 'A B', '', '', '', '', 'อังรีดูนังค์', 'ปทุมวัน', 'ปทุมวัน', 'กรุงเทพมหานคร', '10330', 13.731235783576345, 100.53398532450865, 'ประเทศไทย', 'TH', '021234567', 'facebook.com/test', 'test2', 'test@tes.com', '20', 'http://depot.garage.dev/images/business_1_20161122132905.jpg', 0, 1, 1, 1, 1, 1, 1, '09:00', '00:00', '10:30', '00:00', '', 1, '8', '["10","11","12","13"]', 'test test test testsetset', '', '["1","2","3"]', '["cash","credit"]', '', '', 0, 0, 7, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-06-06 03:48:54', '2017-06-06 03:48:54', '0', 0, 'FirstGathering', 'vip'),
(2, 'abc 123', 'A B', '', '', '', '', 'ถนน เพลินจิต', 'ลุมพินี', 'ปทุมวัน', 'กรุงเทพมหานคร', '10330', 13.7236482960288, 100.52958650172422, 'ประเทศไทย', 'TH', '021234567', 'facebook.com/test', 'test', 'test@tes.com', '219', 'http://depot.garage.dev/images/business_2_20161123032858.jpg', 0, 1, 1, 1, 1, 1, 1, '09:00', '00:00', '10:30', '00:00', '', 1, '9', '["10","11"]', 'test test test testsetset', '', '["1","2","3"]', '["cash","credit"]', '', '', 5, 0, 0, 0, 1, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-06-06 05:16:59', '2017-06-06 05:16:59', '0', 0, 'FirstGathering', 'pending'),
(3, 'ASDXC', 'asdasdasd', '', '', '', '', 'รัชดาภิเษก', 'วัดท่าพระ', 'บางกอกใหญ่', 'กรุงเทพมหานคร', '10600', 13.729703714522532, 100.53672117770384, 'ประเทศไทย', 'TH', '02-123-4567', '', '', '', '224', 'http://depot.garage.dev/images/business_3_20161129005058.jpg', 1, 1, 1, 0, 0, 0, 0, '00:00', '24:00', '00:00', '24:00', '', 0, '8', '["10","11","12"]', '', '', '["1","2"]', '["cash"]', '', '', 5, 0, 1, 0, 1, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-05-21 20:52:31', '2017-05-21 20:52:31', '0', 0, 'FirstGathering', 'active'),
(4, 'TEST TEST', 'TEST TEST', '', '', '', '', 'ซอย สีลม 3', 'สีลม', 'บางรัก', 'กรุงเทพมหานคร', '10500', 13.7259203990867, 100.53099197924803, 'ประเทศไทย', 'TH', '02-123-4567', '', '', '', '255', 'http://depot.garage.dev/images/business_4_20161230201815.jpg', 0, 1, 1, 1, 1, 1, 0, '08:30', '17:00', '00:00', '24:00', '', 0, '8', '["10","11"]', '', '', '["1","2","3"]', '["cash"]', '', '', 0, 0, 25, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-05-06 02:35:50', '2017-05-06 02:35:50', '0', 0, 'FirstGathering', 'active'),
(5, 'Test', 'test', '', '', '', '', 'ริมทางรถไฟ', 'บางยี่เรือ', 'ธนบุรี', 'กรุงเทพมหานคร', '10600', 13.723419000000007, 100.48850578845213, 'ประเทศไทย', 'TH', '021234567', '', '', '', '285', 'http://depot.garage.dev/images/business_5_20170423225424.jpg', 0, 0, 0, 0, 0, 0, 0, '00:00', '00:00', '00:00', '00:00', '', 0, '8', '["10"]', '', '', '["1"]', '["cash"]', '', '', 0, 0, 0, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-04-27 03:42:08', '2017-04-27 03:42:08', '0', 0, 'FirstGathering', 'pending'),
(6, 'Test2222', 'test', '', '', '', '', 'ริมทางรถไฟ', 'บางยี่เรือ', 'ธนบุรี', 'กรุงเทพมหานคร', '10600', 13.723419000000007, 100.48850578845213, 'ประเทศไทย', 'TH', '021234567', '', '', '', '286', 'http://depot.garage.dev/images/business_6_20170423225742.jpg', 1, 1, 0, 0, 0, 0, 0, '00:00', '00:00', '00:00', '00:00', '', 0, '8', '["10"]', '', '', '["1"]', '["cash"]', '', '', 0, 0, 0, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-04-27 03:37:48', '2017-04-27 03:37:48', '0', 0, 'FirstGathering', 'active'),
(7, 'aize', 'test', '', '', '', '', 'ถนน รัชดาภิเษก', 'วัดท่าพระ', 'บางกอกใหญ่', 'กรุงเทพมหานคร', '10600', 13.723419, 100.47623199999998, 'ประเทศไทย', 'TH', '0987654321', '', '', '', '298', 'http://depot.garage.dev/images/business_7_20170522232714.jpg', 1, 1, 1, 0, 0, 0, 0, '03:30', '18:00', '00:00', '24:00', '', 0, '9', '["10","11","12"]', '', '', '["1","2","3"]', '["cash"]', '', '', 0, 0, 0, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-05-22 23:27:14', '2017-05-22 23:27:14', '0', 0, 'FirstGathering', 'vip'),
(8, 'Rwar', 'Aizr', '', '', '', '', 'ถนน รัชดาภิเษก', 'วัดท่าพระ', 'บางกอกใหญ่', 'กรุงเทพมหานคร', '10600', 13.723419, 100.47623199999998, 'ประเทศไทย', 'TH', '0987654321', '', '', '', '299', 'http://depot.garage.dev/images/business_8_20170530223028.jpg', 0, 1, 1, 1, 1, 0, 0, '04:00', '19:30', '00:00', '24:00', '', 0, '8', '["10","11"]', '', '', '["1","2","3","4"]', '["cash"]', '', '', 0, 0, 0, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-05-30 22:30:28', '2017-05-30 22:30:28', '0', 0, 'FirstGathering', 'vip'),
(9, 'asdasd', 'asdasd', '', '', '', '', 'ถนน รัชดาภิเษก', 'วัดท่าพระ', 'บางกอกใหญ่', 'กรุงเทพมหานคร', '10600', 13.723419, 100.47623199999998, 'ประเทศไทย', 'TH', '0987654321', '', '', '', '300', 'http://depot.garage.dev/images/business_9_20170615001722.jpg', 0, 0, 0, 0, 0, 0, 0, '20:00', '04:30', '10:19', '19:57', '', 1, '8', '["10","11"]', '', 'asdasda', '["1","2"]', '["cash"]', '', '', 0, 0, 0, 0, 0, 0, '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-08-08 23:15:54', '2017-08-08 23:15:54', '0', 0, 'FirstGathering', 'vip');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `title`, `type`, `created_date`, `updated_date`, `status`) VALUES
(1, 'ห้องรับรอง', '3', '2016-11-03 03:15:59', '2016-11-03 03:15:59', 'active'),
(2, 'ร้านกาแฟ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active'),
(3, 'Wifi', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active'),
(4, 'บริการรับ - ส่งรถ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active'),
(5, 'ที่จอดรถ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active'),
(6, 'นำสัตว์เลี้ยงเข้าได้', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active'),
(7, 'รับจองคิว ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active'),
(8, 'อู่ซ่อม', '1', '2016-11-03 03:16:34', '2016-11-03 03:16:34', 'active'),
(9, 'ศูนย์บริการ', '1', '2016-11-03 03:16:34', '2016-11-03 03:16:34', 'active'),
(10, 'ซ่อม', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active'),
(11, 'เปลี่ยนอะไหล่', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active'),
(12, 'ทำสี', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active'),
(13, 'ล้างรถ', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active'),
(14, 'ครบวงจร', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active'),
(15, 'test', '1', '2016-11-03 04:47:39', '2016-11-03 04:47:39', 'deleted'),
(16, 'test2', '1', '2016-11-03 20:57:57', '2016-11-03 20:57:57', 'deleted'),
(17, 'test', '3', '2017-08-09 00:24:25', '2017-08-09 00:24:25', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `emergency`
--

CREATE TABLE `emergency` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'emergency',
  `order_number` int(11) NOT NULL DEFAULT '999',
  `status` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `emergency`
--

INSERT INTO `emergency` (`id`, `title`, `tel`, `type`, `order_number`, `status`, `created_time`, `updated_time`) VALUES
(1, 'ก สายด่วนตำรวจ', '191', 'emergency', 2, 'active', '2017-06-06 06:12:20', '2017-06-06 06:12:20'),
(2, 'ข สายด่วนดับเพลิง', '199', 'insurance', 2, 'active', '2017-08-08 23:43:34', '2017-08-08 23:43:34'),
(3, 'Test', '0987654321', 'insurance', 1, 'active', '2017-08-08 23:43:09', '2017-08-08 23:43:09');

-- --------------------------------------------------------

--
-- Table structure for table `history_log`
--

CREATE TABLE `history_log` (
  `id` int(11) NOT NULL,
  `content_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(20) NOT NULL,
  `type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `history_log`
--

INSERT INTO `history_log` (`id`, `content_id`, `user_id`, `user_name`, `created_time`, `status`, `type`) VALUES
(1, 1, 1, 'Aize', '2017-07-05 06:09:00', 'edit', 'advertise'),
(2, 3, 1, 'Aize', '2017-07-05 06:47:24', 'edit', 'promotion'),
(3, 9, 1, 'Aize', '2017-08-08 23:15:54', 'edit', 'article'),
(6, 9, 1, 'Aize', '2017-08-08 23:15:54', 'edit', 'business');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'active',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `type`, `ref_id`, `src`, `name`, `status`, `created_time`, `updated_time`) VALUES
(1, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103171814.jpg', 'business_11_20161103171814.jpg', 'active', NULL, NULL),
(2, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103171815.jpg', 'business_11_20161103171815.jpg', 'active', NULL, NULL),
(3, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103172055.jpg', 'business_11_20161103172055.jpg', 'active', NULL, NULL),
(4, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103172055.jpg', 'business_11_20161103172055.jpg', 'active', NULL, NULL),
(5, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103172424.jpg', 'business_11_20161103172424.jpg', 'active', NULL, NULL),
(6, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103172424.jpg', 'business_11_20161103172424.jpg', 'active', NULL, NULL),
(7, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103173006.jpg', 'business_11_20161103173006.jpg', 'active', NULL, NULL),
(8, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103173006.jpg', 'business_11_20161103173006.jpg', 'active', NULL, NULL),
(9, 'business', 11, 'http://depot.garage.dev/images/business_11_20161103174712.jpg', 'business_11_20161103174712.jpg', 'active', NULL, NULL),
(10, 'business', 13, 'http://depot.garage.dev/images/business_13_20161113000406.jpg', 'business_13_20161113000406.jpg', 'active', NULL, NULL),
(11, 'business', 13, 'http://depot.garage.dev/images/business_13_20161113000406.jpg', 'business_13_20161113000406.jpg', 'active', NULL, NULL),
(12, 'business', 1, 'http://depot.garage.dev/images/business_1_20161113002703.jpg', 'business_1_20161113002703.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08'),
(13, 'business', 1, 'http://depot.garage.dev/images/business_1_20161113002703.jpg', 'business_1_20161113002703.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08'),
(14, 'business', 2, 'http://depot.garage.dev/images/business_2_20161114232545.jpg', 'business_2_20161114232545.jpg', 'active', NULL, NULL),
(15, 'business', 2, 'http://depot.garage.dev/images/business_2_20161114232546.jpg', 'business_2_20161114232546.jpg', 'active', NULL, NULL),
(16, 'business', 1, 'http://depot.garage.dev/images/business_1_20161122132629.png', 'business_1_20161122132629.png', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08'),
(17, 'business', 1, 'http://depot.garage.dev/images/business_1_20161122132727.png', 'business_1_20161122132727.png', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08'),
(18, 'business', 1, 'http://depot.garage.dev/images/business_1_20161122132814.jpg', 'business_1_20161122132814.jpg', 'deleted', '2016-12-10 04:13:18', '2016-12-10 04:13:18'),
(19, 'business', 1, 'http://depot.garage.dev/images/business_1_20161122132814.jpg', 'business_1_20161122132814.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08'),
(20, 'business', 1, 'http://depot.garage.dev/images/business_1_20161122132905.jpg', 'business_1_20161122132905.jpg', 'active', NULL, NULL),
(21, 'business', 1, 'http://depot.garage.dev/images/business_1_20161122135241.jpg', 'business_1_20161122135241.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08'),
(186, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032124.jpg', 'business_2_20161123032124.jpg', 'active', '2016-11-23 03:21:24', '2016-11-23 03:21:24'),
(187, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032125.jpg', 'business_2_20161123032125.jpg', 'active', '2016-11-23 03:21:25', '2016-11-23 03:21:25'),
(188, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032127.jpg', 'business_2_20161123032127.jpg', 'active', '2016-11-23 03:21:27', '2016-11-23 03:21:27'),
(189, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032128.jpg', 'business_2_20161123032128.jpg', 'active', '2016-11-23 03:21:28', '2016-11-23 03:21:28'),
(190, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032129.jpg', 'business_2_20161123032129.jpg', 'active', '2016-11-23 03:21:29', '2016-11-23 03:21:29'),
(191, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032130.jpg', 'business_2_20161123032130.jpg', 'active', '2016-11-23 03:21:30', '2016-11-23 03:21:30'),
(192, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032132.jpg', 'business_2_20161123032132.jpg', 'active', '2016-11-23 03:21:32', '2016-11-23 03:21:32'),
(193, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032133.jpg', 'business_2_20161123032133.jpg', 'active', '2016-11-23 03:21:33', '2016-11-23 03:21:33'),
(194, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032134.jpg', 'business_2_20161123032134.jpg', 'active', '2016-11-23 03:21:34', '2016-11-23 03:21:34'),
(195, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032136.jpg', 'business_2_20161123032136.jpg', 'active', '2016-11-23 03:21:36', '2016-11-23 03:21:36'),
(196, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032137.jpg', 'business_2_20161123032137.jpg', 'active', '2016-11-23 03:21:37', '2016-11-23 03:21:37'),
(197, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032138.jpg', 'business_2_20161123032138.jpg', 'active', '2016-11-23 03:21:38', '2016-11-23 03:21:38'),
(198, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032139.jpg', 'business_2_20161123032139.jpg', 'active', '2016-11-23 03:21:39', '2016-11-23 03:21:39'),
(199, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032141.jpg', 'business_2_20161123032141.jpg', 'active', '2016-11-23 03:21:41', '2016-11-23 03:21:41'),
(200, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032142.jpg', 'business_2_20161123032142.jpg', 'active', '2016-11-23 03:21:42', '2016-11-23 03:21:42'),
(201, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032143.jpg', 'business_2_20161123032143.jpg', 'active', '2016-11-23 03:21:43', '2016-11-23 03:21:43'),
(202, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032145.jpg', 'business_2_20161123032145.jpg', 'active', '2016-11-23 03:21:45', '2016-11-23 03:21:45'),
(203, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032837.jpg', 'business_2_20161123032837.jpg', 'active', '2016-11-23 03:28:37', '2016-11-23 03:28:37'),
(204, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032839.jpg', 'business_2_20161123032839.jpg', 'active', '2016-11-23 03:28:39', '2016-11-23 03:28:39'),
(205, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032840.jpg', 'business_2_20161123032840.jpg', 'active', '2016-11-23 03:28:40', '2016-11-23 03:28:40'),
(206, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032841.jpg', 'business_2_20161123032841.jpg', 'active', '2016-11-23 03:28:41', '2016-11-23 03:28:41'),
(207, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032842.jpg', 'business_2_20161123032842.jpg', 'active', '2016-11-23 03:28:42', '2016-11-23 03:28:42'),
(208, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032844.jpg', 'business_2_20161123032844.jpg', 'active', '2016-11-23 03:28:44', '2016-11-23 03:28:44'),
(209, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032845.jpg', 'business_2_20161123032845.jpg', 'active', '2016-11-23 03:28:45', '2016-11-23 03:28:45'),
(210, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032846.jpg', 'business_2_20161123032846.jpg', 'active', '2016-11-23 03:28:46', '2016-11-23 03:28:46'),
(211, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032848.jpg', 'business_2_20161123032848.jpg', 'active', '2016-11-23 03:28:48', '2016-11-23 03:28:48'),
(212, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032849.jpg', 'business_2_20161123032849.jpg', 'active', '2016-11-23 03:28:49', '2016-11-23 03:28:49'),
(213, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032850.jpg', 'business_2_20161123032850.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(214, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032851.jpg', 'business_2_20161123032851.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(215, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032852.jpg', 'business_2_20161123032852.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(216, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032854.jpg', 'business_2_20161123032854.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(217, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032855.jpg', 'business_2_20161123032855.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(218, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032856.jpg', 'business_2_20161123032856.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(219, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032858.jpg', 'business_2_20161123032858.jpg', 'active', '2016-11-23 03:28:58', '2016-11-23 03:28:58'),
(220, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032859.jpg', 'business_2_20161123032859.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(221, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032900.jpg', 'business_2_20161123032900.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(222, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032902.jpg', 'business_2_20161123032902.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(223, 'business', 2, 'http://depot.garage.dev/images/business_2_20161123032903.jpg', 'business_2_20161123032903.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(224, 'business', 3, 'http://depot.garage.dev/images/business_3_20161129005058.jpg', 'business_3_20161129005058.jpg', 'active', '2016-11-29 00:50:58', '2016-11-29 00:50:58'),
(225, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022318.jpg', 'business_2_20161129022318.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(226, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022319.jpg', 'business_2_20161129022319.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14'),
(227, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022320.jpg', 'business_2_20161129022320.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(228, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022322.jpg', 'business_2_20161129022322.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(229, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022323.jpg', 'business_2_20161129022323.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(230, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022324.jpg', 'business_2_20161129022324.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(231, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022325.jpg', 'business_2_20161129022325.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(232, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022327.jpg', 'business_2_20161129022327.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(233, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022328.jpg', 'business_2_20161129022328.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(234, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022329.jpg', 'business_2_20161129022329.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(235, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129022331.jpg', 'business_2_20161129022331.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(236, 'business', 2, 'http://depot.garage.dev/images/business_2_20161129023951.jpg', 'business_2_20161129023951.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15'),
(237, 'business', 1, 'http://depot.garage.dev/images/business_1_20161204224334.jpg', 'business_1_20161204224334.jpg', 'deleted', '2016-12-10 04:13:29', '2016-12-10 04:13:29'),
(238, 'business', 1, 'http://depot.garage.dev/images/business_1_20161204224357.jpg', 'business_1_20161204224357.jpg', 'deleted', '2016-12-10 04:13:29', '2016-12-10 04:13:29'),
(239, 'business', 1, 'http://depot.garage.dev/images/business_1_20161204224523.jpg', 'business_1_20161204224523.jpg', 'deleted', '2016-12-10 04:13:39', '2016-12-10 04:13:39'),
(240, 'business', 1, 'http://depot.garage.dev/images/business_1_20161204224814.jpg', 'business_1_20161204224814.jpg', 'deleted', '2016-12-10 04:13:39', '2016-12-10 04:13:39'),
(241, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210041418.jpg', 'business_1_20161210041418.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(242, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210042721.jpg', 'business_1_20161210042721.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(243, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210042723.jpg', 'business_1_20161210042723.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(244, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210042725.jpg', 'business_1_20161210042725.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(245, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210042726.jpg', 'business_1_20161210042726.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(246, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210043411.png', 'business_1_20161210043411.png', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(247, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210043554.png', 'business_1_20161210043554.png', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29'),
(248, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210043643.jpg', 'business_1_20161210043643.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47'),
(249, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210043645.jpg', 'business_1_20161210043645.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47'),
(250, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210044102.jpg', 'business_1_20161210044102.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47'),
(251, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210044138.jpg', 'business_1_20161210044138.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47'),
(252, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210045534.jpg', 'business_1_20161210045534.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47'),
(253, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210045620.jpg', 'business_1_20161210045620.jpg', 'active', '2016-12-10 04:56:20', '2016-12-10 04:56:20'),
(254, 'business', 1, 'http://depot.garage.dev/images/business_1_20161210045622.jpg', 'business_1_20161210045622.jpg', 'active', '2016-12-10 04:56:22', '2016-12-10 04:56:22'),
(255, 'business', 4, 'http://depot.garage.dev/images/business_4_20161230201815.jpg', 'business_4_20161230201815.jpg', 'active', '2016-12-30 20:18:15', '2016-12-30 20:18:15'),
(256, 'article', 9, 'http://depot.garage.dev/images/article_9_20161230225501.jpg', 'article_9_20161230225501.jpg', 'active', '2016-12-30 22:55:01', '2016-12-30 22:55:01'),
(257, 'article', 10, 'http://depot.garage.dev/images/article_10_20161230225620.jpg', 'article_10_20161230225620.jpg', 'active', '2016-12-30 22:56:20', '2016-12-30 22:56:20'),
(258, 'article', 1, 'http://depot.garage.dev/images/article_1_20161230225702.jpg', 'article_1_20161230225702.jpg', 'active', '2016-12-30 22:57:02', '2016-12-30 22:57:02'),
(259, 'article', 1, 'http://depot.garage.dev/images/article_1_20161230225803.jpg', 'article_1_20161230225803.jpg', 'active', '2016-12-30 22:58:03', '2016-12-30 22:58:03'),
(260, 'article', 1, 'http://depot.garage.dev/images/article_1_20161230231048.jpg', 'article_1_20161230231048.jpg', 'deleted', '2016-12-31 00:20:24', '2016-12-31 00:20:24'),
(261, 'article', 1, 'http://depot.garage.dev/images/article_1_20161231002024.jpg', 'article_1_20161231002024.jpg', 'active', '2016-12-31 00:20:24', '2016-12-31 00:20:24'),
(262, 'article', 6, 'http://depot.garage.dev/images/article_6_20170118031416.jpg', 'article_6_20170118031416.jpg', 'active', '2017-01-18 03:14:16', '2017-01-18 03:14:16'),
(263, 'article', 7, 'http://depot.garage.dev/images/article_7_20170211223803.jpg', 'article_7_20170211223803.jpg', 'active', '2017-02-11 22:38:03', '2017-02-11 22:38:03'),
(264, 'promotion', 1, 'http://depot.garage.dev/images/promotion_1_20170225125553.jpg', 'promotion_1_20170225125553.jpg', 'deleted', '2017-03-01 22:26:01', '2017-03-01 22:26:01'),
(265, 'test', 1, 'http://depot.garage.dev/images/test_1_20170226014816.jpg', 'test_1_20170226014816.jpg', 'active', '2017-02-26 01:48:16', '2017-02-26 01:48:16'),
(266, 'article', 5, 'http://depot.garage.dev/images/article_5_20170226025741.jpg', 'article_5_20170226025741.jpg', 'active', '2017-02-26 02:57:41', '2017-02-26 02:57:41'),
(267, 'article', 6, 'http://depot.garage.dev/images/article_6_20170226025836.jpg', 'article_6_20170226025836.jpg', 'active', '2017-02-26 02:58:36', '2017-02-26 02:58:36'),
(268, 'article', 1, 'http://depot.garage.dev/images/article_1_20170226025928.jpg', 'article_1_20170226025928.jpg', 'active', '2017-02-26 02:59:28', '2017-02-26 02:59:28'),
(269, 'article', 1, 'http://depot.garage.dev/images/article_1_20170226032332.jpg', 'article_1_20170226032332.jpg', 'active', '2017-02-26 03:23:32', '2017-02-26 03:23:32'),
(270, 'promotion', 1, 'http://depot.garage.dev/images/promotion_1_20170301222601.jpg', 'promotion_1_20170301222601.jpg', 'active', '2017-03-01 22:26:01', '2017-03-01 22:26:01'),
(271, 'article', 2, 'http://depot.garage.dev/images/article_2_20170301224808.jpg', 'article_2_20170301224808.jpg', 'active', '2017-03-01 22:48:08', '2017-03-01 22:48:08'),
(272, 'article', 3, 'http://depot.garage.dev/images/article_3_20170301225431.jpg', 'article_3_20170301225431.jpg', 'active', '2017-03-01 22:54:31', '2017-03-01 22:54:31'),
(273, 'article', 4, 'http://depot.garage.dev/images/article_4_20170303004343.jpg', 'article_4_20170303004343.jpg', 'active', '2017-03-03 00:43:43', '2017-03-03 00:43:43'),
(274, 'article', 5, 'http://depot.garage.dev/images/article_5_20170303005729.jpg', 'article_5_20170303005729.jpg', 'active', '2017-03-03 00:57:29', '2017-03-03 00:57:29'),
(275, 'article', 6, 'http://depot.garage.dev/images/article_6_20170303010008.jpg', 'article_6_20170303010008.jpg', 'active', '2017-03-03 01:00:08', '2017-03-03 01:00:08'),
(276, 'article', 6, 'http://depot.garage.dev/images/article_6_20170303010224.jpg', 'article_6_20170303010224.jpg', 'active', '2017-03-03 01:02:24', '2017-03-03 01:02:24'),
(277, 'article', 7, 'http://depot.garage.dev/images/article_7_20170303010246.jpg', 'article_7_20170303010246.jpg', 'active', '2017-03-03 01:02:46', '2017-03-03 01:02:46'),
(278, 'article', 8, 'http://depot.garage.dev/images/article_8_20170303010600.jpg', 'article_8_20170303010600.jpg', 'active', '2017-03-03 01:06:00', '2017-03-03 01:06:00'),
(279, 'article', 9, 'http://depot.garage.dev/images/article_9_20170303010655.jpg', 'article_9_20170303010655.jpg', 'active', '2017-03-03 01:06:55', '2017-03-03 01:06:55'),
(280, 'article', 10, 'http://depot.garage.dev/images/article_10_20170303010836.jpg', 'article_10_20170303010836.jpg', 'active', '2017-03-03 01:08:36', '2017-03-03 01:08:36'),
(281, 'article', 11, 'http://depot.garage.dev/images/article_11_20170303011258.jpg', 'article_11_20170303011258.jpg', 'active', '2017-03-03 01:12:58', '2017-03-03 01:12:58'),
(282, 'promotion', 2, 'http://depot.garage.dev/images/promotion_2_20170303025916.jpg', 'promotion_2_20170303025916.jpg', 'active', '2017-03-03 02:59:16', '2017-03-03 02:59:16'),
(283, 'promotion', 3, 'http://depot.garage.dev/images/promotion_3_20170303030023.jpg', 'promotion_3_20170303030023.jpg', 'active', '2017-03-03 03:00:23', '2017-03-03 03:00:23'),
(284, 'article', 8, 'http://depot.garage.dev/images/article_8_20170305171445.jpg', 'article_8_20170305171445.jpg', 'active', '2017-03-05 17:14:45', '2017-03-05 17:14:45'),
(285, 'business', 5, 'http://depot.garage.dev/images/business_5_20170423225424.jpg', 'business_5_20170423225424.jpg', 'active', '2017-04-23 22:54:24', '2017-04-23 22:54:24'),
(286, 'business', 6, 'http://depot.garage.dev/images/business_6_20170423225742.jpg', 'business_6_20170423225742.jpg', 'active', '2017-04-23 22:57:42', '2017-04-23 22:57:42'),
(287, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426224930.jpg', 'user_2_20170426224930.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(288, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426224945.jpg', 'user_2_20170426224945.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(289, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225000.jpg', 'user_2_20170426225000.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(290, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225115.jpg', 'user_2_20170426225115.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(291, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225500.jpg', 'user_2_20170426225500.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(292, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225511.jpg', 'user_2_20170426225511.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(293, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225613.jpg', 'user_2_20170426225613.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(294, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225811.jpg', 'user_2_20170426225811.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(295, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426225853.jpg', 'user_2_20170426225853.jpg', 'deleted', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(296, 'user', 2, 'http://depot.garage.dev/images/user_2_20170426230038.jpg', 'user_2_20170426230038.jpg', 'active', '2017-04-26 23:00:38', '2017-04-26 23:00:38'),
(297, 'article', 9, 'http://depot.garage.dev/images/article_9_20170427213225.jpg', 'article_9_20170427213225.jpg', 'active', '2017-04-27 21:32:25', '2017-04-27 21:32:25'),
(298, 'business', 7, 'http://depot.garage.dev/images/business_7_20170522232714.jpg', 'business_7_20170522232714.jpg', 'active', '2017-05-22 23:27:14', '2017-05-22 23:27:14'),
(299, 'business', 8, 'http://depot.garage.dev/images/business_8_20170530223028.jpg', 'business_8_20170530223028.jpg', 'active', '2017-05-30 22:30:28', '2017-05-30 22:30:28'),
(300, 'business', 9, 'http://depot.garage.dev/images/business_9_20170615001722.jpg', 'business_9_20170615001722.jpg', 'active', '2017-06-15 00:17:22', '2017-06-15 00:17:22');

-- --------------------------------------------------------

--
-- Table structure for table `markers`
--

CREATE TABLE `markers` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `address` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `markers`
--

INSERT INTO `markers` (`id`, `name`, `address`, `lat`, `lng`) VALUES
(1, 'Frankie Johnnie & Luigo Too', '939 W El Camino Real, Mountain View, CA', 37.386337, -122.085823),
(2, 'Amici''s East Coast Pizzeria', '790 Castro St, Mountain View, CA', 37.387138, -122.083237),
(3, 'Kapp''s Pizza Bar & Grill', '191 Castro St, Mountain View, CA', 37.393887, -122.078918),
(4, 'Round Table Pizza: Mountain View', '570 N Shoreline Blvd, Mountain View, CA', 37.402653, -122.079353),
(5, 'Tony & Alba''s Pizza & Pasta', '619 Escuela Ave, Mountain View, CA', 37.394012, -122.095528),
(6, 'Oregano''s Wood-Fired Pizza', '4546 El Camino Real, Los Altos, CA', 37.401726, -122.114647);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `title` text,
  `cover_image_id` int(11) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT NULL,
  `detail` text,
  `notice` text,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `business_list` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `viewed` int(11) UNSIGNED NOT NULL,
  `liked` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `shared` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `title`, `cover_image_id`, `cover_image_src`, `detail`, `notice`, `start_time`, `end_time`, `business_list`, `created_time`, `updated_time`, `status`, `viewed`, `liked`, `shared`) VALUES
(1, 'Test Promotion', 270, 'http://depot.garage.dev/images/promotion_1_20170301222601.jpg', 'Test Promotion<br>', NULL, '2017-02-26 00:00:00', '2017-03-31 00:00:00', '4,', '2017-06-06 01:38:18', '2017-06-06 01:38:18', 'active', 1, 0, 0),
(2, 'Test', 282, 'http://depot.garage.dev/images/promotion_2_20170303025916.jpg', 'test', NULL, '2017-03-03 02:59:16', '2017-03-03 02:59:16', '', '2017-03-03 02:59:16', '2017-03-03 02:59:16', 'active', 0, 0, 0),
(3, 'Test', 283, 'http://depot.garage.dev/images/promotion_3_20170303030023.jpg', 'twetwet', NULL, '2017-03-18 00:00:00', '2017-03-31 00:00:00', '', '2017-07-05 06:47:24', '2017-07-05 06:47:24', 'active', 10, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review`
--

INSERT INTO `review` (`id`, `user_id`, `business_id`, `detail`, `score`, `created_time`, `updated_time`, `status`) VALUES
(6, 2, 3, 'Test', '5', '2017-04-23 22:16:44', '2017-04-23 22:17:48', 'deleted'),
(7, 2, 3, 'Test', '5', '2017-04-23 22:17:55', '2017-04-23 22:17:55', 'active'),
(8, 2, 4, 'Test', '5', '2017-04-23 22:18:23', '2017-04-23 22:18:23', 'active'),
(9, 2, 2, 'Test', '5', '2017-04-23 22:20:44', '2017-04-23 22:20:44', 'active'),
(10, 1, 3, 'Test', '5', '2017-04-27 21:20:25', '2017-04-27 21:20:25', 'active'),
(12, 1, 2, 'Test', '5', '2017-06-06 04:49:21', '2017-06-06 04:49:21', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `fb_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `token`, `user_id`, `device_id`, `fb_id`, `fb_token`) VALUES
(1, '2c5b67f774b52f2abded46db5d594784a54e0d4beb8a4a40000243b68b71c2c8d7493767239ed057b4ed1e4cabae5f15a76109ac630d2e18d91cbe5fd205456e', 1, 1000000001, '10205560865053456', 'EAAI7nUxeqMYBABEZA4uZCZBU0hoa1T0D9FY4iymBc8OtaHQhZBKgmAS1IZAuAwRKT8ZA330DyZCZB8idfeCdfMMkMBoxi1J1DY7fbWROxE98ZAoyzWrDeWmGFZBxBxKLs1OW6ZCZA5DzvrEMJgYoxNzj9cHOVMQP8zkPX6po4t1I7xJHM8FHWnOGm8m7'),
(2, '40b244112641dd78dd4f93b6c9190dd46e0099194d5a44257b7efad6ef9ff4683da1eda0244448cb343aa688f5d3efd7314dafe580ac0bcbf115aeca9e8dc114', 2, 0, '', ''),
(4, 'f872b812dd44e2f42578eb72ebb3a9ae47f1187472ec627ac92c39d0e49879de082d62f14615ef95f16aa6292c0b2878bfb7daf5a9e297714f1c213a9d1fbf65', 2, 1000000002, NULL, NULL),
(5, '198dabf4bac21cf35cddb48db0f8b67c56b2bdf63767242aea7342fe68c0b9df8d37f3e47a134648e19f1640e158f2e527e636db122a9143307cf309efcb85d9', 2, 1, NULL, NULL),
(6, '2b196254917f6852a7509f846d7bcbfae611c1d95320f18db13d68d2de95be24d4734cffd20cca7efb5cd48067753dae6c57d8722852ddf4c3cd7aed235cff47', 1, 1000000002, NULL, NULL),
(7, '74a49c698dbd3c12e36b0b287447d833f74f3937ff132ebff7054baa18623c35a705bb18b82e2ac0384b5127db97016e63609f712bc90e3506cfbea97599f46f', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `username`, `password`, `email`, `name`, `tel`, `type`, `status`, `created_date`, `updated_date`) VALUES
(1, 'aize', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin@pueanrod.com', 'Aize', '0987654321', 'admin', 'active', '2017-07-05 05:20:28', '2017-07-05 05:20:28'),
(2, 'Test2', '01fbbd71323dfe904f3565cb488ac2317120af112a69a66d218980c00024eaa46edb7d9cd7007e3b6bc16bea9d7793ae5566d4c3b0bffa203f7f6553ad469672', 'test1@test.com', NULL, NULL, 'admin', 'active', '2017-01-03 18:31:37', '2017-01-03 18:31:37'),
(3, 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin@test.com', 'Admin', '0999999999', 'admin', 'active', '2017-08-09 00:46:50', '2017-08-09 00:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `stat`
--

CREATE TABLE `stat` (
  `id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `reviewed_business` int(11) DEFAULT NULL,
  `reviewed_business_num` int(11) DEFAULT NULL,
  `viewed_business` int(11) DEFAULT NULL,
  `viewed_business_num` int(11) DEFAULT NULL,
  `viewed_article` int(11) DEFAULT NULL,
  `viewed_article_num` int(11) DEFAULT NULL,
  `viewed_promotion` int(11) DEFAULT NULL,
  `viewed_promotion_num` int(11) DEFAULT NULL,
  `viewed_advertise` int(11) DEFAULT NULL,
  `viewed_advertise_num` int(11) DEFAULT NULL,
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stat`
--

INSERT INTO `stat` (`id`, `type`, `title`, `user`, `reviewed_business`, `reviewed_business_num`, `viewed_business`, `viewed_business_num`, `viewed_article`, `viewed_article_num`, `viewed_promotion`, `viewed_promotion_num`, `viewed_advertise`, `viewed_advertise_num`, `created_time`) VALUES
(1, 'daily', '20170315_daily', 0, 0, 0, 2, 16, 1, 2, 1, 9, 1, 3, '2017-03-15 00:00:00'),
(2, 'lifetime', 'lifetime', 0, 0, 0, 9, 82, 5, 26, 5, 57, 5, 30, '2017-06-05 00:00:00'),
(3, 'daily', '20170316_daily', 0, 0, 0, 2, 22, 1, 8, 1, 16, 1, 9, '2017-03-16 00:00:00'),
(4, 'daily', '20170317_daily', 0, 0, 0, 2, 22, 1, 8, 1, 16, 1, 9, '2017-03-17 00:00:00'),
(5, 'daily', '20170318_daily', 0, 0, 0, 2, 22, 1, 8, 1, 16, 1, 9, '2017-03-18 00:00:00'),
(6, 'daily', '20170605_daily', 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, '2017-06-05 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_log`
--

CREATE TABLE `transaction_log` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `data` text NOT NULL,
  `cate` varchar(200) NOT NULL,
  `lat` double NOT NULL DEFAULT '0',
  `lon` double NOT NULL DEFAULT '0',
  `reviewed` text NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(255) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaction_log`
--

INSERT INTO `transaction_log` (`id`, `title`, `data`, `cate`, `lat`, `lon`, `reviewed`, `score`, `created_time`, `type`, `content_id`, `total`) VALUES
(1, '20170319_promotion_3', '', '', 0, 0, '', 0, '2017-03-18 08:30:50', 'promotion', 3, 16),
(2, '20170319_article_1', '', '', 0, 0, '', 0, '2017-03-18 08:30:52', 'article', 1, 8),
(3, '20170319_advertise_1', '', '', 0, 0, '', 0, '2017-03-18 08:30:56', 'advertise', 1, 9),
(4, '20170319_business_4', '', '', 0, 0, '', 0, '2017-03-18 08:31:01', 'business', 4, 7),
(5, '20170319_business_3', '', '', 0, 0, '', 0, '2017-03-18 08:31:04', 'business', 3, 15),
(6, '20170422_business_1', '', '', 0, 0, '', 0, '2017-04-22 21:44:37', 'business', 1, 4),
(7, '20170606_promotion_1', '', '', 0, 0, '', 0, '2017-06-06 01:38:18', 'promotion', 1, 199),
(9, '20170606_business_2', '{"name":"abc 123","score":"0","viewed":"0","reviewed":"0","lat":"13.7236482960288","lon":"100.52958650172422","type":"9"}', '9', 13.7236482960288, 100.52958650172422, '["12","9"]', 10, '2017-06-06 04:42:11', 'business', 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `image_id` int(11) NOT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `reviewed` int(11) NOT NULL DEFAULT '0',
  `created_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `request_code` varchar(50) NOT NULL,
  `request_expired_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fb_id`, `name`, `profile_image`, `image_id`, `birthday`, `gender`, `email`, `mobile`, `password`, `reviewed`, `created_time`, `updated_time`, `status`, `request_code`, `request_expired_date`) VALUES
(1, '10205560865053456', 'Aize', 'http://depot.garage.dev/images/user_2_20170426225511.jpg', 292, '1987-08-10', 'male', 'aize@outlook.com', '', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff', 1, '2017-04-27 21:20:25', '2017-08-09 03:09:53', 'active', '', '2017-08-08 02:18:22'),
(2, '', 'Aize', 'http://depot.garage.dev/images/user_2_20170426230038.jpg', 296, '1987-03-19', 'unknown', 'aize.gamer@gmail.com', '', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff', 3, '2017-08-09 00:43:21', '2017-08-09 00:43:21', 'active', '', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `advertise`
--
ALTER TABLE `advertise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emergency`
--
ALTER TABLE `emergency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history_log`
--
ALTER TABLE `history_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `markers`
--
ALTER TABLE `markers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stat`
--
ALTER TABLE `stat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaction_log`
--
ALTER TABLE `transaction_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `advertise`
--
ALTER TABLE `advertise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `emergency`
--
ALTER TABLE `emergency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `history_log`
--
ALTER TABLE `history_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;
--
-- AUTO_INCREMENT for table `markers`
--
ALTER TABLE `markers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stat`
--
ALTER TABLE `stat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transaction_log`
--
ALTER TABLE `transaction_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
