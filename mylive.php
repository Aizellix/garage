<?php
$u = false;
$v = false;
if (isset($_GET['u'])) {
    $u = $_GET['u'];
}
if (isset($_GET['v'])) {
    $v = $_GET['v'];
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Localhost</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
</head>
<body style="margin:0; overflow:hidden; background: #000; ">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12">
            <div class="row">
                <video id="player" class="video-js vjs-default-skin" controls style="width: 100%">
                    <source
                            src="https://mylive-hls.cdn.byteark.com/hls/<?php echo $u; ?>/index.m3u8"
                            type="application/x-mpegURL">
                </video>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
                <link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">
                <script src="https://unpkg.com/video.js/dist/video.js"></script>
                <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>

                <script>
                    var player = window.player = videojs('player');
                    player.src({
                        src: 'https://mylive-hls.cdn.byteark.com/hls/<?php echo $u; ?>/index.m3u8',
                        type: 'application/x-mpegURL'
                    });
                    player.play();
                </script>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-2 col-xs-12">
            <div class="row">
                <iframe id="chat" src="https://mylive.in.th/chat/<?php echo $v; ?>" style="width:100%; border:none;">
                </iframe>
            </div>
        </div>
    </div>
</div>
<script>
    var h = $(window).height();
    var h2 = 0;
    $(document).ready(function () {
        $(window).resize(function () {
            h = $(window).height();
            w = $(window).width();
            if(w >1300){
                $(".col-lg-3").addClass('col-lg-2');
                $(".col-lg-9").addClass('col-lg-10');
                $(".col-lg-3").removeClass('col-lg-3');
                $(".col-lg-9").removeClass('col-lg-9');
            }else{
                $(".col-lg-2").addClass('col-lg-3');
                $(".col-lg-10").addClass('col-lg-9');
                $(".col-lg-2").removeClass('col-lg-2');
                $(".col-lg-10").removeClass('col-lg-10');
            }
            h2 = w * (9 / 16);
            if (h2 >= h) {
                $("#player").height(h);
                h2 = 0;
            } else {
                $("#player").height(h2);
            }
            $("#chat").height(h - h2);
        });
        w = $(window).width();
        if(w >1300){
            $(".col-lg-3").addClass('col-lg-2');
            $(".col-lg-9").addClass('col-lg-10');
            $(".col-lg-3").removeClass('col-lg-3');
            $(".col-lg-9").removeClass('col-lg-9');
        }else{
            $(".col-lg-2").addClass('col-lg-3');
            $(".col-lg-10").addClass('col-lg-9');
            $(".col-lg-2").removeClass('col-lg-2');
            $(".col-lg-10").removeClass('col-lg-10');
        }
        h2 = w * (9 / 16);
        if (h2 >= h) {
            $("#player").height(h);
            h2 = 0;
        } else {
            $("#player").height(h2);
        }
        $("#chat").height(h - h2);

    })
</script>
<style>
</style>
</body>
</html>