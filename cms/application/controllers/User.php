<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller{
    public $user_data;
    public $api_key;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->api_key = $this->config->item('api_key');
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index(){
        $page = $this->input->get('p');
        $keyword = ($this->input->get('keyword'))?$this->input->get('keyword'):false;
        $gender = ($this->input->get('gender'))?$this->input->get('gender'):'all';
        $register= ($this->input->get('register'))?$this->input->get('register'):'all';
        $register_from= ($this->input->get('register_from'))?$this->input->get('register_from'):'';
        $register_to= ($this->input->get('register_to'))?$this->input->get('register_to'):'';
        $age_from= ($this->input->get('age_from'))?$this->input->get('age_from'):'';
        $age_to= ($this->input->get('age_to'))?$this->input->get('age_to'):'';


        $sort_by = ($this->input->get('sort_by'))?$this->input->get('sort_by'):false;
        $order_by = ($this->input->get('order_by'))?$this->input->get('order_by'):false;
        if(!$page) $page=1;

        $limit = 20;
        $offset = ($page - 1) * $limit;
        if($keyword){
            $sql = "FROM user WHERE ";
            if($keyword){
                $sql .= " ( `name` LIKE '%$keyword%' ";
                $sql .= " OR `email` LIKE '%$keyword%' ) AND ";
            }
            $sql .=" `status` !=  'deleted'";

            if($gender!="all"){
                $sql .= " AND `gender` =  '$gender'";
            }

            if($register_from!=''){
                $sql .= " AND `created_time` >=  '$register_from'";
            }

            if($register_to!=''){
                $sql .= " AND `created_time` <=  '$register_to'";
            }

            if($age_from!=''){
                $age_from_this = date('Y-m-d',strtotime('-'.$age_from.'year'));
                $sql .= " AND `birthday` <=  '$age_from_this'";
            }

            if($age_to!=''){
                $age_to_this = date('Y-m-d',strtotime('-'.($age_to+1).'year'));
                $sql .= " AND `birthday` >=  '$age_to_this'";
            }

            if($register=='fb'){
                $sql .= " AND `fb_id` !=  ''";
            }elseif($register=="email"){
                $sql .= " AND `fb_id` =  ''";
            }

            $query = $this->db->query("SELECT id ".$sql);
            $total_num =  $query->num_rows();

            if($sort_by&&$order_by){
                $sql .= "ORDER BY `".$sort_by."` ".$order_by." ";
            }else{
                $sql .= "ORDER BY `id` DESC ";
            }
            $sql .= "LIMIT $offset, $limit ";
            $this->db->select('id,point,fb_id,email,name,birthday,gender,profile_image,reviewed,created_time,updated_time');
            $query = $this->db->query("SELECT * ".$sql);

        }else{
            $this->db->from('user');
            $where = array('status !='=> 'deleted');
            if($gender!="all"){
                $where['gender'] = $gender;
            }

            if($register=="fb"){
                $where['fb_id !='] = '';
            }elseif($register=="email"){
                $where['fb_id'] = '';
            }

            if($register_from!=''){
                $where['created_time >='] = $register_from;
            }

            if($register_to!=''){
                $where['created_time <='] = $register_to;
            }

            if($age_from!=''){
                $age_from_this = date('Y-m-d',strtotime('-'.$age_from.'year'));
                $where['birthday <='] = $age_from_this;
            }

            if($age_to!=''){
                $age_to_this = date('Y-m-d',strtotime('-'.($age_to+1).'year'));
                $where['birthday >='] = $age_to_this;
            }
            $this->db->where($where);
            $total_num = $this->db->count_all_results();

            if($sort_by&&$order_by){
                $this->db->order_by($sort_by,strtoupper($order_by));
            }else{
                $this->db->order_by('id','DESC');
            }
            $this->db->select('id,point,fb_id,email,name,birthday,gender,profile_image,reviewed,created_time,updated_time');
            $query = $this->db->get_where('user',$where,$limit,$offset);
        }


        $user_list = $query->result();
        $data = array();
        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['limit'] = $limit;
        $data['page'] = $page;
        $data['user_list'] = $user_list;

        $this->load->view('header',array('page_link' => 'user', 'user_data' => $this->user_data));
        $this->load->view('user',$data);
        $this->load->view('footer');
    }

    public function update(){
        $do = $this->input->post('do');
        if ($do=='del'){
            $id = $this->input->post('id');
            $this->db->where('id', $id);
            if ($this->db->update('user', array('status'=>'deleted'))) {
                redirect(site_url('user'));
            }
        }elseif ($do=='del_review'){
            $review_id = $this->input->post('review_id');
            $business_id = $this->input->post('business_id');
            $user_id = $this->input->post('user_id');
            $is_business = (int)$this->input->post('is_business');
            $post_data = array(
                'api_key' => $this->api_key,
                'business_id' => $business_id,
                'review_id' => $review_id,
                'user_id' => $user_id
            );
            $respond = json_decode($this->curl->simple_post(api_site_url('review/delete_review/'),$post_data));
            if($respond->status=="SUCCESS"){
                if($is_business==1){
                    redirect(site_url('business/review/'.$business_id)); die;
                }else{
                    redirect(site_url('user/review/'.$user_id)); die;
                }
            }
        }
        redirect(site_url('user'));

    }
    public function review($id){
        $page = $this->input->get('p');
        if(!$page) $page=1;
        $this->db->from('review');
        $this->db->where(array('user_id'=>$id,'status !='=> 'deleted'));
        $total_num = $this->db->count_all_results();
        $limit = 20;
        $offset = ($page - 1) * $limit;
        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('review',array('user_id'=>$id,'status !='=> 'deleted'),$limit,$offset);
        $review_list = $query->result();
        $business_id_list = array();
        foreach ($review_list as $list){
            $business_id_list[] = $list->business_id;
        }
        $business_ist = array();
        if(count($business_id_list)>0){
            foreach ($business_id_list as $business_id){
                $business_data = json_decode($this->curl->simple_get(api_site_url('business/get_data/'.$business_id)));
                if($this->input->get('debug')=='1'){ echo '<pre>';
                    var_dump($business_id,$business_data);
                    echo '</pre>';
                }
                if(count($business_data->data)>0){
                    $business_ist[$business_id] = $business_data->data[0];
                }
            }
        }
        $data = array();
        $user_data = json_decode($this->curl->simple_get(api_site_url('user/get_data/'.$id)));
        if(empty($user_data->data)){
            redirect(site_url('user'));
            die;
        }
        $deal_log_data = json_decode($this->curl->simple_get(api_site_url('deal/get_user_deal_log/'.$id)));
        if(empty($deal_log_data->data)){
            $total_used_deal = 0;
        }else{
            $total_used_deal = $deal_log_data->total_num;
        }
        foreach ($review_list as $key => $list){
            $query = $this->db->get_where('images',array('ref_id'=>$list->id,"type"=>"review",'status !='=> 'deleted'));
            $img_data = array();
            foreach ($query->result() as $img_list){
                $img_data[] = array("id"=>$img_list->id,"url"=>$img_list->src);
            }
            $review_list[$key]->images = $img_data;
        }
//        echo '<pre>';
//        var_dump($review_list); die;
        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['limit'] = $limit;
        $data['page'] = $page;
        $data['user_data'] = $user_data->data;
        $data['total_used_deal'] = (int)$total_used_deal;
        $data['user_id'] = $id;
        $data['review_list'] = $review_list;
        $data['gender_list'] = array('male'=>"ชาย",'female'=>'หญิง','unknown'=>'ไม่ระบุ');
        $data['business_list'] = $business_ist;
        $this->load->view('header',array('page_link' => 'user', 'user_data' => $this->user_data));
        $this->load->view('review_view',$data);
        $this->load->view('footer');

    }

    public function remove_review_photo(){
        $review_id = $this->input->post("vid");
        $user_id = $this->input->post("uid");
        $image_id = $this->input->post("img_id");
        if(isset($review_id)&&isset($user_id)&&is_array($image_id)){
            $this->db->where('ref_id', $review_id);
            $this->db->where('type', "review");
            $this->db->where_in('id', $image_id);
            if ($this->db->update('images', array('status'=>'deleted'))) {
                $output = array(
                    'status' => "SUCCESS",
                    'code' => 200
                );
                print_json($output);
            }else{
                $output = array(
                    'status' => "CAN_NOT_EDIT_REVIEW",
                    'code' => 200
                );
                print_json($output);
            }
        }else{
            $output = array(
                'status' => "ERROR",
                'code' => 201
            );
            print_json($output);
        }
        die;
    }

}
