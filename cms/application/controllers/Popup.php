<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Popup extends CI_Controller{
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index(){
        $page = $this->input->get('p');
        if(!$page) $page=1;
        $status = ($this->input->get('status'))?$this->input->get('status'):false;
        if(!$page) $page=1;

        if($status=="live"){
            $where = array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }elseif($status=="waiting"){
            $where = array('start_time >'=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }elseif($status=="expired"){
            $where = array('end_time <'=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }else{
            $where = array('status !='=> 'deleted');
        }
        $this->db->from('popup');
        $total_num = $this->db->count_all_results();
        $limit = 20;
        $offset = ($page - 1) * $limit;

        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('popup',$where,$limit,$offset);

        $data_list = $query->result();
        $current_time = time();
        $live_data = array();
        $waiting_data = array();
        $expired_data = array();
        foreach ($data_list as $list){

            $start_time = strtotime($list->start_time);
            $end_time = strtotime($list->end_time);
            if($current_time>=$start_time&&$current_time<=$end_time){
                $live_data[] = $list;
            }else if($current_time<$start_time){
                $waiting_data[] = $list;
            }else{
                $expired_data[] = $list;
            }
        }
        $data_list = array_merge($live_data, $waiting_data, $expired_data);
        $data = array();
        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['limit'] = $limit;
        $data['page'] = $page;
        $data['popup_list'] = $data_list;
        $this->load->view('header',array('page_link' => 'popup', 'user_data' => $this->user_data));
        $this->load->view('popup',$data);
        $this->load->view('footer');
    }

    public function add(){
        $data = array();
        $this->load->view('header',array('page_link' => 'popup', 'user_data' => $this->user_data));
        $this->load->view('popup_add',$data);
        $this->load->view('footer');
    }

    public function view($id){
        $query = $this->db->get_where('history_log',array('content_id'=>$id,'type'=>'popup'));
        $modifier_data = array();
        if($query->num_rows()>0){
            $result_data = $query->result();
            foreach ($result_data as $item){
                $modifier_data[$item->status] = $item;
            }
        }

        $query = $this->db->get_where('popup',array('id'=>$id,'status !='=> 'deleted'));
        $data_list = array();
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            redirect(site_url('popup')); die;
        }
        $data = array();
        $this->load->view('header',array('page_link' => 'popup', 'user_data' => $this->user_data));
        $this_type = $data_list[0]->type;
        if($this_type=="business"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="article"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="promotion"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="deal"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="delivery"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="link"){
            $content_list = array();;
        }
        $popup_type_list = array(
            "business" =>"สถานประกอบการ",
            "article" =>"บทความ",
            "promotion" =>"โปรโมชั่น",
            "deal" =>"ดีล",
            "delivery" =>"เดลิเวอรี",
            "link" =>"Link",
        );
        $data['data_list'] = $data_list[0];
        $data['popup_type_list'] = $popup_type_list;
        $data['content_list'] = $content_list;
//        var_dump($modifier_data['edit']); die;
        $data['modifier_data'] = $modifier_data;
        $this->load->view('popup_view',$data);
        $this->load->view('footer');
    }
    public function edit($id){
        $query = $this->db->get_where('popup',array('id'=>$id,'status !='=> 'deleted'));
        $data_list = array();
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            redirect(site_url('popup')); die;
        }
        $data = array();
        $this->load->view('header',array('page_link' => 'popup', 'user_data' => $this->user_data));
        $this_type = $data_list[0]->type;
        if($this_type=="business"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="article"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="promotion"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="deal"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="delivery"){
            $content_list = $this->get_content_list($data_list[0]->content_id,$this_type);
        }else if($this_type=="link"){
            $content_list = array();;
        }
        $data['data_list'] = $data_list[0];
        $data['content_list'] = $content_list;
        $this->load->view('popup_edit',$data);
        $this->load->view('footer');
    }

    public function update($do){
        if($do=='add'){
            $title = $this->input->post('title');
            $content_id= $this->input->post('content_id');
            $type = $this->input->post('selected_type');
            $order_number = $this->input->post('order_number');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $link_url = $this->input->post('link_url');
            $created_time = date('Y-m-d H:i:s');
            $update_data = array(
                'title'=> $title,
                'start_time'=> $start_time,
                'end_time'=> $end_time,
                'created_time'=> $created_time,
                'updated_time'=> $created_time,
                'content_id'=> $content_id,
                'type'=> $type,
                'link_url'=> $link_url,
                'order_number'=> $order_number,
                'viewed'=> 0,
                'liked'=> 0,
                'shared'=> 0,
                'status'=> 'active'
            );
            if ($this->db->insert('popup', $update_data)) {
                $id = $this->db->insert_id();
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'add','popup');
                $cover_image = $this->update_photo($id,'popup','cover');
                $update_data = array(
                    'cover_image_id'=> $cover_image['id'],
                    'cover_image_src'=> $cover_image['src']
                );
                $this->db->where('id', $id);
                if ($this->db->update('popup', $update_data)) {
                    redirect(site_url('popup/view/'.$id));
                }else{
                    redirect(site_url('popup'));
                }
            }
        }elseif ($do=='edit'){
            $title = $this->input->post('title');
            $content_id= $this->input->post('content_id');
            $type = $this->input->post('selected_type');
            $order_number = $this->input->post('order_number');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $link_url = $this->input->post('link_url');
            $id= $this->input->post('id');
            $image_id= $this->input->post('image_id');
            $updated_time = date('Y-m-d H:i:s');
            $update_data = array(
                'title'=> $title,
                'content_id'=> $content_id,
                'type'=> $type,
                'link_url'=> $link_url,
                'order_number'=> $order_number,
                'start_time'=> $start_time,
                'end_time'=> $end_time,
                'updated_time'=> $updated_time
            );
//            console_log($update_data); die;
            if($_FILES['cover']['tmp_name']!=""){
                if($this->del_photo($image_id)){
                    $cover_image = $this->update_photo($id,'popup','cover');
                    $update_data['cover_image_id'] = $cover_image['id'];
                    $update_data['cover_image_src '] = $cover_image['src'];
                }
            }
            $this->db->where('id', $id);
            if ($this->db->update('popup', $update_data)) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'edit','popup');
                redirect(site_url('popup/view/'.$id));
            }
        }elseif ($do=='del'){
            $id = $this->input->post('id');
            $this->db->where('id', $id);
            if ($this->db->update('popup', array('status'=>'deleted'))) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'del','popup');
                redirect(site_url('popup'));
            }
        }
        redirect(site_url('popup'));

    }

    private function del_photo($id){
        $this->db->where('id', $id);
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }
    }

    private function get_content_list($content_id,$this_type){
        if($this_type=="business"){
            $this->db->select('name,cover_image_src,id,address_province,address_district');
        }
        $this->db->where('id',$content_id);
        $query = $this->db->get($this_type);

        $content_list = $query->result();
        return $content_list;
    }

    private function update_photo($ref_id)
    {
        $POST_DATA = array();
        $img_num = count($_FILES['cover']['tmp_name']);
        $tmp_file = $_FILES['cover']['tmp_name'];
        $file_name = basename($_FILES['cover']['name']);
        $file_type = $_FILES['cover']['type'];
        $POST_DATA['upload_img'] = curl_file_create($tmp_file, $file_type, $file_name);
        $POST_DATA['img_num'] = $img_num;
        $POST_DATA['depot_key'] = $this->config->item('depot_key');
        $POST_DATA['img_type'] = 'popup';
        $POST_DATA['ref_id'] = $ref_id;
        $curl_handle = curl_init($this->config->item('depot_url').'home/upload_image');
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

        $returned_data = curl_exec($curl_handle);
        curl_close($curl_handle);

        $returned_data = json_decode($returned_data, true);
        return $returned_data['result'];
    }
}
