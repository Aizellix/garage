<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

	public function index()
	{
	    $data = array();
        $this->db->where('status', 'active');
        $query = $this->db->get('business');
        $data['business_active_total'] =  $query->num_rows();

        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('user');
        $data['user_total'] =  $query->num_rows();

        $this->db->where('status', 'pending');
        $query = $this->db->get('business');
        $data['business_pending_total'] =  $query->num_rows();

        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('review');
        $data['review_total'] =  $query->num_rows();
//
//        $this->db->where('status !=', 'deleted');
//        $query = $this->db->get('article');
//        $data['article_total'] =  $query->num_rows();
//
//        $this->db->where('status !=', 'deleted');
//        $query = $this->db->get('promotion');
//        $data['promotion_total'] =  $query->num_rows();

        $this->load->view('header',array('page_link' => 'home', 'user_data' => $this->user_data));
		$this->load->view('home',$data);
		$this->load->view('footer');
	}
}
