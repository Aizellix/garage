<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deal extends CI_Controller{
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index(){
        $page = $this->input->get('p');
        if(!$page) $page=1;
        $status = ($this->input->get('status'))?$this->input->get('status'):false;
        if(!$page) $page=1;

        if($status=="live"){
            $where = array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }elseif($status=="waiting"){
            $where = array('start_time >'=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }elseif($status=="expired"){
            $where = array('end_time <'=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }else{
            $where = array('status !='=> 'deleted');
        }

        $sort_by = ($this->input->get('sort_by'))?$this->input->get('sort_by'):false;
        $order_by = ($this->input->get('order_by'))?$this->input->get('order_by'):false;

        $this->db->from('deal');
        $this->db->where($where);
        $total_num = $this->db->count_all_results();
        $limit = 20;
        $offset = ($page - 1) * $limit;
        if($sort_by&&$order_by){
            $this->db->order_by($sort_by,strtoupper($order_by));
        }else{
            $this->db->order_by('order_number','ASC');
            $this->db->order_by('id','DESC');
        }
        $query = $this->db->get_where('deal',$where,$limit,$offset);

        $business_list = $query->result();
        $data = array();
        $current_time = time();
        $live_data = array();
        $waiting_data = array();
        $expired_data = array();
        foreach ($business_list as $list){

            $start_time = strtotime($list->start_time);
            $end_time = strtotime($list->end_time);
            if($current_time>=$start_time&&$current_time<=$end_time){
                $live_data[] = $list;
            }else if($current_time<$start_time){
                $waiting_data[] = $list;
            }else{
                $expired_data[] = $list;
            }
        }
        $business_list = array_merge($live_data, $waiting_data, $expired_data);
        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['limit'] = $limit;
        $data['page'] = $page;
        $data['deal_list'] = $business_list;
        $this->load->view('header',array('page_link' => 'deal', 'user_data' => $this->user_data));
        $this->load->view('deal',$data);
        $this->load->view('footer');
    }

    public function add(){
        $data = array();
        $this->load->view('header',array('page_link' => 'deal', 'user_data' => $this->user_data));
        $this->load->view('deal_add',$data);
        $this->load->view('footer');
    }

    public function view($id){
        $query = $this->db->get_where('history_log',array('content_id'=>$id,'type'=>'deal'));
        $modifier_data = array();
        if($query->num_rows()>0){
            $result_data = $query->result();
            foreach ($result_data as $item){
                $modifier_data[$item->status] = $item;
            }
        }
        $query = $this->db->get_where('deal',array('id'=>$id,'status !='=> 'deleted'));
        $data_list = array();
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            redirect(site_url('deal')); die;
        }
        $data = array();
        $this->load->view('header',array('page_link' => 'deal', 'user_data' => $this->user_data));
        $business_list = $this->get_business_list($data_list[0]->business_list);
        $data['data_list'] = $data_list[0];
        $data['modifier_data'] = $modifier_data;
        $data['business_list'] = $business_list;
        $this->load->view('deal_view',$data);
        $this->load->view('footer');
    }
    public function edit($id){
        $query = $this->db->get_where('deal',array('id'=>$id,'status !='=> 'deleted'));
        $data_list = array();
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            redirect(site_url('deal')); die;
        }
        $data = array();
        $this->load->view('header',array('page_link' => 'deal', 'user_data' => $this->user_data));
        $business_list = $this->get_business_list($data_list[0]->business_list);
        $data['data_list'] = $data_list[0];
        $data['business_list'] = $business_list;
        $this->load->view('deal_edit',$data);
        $this->load->view('footer');
    }

    public function update($do){
        if($do=='add'){
            $title = $this->input->post('title');
            $detail = $this->input->post('detail');
            $business_list= $this->input->post('business_list');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $point_type = $this->input->post('point_type');
            $point_number = $this->input->post('point_number');
            $using_limit = $this->input->post('using_limit');
            $quota_number = $this->input->post('quota_number');
            $type = $this->input->post('type');
            $order_number = $this->input->post('order_number');
            $created_time = date('Y-m-d H:i:s');
            $update_data = array(
                'title'=> $title,
                'detail'=> $detail,
                'start_time'=> $start_time,
                'end_time'=> $end_time,
                'created_time'=> $created_time,
                'updated_time'=> $created_time,
                'business_list'=> $business_list,
                'point_type'=> $point_type,
                'point'=> $point_number,
                'using_limit'=> $using_limit,
                'quota'=> $quota_number,
                'quota_left'=> $quota_number,
                'type'=> $type,
                'order_number'=> $order_number,
                'viewed'=> 0,
                'liked'=> 0,
                'shared'=> 0,
                'status'=> 'active'
            );
            if ($this->db->insert('deal', $update_data)) {
                $id = $this->db->insert_id();
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'add','deal');
                $cover_image = $this->update_photo($id,'deal','cover');
                $update_data = array(
                    'cover_image_id'=> $cover_image['id'],
                    'cover_image_src'=> $cover_image['src']
                );
                $this->db->where('id', $id);
                if ($this->db->update('deal', $update_data)) {
                    redirect(site_url('deal/view/'.$id));
                }else{
                    redirect(site_url('deal'));
                }
            }
        }elseif ($do=='edit'){
            $title = $this->input->post('title');
            $detail = $this->input->post('detail');
            $business_list= $this->input->post('business_list');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $point_type = $this->input->post('point_type');
            $point_number = $this->input->post('point_number');
            $using_limit = $this->input->post('using_limit');
            $quota_number = (int)$this->input->post('quota_number');
            $quota_old = (int)$this->input->post('quota_old');
            $quota_left = (int)$this->input->post('quota_left');
            $type = $this->input->post('type');
            $order_number = $this->input->post('order_number');
            $id= $this->input->post('id');
            $image_id= $this->input->post('image_id');
            $updated_time = date('Y-m-d H:i:s');
            if($quota_old!=$quota_number){
                if($quota_number>$quota_old){
                    $quota_left += ($quota_number-$quota_old);
                    if($quota_left>$quota_number) $quota_left = $quota_number;
                }else if($quota_number<$quota_old){
                    $quota_left -= ($quota_old - $quota_number);
                    if($quota_left<0) $quota_left = 0;
                }
            }
            $update_data = array(
                'title'=> $title,
                'detail'=> $detail,
                'business_list'=> $business_list,
                'point_type'=> $point_type,
                'point'=> $point_number,
                'using_limit'=> $using_limit,
                'quota'=> $quota_number,
                'quota_left'=> $quota_left,
                'type'=> $type,
                'order_number'=> $order_number,
                'start_time'=> $start_time,
                'end_time'=> $end_time,
                'updated_time'=> $updated_time
            );
            if($_FILES['cover']['tmp_name']!=""){
                if($this->del_photo($image_id)){
                    $cover_image = $this->update_photo($id,'deal','cover');
                    $update_data['cover_image_id'] = $cover_image['id'];
                    $update_data['cover_image_src '] = $cover_image['src'];
                }
            }
            $this->db->where('id', $id);
            if ($this->db->update('deal', $update_data)) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'edit','deal');
                redirect(site_url('deal/view/'.$id));
            }
        }elseif ($do=='del'){
            $id = $this->input->post('id');
            $this->db->where('id', $id);
            if ($this->db->update('deal', array('status'=>'deleted'))) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'del','deal');
                redirect(site_url('deal'));
            }
        }
        redirect(site_url('deal'));

    }

    private function del_photo($id){
        $this->db->where('id', $id);
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }
    }

    private function get_business_list($business_list_id){
        $business_list_id = explode(',',$business_list_id);
        $this->db->select('name,cover_image_src,id,address_province,address_district');
        $this->db->where_in('id',$business_list_id);
        $query = $this->db->get('business');

        $business_list = $query->result();
        return $business_list;
    }

    private function update_photo($ref_id)
    {
        $POST_DATA = array();
        $img_num = count($_FILES['cover']['tmp_name']);
        $tmp_file = $_FILES['cover']['tmp_name'];
        $file_name = basename($_FILES['cover']['name']);
        $file_type = $_FILES['cover']['type'];
        $POST_DATA['upload_img'] = curl_file_create($tmp_file, $file_type, $file_name);
        $POST_DATA['img_num'] = $img_num;
        $POST_DATA['depot_key'] = $this->config->item('depot_key');
        $POST_DATA['img_type'] = 'deal';
        $POST_DATA['ref_id'] = $ref_id;
        $curl_handle = curl_init($this->config->item('depot_url').'home/upload_image');
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

        $returned_data = curl_exec($curl_handle);
        curl_close($curl_handle);

        $returned_data = json_decode($returned_data, true);
        return $returned_data['result'];
    }
}
