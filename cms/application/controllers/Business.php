<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {
    public $user_data;
    public $curl;
    public $api_key;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->api_key = $this->config->item('api_key');
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }


    public function index()
	{
        $page = $this->input->get('p');
        $keyword = ($this->input->get('keyword'))?$this->input->get('keyword'):false;
        $status = ($this->input->get('status'))?$this->input->get('status'):false;
        $type = ($this->input->get('type'))?$this->input->get('type'):false;

        $sort_by = ($this->input->get('sort_by'))?$this->input->get('sort_by'):false;
        $order_by = ($this->input->get('order_by'))?$this->input->get('order_by'):false;
        if(!$page) $page=1;
        $limit = 20;
        $offset = ($page - 1) * $limit;
        if($keyword||$status){
            $sql = "FROM business WHERE ";
            if($keyword){
                $sql .= " ( `name` LIKE '%$keyword%' ";
                $sql .= " OR `address_moo` LIKE '%$keyword%' ";
                $sql .= " OR `address_village` LIKE '%$keyword%' ";
                $sql .= " OR `address_soi` LIKE '%$keyword%' ";
                $sql .= " OR `address_road` LIKE '%$keyword%' ";
                $sql .= " OR `address_tambol` LIKE '%$keyword%' ";
                $sql .= " OR `address_district` LIKE '%$keyword%' ";
                $sql .= " OR `address_province` LIKE '%$keyword%' ) AND ";
            }

            if($status=="active"){
                $sql .=" `status` in ('active') ";
            }else if($status=="pending"){
                $sql .=" `status` in ('pending') ";
            }else if($status=="vip"){
                $sql .=" `status` in ('vip') ";
            }else {
                $sql .=" `status` in ('pending', 'active', 'vip') ";
            }
            if($type!="all"){
                $sql .=" AND `type` =".$type."  ";
            }
            $query = $this->db->query("SELECT id ".$sql);
            $total_num =  $query->num_rows();

            if($sort_by&&$order_by){
                $sql .= "ORDER BY `".$sort_by."` ".$order_by." ";
            }else{
                $sql .= "ORDER BY `id` DESC ";
            }
            $sql .= "LIMIT $offset, $limit ";
            $query = $this->db->query("SELECT * ".$sql);

        }else{
            $this->db->from('business');
            $this->db->where(array('status !='=> 'deleted'));
            $total_num = $this->db->count_all_results();
            if($sort_by&&$order_by){
                $this->db->order_by($sort_by,strtoupper($order_by));
            }else{
                $this->db->order_by('id','DESC');
            }
            $query = $this->db->get_where('business',array('status !='=> 'deleted'),$limit,$offset);
        }
        $category_data = json_decode($this->curl->simple_get(api_site_url('category/get_list').'?type=1'));
        $business_list = $query->result();
        $data = array();

        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['page'] = $page;
        $data['business_list'] = $business_list;
        $data['category_list'] = $category_data->list;
		$this->load->view('header',array('page_link' => 'business', 'user_data' => $this->user_data));
		$this->load->view('business',$data);
		$this->load->view('footer');
	}

	public function post_data(){
        $new_business_data = array(
            "name" => $this->get_post_data('name'),
            "owner_name" => $this->get_post_data('owner_name'),
            "type" => $this->get_post_data('type'),
            "address_no" => $this->get_post_data('address_no', ""),
            "address_moo" => $this->get_post_data('address_moo', ""),
            "address_village" => $this->get_post_data('address_village', ""),
            "address_soi" => $this->get_post_data('address_soi', ""),
            "address_road" => $this->get_post_data('address_road', ""),
            "address_tambol" => $this->get_post_data('address_tambol', 'test'),
            "address_district" => $this->get_post_data('address_district'),
            "address_province" => $this->get_post_data('address_province'),
            "address_postcode" => $this->get_post_data('address_postcode'),
            "lat" => $this->get_post_data('lat'),
            "lon" => $this->get_post_data('lon'),
            "country" => $this->get_post_data('country'),
            "country_code" => $this->get_post_data('country_code'),
            "tel" => $this->get_post_data('tel'),
            "contact" => $this->get_post_data('contact', ""),
            "work_day_sun" => $this->get_post_data('work_day_sun', 0),
            "work_day_mon" => $this->get_post_data('work_day_mon', 0),
            "work_day_tue" => $this->get_post_data('work_day_tue', 0),
            "work_day_wed" => $this->get_post_data('work_day_wed', 0),
            "work_day_thu" => $this->get_post_data('work_day_thu', 0),
            "work_day_fri" => $this->get_post_data('work_day_fri', 0),
            "work_day_sat" => $this->get_post_data('work_day_sat', 0),
            "work_time_start" => $this->get_post_data('work_time_start', '00:00:00'),
            "work_time_close" => $this->get_post_data('work_time_close', '00:00:00'),
            "work_time_start_2" => $this->get_post_data('work_time_start_2', '00:00:00'),
            "work_time_close_2" => $this->get_post_data('work_time_close_2', '00:00:00'),
            "detail" => $this->get_post_data('detail', ""),
            "facilities" => $this->get_post_data('facilities', ""),
            "payment" => $this->get_post_data('payment'),
            "award_certification" => $this->get_post_data('award_certification', ""),
            "privilege" => $this->get_post_data('privilege', ""),
            "promoted_time_start" => $this->get_post_data('promoted_time_start', "2016-01-01 00:00:00"),
            "promoted_time_end" => $this->get_post_data('promoted_time_end', "2016-01-01 00:00:00"),
            "created_user_type" => $this->get_post_data('created_user_type', "0"),
            "created_user_id" => $this->get_post_data('created_user_id', "0"),
            "created_user_name" => $this->get_post_data('created_user_name', "FirstGathering"),
            "status" => 'pending');
	    echo '<pre>';
	    var_dump($_FILES);
	    var_dump($_POST);
    }

    public function add(){
        $query = $this->db->get_where('category',array('status'=>'active'));
        $result = $query->result();
        $category_list_1 = array();
        $category_list_2 = array();
        $category_list_3 = array();
        foreach ($result as $item){
            if($item->type==1){
                $category_list_1[] = array('id'=>$item->id,'title'=>$item->title);
            }else if($item->type==2){
                $category_list_2[] = array('id'=>$item->id,'title'=>$item->title);
            }else{
                $category_list_3[] = array('id'=>$item->id,'title'=>$item->title);
            }
        }
        $data['category_list_1'] = $category_list_1;
        $data['category_list_2'] = $category_list_2;
        $data['category_list_3'] = $category_list_3;
//        var_dump($data); die;
        $this->load->view('header',array('page_link' => 'business', 'user_data' => $this->user_data));
        $this->load->view('add_business',$data);
        $this->load->view('footer');
    }

    public function review($content_id){
        if(!$content_id){
            redirect(site_url('business'));
        }else{
            $page = $this->input->get('p');
            if(!$page) $page=1;
            $this->db->from('review');
            $this->db->where(array('business_id'=>$content_id,'status !='=> 'deleted'));
            $total_num = $this->db->count_all_results();
            $limit = 20;
            $offset = ($page - 1) * $limit;
            $this->db->order_by('id','DESC');
            $query = $this->db->get_where('review',array('business_id'=>$content_id,'status !='=> 'deleted'),$limit,$offset);
            $review_list = $query->result();
            $user_id_list = array();
            foreach ($review_list as $list){
                $user_id_list[] = $list->user_id;
            }
            $user_list = array();
            if(count($user_id_list)>0){
                foreach ($user_id_list as $user_id){
                    $user_data = json_decode($this->curl->simple_get(api_site_url('user/get_data/'.$user_id)));
                    if($this->input->get('debug')=='1'&&$user_id==2){ echo '<pre>';
                        var_dump($user_id,$user_data);
                        echo '</pre>';
                    }
                    $user_list[$user_id] = $user_data->data;
                }
            }
            $data = array();
            $business_data = json_decode($this->curl->simple_get(api_site_url('business/get_data/'.$content_id)));
            if(empty($business_data->data)){
                redirect(site_url('business'));
                die;
            }
            foreach ($review_list as $key => $list){
                $query = $this->db->get_where('images',array('ref_id'=>$list->id,"type"=>"review",'status !='=> 'deleted'));
                $img_data = array();
                foreach ($query->result() as $img_list){
                    $img_data[] = array("id"=>$img_list->id,"url"=>$img_list->src);
                }
                $review_list[$key]->images = $img_data;
            }
//        echo '<pre>';
//        var_dump($review_list); die;
            $total_page = ceil($total_num / $limit);
            $data['total_page'] = $total_page;
            $data['total_num'] = $total_num;
            $data['limit'] = $limit;
            $data['page'] = $page;
            $data['business_data'] = $business_data->data[0];
            $data['user_id'] = $content_id;
            $data['review_list'] = $review_list;
            $data['gender_list'] = array('male'=>"ชาย",'female'=>'หญิง');
            $data['user_list'] = $user_list;

            $this->load->view('header',array('page_link' => 'business', 'user_data' => $this->user_data));
            $this->load->view('business_review',$data);
            $this->load->view('footer');
        }
    }

    public function view($content_id){
        if(!$content_id){
            redirect(site_url('business'));
        }else{
            $query = $this->db->get_where('history_log',array('content_id'=>$content_id,'type'=>'business'));
            $modifier_data = array();
            if($query->num_rows()>0){
                $result_data = $query->result();
                foreach ($result_data as $item){
                    $modifier_data[$item->status] = $item;
                }
            }

            $data = array();

            $query = $this->db->get_where('business',array('id'=>$content_id,'status !=' =>"deleted"));
            if($query->num_rows()<=0){
                redirect(site_url('business')); die;
            }
            $result = $query->result();
            foreach ($result as $item){
                $business_data = $item;
            }
//            var_dump($business_data); die;

            $query = $this->db->get_where('category',array('status'=>'active'));
            $result = $query->result();
            $category_list_1 = array();
            $category_list_2 = array();
            $category_list_3 = array();
            foreach ($result as $item){
                if($item->type==1){
                    $category_list_1[$item->id] = $item->title;
                }else if($item->type==2){
                    $category_list_2[$item->id] = $item->title;
                }else{
                    $category_list_3[$item->id] = $item->title;
                }
            }
            $api_url = api_site_url('business/get_photo_list');
            $api_param = array(
                'bid'=>$content_id,
                'limit'=>12,
                'offset'=> 0
            );

            $photo_simple = json_decode($this->curl->simple_get($api_url,$api_param));
            $data['photo_simple'] = $photo_simple->detail;
            $data['photo_total'] = $photo_simple->total;
            $data['category_list_1'] = $category_list_1;
            $data['category_list_2'] = $category_list_2;
            $data['category_list_3'] = $category_list_3;
            $data['business_data'] = $business_data;
            $data['modifier_data'] = $modifier_data;
            $this->load->view('header',array('page_link' => 'business', 'user_data' => $this->user_data));
            $this->load->view('view_business',$data);
            $this->load->view('footer');
        }
    }

    public function edit($content_id){

        if(!$content_id){
            redirect(site_url('business'));
        }else{
            $data = array();

            $query = $this->db->get_where('business',array('id'=>$content_id));
            if($query->num_rows()<=0){
                redirect(site_url('business')); die;
            }
            $result = $query->result();
            foreach ($result as $item){
                $business_data = $item;
            }
//            var_dump($business_data); die;
            $query = $this->db->get_where('category',array('status'=>'active'));
            $result = $query->result();
            $category_list_1 = array();
            $category_list_2 = array();
            $category_list_3 = array();
            foreach ($result as $item){
                if($item->type==1){
                    $category_list_1[] = array('id'=>$item->id,'title'=>$item->title);
                }else if($item->type==2){
                    $category_list_2[] = array('id'=>$item->id,'title'=>$item->title);
                }else{
                    $category_list_3[] = array('id'=>$item->id,'title'=>$item->title);
                }
            }
            $data['category_list_1'] = $category_list_1;
            $data['category_list_2'] = $category_list_2;
            $data['category_list_3'] = $category_list_3;
            $data['business_data'] = $business_data;
            $this->load->view('header',array('page_link' => 'business','user_data' => $this->user_data));
            $this->load->view('edit_business',$data);
            $this->load->view('footer');
        }
    }
    private function del_all_photo($id){
        $this->db->where('ref_id', $id);
        $this->db->where('type', "business");
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }else{
            return true;
        }
    }

    public function del($content_id){
        if(!$content_id){
            redirect(site_url('business'));
        }else{
            $query = $this->db->get_where('review',array('business_id'=>$content_id,'status'=>'active'));
            if($query->num_rows()>0){
                $result = $query->result();
                foreach ($result as $item) {
//                    var_dump($item->id);
                    $post_data = array(
                        'api_key' => $this->api_key,
                        'business_id' => $content_id,
                        'review_id' => $item->id,
                        'user_id' => $item->user_id
                    );
//                    console_log($post_data);
                    $respond = json_decode($this->curl->simple_post(api_site_url('review/delete_review/'),$post_data));
//                    console_log($respond);
                }
            }
            $this->db->where('id', $content_id);
            if ($this->db->update('business', array('status'=>'deleted'))) {
                $this->del_all_photo($content_id);
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$content_id,'del','business');
                redirect(site_url('business'));
            }
        }
    }
}
