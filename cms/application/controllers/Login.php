<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
        if ($this->user_data) $this->user_data = json_decode($this->user_data, true);
    }

    public function index()
    {
        $usernamne = $this->input->post('username');
        $password= $this->input->post('password');

        if ($this->user_data) {
            redirect(site_url('home'));
            die;
        }

        $data = array();
        $do = $this->input->post('do');
        if ($do == 'login') {
//            $query = $this->db->query("SELECT * FROM staff WHERE usernamne=? AND status=? ", array($usernamne, 'active'));
            $query = $this->db->get_where('staff',array('username'=> $usernamne, 'status'=> 'active'));
            if ($query->num_rows() > 0) {
                foreach($query->result() as $item){
                    $user_data = $item;
                }

                $pass_salted = $user_data->password;
                if (hash("sha512", $password, false) == $pass_salted) {
                    $data = array('id' => $user_data->id, 'name' => $user_data->name, 'permission' => $user_data->type, 'is_online' => true, 'log_in_time' => date('Y-m-d H:i:s'));
                    $this->input->set_cookie('user_data', json_encode($data), +7200);
//          var_dump($data); die;
                    redirect(site_url('home'));
                    die;
                } else {
                    redirect(site_url('login?error=2')); die;
                }
                redirect(site_url('login?error=2')); die;
            }
        }else if ($do == 'forgot') {
            $new_pass = $this->password_generation();
            $sent_to = $this->input->post('user_email_forgot');
            $send_from = 'noreply@pueanrod.com';
            $sender_name= 'Pueanrod.com';
            $mail_subject = 'การร้องขอรหัสผ่านใหม่ของผู้ใช้งาน - Pueanrod.com Back Office';
            $msg = '<div style="padding: 20px; background-color: #2466AF;">
                    <div><span style="font-size: 36px; color:#fff;">Pueanrod.com Back Office System</span></div>
                    <div style="padding: 20px; background-color: #FFF;">
                    <div><p style="padding: 16px;">หลังจากที่คุณได้ทำการร้องขอรหัสผ่านใหม่ ทางระบบอัตโนมัติได้ทำการเปลี่ยนรหัสผ่านของคุณ และส่งมาให้ทางอีเมลตามที่คุณได้กรอกไว้ในขั้นตอนการร้องขอรหัสใหม่ คุณสามารถเปลี่ยนรหัสผ่านใหม่ได้ด้วยตนเองเมื่อเข้าสู่ระบบแล้ว</p>
                    <div style="padding: 14px;">รหัสผ่านใหม่ของคุณคือ "<strong>'.$new_pass.'</strong>"</div>
                    </div>
                    </div>
                    </div>';
            $new_pass = hash("sha512", $new_pass, false);
            $now_date = date('Y-m-d H:i:s');
            $update_data = array(
                'password'=>  $new_pass,
                'updated_date'=>  $now_date
            );
            $array = array('email' => $sent_to, 'status !=' => 'deleted');
            $this->db->where($array);
            if ($this->db->update('staff', $update_data)) {
                echo send_email($sent_to,$send_from,$sender_name,$mail_subject,$msg);
                redirect(site_url('login?do=success'));
            }

        }
        $this->load->view('login', $data);
    }

    private function password_generation(){
        $current_time = time();
        $raw1 = sha1($current_time);
        $raw2 = array();
        $raw2[] = $raw1;
        $raw3 = '';
        $n = 0;
        for ($i=0;$i<strlen($raw1);$i+=4){
            if($n%3==0){
//                $raw2[] = strtoupper(substr($raw1,$i,1));
                $raw3 .= strtoupper(substr($raw1,$i,1));
            }else{
//                $raw2[] = strtolower(substr($raw1,$i,1));
                $raw3 .= strtolower(substr($raw1,$i,1));
            }
            $n++;
        }
        return $raw3;
    }
}