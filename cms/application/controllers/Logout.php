<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

    public function index()
    {
        $this->input->set_cookie('user_data', json_encode(array()), -7200);
        redirect(site_url('login'));
    }
}
