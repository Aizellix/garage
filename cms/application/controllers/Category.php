<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index()
	{
        $query = $this->db->get_where('category',array('status'=>'active'));
        $result = $query->result();
        $data = array();
        $category_list = array(1=>array(),2=>array(),3=>array());
        foreach ($result as $item){
            $category_list[$item->type][] = array('id'=>$item->id,'title'=>$item->title);

        }
        $data['category_list'] = $category_list;
		$this->load->view('header',array('page_link' => 'category', 'user_data' => $this->user_data));
		$this->load->view('category',$data);
		$this->load->view('footer');
	}

	public function post_data(){
	    $category_title = $this->input->post('category_title');
	    $category_type = $this->input->post('category_type');
	    $category_do = $this->input->post('category_do');
        $time_now = date('Y-m-d H:i:s');
        if($category_do=="add"){
            $new_category_data = array(
                'title' => $category_title,
                'type' => $category_type,
                'created_date' => $time_now,
                'updated_date' => $time_now,
                'status' => 'active'
            );
            if ($this->db->insert('category', $new_category_data)) {

            }
        }else if ($category_do=="edit"){
            $data = array(
                'title' => $category_title,
                'updated_date' => $time_now
            );
            $category_id = $this->input->post('category_type');
            $this->db->where('id', $category_id);
            $this->db->update('category', $data);
        }else if ($category_do=="delete"){
            $data = array(
                'status' => 'deleted',
                'updated_date' => $time_now
            );
            $category_id = $this->input->post('category_type');
            $this->db->where('id', $category_id);
            $this->db->update('category', $data);
        }
        redirect(site_url('category'));
    }

    public function view($content_id){
        if(!$content_id){
            redirect(site_url('business'));
        }else{
            echo 'View '.$content_id;
        }
    }

    public function edit($content_id){

        if(!$content_id){
            redirect(site_url('business'));
        }else{
            echo 'Edit '.$content_id;
        }
    }
}
