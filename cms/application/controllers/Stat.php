<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stat extends CI_Controller {
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

	public function index()
	{
        $this->db->where('status !=', 'deleted');
        $data = array();

        $this->load->view('header',array('page_link' => 'stat', 'user_data' => $this->user_data));
		$this->load->view('stat',$data);
		$this->load->view('footer');
	}

	public function business(){
        $data = array();
        $start = ($this->input->get('start'))?$this->input->get('start'):"";
        $to = ($this->input->get('to'))?$this->input->get('to'):"";
        $cate = ($this->input->get('cate'))?$this->input->get('cate'):"all";
        $data['start'] = $start;
        $data['to'] = $to;
        $data['total'] = 0;
        $data['total_page'] = 0;
        $data['limit_page'] = 50;
        $data['data'] = array();
        if($start!=""&&$to!=""){
            if($cate!='all'){
                $this->db->where('cate',$cate);
            }
            $query = $this->db->get_where('transaction_log',array('created_time >='=>$start,'created_time <='=>$to,'type'=>'business'));
            $data['total'] = $query->num_rows();
            $total_page = ceil($data['total'] / $data['limit_page']);
            $data['total_page'] = $total_page;
            if($query->num_rows()>0){
                $result_data = $query->result();
                $stat_data = array();
                $id_list = array();
                foreach ($result_data as $item){
                    if($item->reviewed==""){
                        $item->reviewed="[]";
                    }
                    if(isset($stat_data[$item->content_id])){
                        $stat_data[$item->content_id]['total'] += $item->total;
                        $stat_data[$item->content_id]['reviewed'] = array_merge($stat_data[$item->content_id]['reviewed'],json_decode($item->reviewed,true));
                        $stat_data[$item->content_id]['score'] += (int)$item->score;
                    } else{
                        $id_list[] = $item->content_id;
                        $stat_data[$item->content_id] = array('total'=>$item->total,'id'=>$item->content_id,'data'=>json_decode($item->data,true));
                        $stat_data[$item->content_id]['reviewed'] = json_decode($item->reviewed,true);
                        $stat_data[$item->content_id]['score'] = (int)$item->score;
                    }
                }

                $temp_data = array();
                foreach ($stat_data as $item){
                    $temp_data[$item['total'].'.'.$item['id']] = $item;
                }
                krsort($temp_data);
                $data['data'] = $temp_data;
            }

        }

        $query = $this->db->get_where('category',array('type'=>'1','status !='=>'deleted'));

        $cate_data = $query->result();
        $data['cate_data'] = $cate_data;
        $data['this_cate'] = $cate;
        $this->load->view('header',array('page_link' => 'stat_business', 'user_data' => $this->user_data));
        $this->load->view('stat_business',$data);
        $this->load->view('footer');
    }

	public function promotion(){
        $data = array();
        $start = ($this->input->get('start'))?$this->input->get('start'):"";
        $to = ($this->input->get('to'))?$this->input->get('to'):"";
        $data['start'] = $start;
        $data['to'] = $to;
        $data['total'] = 0;
        $data['total_page'] = 0;
        $data['limit_page'] = 50;
        if($start!=""&&$to!=""){
            $query = $this->db->get_where('transaction_log',array('created_time >='=>$start,'created_time <='=>$to,'type'=>'promotion'));
            $data['total'] = $query->num_rows();
            $total_page = ceil($data['total'] / $data['limit_page']);
            $data['total_page'] = $total_page;
            if($query->num_rows()>0){
                $result_data = $query->result();
                $stat_data = array();
                $id_list = array();
                foreach ($result_data as $item){
                    if(isset($stat_data[$item->content_id])){
                        $stat_data[$item->content_id]['total'] += $item->total;
                    } else{
                        $id_list[] = $item->content_id;
                        $stat_data[$item->content_id] = array('total'=>$item->total,'id'=>$item->content_id);
                    }
                }
                $this->db->select('id,title');
                $this->db->where_in('id', $id_list);
                $query = $this->db->get_where('promotion');
                $result_data = $query->result();
                foreach ($result_data as $item){
                    $stat_data[$item->id]['title'] = $item->title;
                }
                $temp_data = array();
                foreach ($stat_data as $item){
                    $temp_data[$item['total'].'.'.$item['id']] = $item;
                }
                krsort($temp_data);
                $data['data'] = $temp_data;
            }

        }

        $this->load->view('header',array('page_link' => 'stat_promotion', 'user_data' => $this->user_data));
        $this->load->view('stat_promotion',$data);
        $this->load->view('footer');
    }

    public function article(){
        $data = array();
        $start = ($this->input->get('start'))?$this->input->get('start'):"";
        $to = ($this->input->get('to'))?$this->input->get('to'):"";
        $data['start'] = $start;
        $data['to'] = $to;
        $data['total'] = 0;
        $data['total_page'] = 0;
        $data['limit_page'] = 50;
        if($start!=""&&$to!=""){
            $query = $this->db->get_where('transaction_log',array('created_time >='=>$start,'created_time <='=>$to,'type'=>'article'));
            $data['total'] = $query->num_rows();
            $total_page = ceil($data['total'] / $data['limit_page']);
            $data['total_page'] = $total_page;
            if($query->num_rows()>0){
                $result_data = $query->result();
                $stat_data = array();
                $id_list = array();
                foreach ($result_data as $item){
                    if(isset($stat_data[$item->content_id])){
                        $stat_data[$item->content_id]['total'] += $item->total;
                    } else{
                        $id_list[] = $item->content_id;
                        $stat_data[$item->content_id] = array('total'=>$item->total,'id'=>$item->content_id);
                    }
                }
                $this->db->select('id,title');
                $this->db->where_in('id', $id_list);
                $query = $this->db->get_where('article');
                $result_data = $query->result();
                foreach ($result_data as $item){
                    $stat_data[$item->id]['title'] = $item->title;
                }
                $temp_data = array();
                foreach ($stat_data as $item){
                    $temp_data[$item['total'].'.'.$item['id']] = $item;
                }
                krsort($temp_data);
                $data['data'] = $temp_data;
            }

        }


        $this->load->view('header',array('page_link' => 'stat_article', 'user_data' => $this->user_data));
        $this->load->view('stat_article',$data);
        $this->load->view('footer');
    }

    public function advertise(){
        $data = array();
        $start = ($this->input->get('start'))?$this->input->get('start'):"";
        $to = ($this->input->get('to'))?$this->input->get('to'):"";
        $data['start'] = $start;
        $data['to'] = $to;
        $data['total'] = 0;
        $data['total_page'] = 0;
        $data['limit_page'] = 50;
        if($start!=""&&$to!=""){
            $query = $this->db->get_where('transaction_log',array('created_time >='=>$start,'created_time <='=>$to,'type'=>'advertise'));
            $data['total'] = $query->num_rows();
            $total_page = ceil($data['total'] / $data['limit_page']);
            $data['total_page'] = $total_page;
            if($query->num_rows()>0){
                $result_data = $query->result();
                $stat_data = array();
                $id_list = array();
                foreach ($result_data as $item){
                    if(isset($stat_data[$item->content_id])){
                        $stat_data[$item->content_id]['total'] += $item->total;
                    } else{
                        $id_list[] = $item->content_id;
                        $stat_data[$item->content_id] = array('total'=>$item->total,'id'=>$item->content_id);
                    }
                }
                $this->db->where_in('id', $id_list);
                $query = $this->db->get_where('advertise');
                $result_data = $query->result();
                foreach ($result_data as $item){
                    $stat_data[$item->id]['cover_image_src'] = $item->cover_image_src;
                    $stat_data[$item->id]['business_id'] = $item->business_id;
                    $stat_data[$item->id]['start_time'] = $item->start_time;
                    $stat_data[$item->id]['end_time'] = $item->end_time;
                }
                $temp_data = array();
                foreach ($stat_data as $item){
                    $temp_data[$item['total'].'.'.$item['id']] = $item;
                }
                krsort($temp_data);
                $data['data'] = $temp_data;
            }

        }


        $this->load->view('header',array('page_link' => 'stat_advertise', 'user_data' => $this->user_data));
        $this->load->view('stat_advertise',$data);
        $this->load->view('footer');
    }

	public function get_stat(){
        $date_from = $this->input->get('date');
        $do = $this->input->get("do");
        if ($do == "getstat") {
            echo $this->get_stat_list($date_from);
        } else if ($do == "get_daily_stat") {
            $from_time = date("Y-m-d 00:00:00", strtotime("- 1 day"));
            $end_time = date("Y-m-d 23:59:59", strtotime("- 1 day"));

            $statData = $this->gen_stat($from_time, $end_time);
            $stat_data = $statData["total"] . $statData["avg"];

            $txt_html = '<table class="table table-striped table-bordered" border="0" style="width:100%">
            <thead><tr>
                <th bgcolor="#999999"></td>
                <th align="center" bgcolor="#999999" style="color:#FFF; text-align:center;">จำนวนผู้ใช้</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมสถานประกอบการ</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมบทความ</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมโปรโมชั่น</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมโฆษณา</th>
            </tr></thead>
            <tbody><tr>
                <td class="border_right" bgcolor="#EEE"></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>สมัครสมาชิกแล้ว</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนสถานประกอบการที่ถูกเข้าชม</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนบทความที่ถูกเข้าชม</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนโปรโมชั่นที่ถูกเข้าชม</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนโฆษณาที่ถูกเข้าชม</strong></td>
            </tr>
           ' . $stat_data . '
        </tbody></table>';

            echo $txt_html; exit;
        }
    }

    private function gen_stat($from_date, $to_date)
    {

        $list_user_id = array();
        $list_bus_id = array();
        $list_active_id = array();
        $txt_html = array();
        $txt_html_total = '';
        $txt_html_avg = '';
        $count_report = 0;
        $i = 0;
        $stat_7day = strtotime($from_date) - (86400 * 6);
        $stat_30day = strtotime($from_date) - (86400 * 29);
        $stat_90day = strtotime($from_date) - (86400 * 89);
        $stat_lifetime = strtotime($to_date);
        $created_time = date("Y-m-d H:i:s", strtotime($from_date));
        $title = date("Ymd",strtotime($created_time));
        $where = array('type' => 'daily','title'=> $title.'_daily');
        $this->db->where($where);
        $query = $this->db->get('stat');
        if($query->num_rows()>0){
            $count_report = 1;
            $result_data = $query->result();
            $stat_data = $result_data[0];
            $where = array('type' => 'lifetime','title'=> 'lifetime');
            $this->db->where($where);
            $query = $this->db->get('stat');
            if($query->num_rows()>0){
                $result_data = $query->result();
                $stat_lifetime_data = $result_data[0];
            }
        }else{
            $stat_data = array();
            $log_data = array();
            $viewed_num = array(
                'business' => 0,
                'promotion' => 0,
                'advertise' => 0,
                'article' => 0
            );
            $log_data = $viewed_num;
            $query = $this->db->get_where('transaction_log',array('created_time >='=>$from_date,'created_time <='=>$to_date));
            if($query->num_rows()>0){
                $result_data = $query->result();
                foreach ($result_data as $item){
                    if(isset($log_data[$item->type])){
                        $log_data[$item->type][$item->content_id] = (int)$item->total;
                        $viewed_num[$item->type] += (int)$item->total;
                    }else{
                        $log_data[$item->type] = array();
                        $log_data[$item->type][$item->content_id] = (int)$item->total;
                        $viewed_num[$item->type] += (int)$item->total;
                    }

                }
            }
            $query = $this->db->get_where('user',array('created_time >='=>$from_date,'created_time <='=>$to_date));
            $new_user = $query->num_rows();
            $stat_data = array(
              'title' => $title.'_daily',
              'type' => 'daily',
              'user' => $new_user,
              'reviewed_business' => 0,
              'reviewed_business_num' => 0,
              'viewed_business' => count($log_data['business']),
              'viewed_business_num' => $viewed_num['business'],
              'viewed_promotion' => count($log_data['promotion']),
              'viewed_promotion_num' => $viewed_num['promotion'],
              'viewed_article' => count($log_data['article']),
              'viewed_article_num' => $viewed_num['article'],
              'viewed_advertise' => count($log_data['advertise']),
              'viewed_advertise_num' => $viewed_num['advertise'],
              'created_time' => $created_time
            );
            $stat_lifetime_data = $stat_data;
            if($this->db->insert('stat', $stat_data)){
                $where = array('type' => 'lifetime','title'=> 'lifetime');
                $this->db->where($where);
                $query = $this->db->get('stat');
                if($query->num_rows()>0){
                    $result_data = $query->result();
                    $result_data = $result_data[0];
                    $stat_lifetime_data['title'] = 'lifetime';
                    $stat_lifetime_data['type'] = 'lifetime';
                    $stat_lifetime_data['user'] += $result_data->user;
                    $stat_lifetime_data['reviewed_business'] += $result_data->reviewed_business;
                    $stat_lifetime_data['reviewed_business_num'] += $result_data->reviewed_business_num;
                    $stat_lifetime_data['viewed_business'] += $result_data->viewed_business;
                    $stat_lifetime_data['viewed_business_num'] += $result_data->viewed_business_num;
                    $stat_lifetime_data['viewed_promotion'] += $result_data->viewed_promotion;
                    $stat_lifetime_data['viewed_promotion_num'] += $result_data->viewed_promotion_num;
                    $stat_lifetime_data['viewed_article'] += $result_data->viewed_article;
                    $stat_lifetime_data['viewed_article_num'] += $result_data->viewed_article_num;
                    $stat_lifetime_data['viewed_advertise'] += $result_data->viewed_advertise;
                    $stat_lifetime_data['viewed_advertise_num'] += $result_data->viewed_advertise_num;
                    $this->db->where(array('title'=>'lifetime'));
                    $this->db->update('stat', $stat_lifetime_data);
                }else{
                    $stat_data['title'] = 'lifetime';
                    $stat_data['type'] = 'lifetime';
                    $this->db->insert('stat', $stat_data);
                }
            }
        }
//        console_log($count_report);console_log($stat_data); die;
        $from_date = date("Y-m-d 00:00:00", strtotime("- 90 day"));
        $to_date = date("Y-m-d 23:59:59", strtotime("- 1 day"));
        $where = array('type' => 'daily','created_time >='=>$from_date,'created_time <='=>$to_date);
        $this->db->where($where);
        $this->db->order_by('created_time','DESC');
        $query = $this->db->get('stat');

        $stat_data_daily = array(
            'user' => 0,
            'reviewed_business' => 0,
            'reviewed_business_num' => 0,
            'viewed_business' => 0,
            'viewed_business_num' => 0,
            'viewed_promotion' => 0,
            'viewed_promotion_num' => 0,
            'viewed_article' => 0,
            'viewed_article_num' => 0,
            'viewed_advertise' => 0,
            'viewed_advertise_num' => 0
        );
        $stat_data_7_data = $stat_data_daily;
        $stat_data_30_data = $stat_data_daily;
        $stat_data_90_data = $stat_data_daily;
        $i = 1;
        if($query->num_rows()>0){
            $result_data = $query->result();

            foreach ($result_data as $item){
                if($i==1){
                    $stat_data_daily['user'] += $item->user;
                    $stat_data_daily['reviewed_business'] += $item->reviewed_business;
                    $stat_data_daily['reviewed_business_num'] += $item->reviewed_business_num;
                    $stat_data_daily['viewed_business'] += $item->viewed_business;
                    $stat_data_daily['viewed_business_num'] += $item->viewed_business_num;
                    $stat_data_daily['viewed_promotion'] += $item->viewed_promotion;
                    $stat_data_daily['viewed_promotion_num'] += $item->viewed_promotion_num;
                    $stat_data_daily['viewed_article'] += $item->viewed_article;
                    $stat_data_daily['viewed_article_num'] += $item->viewed_article_num;
                    $stat_data_daily['viewed_advertise'] += $item->viewed_advertise;
                    $stat_data_daily['viewed_advertise_num'] += $item->viewed_advertise_num;
                    $stat_data_7_data = $stat_data_daily;
                    $stat_data_30_data = $stat_data_daily;
                    $stat_data_90_data = $stat_data_daily;
                }else if($i<=7){
                    $stat_data_7_data['user'] += $item->user;
                    $stat_data_7_data['reviewed_business'] += $item->reviewed_business;
                    $stat_data_7_data['reviewed_business_num'] += $item->reviewed_business_num;
                    $stat_data_7_data['viewed_business'] += $item->viewed_business;
                    $stat_data_7_data['viewed_business_num'] += $item->viewed_business_num;
                    $stat_data_7_data['viewed_promotion'] += $item->viewed_promotion;
                    $stat_data_7_data['viewed_promotion_num'] += $item->viewed_promotion_num;
                    $stat_data_7_data['viewed_article'] += $item->viewed_article;
                    $stat_data_7_data['viewed_article_num'] += $item->viewed_article_num;
                    $stat_data_7_data['viewed_advertise'] += $item->viewed_advertise;
                    $stat_data_7_data['viewed_advertise_num'] += $item->viewed_advertise_num;
                    $stat_data_30_data = $stat_data_7_data;
                    $stat_data_90_data = $stat_data_7_data;
                }else if($i<=30){
                    $stat_data_30_data['user'] += $item->user;
                    $stat_data_30_data['reviewed_business'] += $item->reviewed_business;
                    $stat_data_30_data['reviewed_business_num'] += $item->reviewed_business_num;
                    $stat_data_30_data['viewed_business'] += $item->viewed_business;
                    $stat_data_30_data['viewed_business_num'] += $item->viewed_business_num;
                    $stat_data_30_data['viewed_promotion'] += $item->viewed_promotion;
                    $stat_data_30_data['viewed_promotion_num'] += $item->viewed_promotion_num;
                    $stat_data_30_data['viewed_article'] += $item->viewed_article;
                    $stat_data_30_data['viewed_article_num'] += $item->viewed_article_num;
                    $stat_data_30_data['viewed_advertise'] += $item->viewed_advertise;
                    $stat_data_30_data['viewed_advertise_num'] += $item->viewed_advertise_num;
                    $stat_data_90_data = $stat_data_30_data;
                }else if($i<=90){
                    $stat_data_90_data['user'] += $item->user;
                    $stat_data_90_data['reviewed_business'] += $item->reviewed_business;
                    $stat_data_90_data['reviewed_business_num'] += $item->reviewed_business_num;
                    $stat_data_90_data['viewed_business'] += $item->viewed_business;
                    $stat_data_90_data['viewed_business_num'] += $item->viewed_business_num;
                    $stat_data_90_data['viewed_promotion'] += $item->viewed_promotion;
                    $stat_data_90_data['viewed_promotion_num'] += $item->viewed_promotion_num;
                    $stat_data_90_data['viewed_article'] += $item->viewed_article;
                    $stat_data_90_data['viewed_article_num'] += $item->viewed_article_num;
                    $stat_data_90_data['viewed_advertise'] += $item->viewed_advertise;
                    $stat_data_90_data['viewed_advertise_num'] += $item->viewed_advertise_num;
                }
                $i++;
            }
        }
        $stat_day = array();
        $stat_day['daily'] = date("d/m/Y", strtotime('-1 day'));
        $stat_day['last7day'] = '7 วันล่าสุด';
        $stat_day['last30day'] = '30 วันล่าสุด';
        $stat_day['last90day'] = '90 วันล่าสุด';
        $stat_day['lifetime'] = 'ทั้งหมดที่ผ่านมา';

        $created_date = date('Y-m-d 00:00:00');
        $i = 0;
        foreach ($stat_day as $key => $item) {
            //var_dump($key);
            $i++;
            $subject = $item;

            if (($i % 2) == 0) {
                $color = "EDEDED";
            } else {
                $color = "F8F8F8";
            }
            if($key=="daily"){
                $stat_data = $stat_data_daily;
            }else if($key=="last7day"){
                $stat_data = $stat_data_7_data;
            }else if($key=="last30day"){
                $stat_data = $stat_data_30_data;
            }else if($key=="last90day"){
                $stat_data = $stat_data_90_data;
            }else if($key=="lifetime"){
                $stat_data = $stat_lifetime_data;
            }
            if(!is_array($stat_data)){
                $stat_data = json_encode($stat_data,true);
                $stat_data = json_decode($stat_data,true);
            }
            $txt_html_total .= '
            <tr>
                <td bgcolor="#' . $color . '" style="width:100px;"><strong style="font-size:10px;">' . $subject . '</strong></td>
                
                <td class="border_right" align="center" bgcolor="#' . $color . '">' . $stat_data['user'] . '</td>

                <td align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_business_num'] . '</td>
                <td class="border_right" align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_business'] . '</td>

                <td align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_article_num'] . '</td>
                <td class="border_right" align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_article'] . '</td>

                <td align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_promotion_num'] . '</td>
                <td class="border_right" align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_promotion'] . '</td>

                <td align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_advertise_num'] . '</td>
                <td class="border_right" align="center" bgcolor="#' . $color . '">' . $stat_data['viewed_advertise'] . '</td>

            </tr>
            ';
        }


        $txt_html['total'] = $txt_html_total;
        $txt_html['avg'] = $txt_html_avg;
        return $txt_html;

    }

    private function get_stat_list($datetime)
    {
        if (strlen($datetime) <= 4) {
            $datetime = $datetime . "-01-01 00:00:00";
            $datetime = strtotime($datetime);
            $stat_id = (int)date("Ym00", $datetime);
            $created_date = strtotime(date("Y-m-d", $datetime));

//            $find_date = new MongoDate($datetime);
            $all_year = true;
            $max_date = date("Y-01-01 00:00:00", strtotime("+1 year", $datetime));
            $max_date = strtotime($max_date);
            $stat_id_next = (int)date("Ym00", $max_date);
            $min_date = $datetime;
            $stat_type = 'yearly';
        } else {
            $datetime = strtotime($datetime);
            $stat_id = (int)date("Ym00", $datetime);
            $created_date = strtotime(date("Y-m-d", $datetime));

//            $find_date = new MongoDate($datetime);
            $all_year = false;
            $max_date = date("Y-m-01 00:00:00", strtotime("+1 month", $datetime));
            $max_date = strtotime($max_date);
            $stat_id_next = (int)date("Ym00", $max_date);
            $min_date = $datetime;
            $stat_type = 'monthly';
        }

        $sum_user_total = 0;
        $sum_review = 0;
        $sum_review_tip = 0;
        $sum_tip = 0;
        $sum_photo = 0;
        $sum_join = 0;
        $sum_ios = 0;
        $sum_android = 0;
        $sum_web = 0;
        $sum_bus = 0;
        $sum_active = 0;
        $sum_checkin = 0;
        $sum_shared = 0;
        $sum_bookmark = 0;
        $sum_like = 0;
        $sum_comment = 0;
        $where = array('_id' => $stat_id, 'type' => $stat_type);

        $stat_data = null;
        $count_report = 0;
        if (isset($stat_data) && is_array($stat_data)) {
            $count_report = 1;
        }

        if ($count_report == 0 || isset($stat_data) == false || is_array($stat_data) == false) {

            $txt_html = '<table class="table table-striped table-bordered " style="width:100%">
            <thead><tr>
                <th bgcolor="#999999"></td>
                <th align="center" bgcolor="#999999" style="color:#FFF; text-align:center;">จำนวนผู้ใช้</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมสถานประกอบการ</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมบทความ</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมโปรโมชั่น</th>
                <th align="center" bgcolor="#999999" colspan="2" style="color:#FFF; text-align:center;">เข้าชมโฆษณา</th>
            </tr></thead>
            <tbody><tr>
                <td class="border_right" bgcolor="#EEE"></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>สมัครสมาชิกแล้ว</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนสถานประกอบการที่ถูกเข้าชม</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนบทความที่ถูกเข้าชม</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนโปรโมชั่นที่ถูกเข้าชม</strong></td>
                
                <td align="center" bgcolor="#EEE" ><strong>จำนวนเข้าชม</strong></td>
                <td class="border_right" align="center" bgcolor="#EEE" ><strong>จำนวนโฆษณาที่ถูกเข้าชม</strong></td>
            </tr>';

            $i = 0;
            $today = date("Y-m-d 00:00:00", strtotime("+1 day"));
            while ($min_date < $max_date) {
                $join_num = 0;
                $ios_num = 0;
                $web_num = 0;
                $android_num = 0;
                $user_total_num = 0;
                $active_num = 0;
                $review_num = 0;
                $tip_num = 0;
                $review_tip_num = 0;
                $checkin_num = 0;
                $share_num = 0;
                $bookmark_num = 0;
                $photo_num = 0;
                $photo_bus_num = 0;
                $like_num = 0;
                $comment_num = 0;

                if ($all_year == false) {
                    $daily_stat_id = (int)date('Ymd', $min_date);
                    $show_date = date("j", $min_date);
                    $min_date2 = date("Y-m-d 00:00:00", strtotime("+1 day", $min_date));
                    $min_date2 = strtotime($min_date2);

                    $daily_stat_data = array();
                    if (isset($daily_stat_data['daily']) && is_array($daily_stat_data['daily'])) {
                        $daily_data = $daily_stat_data['daily'];

                        $join_num = $daily_data['join'];
                        $ios_num = $daily_data['ios'];
                        $web_num = $daily_data['web'];
                        $android_num = $daily_data['android'];
                        $active_num = $daily_data['active'];
                        $user_total_num = $daily_data['user_total'];
                        $review_num = $daily_data['review'];
                        $tip_num = $daily_data['tip'];
                        $review_tip_num = $daily_data['review_tip'];
                        $checkin_num = $daily_data['checkin'];
                        $share_num = $daily_data['share'];
                        $bookmark_num = $daily_data['bookmark'];
                        $photo_num = $daily_data['photo'];
                        $photo_bus_num = $daily_data['photo_bus'];
                        $like_num = $daily_data['like'];
                        $comment_num = $daily_data['comment'];

                        $sum_join += $join_num;
                        $sum_ios += $ios_num;
                        $sum_android += $android_num;
                        $sum_web += $web_num;
                        $sum_active += $active_num;
                        $sum_user_total = $user_total_num;
                        $sum_review += $review_num;
                        $sum_tip += $tip_num;
                        $sum_review_tip += $review_tip_num;
                        $sum_checkin += $checkin_num;
                        $sum_shared += $share_num;
                        $sum_bookmark += $bookmark_num;
                        $sum_photo += $photo_num;
                        $sum_bus += $photo_bus_num;
                        $sum_like += $like_num;
                        $sum_comment += $comment_num;
                    }
                } else {
                    $m_thai = array(
                        '',
                        "มกราคม",
                        "กุมภาพัน",
                        "มีนาคม",
                        "เมษายน",
                        "พฤษพาคม",
                        "มิถุนายน",
                        "กรกฎาคม",
                        "สิงหาคม",
                        "กันยายน",
                        "ตุลาคม",
                        "พฤศจิกายน",
                        "ธันวาคม",
                    );
                    $daily_stat_id = (int)date('Ymd', $min_date);
                    $show_date = (int)date("n", $min_date);
                    $show_date = $m_thai[$show_date];
                    $min_date2 = date("Y-m-01 00:00:00", strtotime("+1 month", $min_date));
                    $min_date2 = strtotime($min_date2);
                    $daily_stat_id_next = (int)date('Ymd', $min_date2);

                }
                if (($i % 2) == 0) {
                    $color = "F8F8F8";
                } else {

                    $color = "EDEDED";
                }
                $txt_html .= '<tr onmouseout="this.style.backgroundColor=\'#F8F8F8\';" onmouseover="this.style.backgroundColor=\'#EEEEEE\';" bgcolor="#F8F8F8">
                            <td bgcolor="#' . $color . '" id="date_' . $i . '" class="border_right" align="center">' . $show_date . '</td>
                            <td bgcolor="#' . $color . '" id="total_' . $i . '" class="border_right"" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="checkin_' . $i . '" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="shared_' . $i . '" class="border_right" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="checkin_' . $i . '" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="shared_' . $i . '" class="border_right" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="checkin_' . $i . '" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="shared_' . $i . '" class="border_right" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="checkin_' . $i . '" align="center" >' . 0 . '</td>
                            <td bgcolor="#' . $color . '" id="shared_' . $i . '" class="border_right" align="center" >' . 0 . '</td>
                        </tr>';

                $min_date = $min_date2;

                $i++;
            }

        }
        if (($i % 2) == 0) {
            $color = "F8F8F8";
        } else {

            $color = "EDEDED";
        }
        $txt_html .= '<tr>
                        <td bgcolor="#' . $color . '" class="border_right" align="center" ><strong>รวม</strong></td>
                        <td bgcolor="#' . $color . '" class="border_right" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" class="border_right" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" class="border_right" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" class="border_right" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" align="center" ><strong>' . 0 . '</strong></td>
                        <td bgcolor="#' . $color . '" class="border_right" align="center" ><strong>' . 0 . '</strong></td>
                    </tr>';
        $txt_html .= '<tbody></table>';
        return $txt_html;
    }

    private function DateDiff($strDate1, $strDate2)
    {
        return ($strDate2 - $strDate1) / (60 * 60 * 24); // 1 day = 60*60*24
    }
}
