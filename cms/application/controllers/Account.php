<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }


    public function index()
	{

        $account_id = $this->user_data['id'];
        $this->db->order_by('id','DESC');
        $this->db->select('username,email,type,name,id,tel');
        $query = $this->db->get_where('staff',array('id'=>$account_id,'status !='=> 'deleted'));

        $account_info = $query->result();
        $data = array();
        $data['account_info'] = $account_info[0];
		$this->load->view('header',array('page_link' => 'account', 'user_data' => $this->user_data));
		$this->load->view('account',$data);
		$this->load->view('footer');
	}

    public function update(){
        $do = $this->input->post('do');
        if($do=="user_info"){
            $this->edit_staff();
        }else if($do=="password"){
            $this->change_password();
        }
    }

    private function edit_staff(){
        $now_date = date('Y-m-d H:i:s');
        $email_staff = $this->input->post('email_staff');
        $name_staff = $this->input->post('name_staff');
        $tel_staff = $this->input->post('tel_staff');
        $id = $this->user_data['id'];
        $update_data = array(
            'email'=>  $email_staff,
            'name'=>  $name_staff,
            'tel'=>  $tel_staff,
            'updated_date'=>  $now_date
        );
        $this->db->where('id', $id);
        if ($this->db->update('staff', $update_data)) {
            redirect(site_url('account?success=1'));
        }
    }

    private function change_password(){
        $now_date = date('Y-m-d H:i:s');
        $new_pass = $this->input->post('new_pass');
        $new_pass2 = $this->input->post('new_pass2');
        $old_pass = $this->input->post('old_pass');
        $old_pass = hash("sha512", $old_pass, false);
        $this->db->select('password,id');
        $id = $this->user_data['id'];
        $query = $this->db->get_where('staff',array('id'=>$id,'password'=>$old_pass,'status !='=> 'deleted'));
        if($query->num_rows()>0){
            $account_info = $query->result();
            $account_info = $account_info[0];
            if($account_info->password!=$old_pass){
                redirect(site_url('account?error=1'));
            }else if($new_pass!=$new_pass2){
                redirect(site_url('account?error=2'));
            }else{
                $new_pass = hash("sha512", $new_pass, false);
                $update_data = array(
                    'password'=>  $new_pass,
                    'updated_date'=>  $now_date
                );
                $this->db->where('id', $id);
                if ($this->db->update('staff', $update_data)) {
                    redirect(site_url('account?success=1'));
                }
            }
        }else{
            redirect(site_url('account?error=1'));
        }
    }

}
