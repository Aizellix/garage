<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advertise extends CI_Controller{
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index(){
        $page = $this->input->get('p');
        if(!$page) $page=1;
        $status = ($this->input->get('status'))?$this->input->get('status'):false;
        if(!$page) $page=1;

        if($status=="live"){
            $where = array('start_time <='=>date('Y-m-d H:i:s'), 'end_time >='=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }elseif($status=="waiting"){
            $where = array('start_time >'=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }elseif($status=="expired"){
            $where = array('end_time <'=>date('Y-m-d H:i:s'),'status !='=> 'deleted');
        }else{
            $where = array('status !='=> 'deleted');
        }
        $this->db->from('advertise');
        $this->db->where($where);
        $total_num = $this->db->count_all_results();
        $limit = 20;
        $offset = ($page - 1) * $limit;

        $this->db->order_by('id','ASC');
        $query = $this->db->get_where('advertise',$where,$limit,$offset);

        $business_id_list = array();
        $advertise_list = $query->result();
        $current_time = time();
        $live_data = array();
        $waiting_data = array();
        $expired_data = array();
        foreach ($advertise_list as $list){
            $business_id_list[] = $list->business_id;

            $start_time = strtotime($list->start_time);
            $end_time = strtotime($list->end_time);
            if($current_time>=$start_time&&$current_time<=$end_time){
                $live_data[] = $list;
            }else if($current_time<$start_time){
                $waiting_data[] = $list;
            }else{
                $expired_data[] = $list;
            }
        }
        $advertise_list = array_merge($live_data, $waiting_data, $expired_data);

        $business_ist = array();
        if(count($business_id_list)>0){
            $this->db->where_in('id',$business_id_list);
            $query = $this->db->get_where('business');
            $business_data = $query->result();
            foreach ($business_data as $list){
                $business_ist[$list->id] = $list;
            }
        }

        $data = array();
        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['limit'] = $limit;
        $data['page'] = $page;
        $data['advertise_list'] = $advertise_list;
        $data['business_list'] = $business_ist;
        $this->load->view('header',array('page_link' => 'advertise', 'user_data' => $this->user_data));
        $this->load->view('advertise',$data);
        $this->load->view('footer');
    }

    public function view($id){
        $total_num = $this->db->count_all_results();
        $query = $this->db->get_where('advertise',array('id'=>$id,'status !='=> 'deleted'));
        $business_id_list = array();
        $advertise_list = $query->result();
        foreach ($advertise_list as $list){
            $business_id_list[] = $list->business_id;
        }
        $business_ist = array();
        if(count($business_id_list)>0){
            $this->db->where_in('id',$business_id_list);
            $query = $this->db->get_where('business');
            $business_data = $query->result();
            foreach ($business_data as $list){
                $business_ist[$list->id] = $list;
            }
        }

        $data = array();
        $data['total_num'] = $query->num_rows();
        $data['advertise_list'] = $advertise_list;
        $data['business_list'] = $business_ist;
        $this->load->view('header',array('page_link' => 'advertise', 'user_data' => $this->user_data));
        $this->load->view('advertise',$data);
        $this->load->view('footer');
    }

    public function update(){
        $do = $this->input->post('do');
//        var_dump($_POST);
        if($do=='add'){
            $business_id = $this->input->post('business_id');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $created_time = date('Y-m-d H:i:s');
            $update_data = array(
                'business_id'=> $business_id,
                'start_time'=> $start_time,
                'end_time'=> $end_time,
                'created_time'=> $created_time,
                'updated_time'=> $created_time,
                'status'=> 'active'
            );
            if ($this->db->insert('advertise', $update_data)) {
                $id = $this->db->insert_id();
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'add','advertise');
                $cover_image = $this->update_photo($id,'advertise','cover');
                $update_data = array(
                    'cover_image_id'=> $cover_image['id'],
                    'cover_image_src'=> $cover_image['src']
                );
                $this->db->where('id', $id);
                if ($this->db->update('advertise', $update_data)) {
                    redirect(site_url('advertise'));
                }else{
                    redirect(site_url('advertise'));
                }
            }

        }elseif ($do=='edit'){
            $id = $this->input->post('id');
            $business_id = $this->input->post('business_id');
            $start_time = $this->input->post('start_time');
            $end_time = $this->input->post('end_time');
            $image_id= $this->input->post('image_id');
            $updated_time = date('Y-m-d H:i:s');
            $this->db->where('id', $id);
            $update_data = array(
                'business_id'=> $business_id,
                'start_time'=> $start_time,
                'end_time'=> $end_time,
                'updated_time'=> $updated_time,
            );
            if($_FILES['cover']['tmp_name']!=""){
                if($this->del_photo($image_id)){
                    $cover_image = $this->update_photo($id,'advertise','cover');
                    $update_data['cover_image_id'] = $cover_image['id'];
                    $update_data['cover_image_src '] = $cover_image['src'];
                }
            }
            $this->db->where('id', $id);
            if ($this->db->update('advertise', $update_data)) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'edit','advertise');
                redirect(site_url('advertise'));
            }
        }elseif ($do=='del'){
            $id = $this->input->post('business_id');
            $this->db->where('id', $id);
            if ($this->db->update('advertise', array('status'=>'deleted'))) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'del','advertise');
                redirect(site_url('advertise'));
            }
        }
        redirect(site_url('advertise'));

    }

    public function get_modifier($id){
        $query = $this->db->get_where('history_log',array('content_id'=>$id,'type'=>'advertise'));
        if($query->num_rows()>0){
            $result_data = $query->result();
            $data_list[$result_data[0]->status] = $result_data[0];
        }else{
            $data_list = array();
        }
        print_json($data_list);
    }

    private function del_photo($id){
        $this->db->where('id', $id);
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }else{
            return false;
        }
    }

    private function update_photo($ref_id)
    {
        $POST_DATA = array();
        $img_num = count($_FILES['cover']['tmp_name']);
        $tmp_file = $_FILES['cover']['tmp_name'];
        $file_name = basename($_FILES['cover']['name']);
        $file_type = $_FILES['cover']['type'];
        $POST_DATA['upload_img'] = curl_file_create($tmp_file, $file_type, $file_name);
        $POST_DATA['img_num'] = $img_num;
        $POST_DATA['depot_key'] = $this->config->item('depot_key');
        $POST_DATA['img_type'] = 'article';
        $POST_DATA['ref_id'] = $ref_id;
        $curl_handle = curl_init($this->config->item('depot_url').'home/upload_image');
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

        $returned_data = curl_exec($curl_handle);
        curl_close($curl_handle);

        $returned_data = json_decode($returned_data, true);
        return $returned_data['result'];
    }
}
