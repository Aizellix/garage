<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {
    public $user_data;
    public $curl;
    public $api_key;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
        $this->api_key = $this->config->item('api_key');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

	public function index()
	{
        $data = array();


        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
		$this->load->view('home',$data);
		$this->load->view('footer');
	}

    public function cal_all_review_score(){
        $data = array();
        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('review');
        $data['review_total'] =  $query->num_rows();
        $review_data = $query->result();
        $business_list = array();
        foreach ($review_data as $item){
            if(isset($business_list[$item->business_id])){
                if(update_review_score($item->business_id,$item->score,'add')){
                    $business_list[$item->business_id].=','.$item->score;
                }
            }else{
                if(update_review_score($item->business_id,$item->score,'reset')){
                    $business_list[$item->business_id] = $item->score;
                }

            }

        }
        $review_data = $review_data[0];
        $data['review_data'] = $review_data;
        $data['business_list'] = $business_list;

        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
        $this->load->view('cal_all_review_score',$data);
        $this->load->view('footer');
    }

    public function gen_business_code(){
        $data = array();
        $this->db->where('status !=', 'deleted');
        $this->db->where('business_code', '');
        $query = $this->db->get('business');
        $business_data = $query->result();
        echo '<pre>';
        foreach ($business_data as $item){
            $business_id = $item->id;
            $business_code = create_business_code($business_id);
            $this->db->where('id', $business_id);
            $this->db->where('status !=', "deleted");
            $this->db->update('business', array("business_code"=>$business_code));
            $business_list[$item->id] = 1;
            var_dump($business_id,$business_code);
        }
    }

    public function list_business_code(){
        $data = array();
        $this->db->where('status !=', 'deleted');
        $this->db->where('business_code !=', '');
        $query = $this->db->get('business');
        $business_data = $query->result();
        echo '<pre>';
        foreach ($business_data as $item){
            $business_code = $item->business_code;
            $business_id = decode_business_code($business_code);
            var_dump(array($item->id,$business_code,$business_id));
        }
    }

    public function decode_business_code($business_code){
        echo decode_business_code($business_code);
    }

    public function create_business_code($business_id){
        echo create_business_code($business_id);
    }

    public function cal_all_user_review(){
        $data = array();
        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('review');
        $data['review_total'] =  $query->num_rows();
        $review_data = $query->result();
        $user_list = array();
        foreach ($review_data as $item){
            if(isset($user_list[$item->user_id])){
                if(update_review_user($item->user_id,'add')){
                    $user_list[$item->user_id].=','.$item->score;
                }
            }else{
                if(update_review_user($item->user_id,'reset')){
                    $user_list[$item->user_id] = $item->score;
                }

            }
        }
        $review_data = $review_data[0];
        $data['review_data'] = $review_data;
        $data['business_list'] = $user_list;

        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
        $this->load->view('cal_all_review_score',$data);
        $this->load->view('footer');
    }

    public function clean_data_of_deleted_business(){
        $data = array();
        $this->db->where('status', 'deleted');
        $query = $this->db->get('business');
        $data['review_total'] =  $query->num_rows();
        $business_data = $query->result();
        $business_list = array();
        foreach ($business_data as $item){
            $business_id = $item->id;
            $this->db->where('business_id', $business_id);
            $this->db->where('status !=', "deleted");
            $this->db->update('review', array("status"=>"deleted"));
            $business_list[$item->id] = 1;

        }
        $review_data = $business_data[0];
        $data['review_data'] = $review_data;
        $data['business_list'] = $business_list;

        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
        $this->load->view('cal_all_review_score',$data);
        $this->load->view('footer');
    }

    public function update_user_review(){
        $data = array();
        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('user');
        $data['review_total'] =  $query->num_rows();
        $user_data = $query->result();
        $user_list = array();
        $business_list = array();
        foreach ($user_data as $item){
            $user_id = $item->id;
            $this->db->where('user_id', $user_id);
            $this->db->where('status !=', "deleted");
            $review_query = $this->db->get('review');
            if($review_query->num_rows()>0){
                $user_list[] = $item->id;
            }
            $business_list[$user_id] = 1;
        }
        if(count($user_list)>0){
            $this->db->where_not_in('id ', $user_list);
        }
        $this->db->where('status !=', "deleted");
        $this->db->update('user', array("reviewed"=>0));

        $review_data = $user_data[0];
        $data['review_data'] = $review_data;
        $data['business_list'] = $business_list;

        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
        $this->load->view('cal_all_review_score',$data);
        $this->load->view('footer');
    }
    public function update_business_review(){
        $data = array();
        $this->db->where('status !=', 'deleted');
        $query = $this->db->get('business');
        $data['review_total'] =  $query->num_rows();
        $user_data = $query->result();
        $user_list = array();
        $business_list = array();
        foreach ($user_data as $item){
            $user_id = $item->id;
            $this->db->where('business_id', $user_id);
            $this->db->where('status !=', "deleted");
            $review_query = $this->db->get('review');
            if($review_query->num_rows()>0){
                $user_list[] = $item->id;
            }
            $business_list[$user_id] = 1;
        }
        if(count($user_list)>0){
            $this->db->where_not_in('id ', $user_list);
        }
        $this->db->where('status !=', "deleted");
        $this->db->update('business', array("reviewed"=>0));

        $review_data = $user_data[0];
        $data['review_data'] = $review_data;
        $data['business_list'] = $business_list;

        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
        $this->load->view('cal_all_review_score',$data);
        $this->load->view('footer');
    }

    public function clean_deleted_business_related_data(){
        $data = array();
        $this->db->where('status', 'deleted');
        $query = $this->db->get('business');
        $data['review_total'] =  $query->num_rows();
        $review_data = $query->result();
        $business_list = array();
        foreach ($review_data as $item){
            $business_id = $item->id;
            $business_list[$business_id] = 1;
            $query = $this->db->get_where('review',array('business_id'=>$business_id,'status'=>'active'));
            if($query->num_rows()>0){
                $result = $query->result();
                foreach ($result as $item) {
                    var_dump($item->id);
                    $post_data = array(
                        'api_key' => $this->api_key,
                        'business_id' => $business_id,
                        'review_id' => $item->id,
                        'user_id' => $item->user_id
                    );
                    console_log($post_data);
                    $respond = json_decode($this->curl->simple_post(api_site_url('review/delete_review/'),$post_data));
                    console_log($respond);
                }
            }
            $this->db->where('id', $business_id);
            if ($this->db->update('business', array('status'=>'deleted'))) {
                $this->del_all_photo($business_id);
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$business_id,'del','business');
            }
        }
        $review_data = $review_data[0];
        $data['review_data'] = $review_data;
        $data['business_list'] = $business_list;

        $this->load->view('header',array('page_link' => '', 'user_data' => $this->user_data));
        $this->load->view('cal_all_review_score',$data);
        $this->load->view('footer');
    }

    private function del_all_photo($id){
        $this->db->where('ref_id', $id);
        $this->db->where('type', "business");
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }else{
            return true;
        }
    }
}
