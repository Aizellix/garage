<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emergency extends CI_Controller{
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index(){

        $total_num = $this->db->count_all_results();
        $this->db->order_by('type','ASC');
        $this->db->order_by('order_number','ASC');
        $this->db->order_by('title','ASC');
        $query = $this->db->get_where('emergency',array('status !='=> 'deleted'));

        $business_list = $query->result();
        $data = array();

        $data['total_num'] = $query->num_rows();
        $data['emergency_list'] = $business_list;
        $this->load->view('header',array('page_link' => 'emergency', 'user_data' => $this->user_data));
        $this->load->view('emergency',$data);
        $this->load->view('footer');
    }

    public function update(){
        $do = $this->input->post('do');
//        var_dump($_POST);
        if($do=='add'){
            $title = $this->input->post('title');
            $tel = $this->input->post('tel');
            $type = $this->input->post('type');
            $order_number= $this->input->post('order_number');
            $created_time = date('Y-m-d H:i:s');
            $update_data = array(
                'title'=> $title,
                'tel'=> $tel,
                'type'=> $type,
                'order_number'=> $order_number,
                'created_time'=> $created_time,
                'updated_time'=> $created_time,
                'status'=> 'active'
            );
            if ($this->db->insert('emergency', $update_data)) {
                redirect(site_url('emergency'));
            }
        }elseif ($do=='edit'){
            $title = $this->input->post('title');
            $tel = $this->input->post('tel');
            $type = $this->input->post('type');
            $id = $this->input->post('id');
            $order_number= $this->input->post('order_number');
            $updated_time = date('Y-m-d H:i:s');
            $this->db->where('id', $id);
            $update_data = array(
              'title'=> $title,
              'tel'=> $tel,
              'order_number'=> $order_number,
              'type'=> $type,
              'updated_time'=> $updated_time,
            );
            $this->db->where('id', $id);
            if ($this->db->update('emergency', $update_data)) {
                redirect(site_url('emergency'));
            }
        }elseif ($do=='del'){
            $id = $this->input->post('id');
            $this->db->where('id', $id);
            if ($this->db->update('emergency', array('status'=>'deleted'))) {
                redirect(site_url('emergency'));
            }
        }
        redirect(site_url('emergency'));

    }
}
