<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index()
	{
	    if($this->user_data['permission']!="admin"){
	        redirect(site_url('home'));
        }
        $this->db->order_by('id','DESC');
        $query = $this->db->get_where('staff',array('status !='=> 'deleted'));

        $staff_list = $query->result();
        $data = array();

        $data['total_num'] = $query->num_rows();
        $data['staff_list'] = $staff_list;

		$this->load->view('header',array('page_link' => 'staff', 'user_data' => $this->user_data));
		$this->load->view('staff',$data);
		$this->load->view('footer');
	}

    public function view($content_id){

        if(!$content_id){
            redirect(site_url('business'));
        }else{
            echo 'View '.$content_id;
        }
    }

    public function update(){
        $do = $this->input->post('do');
        if($do=="add"){
            $this->add_staff();
        }else if($do=="edit"){
            $this->edit_staff();
        }else if($do=="del"){
            $this->del_staff();
        }
    }

    private function add_staff(){
        $now_date = date('Y-m-d H:i:s');
        $email_staff = $this->input->post('email_staff');
        $name_staff = $this->input->post('name_staff');
        $tel_staff = $this->input->post('tel_staff');
        $user_staff = $this->input->post('user_staff');
        $type_staff = $this->input->post('type_staff');
        $password =$this->password_generation();
        $hashed_password = hash("sha512", $password, false);
        $update_data = array(
          'username'=>  $user_staff,
          'email'=>  $email_staff,
          'name'=>  $name_staff,
          'tel'=>  $tel_staff,
          'password'=>  $hashed_password,
          'created_date'=>  $now_date,
          'updated_date'=>  $now_date,
          'type'=>  $type_staff,
          'status'=>  'active'
        );
        if ($this->db->insert('staff', $update_data)) {
            $sent_to = $email_staff;
            $send_from = 'noreply@pueanrod.com';
            $sender_name= 'Pueanrod.com';
            $mail_subject = 'New Staff Password - Pueanrod.com Back Office';
            $msg = '<div style="padding: 20px; background-color: #2466AF;">
                    <div><span style="font-size: 36px; color:#fff;">Pueanrod.com Back Office System</span></div>
                    <div style="padding: 20px; background-color: #FFF;">
                    <div><p style="padding: 16px;">ผู้ดูแลระบบ Pueanrod.com Back Office System ได้ทำการสร้างบัญชีผู้ใช้งานในประเภท '.$type_staff.' ให้กับคุณโดยได้ลงทะเบียนโดยชื่อบัญชีอีเมลนี้
                    เพื่อทำการส่งรหัสผ่านที่ใช้ในการเข้าสู่ระบบให้กับคุณ คุณสามารถเปลี่ยนรหัสผ่านใหม่ได้ด้วยตนเองเมื่อเข้าสู่ระบบแล้ว</p>
                    <div style="padding: 14px;">รหัสผ่านของคุณคือ "<strong>'.$password.'</strong>"</div>
                    </div>
                    </div>
                    </div>';
            echo send_email($sent_to,$send_from,$sender_name,$mail_subject,$msg);

            redirect(site_url('staff'));
        }
    }

    private function edit_staff(){
        $now_date = date('Y-m-d H:i:s');
        $email_staff = $this->input->post('email_staff');
        $name_staff = $this->input->post('name_staff');
        $tel_staff = $this->input->post('tel_staff');
        $user_staff = $this->input->post('user_staff');
        $type_staff = $this->input->post('type_staff');
        $id = $this->input->post('id');
        $update_data = array(
            'username'=>  $user_staff,
            'email'=>  $email_staff,
            'name'=>  $name_staff,
            'tel'=>  $tel_staff,
            'updated_date'=>  $now_date,
            'type'=>  $type_staff
        );
        $this->db->where('id', $id);
        if ($this->db->update('staff', $update_data)) {
            redirect(site_url('staff'));
        }
    }
    private function del_staff(){
        $now_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $update_data = array(
            'status'=>  'deleted',
            'updated_date'=>  $now_date
        );
        if ($this->db->update('staff', $update_data)) {
            redirect(site_url('staff'));
        }
    }

    private function password_generation(){
        $current_time = time();
        $raw1 = sha1($current_time);
        $raw2 = array();
        $raw2[] = $raw1;
        $raw3 = '';
        $n = 0;
        for ($i=0;$i<strlen($raw1);$i+=4){
            if($n%3==0){
//                $raw2[] = strtoupper(substr($raw1,$i,1));
                $raw3 .= strtoupper(substr($raw1,$i,1));
            }else{
//                $raw2[] = strtolower(substr($raw1,$i,1));
                $raw3 .= strtolower(substr($raw1,$i,1));
            }
            $n++;
        }
        return $raw3;
    }

}
