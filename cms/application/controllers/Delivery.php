<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Delivery extends CI_Controller{
    public $user_data;
    public $curl;
    public function __construct()
    {
        parent::__construct();
        $this->curl = new Curl();
        $this->user_data = $this->input->cookie('user_data');
//        var_dump($this->user_data); die;
        if ($this->user_data) {
            $this->user_data = json_decode($this->user_data, true);
            $this->load->helper('cookie');
            if (is_array($this->user_data) && count($this->user_data) > 0) {
                if (!$this->user_data['is_online']) {
                    delete_cookie('user_data');
                    redirect(site_root_url('login'));
                    die;
                } else {
                    if ($this->user_data['id'] != "0") {
                    }
                }
            } else {
                redirect(site_root_url('login'));
                die;
            }
        } else {
            redirect(site_root_url('login'));
            die;
        }
    }

    public function index(){
        $page = $this->input->get('p');
        if(!$page) $page=1;
        $type = ($this->input->get('type'))?$this->input->get('type'):false;
        $keyword = ($this->input->get('keyword'))?$this->input->get('keyword'):false;
        if(!$page) $page=1;

        if($type=="normal"){
            $where = array('type'=> 'normal','status !='=> 'deleted');
        }elseif($type=="lady"){
            $where = array('type'=> 'lady','status !='=> 'deleted');
        }else{
            $where = array('status !='=> 'deleted');
        }

        $this->db->from('delivery');
        if($keyword && (strlen($keyword)>0)){
            $this->db->where("title LIKE '%$keyword%' OR line_id LIKE '%$keyword%'");
        }
        $this->db->where($where);
        $total_num = $this->db->count_all_results();
        $limit = 20;
        $offset = ($page - 1) * $limit;

        $this->db->order_by('order_number','ASC');
        if($keyword && (strlen($keyword)>0)){
            $this->db->where("title LIKE '%$keyword%' OR line_id LIKE '%$keyword%'");
        }
        $query = $this->db->get_where('delivery',$where,$limit,$offset);

        $data_list = $query->result();
        $data = array();
        $total_page = ceil($total_num / $limit);
        $data['total_page'] = $total_page;
        $data['total_num'] = $total_num;
        $data['limit'] = $limit;
        $data['page'] = $page;
        $data['delivery_list'] = $data_list;
        $this->load->view('header',array('page_link' => 'delivery', 'user_data' => $this->user_data));
        $this->load->view('delivery',$data);
        $this->load->view('footer');
    }

    public function add(){
        $data = array();
        $this->load->view('header',array('page_link' => 'delivery', 'user_data' => $this->user_data));
        $this->load->view('delivery_add',$data);
        $this->load->view('footer');
    }

    public function view($id){
        $query = $this->db->get_where('history_log',array('content_id'=>$id,'type'=>'delivery'));
        $modifier_data = array();
        if($query->num_rows()>0){
            $result_data = $query->result();
            foreach ($result_data as $item){
                $modifier_data[$item->status] = $item;
            }
        }
        $query = $this->db->get_where('delivery',array('id'=>$id,'status !='=> 'deleted'));
        $data_list = array();
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            redirect(site_url('delivery')); die;
        }
        $data = array();
        $this->load->view('header',array('page_link' => 'delivery', 'user_data' => $this->user_data));
        $data['data_list'] = $data_list[0];
        $data['modifier_data'] = $modifier_data;
        $this->load->view('delivery_view',$data);
        $this->load->view('footer');
    }
    public function edit($id){
        $query = $this->db->get_where('delivery',array('id'=>$id,'status !='=> 'deleted'));
        $data_list = array();
        if($query->num_rows()>0){
            $data_list = $query->result();
        }else{
            redirect(site_url('delivery')); die;
        }
        $data = array();
        $this->load->view('header',array('page_link' => 'delivery', 'user_data' => $this->user_data));
        $data['data_list'] = $data_list[0];
        $this->load->view('delivery_edit',$data);
        $this->load->view('footer');
    }

    public function update($do){
        if($do=='add'){
            $title = $this->input->post('title');
            $detail = $this->input->post('detail');
            $line_id = $this->input->post('line_id');
            $type = $this->input->post('type');
            $order_number = $this->input->post('order_number');
            $created_time = date('Y-m-d H:i:s');
            $update_data = array(
                'title'=> $title,
                'detail'=> $detail,
                'created_time'=> $created_time,
                'updated_time'=> $created_time,
                'line_id'=> $line_id,
                'type'=> $type,
                'order_number'=> $order_number,
                'viewed'=> 0,
                'liked'=> 0,
                'shared'=> 0,
                'status'=> 'active'
            );
//            console_log($update_data);
//            die;
            if ($this->db->insert('delivery', $update_data)) {
                $id = $this->db->insert_id();
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'add','delivery');
                $cover_image = $this->update_photo($id,'delivery','cover');
                $update_data = array(
                    'cover_image_id'=> $cover_image['id'],
                    'cover_image_src'=> $cover_image['src']
                );
                $this->db->where('id', $id);
                if ($this->db->update('delivery', $update_data)) {
                    redirect(site_url('delivery/view/'.$id));
                }else{
                    redirect(site_url('delivery'));
                }
            }
        }elseif ($do=='edit'){
            $title = $this->input->post('title');
            $detail = $this->input->post('detail');
            $line_id = $this->input->post('line_id');
            $order_number = $this->input->post('order_number');
            $type = $this->input->post('type');
            $id= $this->input->post('id');
            $image_id= $this->input->post('image_id');
            $updated_time = date('Y-m-d H:i:s');
            $update_data = array(
                'title'=> $title,
                'detail'=> $detail,
                'line_id'=> $line_id,
                'order_number'=> $order_number,
                'type'=> $type,
                'updated_time'=> $updated_time
            );
            if($_FILES['cover']['tmp_name']!=""){
                if($this->del_photo($image_id)){
                    $cover_image = $this->update_photo($id,'delivery','cover');
                    $update_data['cover_image_id'] = $cover_image['id'];
                    $update_data['cover_image_src '] = $cover_image['src'];
                }
            }
            $this->db->where('id', $id);
            if ($this->db->update('delivery', $update_data)) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'edit','delivery');
                redirect(site_url('delivery/view/'.$id));
            }
        }elseif ($do=='del'){
            $id = $this->input->post('id');
            $this->db->where('id', $id);
            if ($this->db->update('delivery', array('status'=>'deleted'))) {
                $log_user_id = $this->user_data['id'];
                $log_user_name = $this->user_data['name'];
                history_log($log_user_id,$log_user_name,$id,'del','delivery');
                redirect(site_url('delivery'));
            }
        }
        redirect(site_url('delivery'));

    }

    private function del_photo($id){
        $this->db->where('id', $id);
        if ($this->db->update('images', array('status'=>'deleted'))) {
            return true;
        }
    }

    private function get_business_list($business_list_id){
        $business_list_id = explode(',',$business_list_id);
        $this->db->select('name,cover_image_src,id,address_province,address_district');
        $this->db->where_in('id',$business_list_id);
        $query = $this->db->get('business');

        $business_list = $query->result();
        return $business_list;
    }

    private function update_photo($ref_id)
    {
        $POST_DATA = array();
        $img_num = count($_FILES['cover']['tmp_name']);
        $tmp_file = $_FILES['cover']['tmp_name'];
        $file_name = basename($_FILES['cover']['name']);
        $file_type = $_FILES['cover']['type'];
        $POST_DATA['upload_img'] = curl_file_create($tmp_file, $file_type, $file_name);
        $POST_DATA['img_num'] = $img_num;
        $POST_DATA['depot_key'] = $this->config->item('depot_key');
        $POST_DATA['img_type'] = 'delivery';
        $POST_DATA['ref_id'] = $ref_id;
        $curl_handle = curl_init($this->config->item('depot_url').'home/upload_image');
        curl_setopt($curl_handle, CURLOPT_POST, 1);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $POST_DATA);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);

        $returned_data = curl_exec($curl_handle);
        curl_close($curl_handle);

        $returned_data = json_decode($returned_data, true);
        return $returned_data['result'];
    }
}
