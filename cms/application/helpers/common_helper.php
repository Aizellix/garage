<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * Site Assets URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('site_assets_url'))
{
	function site_assets_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url('assets/'.$uri);
	}
}

if ( ! function_exists('print_json'))
{
    function print_json($data = array())
    {
        header('Content-type:Application/json');
        echo json_encode($data,true);
    }
}

if ( ! function_exists('api_site_url'))
{
    function api_site_url($uri = '')
    {
        $CI =& get_instance();
        $CI->config->load('config');
        $api_site = $CI->config->item('api_url');
        return $api_site.$uri;
    }
}
if ( ! function_exists('depot_site_url'))
{
    function depot_site_url($uri = '')
    {
        $CI =& get_instance();
        $CI->config->load('config');
        $api_site = $CI->config->item('depot_url');
        return $api_site.$uri;
    }
}
if ( ! function_exists('api_key'))
{
    function api_key()
    {
        $CI =& get_instance();
        $CI->config->load('config');
        $api_key = $CI->config->item('api_key');
        return $api_key;
    }
}
if ( ! function_exists('google_key'))
{
    function google_key()
    {
        $CI =& get_instance();
        $CI->config->load('config');
        $api_key = $CI->config->item('google_key');
        return $api_key;
    }
}
// ------------------------------------------------------------------------

/**
 * Site Root URL
 *
 * Create a local URL based on your basepath. Segments can be passed via the
 * first parameter either as a string or an array.
 *
 * @access	public
 * @param	string
 * @return	string
 */
if ( ! function_exists('console_log'))
{
    function console_log($data = '')
    {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
    }
}


if ( ! function_exists('site_root_url'))
{
	function site_root_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->base_url($uri);
	}
}

if ( ! function_exists('time_ago') ) {
  function time_ago( $date, $granularity = 2 ) {
    $date = strtotime($date);
    $difference = time() - $date;
    $periods = array(
      'decade' => 315360000,
      'year'   => 31536000,
      'month'  => 2628000,
      'week'   => 604800,
      'day'    => 86400,
      'hour'   => 3600,
      'minute' => 60,
      'second' => 1
    );
    $retval = '';
    foreach ($periods as $key => $value) {
      if ($difference >= $value) {
        $time = floor($difference/$value);
        $difference %= $value;
        $retval .= ($retval ? ' ' : '').$time.' ';
        $retval .= (($time > 1) ? $key.'s' : $key);
        $granularity--;
      }
      if ($granularity == '0') { break; }
    }
    if ( $retval == '' ) { $retval = 'a few second';}
    return ' posted '.$retval.' ago';
  }
}

if ( ! function_exists('send_email') ) {
  function send_email( $send_to, $send_from, $sender_name, $mail_subject,$msg)
  {
//      var_dump($send_to, $send_from, $sender_name, $mail_subject,$msg); die;
      $CI =& get_instance();
      $config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'pueanrod.com@gmail.com',
          'smtp_pass' => '#Pueanrod$2017!',
          'mailtype' => 'html',
          'charset' => 'UTF-8'
      );
      if(strpos($_SERVER['HTTP_HOST'],'garage.test')===false)
      {
          $CI->load->library('email');
      } else {
          $CI->load->library('email', $config);
      }

      $CI->email->set_newline("\r\n");
      $CI->email->from($send_from, $sender_name);
      $CI->email->to($send_to);
      $CI->email->set_mailtype("html");
      $CI->email->subject($mail_subject);
      $CI->email->message($msg);
      $CI->email->send();
      return $CI->email->print_debugger();
  }
}



if ( ! function_exists('update_review_score'))
{
    function update_review_score($business_id = '',$score = 0,$operation = '',$old_score=0)
    {
        $CI =& get_instance();
        $CI->db->select('id,score,reviewed');
        $CI->db->where(array('id'=>$business_id));
        $query = $CI->db->get('business');
        if($query->num_rows()>0){
            $business_data = $query->result();
            $business_data = $business_data[0];
            $new_score = (int)$business_data->score;
            $new_reviewed = (int)$business_data->reviewed;
            if($operation=="reset"){
                $new_score = $score;
                $new_reviewed =1;
//                echo '<pre>'; var_dump(' $business_id='.$business_id,' $score='.$score,' $new_score='.$new_score); echo '</pre>';
            }else if($operation=="add"){
                $old_score = $new_score;
                $new_score += $score;
                $new_reviewed +=1;
//                echo '<pre>'; var_dump(' $business_id='.$business_id,' $score='.$score,' $new_score='.$new_score, ' $old_score='.$old_score); echo '</pre>';
            }else if($operation=="edit"){
                $new_score -= $old_score;
                $new_score += $score;
                $new_reviewed += 0;
            }else{
                $new_score -= $score;
                $new_reviewed -=1;
            }
            if($new_score<0) $new_score = 0;
            if($new_reviewed<0) $new_reviewed = 0;
            $update_data = array(
                'score' => $new_score,
                'reviewed' => $new_reviewed
            );
            $CI->db->where('id', $business_id);
            if ($CI->db->update('business', $update_data)) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

if ( ! function_exists('update_review_user'))
{
    function update_review_user($user_id = '',$operation = '')
    {
        $CI =& get_instance();
        $CI->db->select('id,reviewed');
        $CI->db->where(array('id'=>$user_id));
        $query = $CI->db->get('user');
        if($query->num_rows()>0){
            $user_data = $query->result();
            $user_data = $user_data[0];
            $new_reviewed = (int)$user_data->reviewed;
            if($operation=="reset"){
                $new_reviewed =1;
            }else if($operation=="add"){
                $new_reviewed +=1;
            }else if($operation=="edit"){
                $new_reviewed += 0;
            }else{
                $new_reviewed -=1;
            }
            if($new_reviewed<0){ $new_reviewed = 0; }
            $update_data = array(
                'reviewed' => $new_reviewed
            );
//            if($user_id=="52"){
//                echo '<pre>';
//                var_dump($operation,$update_data);
//                echo '</pre>';
//            }
            $CI->db->where('id', $user_id);
            if ($CI->db->update('user', $update_data)) {
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

if ( ! function_exists('history_log'))
{
    function history_log($user_id = '',$user_name = '',$content_id = '', $status='', $type='')
    {
        $CI =& get_instance();
        $query = $CI->db->get_where('history_log',array('content_id'=>$content_id,'status'=>$status,'type'=>$type));
        $created_time = date('Y-m-d H:i:s');
        if($query->num_rows()>0){
            $update_data = array(
                'user_id' => $user_id,
                'user_name' => $user_name,
                'created_time' => $created_time
            );
            $CI->db->where(array('content_id'=>$content_id,'status'=>$status));
            if ($CI->db->update('history_log', $update_data)) {
                return true;
            }else{
                return false;
            }
        }else{
            $update_data = array(
                'user_id' => $user_id,
                'user_name' => $user_name,
                'content_id' => $content_id,
                'status' => $status,
                'created_time' => $created_time,
                'type' => $type
            );
            if ($CI->db->insert('history_log', $update_data)) {
                return true;
            }else{
                return false;
            }
        }
    }
}

if ( ! function_exists('create_business_code'))
{
    function create_business_code($business_id){
        $max_length = 7;
        echo "<per>";
        $business_id_base20 = base_convert($business_id, 10, 20);
        $decimal_num = array("U","K","L","M","H","I","J","R","S","T","N","O","E","F","G","P","Q","B","C","D"); //0-19
        $separate_num = array("X","Y","Z"); //0-2
        $random_num = array("A","B","C","D","E","F","G","R","S","T","U","V","W","H","I","J","K","L","M","N","O","P","Q"); //0-22
        $business_code = array();

        $business_id_arr = str_split($business_id_base20, 1);

        $pre_code_length = $max_length-count($business_id_arr);
        if($pre_code_length-1<=0) $pre_code_length+=2;
        for($i=0;$i<$pre_code_length-1;$i++){
            $business_code[] = $random_num[rand(0,22)];
        }
        $business_code[] = $separate_num[rand(0,2)];
        foreach ($business_id_arr as $item){
            $key = base_convert($item, 20, 10);
            $business_code[] = $decimal_num[$key];
        }
        $business_code = implode($business_code);
        //var_dump($business_id,$business_code);
        return strtoupper($business_code);
    }
}

if ( ! function_exists('decode_business_code'))
{
    function decode_business_code($business_code){
        $business_code_arr = str_split($business_code,1);
        //echo "<pre>";
        $decimal_num = array("U","K","L","M","H","I","J","R","S","T","N","O","E","F","G","P","Q","B","C","D"); //0-19
        $separate_num = array("X","Y","Z"); //0-2
        $separate_key = -1;
        foreach ($separate_num as $item){
            $result = array_keys($business_code_arr,$item);
            if(count($result)>0){
                //var_dump($result);
                $separate_key = $result[0];
            }
        }
        $business_id_base20_encoded_arr = array_slice($business_code_arr,$separate_key+1);
        $business_id_base20_arr = array();
        foreach ($business_id_base20_encoded_arr as $item){
            $result = array_keys($decimal_num,$item);
            if(count($result)>0){
                //var_dump($result);
                $key = $result[0];
                $business_id_base20_arr[] = base_convert($key, 10, 20);
            }
        }
        $business_id_base20 = implode($business_id_base20_arr);
        $business_id = base_convert($business_id_base20, 20, 10);
        //var_dump($business_code,$business_id_base20_encoded_arr,$business_id_base20,$business_id);
        return $business_id;
    }
}