<?php
$c_user_data = $this->input->cookie('user_data');
$this->input->set_cookie('user_data', $c_user_data, +7200);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Pueanrod.com Back Office System</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.structure.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('fancybox/jquery.fancybox.css'); ?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrapValidator.min.css');?>">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap-theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('fonts/stylesheet.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/style.css');?>">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo site_assets_url('js/jquery-3.1.1.min.js');?>"></script>
    <script src="<?php echo site_assets_url('fancybox/jquery.fancybox.js'); ?>"></script>
    <script src="<?php echo site_assets_url('fancybox/jquery.fancybox.pack.js'); ?>"></script>
    <script src="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrap.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrapValidator.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/main.js');?>"></script>
</head>
<body>
<div class="container-fluid" id="mainPage">
    <div class="row" style="background-color: #eb4e25;  <?php if($this->input->get('print')=="1") echo 'display: none;'; ?>" id="header_bar">
        <div class="col-md-6" style="padding: 10px 0; ">
            <img src="<?php echo site_assets_url('images/header_title.png');?>" style="width: 480px;">
        </div>
        <div class="col-md-6" style="background-color: #eb4e25;">
            <div class="text-right" style="line-height: 48px;">
                <span></span>
                <a href="<?php echo site_url('logout')?>" style="color: #FFF;"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
            </div>
        </div>
    </div>
    <div class="" style="margin-top: 4px;">
        <div class="col-md-2" id="navPage" <?php if($this->input->get('print')=="1") echo 'style="display: none;"'; ?>>
            <div class="clearfix" style="padding-bottom: 20px;"></div>
            <div class="row" style="padding-right: 10px;">
                <div class="row">
                    <ul class="nav nav-pills nav-stacked" style="padding-left: 5px;">
                        <li role="presentation" class="<?php echo ($page_link=='home')?'active':'';?>">
                            <a href="<?php echo site_url('home')?>">
                                <span class="glyphicon glyphicon-home"></span> หน้าแรก
                            </a>
                        </li>
                        <li style="border-bottom: 1px solid #111; margin: 4px 0;"></li>
                        <!--                    <li role="presentation" class="dropdown --><?php //echo ($page_link=='manage_article')?'active':'';?><!-- disabled">-->
                        <!--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">-->
                        <!--                            <span class="glyphicon glyphicon-file"></span> ข้อมูลประชาสัมพันธ์ <span class="caret"></span>-->
                        <!--                        </a>-->
                        <!--                        <ul class="dropdown-menu disabled">-->
                        <!--                            <li role="presentation"><a href="#--><?php ////echo site_url('article')?><!--">บทความ</a></li>-->
                        <!--                            <li role="presentation"><a href="#--><?php ////echo site_url('promotion')?><!--">โปรโมชั้น</a></li>-->
                        <!--                            <li role="presentation"><a href="#--><?php ////echo site_url('advertise')?><!--">โฆษณา</a></li>-->
                        <!--                            <li role="presentation"><a href="#--><?php ////echo site_url('emergency')?><!--">การติดต่อฉุกเฉิน</a></li>-->
                        <!--                        </ul>-->
                        <!--                    </li>-->
                        <li role="presentation" class="<?php echo ($page_link=='popup')?'active':'';?> ">
                            <a href="<?php echo site_url('popup')?>">
                                <span class="glyphicon glyphicon-modal-window"></span> ป๊อปอัพ
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='delivery')?'active':'';?> ">
                            <a href="<?php echo site_url('delivery')?>">
                                <span class="glyphicon glyphicon-gift"></span> เดลิเวอรี่
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='deal')?'active':'';?> ">
                            <a href="<?php echo site_url('deal')?>">
                                <span class="glyphicon glyphicon-shopping-cart"></span> ดีล
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='article')?'active':'';?> ">
                            <a href="<?php echo site_url('article')?>">
                                <span class="glyphicon glyphicon-file"></span> บทความ
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='promotion')?'active':'';?> ">
                            <a href="<?php echo site_url('promotion')?>">
                                <span class="glyphicon glyphicon-tag"></span> โปรโมชั่น
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='advertise')?'active':'';?> ">
                            <a href="<?php echo site_url('advertise')?>">
                                <span class="glyphicon glyphicon-bullhorn"></span> โฆษณา
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='emergency')?'active':'';?> ">
                            <a href="<?php echo site_url('emergency')?>">
                                <span class="glyphicon glyphicon-alert"></span> รายการติดต่อฉุกเฉิน
                            </a>
                        </li>
                        <li style="border-bottom: 1px solid #111; margin: 4px 0;"></li>
                        <li role="presentation" class="<?php echo ($page_link=='user')?'active':'';?> ">
                            <a href="<?php echo site_url('user')?>">
                                <span class="glyphicon glyphicon-user"></span> ข้อมูลผู้ใช้งาน
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='business')?'active':'';?>">
                            <a href="<?php echo site_url('business')?>">
                                <span class="glyphicon glyphicon-flag"></span> ข้อมูลผู้ประกอบการ
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='review')?'active':'';?>" style="display: none;">
                            <a href="<?php echo site_url('review')?>">
                                <span class="glyphicon glyphicon-thumbs-up"></span> ข้อมูลรีวิวสถานประกอบการ
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='category')?'active':'';?>">
                            <a href="<?php echo site_url('category')?>">
                                <span class="glyphicon glyphicon-th-list"></span> ข้อมูลตัวเลือก
                            </a>
                        </li>
                        <li style="border-bottom: 1px solid #111; margin: 4px 0;"></li>
                        <li role="presentation" class="<?php echo ($page_link=='stat')?'active':'';?>" style="display: ">
                            <a href="<?php echo site_url('stat'); ?>">
                                <span class="glyphicon glyphicon-signal"></span> ข้อมูลสถิติภาพรวม
                            </a>
                        </li>
                        <li role=
                        <li role="presentation" class="<?php echo ($page_link=='stat_business')?'active':'';?>" style="display: ">
                            <a href="<?php echo site_url('stat/business'); ?>">
                                <span class="glyphicon glyphicon-signal"></span> ข้อมูลสถิติผู้ประกอบการ
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='stat_article')?'active':'';?>" style="display: ">
                            <a href="<?php echo site_url('stat/article'); ?>">
                                <span class="glyphicon glyphicon-signal"></span> ข้อมูลสถิติบทความ
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='stat_promotion')?'active':'';?>" style="display: ">
                            <a href="<?php echo site_url('stat/promotion'); ?>">
                                <span class="glyphicon glyphicon-signal"></span> ข้อมูลสถิติโปรโมชั่น
                            </a>
                        </li>
                        <li role="presentation" class="<?php echo ($page_link=='stat_advertise')?'active':'';?>" style="display: ">
                            <a href="<?php echo site_url('stat/advertise'); ?>">
                                <span class="glyphicon glyphicon-signal"></span> ข้อมูลสถิติโฆษณา
                            </a>
                        </li>
                        <li style="border-bottom: 1px solid #111; margin: 4px 0;"></li>
                        <?php if($user_data['permission']=='admin'){ ?>
                        <li role="presentation" class="<?php echo ($page_link=='staff')?'active':'';?> ">
                            <a href="<?php echo site_url('staff')?>">
                                <span class="glyphicon glyphicon-eye-open"></span> ข้อมูลผู้ดูแลระบบ
                            </a>
                        </li>
                        <?php } ?>
                        <li role="presentation" class="<?php echo ($page_link=='account')?'active':'';?> " style="display: ">
                            <a href="<?php echo site_url('account'); ?>">
                                <span class="glyphicon glyphicon-cog"></span> แก้ไขข้อมูลส่วนตัว
                            </a>
                        </li>
                        <li style="border-bottom: 1px solid #111; margin: 4px 0;"></li>
                        <li role="presentation">
                            <a href="<?php echo site_url('logout')?>">
                                <span class="glyphicon glyphicon-log-out"></span> ออกจากระบบ
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-10" id="contentPage" style="">
            <div class="row">