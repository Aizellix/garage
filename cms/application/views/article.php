<?php
$all_selected = '';
$live_selected = '';
$expired_selected = '';
$waiting_selected = '';
$param_url = '';
$status = $this->input->get('status');
if($status=="all"){
    $all_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="live"){
    $live_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="waiting"){
    $waiting_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="expired"){
    $expired_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}

$page_title = 'article';
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back.$param_url) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
    if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_back) {
        $html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_next) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
    } else {
        $html_page .= "#";
    }
}

while (strpos($html_page, "##") > 1) {
    $html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next.$param_url) . '">&raquo;</a></li>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการบทความ</strong>
</div>
<div class="col-lg-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการบทความ</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-6 text-left">
            <form class="form-inline" action="<?php echo site_url('article')?>" method="">
                <div class="form-group">
                    <label for="search_keyword">สถานะ:
                        <select name="status" id="search_status" class="form-control">
                            <option value="all" <?php echo $all_selected;?>>ทั้งหมด</option>
                            <option value="live" <?php echo $live_selected;?>>กำลังเผยแพร่</option>
                            <option value="waiting" <?php echo $waiting_selected;?>>รอการเผยแพร่</option>
                            <option value="expired" <?php echo $expired_selected;?>>หมดอายุ</option>
                        </select>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </label>
                </div>
                <?php if($status && isset($status)&& $status!=''){?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                    </div>
                <?php } ?>
            </form>
        </div>
        <div class="col-lg-6 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="<?php echo site_url('article/add')?>"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th class="text-center" style="width: 100px;">วันที่สร้าง</th>
                <th class="text-center" style="width: 100px;" >รูป</th>
                <th class="text-center" style="" >รายชื่อ</th>
                <th class="text-center" style="width: 150px;">เริ่มเผยแพร่</th>
                <th class="text-center" style="width: 150px;">สิ้นสุด</th>
                <th class="text-center" style="width: 110px;">สถานะ</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($article_list)>0){
                foreach ($article_list as $item){
                    ?>
                    <tr>
                        <td class="text-left"><?php echo date("Y-m-d",strtotime($item->created_time)) ;?></td>
                        <td class="text-left">
                            <a href="<?php echo $item->cover_image_src;?>" data-fancybox="images" data-width="2048" data-height="1365">
                                <img src="<?php echo $item->cover_image_src;?>" style="width: 100%">
                            </a>
                        </td>
                        <td class="text-left"><a href="<?php echo site_url('article/view/'.$item->id);?>" target="_blank"><?php echo $item->title ;?></a></td>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->start_time));?></td>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->end_time));?></td>
                        <td class="text-center"><?php
                            $current_time = time();
                            $start_time = strtotime($item->start_time);
                            $end_time = strtotime($item->end_time);
                            if($current_time>=$start_time&&$current_time<=$end_time){
                                echo '<strong style="color: green">กำลังเผยแพร่</strong>';
                            }else if($current_time<$start_time){
                                echo '<strong style="color: #999">รอเผยแพร่</strong>';
                            }else{
                                echo '<strong style="color: red">หมดอายุ</strong>';
                            }
                            ?></td>
                        <td class="text-center">
                            <a href="<?php echo site_url('article/edit/'.$item->id);?>" >แก้ไข</a> |
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>);">ลบ</a>
                        </td>

                    </tr>
                    <?php }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="7">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 450px;">
            <div class="col-md-12">
                <form action="<?php echo site_url('article/update/del')?>" method="post" id="article_form">
                    <input value="" name="do" id="post_method" type="hidden">
                    <input value="" name="id" id="item_id" type="hidden">
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function del_data(id) {
        if(confirm('คุณต้องการลบบทความนี้หรือไม่?')){
            $("#post_method").val('del');
            $("#item_id").val(id);
            $("#article_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });
</script>