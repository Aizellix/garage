<?php
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;
$page_title = 'business';
$html_page = '<li><a href="' . site_url('/business?p=' . $page_back) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
    if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/business?p=' . $i) . '">' . $i . '</a></li>';
    } else if ($i == $page_back) {
        $html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/business?p=' . $i) . '">' . $i . '</a></li>';
    } else if ($i == $page_next) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/business?p=' . $i) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
    } else {
        $html_page .= "#";
    }
}

while (strpos($html_page, "##") > 1) {
    $html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next) . '">&raquo;</a></li>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลผู้ประกอบการ</strong>
</div>

<div class="col-lg-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการผู้ประกอบการ</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-9">
            <form class="form-inline" action="<?php echo site_url('business')?>" method="">
                <div class="form-group">
                    <label for="search_status">สถานะ:
                        <select name="status" id="search_status" class="form-control">
                            <option value="all" <?php if($this->input->get('status')=="all")echo 'selected="selected"';?>>ทั้งหมด</option>
                            <option value="active" <?php if($this->input->get('status')=="active")echo 'selected="selected"';?>>Active</option>
                            <option value="pending" <?php if($this->input->get('status')=="pending")echo 'selected="selected"';?>>Pending</option>
                            <option value="vip" <?php if($this->input->get('status')=="vip")echo 'selected="selected"';?>>VIP</option>
                        </select>
                    </label>
                </div>
                <div class="form-group">
                    <label for="search_type">ประเภท:
                        <select name="type" id="search_type" class="form-control">
                            <option value="all">ทั้งหมด</option>
                            <?php foreach ($category_list as $item){?>
                                <option value="<?php echo $item->id;?>" <?php if($this->input->get('type')==$item->id)echo 'selected="selected"';?>><?php echo $item->title;?></option>
                            <?php }?>
                        </select>
                    </label>
                </div>
                <div class="form-group">
                    <label for="search_keyword">
                        <input type="text" class="form-control" id="search_keyword" name="keyword" placeholder="ค้นหา" style="width: 230px;">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </label>
                </div>
                <?php $keyword = $this->input->get('keyword'); $status = $this->input->get('status'); if(($keyword && isset($keyword)&& $keyword!='')||($status && isset($status)&& $status!='')){?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                    </div>
                <?php } ?>
            </form>
        </div>
        <div class="col-lg-3 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="<?php echo site_url('business/add')?>"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered">
            <thead>
            <tr>
                <?php
                $order_by = $this->input->get("order_by");
                $sort_by = $this->input->get("sort_by");
                $all_param = str_replace("sort_by=".$sort_by."&order_by=".$order_by,"",$_SERVER['QUERY_STRING']);
                $all_param = str_replace("&","",$all_param);
                $id_order = "DESC";
                $reviewed_order = "DESC";
                $id_sort_icon = "glyphicon-sort-by-attributes-alt";
                $reviewed_sort_icon = "glyphicon-sort";
                if($sort_by=="id"){
                    $id_order = ($order_by=="DESC")?"ASC":"DESC";
                    $id_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                }else if($sort_by=="reviewed"){
                    $reviewed_order = ($order_by=="DESC")?"ASC":"DESC";
                    $reviewed_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                    $id_sort_icon = "glyphicon-sort";
                }
                $id_order.="&".$all_param;
                $reviewed_order.="&".$all_param;
                ?>
                <th class="text-center" style="width: 50px; cursor: pointer;"><a href="<?php echo site_url('business?sort_by=id&order_by='.$id_order);?>"><span class="glyphicon <?php echo $id_sort_icon ?> small"></span> ID</a></th>
                <th class="text-center" style="width: 96px;">Cover</th>
                <th class="text-center" style="width: 300px;">Business Name</th>
                <th class="text-center" style="">Address</th>
                <th class="text-center" style="width: 100px;"><a href="<?php echo site_url('business?sort_by=reviewed&order_by='.$reviewed_order);?>"><span class="glyphicon <?php echo $reviewed_sort_icon ?> small"></span> รีวิว</a></th>
                <th class="text-center" style="width: 150px;">Created Date</th>
                <th class="text-center" style="width: 100px;">Status</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($business_list)>0){
                foreach ($business_list as $item){
                    if($item->cover_image_src!=''){
                        $image = $item->cover_image_src;
                        $image = '<img src="'.$image.'" width="90">';
                    }else{
                        $image = '';
                    }
                    $address = '';
                    if($item->address_no!=""    ) $address = $item->address_no;
                    if($item->address_moo!="") $address .= " หมู่ที่ ".$item->address_moo;
                    if($item->address_village!="") $address .= " หมู่บ้าน ".$item->address_moo;
                    if($item->address_soi!="") $address .= " ซอย ".$item->address_soi;
                    if($item->address_road!="") $address .= " ".$item->address_road;
                    if($item->address_tambol!="") $address .= " ".$item->address_tambol;
                    if($item->address_district!="") $address .= " ".$item->address_district;
                    if($item->address_province!="") $address .= " ".$item->address_province;
                    if($item->address_postcode!="") $address .= " ".$item->address_postcode;
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $item->id;?></td>
                        <td class="text-center" style="padding: 2px;">
                            <?php if($image!=""){?>
                                <a href="<?php echo $item->cover_image_src;?>" data-fancybox="images" data-width="500" data-height="400">
                                    <?php echo $image;?>
                                </a>
                            <?php }else { echo $image; }?>

                        </td>
                        <td class="text-left"><a href="<?php echo site_url('business/view/'.$item->id)?>"><?php echo $item->name;?></a></td>

                        <td class="text-left"><?php echo $address ;?></td>
                        <td class="text-center"><a href="<?php echo site_url('business/review/'.$item->id) ;?>" target="_blank"><?php echo $item->reviewed ;?></a></td>
                        <td class="text-left"><?php echo date("Ym-d H:i:s",strtotime($item->created_time));?></td>
                        <td class="text-left"><strong class="text-capitalize status_<?php echo $item->status;?>"><?php echo $item->status;?></strong></td>
                        <td class="text-left">
                            <a href="<?php echo site_url('business/edit/'.$item->id)?>">แก้ไข</a> |
                            <a href="javascript:void(0)" onclick="del_business(<?php echo $item->id?>)">ลบ</a>
                        </td>

                    </tr>
                <?php }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="5">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp" style="background-color: #555;">

    </div>
</div>
<script>
    function add_staff() {
        $.fancybox.open({href : '#markUp'} );
    }

    function del_business(id) {
        if(confirm('คุณต้องการลบสถานที่หรือไม่')){
            window.location.assign('<?php echo site_url('business/del/')?>'+id);
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });
</script>
<style>
    .text-capitalize.status_active{
         color: green;
     }
    .text-capitalize.status_pending{
        color: silver;
    }
    .text-capitalize.status_vip{
        color: red;
    }
</style>