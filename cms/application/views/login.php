<?php
$do = $this->input->get('do');
$error = $this->input->get('error');

$is_notification = false;
if($do||$error){
    $is_notification = true;
    if($error){
        $error_str = array(
            '1' => "Your e-mail is not found.",
            '2' => "Wrong e-mail or password."
        );
        $notification_info = '
    <div role="alert" class="alert alert-danger" style="color: #555;">
      <strong>Error!</strong> '.$error_str[$error].'
    </div>';
    }else{
        $notification_info = '
    <div role="alert" class="alert alert-success" style="text-align: center;">
        <strong>สำเร็จ!</strong> รหัสผ่านใหม่ของคุณได้ถูกส่งไปยังอีเมลเรียบร้อยแล้ว.
    </div>';

    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Pueanrod.com Back Office System</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.structure.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrapValidator.min.css');?>">

    <!-- Optional theme -->
    <link rel="stylesheet" href="<?php echo site_assets_url('css/bootstrap-theme.min.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('fonts/stylesheet.css');?>">
    <link rel="stylesheet" href="<?php echo site_assets_url('css/style.css');?>">

    <!-- Latest compiled and minified JavaScript -->
    <script src="<?php echo site_assets_url('js/jquery-3.1.1.min.js');?>"></script>
    <script src="<?php echo site_assets_url('jquery-ui-1.12.1/jquery-ui.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrap.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/bootstrapValidator.min.js');?>"></script>
    <script src="<?php echo site_assets_url('js/main.js');?>"></script>
    <script>
        var site_url = "<?php echo site_url();?>";

        $(document).ready(function() {
            $('#login_form')
                .on('init.field.bv', function(e, data) {
                    var $parent = data.element.parents('.form-group'),
                        $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');
                    $icon.on('click.clearing', function() {
                        if ($icon.hasClass('glyphicon-remove')) {
                            data.bv.resetField(data.element);
                        }
                    });
                })

                .bootstrapValidator({
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        username: {
                            validators: {
                                notEmpty: {
                                    message: 'The Email is required'
                                }
                            }

                        },
                        password: {

                            validators: {
                                notEmpty: {
                                    message: 'The Password is required'
                                }
                            }
                        }
                    }
                });

            $('#forgot_pass_form')
                .on('init.field.bv', function(e, data) {
                    var $parent = data.element.parents('.form-group'),
                        $icon   = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');
                    $icon.on('click.clearing', function() {
                        if ($icon.hasClass('glyphicon-remove')) {
                            data.bv.resetField(data.element);
                        }
                    });
                })

                .bootstrapValidator({
                    feedbackIcons: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields: {
                        user_email_forgot: {
                            validators: {
                                notEmpty: {
                                    message: 'The Email is required'
                                }
                            }

                        }
                    }
                });
        });
    </script>
    <style></style>
</head>

<body>
<div class="container" style="padding-top: 50px;" id="mainPage">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3">
            <div class="panel panel-primary text-center">
                <div class="panel-heading">
                    <h3>Pueanrod.com Back Office System</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="<?php echo site_url('login'); ?>" method="post" id="login_form">
                        <div class="form-group">
                            <label for="user_email">Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Enter Username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="user_password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Log in</button>
                        </div>
                        <input name="do" value="login" type="hidden">
                    </form>
                    <form style="display: " role="form" action="<?php echo site_url('login'); ?>" method="post" id="forgot_pass_form">
                        <div class="form-group" id="forgot_password">
                            <a data-toggle="collapse" data-parent="#forgot_password" href="#collapseOne" aria-expanded="true"
                               aria-controls="collapseOne">
                                Forgot password
                            </a>

                            <div id="collapseOne" class="form-group panel-collapse collapse form-inline">
                                <div class="input-group">
                                    <input type="email" class="form-control" id="user_email_forgot" placeholder="Enter email"
                                           name="user_email_forgot" style="width: 200px;">
                                    <span class="input-group-btn">
										<button type="submit" class="btn btn-default">
											Request new password
										</button>
									</span>
                                </div>

                            </div>
                            <input name="do" value="forgot" type="hidden">
                        </div>
                    </form>

                </div>
                <div class="panel-footer small">Pueanrod.com Back Office System, &copy 2016 Pueanrod.com</div>
            </div>
        </div>
        <?php if($is_notification){?>
            <div class="col-sm-6 col-sm-offset-3">
                <?php echo $notification_info; ?>
            </div>
        <?php }?>
    </div>
</div>
</body>

</html>