<style>
    pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
    }

    #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
    }

    .pac-controls {
        display: inline-block;
        padding: 5px 11px;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 3px 11px 3px 13px;
        text-overflow: ellipsis;
        width: 50%;
        margin-top: 10px;
    }

</style>
<script type="text/javascript">
    var geocoder;
    var marker;
    var obj_address;
    function myMaps() {
        var Position = new google.maps.LatLng(13.723419, 100.476232);
        geocoder = new google.maps.Geocoder();
        var myOptions = {
            center: Position,
            scrollwheel: false,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        marker = new google.maps.Marker({
            position: Position,
            draggable: false,
            title: "คลิกแล้วเคลื่อนย้ายหมุดไปยังตำแหน่งที่ต้องการ"
        });


        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });

        var Posi = marker.getPosition();
        myMaps_locat(Posi.lat(), Posi.lng());
        marker.setMap(map);
        google.maps.event.addListener(map, 'center_changed', function (ev) {
            window.setTimeout(function () {
                var location = map.getCenter();
                marker.setPosition(location);
                myMaps_locat(location.lat(), location.lng());
            }, 100);
        });
        google.maps.event.addListener(marker, 'click', function (ev) {
            var location = ev.latLng;
            myMaps_locat(location.lat(), location.lng());
//            console.log(marker.getPosition());
        });
        google.maps.event.addListener(map, 'zoom_changed', function (ev) {
            zoomLevel = map.getZoom();
            $('#mapsZoom').val(zoomLevel);
        });
    }
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
//            console.log(responses);
            if (responses && responses.length > 0) {
                obj_address = responses[0];
                generate_address(responses[0]);
            } else {
                alert('ไม่สามารถหาที่อยู่จากตำแหน่งนี้ได้');
            }
        });

    }

    function myMaps_locat(lat, lng) {
        $('#U_LAT').val(lat);//เอาค่าละติจูดไปแสดงที่ Tag HTML ที่มีแอตทริบิวต์ id ชื่อ mapsLat
        $('#U_LONG').val(lng);//เอาค่าลองติจูดไปแสดงที่ Tag HTML ที่มีแอตทริบิวต์ id ชื่อ mapsLng
    }
    $(document).ready(function () {
        myMaps();//แสดงแผนที่
//            $('#maps_form').myMaps_submit();//ตรวจสอบการSubmit Form
    });
</script>

<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลผู้ประกอบการ</strong>
</div>

<div class="col-lg-12" style="clear: both;">
    <strong style="font-size: 16px;">เพิ่มผู้ประกอบการ</strong>
    <form action="<?php echo api_site_url('business/add_business') ?>" method="post" enctype="multipart/form-data"
          id="add_business_form">
        <div id="main_content" class="">
            <div class="row">
                <div class="col-md-12 col-lg-9">
                    <input id="pac-input" class="controls" type="text" placeholder="ค้นหาสถานที่">
                    <div id="map_canvas" style="width:100%; height:300px;"></div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="U_LAT">Latitude</label>
                                <input type="text" class="form-control" id="U_LAT" name="lat">
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="U_LONG">Longitude</label>
                                <input type="text" class="form-control" id="U_LONG" name="lon">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group text-center">
                                <label for="get_location_data_btn">ขอข้อมูล</label>
                                <button type="button" class="btn-link" id="get_location_data_btn"
                                        onclick="get_location_data()">
                                    <span class="glyphicon glyphicon-download" style="font-size: 26px;"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_no">เลขที่</label>
                                <input type="text" class="form-control" id="address_no" name="address_no"
                                       placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_moo">หมู่ที่</label>
                                <input type="text" class="form-control" id="address_moo" name="address_moo"
                                       placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_village">หมู่บ้าน</label>
                                <input type="text" class="form-control" id="address_village" name="address_village">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_soi">ซอย</label>
                                <input type="text" class="form-control" id="address_soi" name="address_soi">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_road">ถนน</label>
                                <input type="text" class="form-control" id="address_road" name="address_road">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_tambol">ตำบล/แขวง</label>
                                <input type="text" class="form-control" id="address_tambol" name="address_tambol">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_district">อำเภอ/เขต</label>
                                <input type="text" class="form-control" id="address_district" name="address_district">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_province">จังหวัด</label>
                                <input type="text" class="form-control" id="address_province" name="address_province">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="address_postcode">รหัสไปษณีย์</label>
                                <input type="text" class="form-control" id="address_postcode" name="address_postcode">
                                <input type="hidden" class="form-control" id="country" name="country" value="">
                                <input type="hidden" class="form-control" id="country_code" name="country_code" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tel">เบอโทร์</label>
                                <input type="text" class="form-control" id="tel" name="tel" placeholder="02-123-4567, 0987654321">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="facebook">Facebook</label>
                                <input type="text" class="form-control" id="facebook" name="facebook" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="line">Line</label>
                                <input type="text" class="form-control" id="line" name="line" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="me@email.com">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="detail">คำบรรยายสภาพแวดล้อม</label>
                        <textarea class="form-control" id="detail" name="detail" rows="8"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="work_day_sun">วันทำการ</label>
                                <div>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_sun" name="work_day_sun" value="1">
                                        อาทิตย์
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_mon" name="work_day_mon" value="1">
                                        จันทร์
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_tue" name="work_day_tue" value="1"> อังคาร
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_wed" name="work_day_wed" value="1"> พุธ
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_thu" name="work_day_thu" value="1">
                                        พฤหัสบดี
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_fri" name="work_day_fri" value="1"> ศุกร์
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="work_day_sat" name="work_day_sat" value="1"> เสาร์
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="work_time_section">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="show_work_time">เวลาทำการ:</label>
                                <input type="text" id="show_work_time" readonly
                                       style="border:0; color:#f6931f; font-weight:bold; width: 90px">
                                <input type="hidden" id="work_time_start" name="work_time_start">
                                <input type="hidden" id="work_time_close" name="work_time_close">
                                <div class="row">
                                    <div class="col-md-6 form-inline">
                                        <div class="form-group">
                                            <label for="work_time_start_h">เปิดทำการ:</label>
                                            <select class="form-control" id="work_time_start_h" name="work_time_start_h">
                                                <?php
                                                for($i=0;$i<=23;$i++){
                                                    if($i<10){ $h_time = "0".$i; }else{ $h_time = $i;}
                                                    echo '<option value="'.$h_time.'">'.$h_time.'</option>';
                                                }
                                                ?>
                                            </select>
                                            <select class="form-control" id="work_time_start_m" name="work_time_start_m">
                                                <?php
                                                for($i=0;$i<=59;$i++){
                                                    if($i<10){ $m_time = "0".$i; }else{ $m_time = $i;}
                                                    echo '<option value="'.$m_time.'">'.$m_time.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 form-inline">
                                        <div class="form-group">
                                            <label for="work_time_start_h">ปิดทำการ:</label>
                                            <select class="form-control" id="work_time_close_h" name="work_time_close_h">
                                                <?php
                                                for($i=0;$i<=23;$i++){
                                                    if($i<10){ $h_time = "0".$i; }else{ $h_time = $i;}
                                                    echo '<option value="'.$h_time.'">'.$h_time.'</option>';
                                                }
                                                ?>
                                            </select>
                                            <select class="form-control" id="work_time_close_m" name="work_time_close_m">
                                                <?php
                                                for($i=0;$i<=59;$i++){
                                                    if($i<10){ $m_time = "0".$i; }else{ $m_time = $i;}
                                                    echo '<option value="'.$m_time.'">'.$m_time.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="work_time_section2">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="more_work_time">เพิ่มเวลาทำการเพิ่มเติม</label>
                                <input type="checkbox" id="more_work_time" name="more_work_time" value="1" onclick="add_more_work_time()">
                                <div id="work_time_panel" style="display: none">
                                    <label for="work_time_remark">หมายเหตุเวลาทำการเพิ่มเติม</label>
                                    <input type="text" class="form-control" id="work_time_remark"
                                           placeholder="เช่น: เสาร์-อาทิตย์" name="work_time_remark">
                                    <label for="show_work_time2">เวลาทำการ:</label>
                                    <input type="text" id="show_work_time2" readonly
                                           style="border:0; color:#f6931f; font-weight:bold; width: 90px;">
                                    <label for="show_work_time2">(เพิ่มเติม):</label>
                                    <input type="hidden" id="work_time_start_2" name="work_time_start_2">
                                    <input type="hidden" id="work_time_close_2" name="work_time_close_2">
                                    <div class="row">
                                        <div class="col-md-6 form-inline">
                                            <div class="form-group">
                                                <label for="work_time_start_2_h">เปิดทำการ:</label>
                                                <select class="form-control" id="work_time_start_2_h" name="work_time_start_2_h">
                                                    <?php
                                                    for($i=0;$i<=23;$i++){
                                                        if($i<10){ $h_time = "0".$i; }else{ $h_time = $i;}
                                                        echo '<option value="'.$h_time.'">'.$h_time.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                                <select class="form-control" id="work_time_start_2_m" name="work_time_start_2_m">
                                                    <?php
                                                    for($i=0;$i<=59;$i++){
                                                        if($i<10){ $m_time = "0".$i; }else{ $m_time = $i;}
                                                        echo '<option value="'.$m_time.'">'.$m_time.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 form-inline">
                                            <div class="form-group">
                                                <label for="work_time_start_2_h">ปิดทำการ:</label>
                                                <select class="form-control" id="work_time_close_2_h" name="work_time_close_2_h">
                                                    <?php
                                                    for($i=0;$i<=23;$i++){
                                                        if($i<10){ $h_time = "0".$i; }else{ $h_time = $i;}
                                                        echo '<option value="'.$h_time.'">'.$h_time.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                                <select class="form-control" id="work_time_close_2_m" name="work_time_close_2_m">
                                                    <?php
                                                    for($i=0;$i<=59;$i++){
                                                        if($i<10){ $m_time = "0".$i; }else{ $m_time = $i;}
                                                        echo '<option value="'.$m_time.'">'.$m_time.'</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">ชื่อสถานประกอบการ</label>
                        <input type="text" class="form-control" id="name" name="name"
                               placeholder="ชื่อสถานประกอบการ">
                    </div>
                    <div class="form-group">
                        <label for="owner_name">ชื่อ – สกุลเจ้าของกิจการ</label>
                        <input type="text" class="form-control" id="owner_name" name="owner_name"
                               placeholder="ชื่อ – สกุลเจ้าของกิจการ">
                    </div>
                    <!--                    <div class="form-group">-->
                    <!--                        <label for="exampleInputEmail1">เลขที่ใบอนุญาต</label>-->
                    <!--                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">-->
                    <!--                    </div>-->
                    <div class="form-group">
                        <label for="type">ประเภทการให้บริการ</label>
                        <select class="form-control" id="type" name="type">
                            <?php if (count($category_list_1) > 0) {
                                foreach ($category_list_1 as $cate) {
                                    echo '<option value="' . $cate['id'] . '">' . $cate['title'] . '</option>';
                                }
                            } else {
                                echo '<option value="0">ไม่มีข้อมูล</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="">ประเภทงานบริการ</label>
                                <div>
                                    <?php if (count($category_list_2) > 0) {
                                        foreach ($category_list_2 as $cate) {
                                            echo '<label class="checkbox-inline">';
                                            echo '<input type="checkbox" id="cate_' . $cate['id'] . '" name="service_type[]" value="' . $cate['id'] . '"> ' . $cate['title'];
                                            echo '</label>';
                                        }
                                    } else {
                                        echo '<label class="checkbox-inline">ไม่มีข้อมูล</label>';
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="cover">รูปหน้าปกหลักของสถานประกอบการ  (ใช้รูปขนาด 1024 x 768 เท่านั้น)</label>
                        <input type="file" id="cover" name="cover">
                        <p class="help-block">ต้องเป็นไฟล์ jpg, png ขนาดไม่เกิน 4MB.</p>
                    </div>
                    <div class="form-group">
                        <label for="slogan">คำจำกัดความการให้บริการ (Slogan) ไม่เกิน 100ตัวอักษร</label>
                        <textarea class="form-control" id="slogan" name="slogan" rows="8" maxlength="100"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="">สิ่งอำนวยความสะดวก</label>
                                <div>
                                    <?php if (count($category_list_3) > 0) {
                                        foreach ($category_list_3 as $cate) {
                                            echo '<label class="checkbox-inline">';
                                            echo '<input type="checkbox" id="cate_' . $cate['id'] . '" name="facilities[]" value="' . $cate['id'] . '"> ' . $cate['title'];
                                            echo '</label>';
                                        }
                                    } else {
                                        echo '<label class="checkbox-inline">ไม่มีข้อมูล</label>';
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="payment_1">การชำระเงิน</label>
                                <div>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="payment_1" name="payment[]" value="cash"> เงินสด
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="payment_2" name="payment[]" value="credit">
                                        บัตรเครดิต
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="award_certification">รางวัลที่ได้รับ/ใบรับรองมาตรฐาน</label>
                        <input type="text" class="form-control" id="award_certification" name="award_certification"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="privilege">สิทธิพิเศษ</label>
                        <input type="text" class="form-control" id="privilege" name="privilege" placeholder="">
                    </div>

                    <div class="form-group">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <label for="payment_1">สถานะ</label>
                                <div class="panel-body">
                                    <label for="exampleInputEmail1">สถานะ</label>
                                    <label class="checkbox-inline">
                                        <input type="radio" id="bus_status_pending" name="status" value="pending" checked > Pending
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" id="bus_status_active" name="status" value="active" > Active
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" id="bus_status_active" name="status" value="vip" > V.I.P
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row" style="padding-bottom: 16px;">
                <div class="col-md-12 text-center">
                    <input type="hidden" class="form-control" id="do" name="do" value="post">
                    <input type="hidden" class="form-control" id="client" name="client" value="cms">
                    <input type="hidden" class="form-control" id="callback" name="callback" value="<?php echo site_url('business');?>">
                    <input type="hidden" class="form-control" id="api_key" name="api_key" placeholder="<?php echo api_key();?>">
                    <button type="reset" class="btn btn-default">Reset</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</div>

<div id="markUp">
    <div id="showMarkUp" style="background-color: #555;">

    </div>
</div>
<script>
    function add_staff() {
        $.fancybox.open({href: '#markUp'});
    }

    function del_business() {
        if (confirm('คุณต้องการลบสถานที่หรือไม่')) {

        }
    }
    function add_more_work_time() {
        if ($("#more_work_time:checked").val() == 1) {
            $("#work_time_panel").show();
        } else {
            $("#work_time_panel").hide();
        }
    }
    function get_location_data() {
        geocodePosition(marker.getPosition());
    }
    function generate_address(location_data) {
//        console.log(location_data.address_components);
        var road = '', tambol = '', district = '', province = '', postcode = '',country = '', country_code = '';
        $.each(location_data.address_components, function (index, value) {
            if (value.types[0] == 'route') {
                road = value.long_name;
            } else if (value.types[0] == 'political') {
                if (value.types[2] == 'sublocality_level_2') {
                    tambol = value.long_name.replace("ตำบล ", "");
                    tambol = tambol.replace("ตำบล", "");
                    tambol = tambol.replace("แขวง ", "");
                    tambol = tambol.replace("แขวง", "");
                } else if (value.types[2] == 'sublocality_level_1') {
                    district = value.long_name.replace("อำเภอ ", "");
                    district = district.replace("อำเภอ", "");
                    district = district.replace("เขต ", "");
                    district = district.replace("เขต", "");
                }
            } else if (value.types[0] == 'administrative_area_level_1' && value.types[1] == 'political') {
                province = value.long_name;
            } else if (value.types[0] == 'administrative_area_level_2' && value.types[1] == 'political') {
                district = value.long_name.replace("อำเภอ ", "");
                district = district.replace("อำเภอ", "");
                district = district.replace("แขวง ", "");
                district = district.replace("แขวง", "");
            } else if (value.types[0] == 'postal_code') {
                postcode = value.long_name;
            }else if (value.types[0] == 'country'&&value.types[1] == 'political') {
                country = value.long_name;
                country_code = value.short_name;
            }

        });
//        console.log(tambol);
        if (tambol == '') {
            $.each(location_data.address_components, function (index, value) {
                if (value.types[0] == 'locality' && value.types[1] == 'political') {
                    tambol = value.long_name.replace("ตำบล ", "");
                    tambol = tambol.replace("ตำบล", "");
                    tambol = tambol.replace("แขวง ", "");
                    tambol = tambol.replace("แขวง", "");
                }
            });
        }
//        console.log(tambol);
        if (road == "Unnamed Road") {
            road = '';
        }
        $("#address_road").val(road);
        $("#address_tambol").val(tambol);
        $("#address_district").val(district);
        $("#address_province").val(province);
        $("#address_postcode").val(postcode);
        $("#country").val(country);
        $("#country_code").val(country_code);
        check_valid();
    }
    function initMap() {
    }
    function update_showtime(section) {
        if(section==1){
            var work_time_start_h = $("#work_time_start_h").val();
            var work_time_start_m = $("#work_time_start_m").val();
            var work_time_close_h = $("#work_time_close_h").val();
            var work_time_close_m = $("#work_time_close_m").val();
            $("#show_work_time").val(work_time_start_h+':'+work_time_start_m+' - '+work_time_close_h+':'+work_time_close_m);
            $("#work_time_start").val(work_time_start_h+':'+work_time_start_m);
            $("#work_time_close").val(work_time_close_h+':'+work_time_close_m);
        }else{
            var work_time_start_2_h = $("#work_time_start_2_h").val();
            var work_time_start_2_m = $("#work_time_start_2_m").val();
            var work_time_close_2_h = $("#work_time_close_2_h").val();
            var work_time_close_2_m = $("#work_time_close_2_m").val();
            $("#show_work_time2").val(work_time_start_2_h+':'+work_time_start_2_m+' - '+work_time_close_2_h+':'+work_time_close_2_m);
            $("#work_time_start_2").val(work_time_start_2_h+':'+work_time_start_2_m);
            $("#work_time_close_2").val(work_time_close_2_h+':'+work_time_close_2_m);
        }
    }

    $(document).ready(function () {
        update_showtime(1);
        update_showtime(2);
        $("#work_time_section select").on('change', function(){
            update_showtime(1);
        });
        $("#work_time_section2 select").on('change', function(){
            update_showtime(2);
        });
        $('#add_business_form')
            .on('init.field.bv', function (e, data) {
                var $parent = data.element.parents('.form-group'),
                    $icon = $parent.find('.form-control-feedback[data-bv-icon-for="' + data.field + '"]');
                $icon.on('click.clearing', function () {
                    if ($icon.hasClass('glyphicon-remove')) {
                        data.bv.resetField(data.element);
                    }
                });
            })

            .bootstrapValidator({
                feedbackIcons: {
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    name: {validators: {notEmpty: {message: 'กรุณาใส่ชื่อสถานประกอบการ'}}},
                    owner_name: {validators: {notEmpty: {message: 'กรุณาใส่ชื่อ - สกุล เจ้าของสถานประกอบการ'}}},
                    tel: {validators: {notEmpty: {message: 'กรุณากรอกข้อมูล'}}},
                    type: {validators: {notEmpty: {message: 'กรุณาเลือกข้อมูล'}}},
                    cover: {validators: {notEmpty: {message: 'กรุณาเลือกรูปภาพประจำสถานประกอบการ'}}},
                    address_tambol: {validators: {notEmpty: {message: 'กรุณากรอกข้อมูล'}}},
                    address_district: {validators: {notEmpty: {message: 'กรุณากรอกข้อมูล'}}},
                    address_province: {validators: {notEmpty: {message: 'กรุณากรอกข้อมูล'}}},
                    address_postcode: {validators: {notEmpty: {message: 'กรุณากรอกข้อมูล'}}},
                    'contact[]': {validators: {notEmpty: {message: 'กรุณาเลือกข้อมูล'}}},
                    'service_type[]': {validators: {notEmpty: {message: 'กรุณาเลือกข้อมูล'}}},
                    'facilities[]': {validators: {notEmpty: {message: 'กรุณาเลือกข้อมูล'}}},
                    'payment[]': {validators: {notEmpty: {message: 'กรุณาเลือกข้อมูล'}}}
                }
            });
    });

    function check_valid() {
        $('#add_business_form').bootstrapValidator('revalidateField', 'name');
        $('#add_business_form').bootstrapValidator('revalidateField', 'owner_name');
        $('#add_business_form').bootstrapValidator('revalidateField', 'tel');
        $('#add_business_form').bootstrapValidator('revalidateField', 'type');
        $('#add_business_form').bootstrapValidator('revalidateField', 'cover');
        $('#add_business_form').bootstrapValidator('revalidateField', 'address_tambol');
        $('#add_business_form').bootstrapValidator('revalidateField', 'address_district');
        $('#add_business_form').bootstrapValidator('revalidateField', 'address_province');
        $('#add_business_form').bootstrapValidator('revalidateField', 'address_postcode');
        $('#add_business_form').bootstrapValidator('revalidateField', 'contact[]');
        $('#add_business_form').bootstrapValidator('revalidateField', 'facilities[]');
        $('#add_business_form').bootstrapValidator('revalidateField', 'service_type[]');
        $('#add_business_form').bootstrapValidator('revalidateField', 'payment');
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo google_key();?>&libraries=places&callback=myMaps&language=th&region=TH" async defer></script>

