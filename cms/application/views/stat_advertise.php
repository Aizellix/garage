<?php
$errorMSG = "";
$doneMSG = "";
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">ข้อมูลสถิติการเข้าชมโฆษณา</strong>
</div>
<div class="col-md-12" style="clear: both;">

    <div id="main_content" class="">
        <div id="overall_stat">
            <form method="get">
                <div class="form-group" >
                    <div class="row">
                        <div class="form-inline col-md-12">
                            <label for="start_time">ข้อมูลสถิติ ตั้งแต่</label>
                            <input type="text" class="form-control" id="start_time" name="start" style="width: 220px;" value="<?php echo $start?>">
                            <label for="end_time">ถึง</label>
                            <input type="text" class="form-control" id="end_time" name="to" style="width: 220px;" value="<?php echo $to?>">
                            <button class="btn btn-primary" type="submit">ดำเนินการ</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div id="show_stat">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 50px;" class="text-center">ลำดับ</th>
                                <th class="text-center">รายชื่อ</th>
                                <th style="width: 100px;" class="text-center">การเข้าชม</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($total>0){
                                $i = 1;
                                $class_page = 1;
                                $j = 0;
                                foreach ($data as $item){
                                    echo '<tr class="all_page page_'.$class_page.'" style="display: none;"> <td class="text-center">'.$i.'</td>';
                                    echo '<td class="text-left"><img src="'.$item['cover_image_src'].'" style="height: 80px; cursor: pointer;" onclick="view_data('.$item['id'].')"></td>';
                                    echo '<td class="text-center">'.$item['total'].'</td></tr>';
                                    echo '<input type="hidden" name="start_time_'.$item['id'].'" id="start_time_'.$item['id'].'" value="'.$item['start_time'].'">';
                                    echo '<input type="hidden" name=end_time_'.$item['id'].'"" id="end_time_'.$item['id'].'" value="'.$item['end_time'].'">';
                                    echo '<input type="hidden" name="image_src_'.$item['id'].'" id="image_src_'.$item['id'].'" value="'.$item['cover_image_src'].'">';
                                    echo '<input type="hidden" name="bid_'.$item['id'].'" id="bid_'.$item['id'].'" value="'.$item['business_id'].'">';

                                    $i++;
                                    $j++;
                                    if($j>=$limit_page){
                                        $j=0;
                                        $class_page++;
                                    }
                                }
                            }else{
                                echo '<tr> <td class="text-center" colspan="3">ไม่พอข้อมูบ</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <ul class="pagination" id="paging">
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="clearfix"></div>
</div>
<div id="markUp">
    <div id="showMarkUp" style="width: 550px;">
        <div style="max-width: 530px; width: 99%">
            <div class="col-md-12">

                <div id="advertise_editor" style="padding-top: 16px; height: 550px; width: 500px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #ccc;">
                            <th colspan="2" class="text-center" id="header_form_ads">
                                ข้อมูลของโฆษณา
                            </th>
                        </tr>
                        </thead>
                        <tbody id="searchResult" >
                        <tr  style="width: 100%; " id="ad_img_show">
                            <td colspan="2" class="text-left" >
                                <strong>รูปประกอบโฆณา: </strong>
                                <img src="" style="width: 100%" id="ad_img">
                            </td>
                        </tr>
                        <tr  style="width: 100%;">
                            <td colspan="2" class="text-left" >
                                <strong>ชื่อถสถานประกอบการ: </strong>
                                <a class="text-center" id="ad_business_name" href="" target="_blank"></a>
                            </td>
                        </tr>
                        <tr style="width: 100%;">
                            <td colspan="2" class="text-left" id="">
                                <label for="">ระยะเวลาการเผยแพร่</label>
                            </td>
                        </tr>
                        <tr style="width: 100%;">
                            <td class="text-left" id="">
                                <label for="start_time">ตั้งแต่</label>
                            </td>
                            <td class="text-left" id="">
                                <span id="start_time_text"></span>
                            </td>
                        </tr>
                        <tr style="width: 100%;">
                            <td class="text-left" id="">
                                <label for="end_time">ถึง</label>
                            </td>
                            <td class="text-left" id="">
                                <span id="end_time_text"></span>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
        gern_page();
    } );
    var page = 1;
    var total_page = <?php echo $total_page; ?>;

    function goto_page(page_id) {
        page = page_id;
        gern_page();
    }

    function gern_page() {
        var html_page = '';
        if(total_page>0){
            if ((page - 1) > 0) page_back = page - 1; else page_back = page;
            if ((page + 1) < total_page) page_next = page + 1; else page_next = total_page;
            html_page = '<li><a href="javascript:void(0);" onclick="goto_page(' + page_back + ')">&laquo;</a></li>';
            for (var i = 1; i <= total_page; i++) {
                if (i == page) is_active = ' class="active"'; else is_active = '';
                if (i <= 3 || i >= (total_page - 2) || i == page) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_back) {
                    html_page += '<li class="disabled"><span>...</span></li> <li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_next) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li><li class="disabled"><span>...</span></li>';
                } else {
                    html_page += "#";
                }
            }
            while (html_page.indexOf("##") > 1) {
                html_page = html_page.replace("##", '#');
            }
            html_page = html_page.replace("#", '<li class="disabled"><span>...</span></li>');
            html_page = html_page.replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>');
            html_page += '<li><a href="javascript:void(0);" onclick="goto_page(' + page_next + ')">&raquo;</a></li>';
        }
        $("#paging").html(html_page);
        $(".all_page").hide();
        $(".page_"+page).show();
    }

    function view_data(id) {
        var b_id = $("#bid_"+id).val();
        $.ajax({
            url: '<?php echo api_site_url('business/get_data'); ?>/'+b_id,
            type: 'get',
            dataType: 'json',
            success: function (respond) {
                $(respond.data).each(function (index) {
                    var item = respond.data[index];
                    var b_name = item.name;

                    var start_time = $("#start_time_"+id).val();
                    var end_time = $("#end_time_"+id).val();
                    var image_src = $("#image_src_"+id).val();
                    $("#ad_id").val(id);
                    $("#ad_business_name").html(b_name);
                    $("#ad_business_name").attr('href','<?php echo site_url('business/view')?>/'+b_id);
                    $("#ad_business_id").val(b_id);
                    $("#start_time_text").html(start_time);
                    $("#end_time_text").html(end_time);
                    if(image_src!=""){
                        $("#ad_img").attr('src',image_src);
                        $("#ad_img").show();
                    }else{
                        $("#ad_img").hide();
                        $("#ad_img").attr('src',"");
                    }

                    $("#do_update").val('view');
                    $.fancybox.open({href : '#markUp'} );
                })

            }
        });

    }

</script>
