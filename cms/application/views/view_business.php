<script>
    $(function () {
        $("#slider-range").slider({
            range: true,
            min: 0,
            max: 1440,
            step: 30,
            values: [0, 1440],
            slide: function (event, ui) {
                set_worktime(ui.values[0], ui.values[1], $("#work_time_start"), $("#work_time_close"), $("#show_work_time"));
            }
        });
        $("#slider-range2").slider({
            range: true,
            min: 0,
            max: 1440,
            step: 30,
            values: [0, 1440],
            slide: function (event, ui) {
                set_worktime(ui.values[0], ui.values[1], $("#work_time_start_2"), $("#work_time_close_2"), $("#show_work_time2"));
            }
        });
        set_worktime($("#slider-range").slider("values", 0), $("#slider-range").slider("values", 1), $("#work_time_start"), $("#work_time_close"), $("#show_work_time"));
        set_worktime($("#slider-range2").slider("values", 0), $("#slider-range2").slider("values", 1), $("#work_time_start_2"), $("#work_time_close_2"), $("#show_work_time2"));
    });

    function set_worktime(num_start, num_end, obj_time_tart, obj_time_end, obj_show_time) {
        var time_start = Math.floor(num_start / 60);
        if (time_start < 10)time_start = "0" + time_start;
        if (num_start % 60 < 10) {
            time_start = time_start + ":0" + num_start % 60;
        } else {
            time_start = time_start + ":" + num_start % 60;
        }
        var time_end = Math.floor(num_end / 60);
        if (time_end < 10)time_end = "0" + time_end;
        if (num_end % 60 < 10) {
            time_end = time_end + ":0" + num_end % 60;
        } else {
            time_end = time_end + ":" + num_end % 60;
        }
        obj_time_tart.val(time_start);
        obj_time_end.val(time_end);
        obj_show_time.val(time_start + " - " + time_end);
    }
</script>

<script type="text/javascript">

    var marker;

    function myMaps() {
        var mapsGoo = google.maps;
        var Position = new mapsGoo.LatLng(<?php echo $business_data->lat;?>, <?php echo $business_data->lon;?>);

        var myOptions = {
            center: Position,
            scrollwheel: false,
            draggable: false,
            zoomControl: true,
            disableDoubleClickZoom: true,
            zoom: 16,
            mapTypeId: mapsGoo.MapTypeId.ROADMAP
        };
        var map = new mapsGoo.Map(document.getElementById("map_canvas"), myOptions);

        marker = new mapsGoo.Marker({
            position: Position,
            draggable: false,
            title: "คลิกแล้วเคลื่อนย้ายหมุดไปยังตำแหน่งที่ต้องการ"
        });
        var Posi = marker.getPosition();
        marker.setMap(map);

    }

    $(document).ready(function () {
        myMaps();//แสดงแผนที่
//            $('#maps_form').myMaps_submit();//ตรวจสอบการSubmit Form
    });
    function initMap() {
    }
</script>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลผู้ประกอบการ</strong>
</div>

<div class="col-lg-12" style="clear: both;">
    <strong style="font-size: 16px;">ข้อมูลผู้ประกอบการ</strong>
    <form action="<?php echo api_site_url('business/add_business') ?>" method="post" enctype="multipart/form-data"
          id="add_business_form">
        <div id="main_content" class="">
            <div class="row">
                <div class="col-md-12 col-lg-9" style="">
                    <div id="map_canvas" style="width:100%; height:300px;"></div>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <strong for="exampleInputEmail1">Latitude: </strong>
                                        <span><?php echo $business_data->lat;?></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <strong for="exampleInputEmail1">Longitude: </strong>
                                        <span><?php echo $business_data->lon;?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_no">เลขที่: </strong>
                                        <span><?php echo $business_data->address_no;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_moo">หมู่ที่: </strong>
                                        <span><?php echo $business_data->address_moo;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_village">หมู่บ้าน: </strong>
                                        <span><?php echo $business_data->address_village;?></span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_soi">ซอย: </strong>
                                        <span><?php echo $business_data->address_soi;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_road">ถนน: </strong>
                                        <span><?php echo $business_data->address_road;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_tambol">ตำบล/แขวง: </strong>
                                        <span><?php echo $business_data->address_tambol;?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_district">อำเภอ/เขต: </strong>
                                        <span><?php echo $business_data->address_district;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_province">จังหวัด: </strong>
                                        <span><?php echo $business_data->address_province;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="address_postcode">รหัสไปษณีย์: </strong>
                                        <span><?php echo $business_data->address_postcode;?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="tel">เบอโทร์: </strong>
                                        <span><?php echo $business_data->tel;?></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="tel">Facebook: </strong>
                                        <span>
                                    <?php if(isset($business_data->facebook)&&$business_data->facebook!=""){
                                        $fb_link = $business_data->facebook;
                                        if (strpos($fb_link,'//') === false) {
                                            $fb_link = 'http://'.$fb_link;
                                        }
                                        ?>
                                        <a href="<?php echo $fb_link;?>" target="_blank"><?php echo $business_data->facebook;?></a>
                                    <?php }?>
                                </span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="tel">Line: </strong>
                                        <span><?php echo $business_data->line;?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <strong for="tel">Email: </strong>
                                        <span><?php echo $business_data->email;?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div>
                                    <strong for="exampleInputEmail1">วันทำการ: </strong>
                                    <span>
                                    <?php
                                    if($business_data->work_day_sun==1) echo 'อาทิตย์, ';
                                    if($business_data->work_day_mon==1) echo 'จันทร์, ';
                                    if($business_data->work_day_tue==1) echo 'อังคร, ';
                                    if($business_data->work_day_wed==1) echo 'พุธ, ';
                                    if($business_data->work_day_thu==1) echo 'พฤหัสบดี, ';
                                    if($business_data->work_day_fri==1) echo 'ศุกร์, ';
                                    if($business_data->work_day_sat==1) echo 'เสาร์, ';
                                    ?>
                                </span>
                                    <strong for="show_work_time"> &nbsp; | &nbsp; เวลาทำการ: </strong>
                                    <span> <?php echo $business_data->work_time_start.' - '.$business_data->work_time_close;?> </span>
                                </div>
                                <div>
                                    <strong for="show_work_time">*เวลาทำการสำรอง: </strong>
                                    <span> <?php echo $business_data->work_time_start_2.' - '.$business_data->work_time_close_2;?> </span>
                                </div>
                                <div>
                                    <strong for="exampleInputEmail1">*หมายเหตุ: </strong>
                                    <span> <?php echo $business_data->work_time_remark;?> </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <strong for="name">ชื่อสถานประกอบการ: </strong>
                                <span><?php echo $business_data->name;?></span>
                            </div>
                            <div class="form-group">
                                <strong for="owner_name">ชื่อ – สกุลเจ้าของกิจการ: </strong>
                                <span><?php echo $business_data->owner_name;?></span>
                            </div>
                            <!--                    <div class="form-group">-->
                            <!--                        <label for="exampleInputEmail1">เลขที่ใบอนุญาต</label>-->
                            <!--                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">-->
                            <!--                    </div>-->
                            <div class="form-group">
                                <strong for="type">ประเภทการให้บริการ: </strong>
                                <span><?php echo $category_list_1[$business_data->type];?></span>
                            </div>

                            <div class="form-group">
                                <strong for="exampleInputEmail1">ประเภทงานบริการ: </strong>
                                <span><?php

                                    $service_type = json_decode($business_data->service_type);
                                    if(count($service_type)>0){
                                        foreach ($service_type as $item){
                                            if(isset($item->id)){
                                                $item_id = $item->id;
                                            }else{
                                                $item_id = $item;
                                            }
                                            if(isset($category_list_2[$item_id])){
                                                echo $category_list_2[$item_id].', ';
                                            }
                                        }
                                    }
                                    ?>
                                </span>
                            </div>

                            <div class="form-group">
                                <strong for="type">รหัสสถานประกอบการ: </strong>
                                <strong style="font-size: 32px; color: red;"><?php echo $business_data->business_code;?></strong>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <strong>รูปหน้าปกหลักของสถานประกอบการ: </strong>
                                        <span>
                                    <a href="<?php echo $business_data->cover_image_src;?>" data-fancybox="images" data-width="500" data-height="400">
                                    <img id="cover_image" src="<?php echo $business_data->cover_image_src;?>" style="width: 100%">
                                    </a>
                                    <input type="hidden" id="recent_ceover_image_id" value="<?php echo $business_data->cover_image_id;?>">
                                </span>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <strong>รูปทั้งหมด: <span id="total_image"><?php echo $photo_total;?></span> รูป: </strong>
                                            <a href="javascript:void(0);" onclick="open_album(<?php echo $business_data->id;?>)">ดูอัลบั้ม</a>
                                        </div>
                                        <div class="row">
                                            <?php if(count($photo_simple)>0){
                                                $n=1;
                                                foreach ($photo_simple as $item){

                                                    ?>
                                                    <div class="col-md-4" style="padding: 0; margin: 0;">
                                                        <a href="<?php echo $item->src;?>" data-fancybox="images" data-width="500" data-height="400">
                                                            <div class="album_img_bg_<?php echo $n;?>" style="display: inline-block; height: 96px; width: 100%; background: url('<?php echo $item->src;?>')no-repeat scroll center center / cover; border: 1px solid #555;">

                                                            </div>
                                                        </a>
                                                    </div>
                                                    <?php $n++;  }?>

                                            <?php }else{ ?>
                                                <div class="col-md-12 text-center">
                                                    ไม่มีรูปในอัลบั้ม
                                                </div>
                                            <?php }?>
                                        </div>
                                        <div class="row">
                                            <button style="width: 100%;" type="button" class="btn btn-default" onclick="show_uploadAlbum()">เพิ่มรูปในอัลบั้ม</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <strong for="detail">คำจำกัดความการให้บริการ (Slogan): </strong>
                                <span><?php echo $business_data->slogan;?></span>
                            </div>
                            <div class="form-group">
                                <strong for="detail">คำบรรยายสภาพแวดล้อม: </strong>
                                <div><?php echo $business_data->detail;?></div>
                            </div>
                            <div class="form-group">
                                <strong for="exampleInputEmail1">สิ่งอำนวยความสะดวก: </strong>
                                <span>
                            <?php
                            $facilities = json_decode($business_data->facilities);
                            if(count($facilities)>0){
                                foreach ($facilities as $item){
                                    if(isset($category_list_3[$item])){
                                        echo $category_list_3[$item].', ';
                                    }
                                }
                            }
                            ?>
                        </span>
                            </div>
                            <div class="form-group">
                                <strong for="exampleInputEmail1">การชำระเงิน: </strong>
                                <span>
                            <?php
                            $payment = json_decode($business_data->payment);
                            if(count($payment)>0){
                                $payment_list = array('cash'=>'เงินสด','credit'=>'บัครเคดิต');
                                foreach ($payment as $item){
                                    if(isset($payment_list[$item])){
                                        echo $payment_list[$item].', ';
                                    }
                                }
                            }
                            ?>
                        </span>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <strong for="award_certification">รางวัลที่ได้รับ/ใบรับรองมาตรฐาน: </strong>
                                <span><?php echo $business_data->award_certification;?></span>
                            </div>
                            <div class="form-group">
                                <strong for="privilege">สิทธิพิเศษ: </strong>
                                <span><?php echo $business_data->privilege;?></span>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="form-group">
                                <strong for="exampleInputEmail1">สถานะ</strong>
                                <span> <?php echo $business_data->status;?> </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <?php
                        $created_time = $business_data->created_time;
                        $updated_time = $business_data->updated_time;
                        if(isset($modifier_data['add'])){
                            $created_by = $modifier_data['add']->user_name;
                        }else{
                            $created_by = 'ไม่ทราบ';
                        }
                        if(isset($modifier_data['edit'])){
                            $updated_by = $modifier_data['edit']->user_name;
                        }else{
                            $updated_by = 'ไม่ทราบ';
                        }
                        ?>
                        <div style="font-size: 12px; color: gray;">
                            <strong>สร้างโดย: </strong><span id="created_by" style="color: #000;"><?php echo $created_by;?></span> เมื่อ <span id="created_time" style="color: #000;"><?php echo $created_time;?></span>
                        </div>
                        <div style="font-size: 12px; color: gray;">
                            <strong>ปรับปรุงโดย: </strong><span id="updated_by" style="color: #000;"><?php echo $updated_by;?></span> เมื่อ <span id="updated_time" style="color: #000;"><?php echo $updated_time;?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">

                </div>
                <div class="col-md-6">

                </div>
            </div>
            <div class="row" style="padding-bottom: 16px;">
                <div class="col-md-12 text-center">
                    <a href="<?php echo site_url('business/edit/'.$business_data->id)?>" class="btn btn-default" style="width: 200px;">แก้ไข</a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div id="img_tool" style="padding-bottom: 10px;">
            <div style="clear: both;">
                <div style="display: inline-block; float: left;">
                    <button type="button" class="btn btn-default" onclick="show_change_cover()">เลือกเปลี่ยนรูปปกใหม่</button>
                </div>
                <div style="display: inline-block; float: right;">
                    <button type="button" class="btn btn-danger" onclick="show_delete_photo()">เลือกรูปที่ต้องการลบ</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div id="img_sub_tool" style="padding-bottom: 10px; display: none;">
            <div style="clear: both;">
                <div style="display: inline-block; float: left;">
                    <button type="button" class="btn btn-default" onclick="hide_img_sub_tool()">ยกเลิก</button>
                </div>
                <div style="display: inline-block; float: right;">
                    <button style="display: none" type="button" class="btn btn-danger set_delete_image_btn" onclick="set_delete_image()">ตกลง</button>
                    <button style="display: none" type="button" class="btn btn-success set_image_cover_btn" onclick="set_image_cover()">ตกลง</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div style="background: #ddd; padding: 4px 0 4px 0px ">
            <div id="img_list">
                
            </div>
        </div>
        <div id="load_more">
            <button style="width: 100%" type="button" class="btn btn-default" onclick="load_more()">More</button>
            <input type="hidden" id="next_url" value="">
        </div>
    </div>
</div>
<div id="uploadPhoto" style="display: none;">
    <div>
        <div class="col-md-12">
            <strong for="cover">เพิ่มรูปในอัลบั้ม: </strong>
            <form id="uploadPhotoForm">
                <div class="row">
                    <div class="col-md-12"><input id="files" type="file" name="photo[]" multiple></div>
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-default" id="uploadPhotoBTN" onclick="upload_photo(0)">Upload</button>
                    </div>
                </div>
            </form>
            <output id="result">
                <div class="row"></div>
            </output>
        </div>
    </div>
</div>
<script>
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=<?php echo google_key();?>&callback=myMaps&language=th&region=TH"></script>
<script>
    var bid = '<?php echo $business_data->id;?>';
    var recent_cover_image_id = $("#recent_ceover_image_id").val();
    function open_album(bid) {
        hide_img_sub_tool();
        $.ajax({
            url:'<?php echo api_site_url('business/get_photo_list'); ?>?bid='+bid+'&limit=20&offset=0',
            dataType:'json',
            success:function(data) {
                $('#img_list').html('');
                if(data.next.length>0){
                    $('#load_more').show();
                }else{
                    $('#load_more').hide();
                }
                for(var i=0;i<data.detail.length;i+=4){
                    html_text ='<div class="row">';
                    if(data.detail[i]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i])+'</div>';
                    }
                    if(data.detail[i+1]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i+1].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i+1])+'</div>';
                    }else{
                        html_text +='<div class="col-md-3"></div>';
                    }
                    if(data.detail[i+2]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i+2].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i+2])+'</div>';
                    }else{
                        html_text +='<div class="col-md-3"></div>';
                    }
                    if(data.detail[i+3]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i+3].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i+3])+'</div>';
                    }else{
                        html_text +='<div class="col-md-3"></div>';
                    }
                    html_text +='</div>';
                    $('#img_list').append(html_text);
                }
                $('#next_url').val(data.next);
                $.fancybox.open({href : '#markUp', width : 640,
                    height : 495,
                    fitToView : false,
                    autoSize : false} );
            }
        });
    }
    function set_image_cover() {
        show_loading('on');
        obj = $("input[name='cover_image']:checked").parent().parent();
        obj_data = $(obj).find('data');
//        console.log(obj_data.data('id'),obj_data.data('src'));
        $.ajax({
            url: '<?php echo api_site_url('business/set_cover_image'); ?>',
            type: 'post',
            data: {'bid':bid,'img_id':obj_data.data('id'),'img_src':obj_data.data('src')},
            dataType: 'json',
            success: function (data) {
                $("#cover_image").attr('src',data.detail.img_src);
                alert(data.msg);
                if(data.status=="SUCCESS"){
                    recent_cover_image_id = data.detail.img_id;
                    $("#recent_ceover_image_id").val(data.detail.img_id);
                    show_loading('off');
                    $.fancybox.close();
                }
            }
        });
    }
    function set_delete_image() {
        img_num = 0;
        img_id = [];
        $("input[name='delete_image[]']:checked").each(function(index){
            img_num++;
            img_id.push($(this).val());
        });
        if(confirm('คุณต้องการจะลบรูปทั้งหมด '+img_num+' รูป ใช่หรือไม่')){
            show_loading('on');
            $.ajax({
                url: '<?php echo api_site_url('business/set_delete_image'); ?>',
                type: 'post',
                data: {'bid':bid,'img_id':img_id},
                dataType: 'json',
                success: function (data) {
//                    console.log(data);
                    alert(data.msg);
                    if(data.status=="SUCCESS"){
                        reload_album_image();
                    }
                }
            });
        }
    }
    function checked_box_delete(obj) {
        if($(obj).find('input').prop('checked')==true){
            $(obj).removeClass('image_overlay_box_unselected');
            $(obj).addClass('image_overlay_box_selected');
        }else{
            $(obj).removeClass('image_overlay_box_selected');
            $(obj).addClass('image_overlay_box_unselected');
        }
        if($('.select_delete input:checked').each(function(){}).length>0){
            $('.set_delete_image_btn').prop('disabled',false);
        }else{
            $('.set_delete_image_btn').prop('disabled',true);
        }
    }
    function checked_box_cover(obj) {
        $('.select_img_c').removeClass('image_overlay_box_selected');
        $('.select_img_c').addClass('image_overlay_box_unselected');
        $(obj).removeClass('image_overlay_box_unselected');
        $(obj).addClass('image_overlay_box_selected');
        if($('.select_cover input:checked').prop('checked')){
            $('.set_image_cover_btn').prop('disabled',false);
        }else{
            $('.set_image_cover_btn').prop('disabled',true);
        }
    }
    function hide_img_sub_tool() {
        $('.select_img_c').removeClass('image_overlay_box_selected');
        $('.select_img_c').addClass('image_overlay_box_unselected');
        $(".select_cover input").prop('checked', false);
        $(".select_delete input").prop('checked', false);
        $('.set_image_cover_btn').hide();
        $('.set_delete_image_btn').hide();
        $('#img_sub_tool').hide();
        $('.select_img_c').hide();
        $('#img_tool').show();
        $('#load_more button').show();
    }
    function img_select_box(img_data) {
        html_txt =  '<data data-id="'+img_data.id+'" data-src="'+img_data.src+'"></data>';

        if(recent_cover_image_id==img_data.id){
            html_txt +=  '<label onclick="checked_box_cover(this)" class="select_cover image_overlay_box_unselected select_img_c">';
            html_txt += '<span>ถูกตั้งเป็นรูปปก</span>';
            html_txt += '</label>';
            html_txt += '<label onclick="checked_box_delete(this)" class="select_delete image_overlay_box_unselected select_img_c">';
            html_txt += '<span>ถูกตั้งเป็นรูปปก</span>';
            html_txt += '</label>';
        }else{
            html_txt +=  '<label onclick="checked_box_cover(this)" class="select_cover image_overlay_box_unselected select_img_c"><input type="radio" name="cover_image" value="'+img_data.id+'"></label>';
            html_txt += '<label onclick="checked_box_delete(this)" class="select_delete image_overlay_box_unselected select_img_c"><input type="checkbox" name="delete_image[]" value="'+img_data.id+'"></label>';
        }
        return html_txt;
    }
    function show_uploadAlbum() {
        $("#files").val('');
        $("#uploadPhotoBTN").prop('disabled',true);
        $.fancybox.open({href : '#uploadPhoto','beforeClose': function() {
            if(image_to_upload_num>0){
                image_to_upload_num = 0;
                reload_album_image();
            }
        } });
        $("#result .row").html('');
    }
    
    function show_change_cover() {
        $('#img_sub_tool').show();
        $('#load_more button').hide();
        $('#img_tool').hide();
        $('.set_image_cover_btn').show();
        $('.set_image_cover_btn').prop('disabled',true);
        $('.select_cover').show();
        $('.img-thumbnail.img_all').each(function (index) {
//            console.log($( this ).height(),$( this ).parent().find('.select_img_c'));
            $( this ).parent().find('.select_img_c').height($( this ).height());
            $( this ).parent().find('.select_img_c input').css('margin-top',($( this ).height()/2)-8);
            $( this ).parent().find('.select_img_c span').css('margin-top',($( this ).height()/2)-12);
        })
    }

    function show_delete_photo() {
        $('#img_sub_tool').show();
        $('#load_more button').hide();
        $('#img_tool').hide();
        $('.set_delete_image_btn').show();
        $('.set_delete_image_btn').prop('disabled',true);
        $('.select_delete').show();
        $('.img-thumbnail.img_all').each(function (index) {
//            console.log($( this ).height(),$( this ).parent().find('.select_img_c'));
            $( this ).parent().find('.select_img_c').height($( this ).height());
            $( this ).parent().find('.select_img_c input').css('margin-top',($( this ).height()/2)-8);
            $( this ).parent().find('.select_img_c span').css('margin-top',($( this ).height()/2)-12);
        })
    }


    function load_more() {
        $.ajax({
            url:$('#next_url').val(),
            dataType:'json',
            success:function(data) {
                for(var i=0;i<data.detail.length;i+=4){
                    html_text ='<div class="row">';
                    if(data.detail[i]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i])+'</div>';
                    }
                    if(data.detail[i+1]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i+1].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i+1])+'</div>';
                    }else{
                        html_text +='<div class="col-md-3"></div>';
                    }
                    if(data.detail[i+2]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i+2].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i+2])+'</div>';
                    }else{
                        html_text +='<div class="col-md-3"></div>';
                    }
                    if(data.detail[i+3]!=undefined){
                        html_text +='<div class="col-md-3"><img class="img-thumbnail img_all" src="'+data.detail[i+3].src+'" style="max-height: 100%; max-width: 100%;">'+img_select_box(data.detail[i+3])+'</div>';
                    }else{
                        html_text +='<div class="col-md-3"></div>';
                    }
                    html_text +='</div>';
                    $('#img_list').append(html_text);
                }
                if(data.next.length>0){
                    $('#next_url').val(data.next);
                }else{
                    $('#load_more').hide();
                }
//                $.fancybox.open({href : '#markUp'} );
            }
        });
    }
    function reload_album_image() {
        $.ajax({
            url:'<?php echo api_site_url('business/get_photo_list'); ?>?bid='+bid+'&limit=6&offset=0',
            dataType:'json',
            success:function(data) {
                for(var i=0;i<data.detail.length;i++){
                    if($('.album_img_bg_'+(i+1)).html()==undefined){
//                        console.log('<div class="col-md-4" style="padding: 0; margin: 0;"><div class="album_img_bg_'+(i+1)+'" style="display: inline-block; height: 96px; width: 100%; background: transparent url(\''+data.detail[i].src+'\');"></div></div>');
                        $('.album_img_bg_'+(i)).parent().after('<div class="col-md-4" style="padding: 0; margin: 0;"><div class="album_img_bg_'+(i+1)+'" style="display: inline-block; height: 96px; width: 100%; background: transparent url(\''+data.detail[i].src+'\') no-repeat scroll center center / cover ; border: 1px solid rgb(85, 85, 85);"></div></div>')
                    }else{
                        $('.album_img_bg_'+(i+1)).css('background-image',"url('"+data.detail[i].src+"')");
                        $('.album_img_bg_'+(i+1)).show();
                    }
                }
                if(data.detail.length<6){
                    for(var i=6;i>data.detail.length;i--){
                        $('.album_img_bg_'+(i)).hide();
                    }
                }
                $("#total_image").html(data.total);
                show_loading('off');
                $.fancybox.close();
            }
        });
    }

    function upload_photo(index_img) {
        $("#uploadPhotoBTN").prop('disabled',true);
        var  formData = new FormData();
        var file_data = $("#files").prop("files")[index_img];
        if(file_data==undefined){
            return false;
        }
        formData.append("photo", file_data);
        formData.append("do", 'post');
        formData.append("bid", '<?php echo $business_data->id;?>');
        setTimeout(function(){
            $.ajax({
                url: '<?php echo api_site_url('business/upload_image?'); ?>'+file_data.lastModified,
                type: 'POST',
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',function (e) {
                            if(e.lengthComputable) {
                                $('#img_p'+index_img).attr({value: e.loaded, max: e.total});
                                image_to_upload_num++;
                            }
                        }, false);
                    }
                    return myXhr;
                },
                success: function (data) {
                    upload_photo(index_img+=1);
//                    console.log(index_img,data);
                    if(data.result=='ERROR'){
                        $('#img_p'+index_img).before('<span style="color: #FF0000;">ไม่สำเร็จ</span>');
                        $('#img_p'+index_img).hide();
                    }
                },
                data: formData,
                cache: false,
                contentType: false,
                processData: false
            });
        }, 1000);

        return false
    }

    var image_to_upload_num = 0;

    function handleFileSelect(event) {
        image_to_upload_num = 0;
        var files = event.target.files;
        $("#result .row").html('');
        var n = 0;
        if(files.length>0){
            $("#uploadPhotoBTN").prop('disabled',false);
        }
        for(var i = 0; i< files.length; i++)
        {
            var file = files[i];
            if(!file.type.match('image'))
                continue;
            var picReader = new FileReader();

            picReader.addEventListener("load",function(event){
                var picFile = event.target;
                var html_text ='<div class="col-md-3 thumbnail" >';
                html_text +='<img class="" src="'+picFile.result+'" style="max-height: 100%; max-width: 100%;">';
                html_text +='<progress id="img_p'+n+'" value="0" max="100"></progress></div>';
                n++;
                $("#result .row").append(html_text);
            });
            picReader.readAsDataURL(file);
        }
    }
    document.getElementById('files').addEventListener('change', handleFileSelect, false);

    function show_loading(state) {
        if(state=='on'){
            $("#wait_loading").show();
        }else{
            $("#wait_loading").hide();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });
</script>

<style>

    #showMarkUp, #uploadPhoto{
        width: 640px;
        height: 490px;
    }
    #uploadPhoto .thumbnail{
        padding: 4px;
    }
    #uploadPhoto img{
        height: 90px;
        padding-bottom: 2px;
    }
    #uploadPhoto progress{
        width: 100%;
    }
    #uploadPhoto{
        overflow-y: scroll;
    }
    #img_list{
        width: 100%;
        overflow-y: scroll;

        height: 400px;
    }
    #img_list .row{
        margin: 0;
    }
    #img_list div div{
        padding-top: 8px;
    }
    #img_list div div img{
        border: 1px solid #ccc;
    }
    #load_more{
        padding-top: 8px;
    }
</style>