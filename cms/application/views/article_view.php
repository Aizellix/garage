<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการบทความ</strong>
</div>

<div class="col-md-12 col-lg-8" style="clear: both;">
    <strong style="font-size: 16px;">บทความ</strong>
    <form action="<?php echo site_url('article/update/add')?>" method="post" enctype="multipart/form-data">
        <div id="main_content">
            <div class="form-group">
                <label for="article_title">ชื่อหัวข้อบทความ</label>
                <p style="font-size: 24px; font-weight: bold;"><?php echo $data_list->title;?></p>
            </div>
            <div class="form-group">
                <label for="article_cover">รูปภาพหัวข้อบทความ</label>
                <div>
                    <img src="<?php echo $data_list->cover_image_src;?>" style="width: 50%">
                </div>
            </div>
            <div class="form-group">
                <label for="article_detail">ข้อความบรรยาย</label>
                <div style="padding: 20px; border: 1px #EEE solid; background-color: #F5F5F5;">
                    <?php echo $data_list->detail;?>
                </div>
            </div>
            <div class="form-group" >
                <label for="">ระยะเวลาการเผยแพร่ :
                    <?php echo date('Y-m-d',strtotime($data_list->start_time));?> -
                    <?php echo date('Y-m-d',strtotime($data_list->end_time));?>
                </label>
            </div>
            <div class="form-group" >
                <label for="">รายชื่อสถานประกอบการ</label>
                <table class="table table-bordered">
                    <thead>
                    <tr style="background: #ccc;">
                        <th colspan="2">
                            <div class="row">
                                <div class="col-md-6 text-left">ทั้งหมด: <span id="b_total"><?php echo count($business_list);?></span></div>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="businessList">
                    <?php if(count($business_list)>0){
                        foreach ($business_list as $item){
                            ?>
                            <tr id="0_list">
                                <td class="text-center" style="padding: 4px; width: 120px;" valign="middle">
                                    <img src="<?php echo $item->cover_image_src;?>" style="width: 100%">
                                </td>
                                <td class="text-left">
                                    <a href="<?php echo site_url('business/view/'.$item->id);?>" target="_blank"><?php echo $item->name;?></a><br>
                                    <span class="small" style="color: gray;"><?php echo $item->address_district;?>, <?php echo $item->address_province;?></span>
                                </td>
                            </tr>
                        <?php } }else{ ?>
                        <tr id="0_list">
                            <td class="text-center" colspan="2">
                                ไม่พบข้อมูล
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <?php
                $created_time = $data_list->created_time;
                $updated_time = $data_list->updated_time;
                if(isset($modifier_data['add'])){
                    $created_by = $modifier_data['add']->user_name;
                }else{
                    $created_by = 'ไม่ทราบ';
                }
                if(isset($modifier_data['edit'])){
                    $updated_by = $modifier_data['edit']->user_name;
                }else{
                    $updated_by = 'ไม่ทราบ';
                }
                ?>
                <div style="font-size: 12px; color: gray;">
                    <strong>สร้างโดย: </strong><span id="created_by" style="color: #000;"><?php echo $created_by;?></span> เมื่อ <span id="created_time" style="color: #000;"><?php echo $created_time;?></span>
                </div>
                <div style="font-size: 12px; color: gray;">
                    <strong>ปรับปรุงโดย: </strong><span id="updated_by" style="color: #000;"><?php echo $updated_by;?></span> เมื่อ <span id="updated_time" style="color: #000;"><?php echo $updated_time;?></span>
                </div>
            </div>
            <button type="button" class="btn btn-default" onclick="javascript:location.assign('<?php echo site_url('article/edit/'.$data_list->id);?>')">แก้ไข</button>

        </div>
    </form>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 450px;">
            <div class="col-md-12">
                <form action="<?php echo site_url('article/update')?>" method="post" id="emergency_form">
                    <input value="" name="do" id="post_method" type="hidden">
                    <input value="" name="id" id="item_id" type="hidden">

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
         var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd 00:00',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd 23:59',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
</script>
<script>
    function del_data(id) {
        if(confirm('คุณต้องการลบบทความนี้หรือไม่?')){
            $("#post_method").val('del');
            $("#item_id").val(id);
            $("#emergency_form").submit();
        }
    }
</script>