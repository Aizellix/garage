<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">การจัดการผู้ดูแลระบบ</strong>
</div>

<div class="col-lg-12" style="">
    <div class="row">
        <div class="col-md-6 col-lg-6">
            <?php if($this->input->get('error')=='1'){ ?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-alert"></span> พบข้อมผิดพลาด! คุณใส่หรัสผ่านเดิมไม่ถูกต้อง.
                </div>
            <?php }else if($this->input->get('error')=='2'){?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-alert"></span> พบข้อมผิดพลาด! คุณกรอกรกัสผ่านใหม่และยืนยันรหัสผ่านใหม่ไม่ตรงกัน.
                </div>
            <?php }else if($this->input->get('success')=='1'){?>
                <div class="alert alert-success" role="alert">
                    <span class=" glyphicon glyphicon-ok-circle"></span> ระบบทำการบันทึกข้อมูลส่วนตัวของคุณเรียบร้อยแล้ว.
                </div>
            <?php }?>
        </div>
    </div>
    <strong style="font-size: 16px;">รายการผู้ดูแลระบบ</strong>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <strong class="col-md-12" style="font-size: 16px;">Edit Information</strong>
                <form id="edit_form" action="<?php echo site_url('account/update'); ?>" method="post"
                      accept-charset="utf-8">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label form="name_staff">ชื่อ - สกุล</label>
                                <input class="form-control" type="text" name="name_staff" id="name_staff" placeholder="Name Surname" value="<?php echo $account_info->name;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label form="email_staff">E-mail</label>
                                <input class="form-control" type="email" name="email_staff" id="email_staff" placeholder="email@example.com" value="<?php echo $account_info->email;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label form="tel_staff">หมายของโทรศัพท์</label>
                                <input class="form-control" type="text" name="tel_staff" id="tel_staff" placeholder="0987654321" value="<?php echo $account_info->tel;?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right" type="button" onclick="updateUserInfo()">ยืนยัน</button>
                                <button class="btn btn-default pull-left" type="reset">รีเซต</button>
                                <input type="hidden" value="<?php echo $account_info->id;?>" name="user_id">
                                <input type="hidden" value="user_info" name="do">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <strong class="col-md-12" style="font-size: 16px;">เปลี่ยนรหัสผ่าน</strong>
                <form id="change_pass" action="<?php echo site_url('account/update'); ?>" method="post"
                      accept-charset="utf-8">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label form="new_pass">รหัสผ่านใหม่</label>
                                <input class="form-control" type="password" name="new_pass" id="new_pass">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label form="new_pass2">ยืนยันรหัสผ่านใหม่</label>
                                <input class="form-control" type="password" name="new_pass2" id="new_pass2">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <label form="old_pass">รหัสผ่านเก่า</label>
                                <input class="form-control" type="password" name="old_pass" id="old_pass">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="col-md-12">
                                <button class="btn btn-primary pull-right" type="button" onclick="changePassword()">เปลี่ยน</button>
                                <input type="hidden" value="<?php echo $account_info->id;?>" name="user_id">
                                <input type="hidden" value="password" name="do">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function updateUserInfo() {
        var email_staff = $("#email_staff").val();
        var name_staff = $("#name_staff").val();
        var tel_staff = $("#tel_staff").val();
        if(email_staff!=""&&name_staff!=""&&tel_staff!=""){
            $("#edit_form").submit();
        }else{
            alert('กรุณากรอกข้อมูลให้ครบก่อนกดยืนยัน');
        }
    }

    function changePassword() {
        var new_pass = $("#new_pass").val();
        var new_pass2 = $("#new_pass2").val();
        var old_pass = $("#old_pass").val();
        if(new_pass!=""&&new_pass2!=""&&old_pass!=""){
            if(new_pass!=new_pass2){
                alert('คุณกรอกรกัสผ่านใหม่และยืนยันรหัสผ่านใหม่ไม่ตรงกัน');
            }else{
                $("#change_pass").submit();
            }
        }else{
            alert('กรุณากรอกรหัสผ่านให้ถูกต้องและครบทุกช่อง');
        }
    }
</script>