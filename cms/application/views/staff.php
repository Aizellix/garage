<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">การจัดการผู้ดูแลระบบ</strong>
</div>

<div class="col-lg-12" style="">
    <strong style="font-size: 16px;">รายการผู้ดูแลระบบ</strong>
    <div class="row" style="padding-bottom: 8px;">
        <div class="col-lg-12" style="display: none">
            <form class="form-inline">
                <div class="form-group">
                    <input type="text" class="form-control" id="exampleInputName2" placeholder="ค้นหา">
                </div>
                <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
            </form>
        </div>
        <div class="col-lg-12 text-right">
            <button id="add_staff_btn" type="button" class="btn btn-default" style="color: green;"  onclick="add_staff()"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</button>
            <button id="del_staff_btn" type="button" class="btn btn-default disabled" style="color: gray;"  onclick="del_staff()"><span class="glyphicon glyphicon-remove"></span> ลบ</button>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th class="text-center" style="width: 150px;">วันที่สร้าง</th>
            <th class="text-center" style="">Username</th>
            <th class="text-center" style="">Email</th>
            <th class="text-center" style="">ชื่อ-นามสกุล</th>
            <th class="text-center" style="">หมายเลขโทรศัพท์</th>
            <th class="text-center" style="">ประเภท</th>
            <th class="text-center" style="width: 110px;">Tools</th>
        </tr>
        </thead>
        <tbody>
        <?php if(count($staff_list)>0){ foreach ($staff_list as $item){?>
        <tr>
            <td class="text-center"><?php echo date('Y-m-d H:i:s',strtotime($item->created_date))?></td>
            <td><?php echo $item->username?></td>
            <td><?php echo $item->email?></td>
            <td><?php echo $item->name?></td>
            <td><?php echo $item->tel?></td>
            <td class="text-center"><?php echo $item->type?></td>
            <td class="text-center">
                <a href="javascript:void(0);"  onclick="edit_data(<?php echo $item->id?>)" >แก้ไข</a> |
                <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>)">ลบ</a>
                <input type="hidden" name="username_<?php echo $item->id?>" id="username_<?php echo $item->id?>" value="<?php echo $item->username?>">
                <input type="hidden" name="email_<?php echo $item->id?>" id="email_<?php echo $item->id?>" value="<?php echo $item->email?>">
                <input type="hidden" name="type_<?php echo $item->id?>" id="type_<?php echo $item->id?>" value="<?php echo $item->type?>">
                <input type="hidden" name="tel_<?php echo $item->id?>" id="type_<?php echo $item->id?>" value="<?php echo $item->tel?>">
                <input type="hidden" name="name_<?php echo $item->id?>" id="type_<?php echo $item->id?>" value="<?php echo $item->name?>">
            </td>
        </tr>
        <?php }}else{ ?>
        <tr>
            <td class="text-center" colspan="5">ไม่มีข้อมูล</td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<div id="markUp">
    <div id="showMarkUp" style="width: 500px;">
        <form action="<?php echo site_url('staff/update')?>" method="post" id="staff_form">
            <div class="form-group">
                <label for="email_staff">Email address</label>
                <input type="email" class="form-control" id="email_staff" placeholder="Email" name="email_staff">
            </div>
            <div class="form-group">
                <label for="user_staff">Username</label>
                <input type="text" class="form-control" id="user_staff" placeholder="Username" name="user_staff">
            </div>
            <div class="form-group">
                <label for="user_staff">ชื่อ นามสกุล</label>
                <input type="text" class="form-control" id="name_staff" placeholder="Username" name="name_staff">
            </div>
            <div class="form-group">
                <label for="user_staff">หมายเลขโทรศัพท์</label>
                <input type="text" class="form-control" id="tel_staff" placeholder="Username" name="tel_staff">
            </div>
            <div class="form-group">
                <label for="type_staff">Type</label>
                <select class="form-control" name="type_staff" id="type_staff">
                    <option value="editor" id="type_editor">Editor</option>
                    <option value="admin" id="type_admin">Admin</option>
                </select>
            </div>
            <div>
                <div class="col-md-6 text-left" style="padding: 0;">
                    <button type="reset" class="btn btn-default">รีเซต</button>
                </div>
                <div class="col-md-6 text-right" style="padding: 0;">
                    <button type="button" class="btn btn-success" onclick="submitForm()">ยืนยัน</button>
                    <input type="hidden" name="id" id="id_staff" value="">
                    <input type="hidden" name="do" id="do_staff" value="">
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    var checked_num = 0;
    function check_del() {
        checked_num = 0;
        $(".multi-check:checked").each(function( index ) {
            checked_num++;
        });

        if(checked_num>0){
            $("#del_staff_btn").removeClass('disabled');
            $("#del_staff_btn").css('color','red');
        }else{
//            $('.multi-check').prop('checked',true);
            $("#del_staff_btn").addClass('disabled');
            $("#del_staff_btn").css('color','gray');
        }

    }

    function submitForm() {
        var user_staff = $("#user_staff").val();
        var email_staff = $("#email_staff").val();
        var name_staff = $("#name_staff").val();
        var tel_staff = $("#tel_staff").val();
        if(user_staff!=""&&email_staff!=""&&name_staff!=""&&tel_staff!=""){
            $("#staff_form").submit();
        }else{
            alert('กรุณากรอกข้อมูลให้ครบก่อนกดยืนยัน');
        }
    }

    function add_staff() {
        $("#user_staff").val('');
        $("#email_staff").val('');
        $("#name_staff").val('');
        $("#tel_staff").val('');
        $("#id_staff").val('');
        $("#do_staff").val('add');
        $("#type_staff option").prop('selected',false);
        $.fancybox.open({href : '#markUp'} );
    }
    function edit_data(id) {

        var user_staff = $("#username_"+id).val();
        var email_staff = $("#email_"+id).val();
        var name_staff = $("#name_"+id).val();
        var tel_staff = $("#tel_"+id).val();
        var type_staff = $("#type_"+id).val();
        $("#user_staff").val(user_staff);
        $("#email_staff").val(email_staff);
        $("#name_staff").val(name_staff);
        $("#tel_staff").val(tel_staff);
        $("#id_staff").val(id);
        $("#do_staff").val('edit');
        $("#type_"+type_staff).prop('selected',true);
        $.fancybox.open({href : '#markUp'} );

    }

    function del_data(id) {
        if(confirm('คุณต้องการลบบัญชีเจ้าหน้าที่นี้หรือไม่')){
            $("#id_staff").val(id);
            $("#do_staff").val('del');
            $("#staff_form").submit();
        }
    }

</script>