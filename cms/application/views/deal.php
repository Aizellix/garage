<?php
$all_selected = '';
$live_selected = '';
$expired_selected = '';
$waiting_selected = '';
$param_url = '';
$status = $this->input->get('status');
if($status=="all"){
    $all_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="live"){
    $live_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="waiting"){
    $waiting_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="expired"){
    $expired_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}

$page_title = 'deal';
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back.$param_url) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
    if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_back) {
        $html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_next) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
    } else {
        $html_page .= "#";
    }
}

while (strpos($html_page, "##") > 1) {
    $html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next.$param_url) . '">&raquo;</a></li>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการดีล</strong>
</div>
<div class="col-lg-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการดีล</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-6 text-left">
            <form class="form-inline" action="<?php echo site_url('deal')?>" method="">
                <div class="form-group">
                    <label for="search_keyword">สถานะ:
                        <select name="status" id="search_status" class="form-control">
                            <option value="all" <?php echo $all_selected;?>>ทั้งหมด</option>
                            <option value="live" <?php echo $live_selected;?>>กำลังเผยแพร่</option>
                            <option value="waiting" <?php echo $waiting_selected;?>>รอการเผยแพร่</option>
                            <option value="expired" <?php echo $expired_selected;?>>หมดอายุ</option>
                        </select>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </label>
                </div>
                <?php if($status && isset($status)&& $status!=''){?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                    </div>
                <?php } ?>
            </form>
        </div>
        <div class="col-lg-6 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="<?php echo site_url('deal/add')?>"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <?php
                $order_by = $this->input->get("order_by");
                $sort_by = $this->input->get("sort_by");
                $all_param = str_replace("sort_by=".$sort_by."&order_by=".$order_by,"",$_SERVER['QUERY_STRING']);
                $all_param = str_replace("&","",$all_param);
                $id_order = "DESC";
                $usage_order = "DESC";
                $id_sort_icon = "glyphicon-sort-by-attributes-alt";
                $usage_sort_icon = "glyphicon-sort";
                if($sort_by=="id"){
                    $id_order = ($order_by=="DESC")?"ASC":"DESC";
                    $id_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                }else if($sort_by=="usage"){
                    $usage_order = ($order_by=="DESC")?"ASC":"DESC";
                    $usage_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                    $id_sort_icon = "glyphicon-sort";
                }
                $id_order.="&".$all_param;
                $usage_order.="&".$all_param;
                ?>
                <th class="text-center" style="width: 100px;">วันที่สร้าง</th>
                <th class="text-center" style="width: 50px;" ><a href="<?php echo site_url('deal?sort_by=order_number&order_by='.$id_order);?>"><span class="glyphicon <?php echo $id_sort_icon ?> small"></span> ลำดับ</a></th>
                <th class="text-center" style="width: 100px;" >รูป</th>
                <th class="text-center" style="" >รายชื่อ</th>
                <th class="text-center" style="width: 80px;" ><a href="<?php echo site_url('deal?sort_by=usage&order_by='.$usage_order);?>"><span class="glyphicon <?php echo $usage_sort_icon ?> small"></span><br/> จำนวน<br/>ที่ถูกใช้</a></th>
                <th class="text-center" style="width: 100px;">เริ่มเผยแพร่</th>
                <th class="text-center" style="width: 100px;">สิ้นสุด</th>
                <th class="text-center" style="width: 110px;">สถานะ</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($deal_list)>0){
                foreach ($deal_list as $item){
                    ?>
                    <tr>
                        <td class="text-left"><?php echo date("Y-m-d",strtotime($item->created_time)) ;?></td>
                        <td class="text-left"><?php echo $item->order_number ;?></td>
                        <td class="text-left">
                            <a href="<?php echo $item->cover_image_src;?>" data-fancybox="images" data-width="2048" data-height="1365">
                                <img src="<?php echo $item->cover_image_src;?>" style="width: 100%">
                            </a>
                        </td>
                        <td class="text-left"><a href="<?php echo site_url('deal/view/'.$item->id);?>" target="_blank"><?php echo $item->title ;?></a></td>
                        <td class="text-center"><?php
                            $usage_number = (int)$item->usage;
                            if($usage_number>0){
                                echo '<a href="javascript:void(0);" onclick="open_log('.$item->id.');">'.$usage_number.'</a>';
                            }else{
                                echo $usage_number;
                            }
                            ?></td>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->start_time));?></td>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->end_time));?></td>
                        <td class="text-center"><?php
                            $current_time = time();
                            $start_time = strtotime($item->start_time);
                            $end_time = strtotime($item->end_time);
                            if($current_time>=$start_time&&$current_time<=$end_time){
                                echo '<strong style="color: green">กำลังเผยแพร่</strong>';
                            }else if($current_time<$start_time){
                                echo '<strong style="color: #999">รอเผยแพร่</strong>';
                            }else{
                                echo '<strong style="color: red">หมดอายุ</strong>';
                            }
                            ?></td>
                        <td class="text-center">
                            <a href="<?php echo site_url('deal/edit/'.$item->id);?>" >แก้ไข</a> |
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>);">ลบ</a>
                        </td>

                    </tr>
                    <?php }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="7">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 550px;">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr style="background: #ccc;">
                        <th class="text-center"> วันที่ </th>
                        <th class="text-center"> ชื่อผู้ใช้ </th>
                        <th class="text-center"> สถานประกอบการ </th>
                        <th class="text-center"> แต้ม </th>
                    </tr>
                    </thead>
                    <tbody id="searchResult">
                    <tr>
                        <td colspan="4" class="text-center">ไม่พบข้อมูล1</td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <ul class="pagination" id="log_paging">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <form action="<?php echo site_url('deal/update/del')?>" method="post" id="article_form">
            <input value="" name="do" id="post_method" type="hidden">
            <input value="" name="id" id="item_id" type="hidden">
        </form>
    </div>
</div>

<script type="text/javascript">
    function del_data(id) {
        if(confirm('คุณต้องการลบดีลนี้หรือไม่?')){
            $("#post_method").val('del');
            $("#item_id").val(id);
            $("#article_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });

    var total_page = 0;
    var page = 1;
    var current_deal_id = 0;
    function open_log(deal_id) {
        current_deal_id = deal_id;
        $.fancybox.open({href : '#markUp'} );
        $.ajax({
            url: "<?php echo api_site_url('deal/get_deal_log/'); ?>"+deal_id,
            type: 'get',
            data: {'limit':20,'page':page},
            dataType: 'json',
            success: function (respond) {
                var textHTML = '';
                total_page = respond.total_page;
                gen_page(total_page);

                var point = "";
                if(respond.total_num>0){
                    $(respond.data).each(function (index) {
                        var item = respond.data[index];
                        if(item.point_type=="use"){
                            point = "<span style='color: red;'>-"+item.point+"</span>";
                        }else{
                            point = "<span style='color: green;'>+"+item.point+"</span>";
                        }
                        textHTML += '<tr><td class="text-left small">'+item.created_time+'</td>';
                        textHTML += '<td class="text-left"><a href="/user/review/'+item.user_id+'" target="_blank">'+item.user_name+'</a></td>';
                        textHTML += '<td class="text-left"><a href="/business/view/'+item.business_id+'" target="_blank">'+item.business_name+'</a></td>';
                        textHTML += '<td class="text-left">'+point+'</td></tr>';
                    });
                }else{
                    textHTML += '<tr><td colspan="4" class="text-center">ไม่มีข้อมูล</td></tr>';
                }
                $("#searchResult").html(textHTML);
            }
        });
    }
    function goto_page(page_id) {
        page = page_id;
        open_log(current_deal_id);
    }

    function gen_page() {
        if(total_page>0){
            if ((page - 1) > 0) page_back = page - 1; else page_back = page;
            if ((page + 1) < total_page) page_next = page + 1; else page_next = total_page;
            html_page = '<li><a href="javascript:void(0);" onclick="goto_page(' + page_back + ')">&laquo;</a></li>';
            for (var i = 1; i <= total_page; i++) {
                if (i == page) is_active = ' class="active"'; else is_active = '';
                if (i <= 3 || i >= (total_page - 2) || i == page) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_back) {
                    html_page += '<li class="disabled"><span>...</span></li> <li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_next) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li><li class="disabled"><span>...</span></li>';
                } else {
                    html_page += "#";
                }
            }
            while (html_page.indexOf("##") > 1) {
                html_page = html_page.replace("##", '#');
            }
            html_page = html_page.replace("#", '<li class="disabled"><span>...</span></li>');
            html_page = html_page.replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>');
            html_page += '<li><a href="javascript:void(0);" onclick="goto_page(' + page_next + ')">&raquo;</a></li>';
        }
        $("#log_paging").html(html_page);
    }
</script>