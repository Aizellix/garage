<?php
$errorMSG = "";
$doneMSG = "";
//echo '<pre>';
//var_dump($data);
//echo '</pre>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">ข้อมูลสถิติการเข้าชมสถานประกอบการ</strong>
</div>
<div class="col-md-12" style="clear: both;">

    <div id="main_content" class="">
        <div id="overall_stat">
            <form method="get">
                <div class="form-group" >
                    <div class="row">
                        <div class="form-inline col-md-12">
                            <label >ข้อมูลสถิติ</label>
                            <label for="business_type"> ประเภท</label>
                            <select class="form-control" id="business_type" name="cate">
                                <option value="all">ทุกประเภท</option>
                                <?php foreach ($cate_data as $cate){
                                    if($cate->id==$this_cate){
                                        echo '<option value="'.$cate->id.'" selected>'.$cate->title.'</option>';
                                    }else{
                                        echo '<option value="'.$cate->id.'">'.$cate->title.'</option>';
                                    }

                                }?>
                            </select>
                            <label for="start_time"> ตั้งแต่</label>
                            <input type="text" class="form-control" id="start_time" name="start" style="width: 220px;" value="<?php echo $start?>">
                            <label for="end_time">ถึง</label>
                            <input type="text" class="form-control" id="end_time" name="to" style="width: 220px;" value="<?php echo $to?>">
                            <button class="btn btn-primary" type="submit">ดำเนินการ</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div id="show_stat">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 50px;" class="text-center">ลำดับ</th>
                                <th class="text-center">รายชื่อ</th>
                                <th style="width: 100px;" class="text-center">การเข้าชม</th>
                                <th style="width: 100px;" class="text-center">รีวิว</th>
                                <th style="width: 100px;" class="text-center">เรตติ้ง</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($total>0){
                                $i = 1;
                                $class_page = 1;
                                $j = 0;
                                foreach ($data as $item){
                                    $reviewed_id_list = '';
                                    foreach ($item['reviewed'] as $id){
                                        $reviewed_id_list .= $id."|";
                                    }
                                    echo '<tr class="all_page page_'.$class_page.'" style="display: none;"> <td class="text-center">'.$i.'</td>';
                                    echo '<td class="text-left"><a href="'.site_url('business/view/'.$item['id']).'" target="_blank">'.$item['data']['name'].'</a></td>';
                                    echo '<td class="text-center">'.$item['total'].'</td>';
                                    if(count($item['reviewed'])>0){
                                        echo '<td class="text-center"><a href="javascript:void(0);" onclick="view_data('.$item['id'].')">'.count($item['reviewed']).'</a></td>';
                                        echo '<td class="text-center">'.round($item['score']/count($item['reviewed'])).'</td></tr>';
                                    }else{
                                        echo '<td class="text-center">0</td>';
                                        echo '<td class="text-center">0</td></tr>';
                                    }

                                    echo '<input type="hidden" id="id_list_'.$item['id'].'" value="'.$reviewed_id_list.'">';
                                    $i++;
                                    $j++;
                                    if($j>=$limit_page){
                                        $j=0;
                                        $class_page++;
                                    }
                                }
                            }else{
                                echo '<tr> <td class="text-center" colspan="5">ไม่พอข้อมูบ</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <ul class="pagination" id="paging">
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="clearfix"></div>
</div>
<div id="markUp">
    <div id="showMarkUp" style="width: 800px;">
        <div style="width: 100%">
            <div class="col-md-12">
                <strong style="font-size: 16px; padding-top: 20px; display: block;">รายการรีวิวของผู้ใช้งาน</strong>
                <table class="table table-bordered">
                    <tbody id="review_showing">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
        gern_page();
    } );
    var page = 1;
    var total_page = <?php echo $total_page; ?>;

    function goto_page(page_id) {
        page = page_id;
        gern_page();
    }

    function gern_page() {
        var html_page = '';
        if(total_page>0){
            if ((page - 1) > 0) page_back = page - 1; else page_back = page;
            if ((page + 1) < total_page) page_next = page + 1; else page_next = total_page;
            html_page = '<li><a href="javascript:void(0);" onclick="goto_page(' + page_back + ')">&laquo;</a></li>';
            for (var i = 1; i <= total_page; i++) {
                if (i == page) is_active = ' class="active"'; else is_active = '';
                if (i <= 3 || i >= (total_page - 2) || i == page) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_back) {
                    html_page += '<li class="disabled"><span>...</span></li> <li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_next) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li><li class="disabled"><span>...</span></li>';
                } else {
                    html_page += "#";
                }
            }
            while (html_page.indexOf("##") > 1) {
                html_page = html_page.replace("##", '#');
            }
            html_page = html_page.replace("#", '<li class="disabled"><span>...</span></li>');
            html_page = html_page.replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>');
            html_page += '<li><a href="javascript:void(0);" onclick="goto_page(' + page_next + ')">&raquo;</a></li>';
        }
        $("#paging").html(html_page);
        $(".all_page").hide();
        $(".page_"+page).show();
    }

    function view_data(id) {
        var html_page = '';
        var id_list = $('#id_list_'+id).val();
        $.ajax({
            url: '<?php echo api_site_url('review/get_list'); ?>/'+id+'?limit=100000&offset=0',
            type: 'get',
            data: {id_list:id_list},
            dataType: 'json',
            success: function (respond) {
                $(respond.data).each(function (index) {
                    var review_data = respond.data[index];
                    html_page += '<tr>';
                    html_page += '<td class="text-left" rowspan="3" style="width: 150px;">';
                    html_page += '<img src="'+review_data.user_data.profile_image+'" style="width: 100%">';
                    html_page += '</td>';
                    html_page += '<td class="text-left" style="width: 150px;"><strong>สถานประกอบการ</strong></td>';
                    html_page += '<td class="text-left">';
                    html_page += '<a href="<?php echo site_url('user/review/'); ?>'+id+'" target="_blank">'+review_data.user_data.name+'</a>';
                    html_page += '</td>';
                    html_page += '<td class="text-left" style="width: 150px;"><strong>คะแนน</strong></td>';
                    html_page += '<td class="text-left">'+review_data.score+'</td>';
                    html_page += '</tr>';
                    html_page += '<tr>';
                    html_page += '<td class="text-left" style="width: 100px;"><strong>วันที่สร้าง</strong></td>';
                    html_page += '<td class="text-left">'+review_data.created_time+'</td>';
                    html_page += '<td class="text-left" style="width: 100px;"><strong>วันที่แก้ไขล่าสุด</strong></td>';
                    html_page += '<td class="text-left">'+review_data.updated_time+'</td>';
                    html_page += '</tr>';
                    html_page += '<tr>';
                    html_page += '<td class="text-left" colspan="4">'+review_data.detail+'</td>';
                    html_page += '</tr>';
                })
                $("#review_showing").html(html_page);
                $.fancybox.open({href : '#markUp'} );
            }
        });

    }
</script>
