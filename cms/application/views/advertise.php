<?php
$all_selected = '';
$live_selected = '';
$expired_selected = '';
$waiting_selected = '';
$param_url = '';
$status = $this->input->get('status');
if($status=="all"){
    $all_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="live"){
    $live_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="waiting"){
    $waiting_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="expired"){
    $expired_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}

$page_title = 'advertise';
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back.$param_url) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
$html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
} else if ($i == $page_back) {
$html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
} else if ($i == $page_next) {
$html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
} else {
$html_page .= "#";
}
}

while (strpos($html_page, "##") > 1) {
$html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next.$param_url) . '">&raquo;</a></li>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการโฆษณา</strong>
</div>

<div class="col-md-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการโฆษณา</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-6 text-left">
            <form class="form-inline" action="<?php echo site_url('advertise')?>" method="">
                <div class="form-group">
                    <label for="search_keyword">สถานะ:
                        <select name="status" id="search_status" class="form-control">
                            <option value="all" <?php echo $all_selected;?>>ทั้งหมด</option>
                            <option value="live" <?php echo $live_selected;?>>กำลังเผยแพร่</option>
                            <option value="waiting" <?php echo $waiting_selected;?>>รอการเผยแพร่</option>
                            <option value="expired" <?php echo $expired_selected;?>>หมดอายุ</option>
                        </select>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </label>
                </div>
                <?php if($status && isset($status)&& $status!=''){?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                    </div>
                <?php } ?>
            </form>
        </div>
        <div class="col-lg-6 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="javascript:void(0);" onclick="add_data()"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th class="text-center" style="width: 48px;">#</th>
                <th class="text-center" style="width: 100px;">รูป</th>
                <th class="text-center" style="">รายชื่อสถานประกอบการ</th>
                <th class="text-center" style="width: 150px;">เริ่มเผยแพร่</th>
                <th class="text-center" style="width: 150px;">สิ้นสุด</th>
                <th class="text-center" style="width: 110px;">สถานะ</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($advertise_list)>0){
                $n = (($page-1)*$limit)+1;
                foreach ($advertise_list as $item){
                    $business_data = $business_list[$item->business_id];
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $n;?></td>
                        <td class="text-left">
                            <a href="<?php echo $item->cover_image_src;?>" data-fancybox="images" data-width="500" data-height="400">
                                <img src="<?php echo $item->cover_image_src;?>" style="width: 100%">
                            </a>
                        </td>
                        <?php if($business_data->status!='deleted'){?>
                        <td class="text-left"><a href="javascript:void(0);" onclick="view_data(<?php echo $item->id?>)"><?php echo $business_data->name ;?></a></td>
                        <?php }else{ ?>
                            <td class="text-left"><span style="color: gray;"><?php echo $business_data->name ;?></span><strong class="small" style="color: red;">[สถานประกอบการนี้ถูกลบไปแล้ว]</strong></td>
                        <?php }?>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->start_time));?></td>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->end_time));?></td>
                        <td class="text-center"><?php
                            $current_time = time();
                            $start_time = strtotime($item->start_time);
                            $end_time = strtotime($item->end_time);
                            if($current_time>=$start_time&&$current_time<=$end_time){
                                echo '<strong style="color: green">กำลังเผยแพร่</strong>';
                            }else if($current_time<$start_time){
                                echo '<strong style="color: #999">รอเผยแพร่</strong>';
                            }else{
                                echo '<strong style="color: red">หมดอายุ</strong>';
                            }
                            ?></td>
                        <td class="text-center">
                            <a href="javascript:void(0);" onclick="edit_data(<?php echo $item->id?>)">แก้ไข</a> |
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>)">ลบ</a>
                            <input type="hidden" id="business_name_<?php echo $item->id?>" value="<?php echo $business_data->name?>">
                            <input type="hidden" id="business_id_<?php echo $item->id?>" value="<?php echo $item->business_id?>">
                            <input type="hidden" id="start_time_<?php echo $item->id?>" value="<?php echo date('Y-m-d',strtotime($item->start_time));?>">
                            <input type="hidden" id="end_time_<?php echo $item->id?>" value="<?php echo date('Y-m-d',strtotime($item->end_time));?>">
                            <input type="hidden" id="image_id_<?php echo $item->id?>" value="<?php echo $item->cover_image_id?>">
                            <input type="hidden" id="image_src_<?php echo $item->id?>" value="<?php echo $item->cover_image_src?>">
                            <input type="hidden" id="created_time_<?php echo $item->id?>" value="<?php echo $item->created_time?>">
                            <input type="hidden" id="updated_time_<?php echo $item->id?>" value="<?php echo $item->updated_time?>">
                        </td>

                    </tr>
                    <?php $n++; }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="7">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp" style="width: 550px;">
        <div style="max-width: 530px; width: 99%">
            <div class="col-md-12">
                <div id="search_box" style="width: 500px;">
                    <label for="exampleInputEmail1">ค้นหาสถานประกอบการ</label>
                    <form class="form-inline">
                        <div class="form-group">

                            <input type="text" class="form-control" id="s_keyword" placeholder="ชื่อสถานประกอบการ / ที่อยู่" style="width: 410px;">
                        </div>
                        <button type="button" class="btn btn-default" onclick="searchBusiness()"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                    </form>
                </div>
                <div id="search_result" style="padding-top: 16px; height: 500px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #ccc;">
                            <th colspan="2" class="text-center">
                                รายการสถานประกอบการ
                            </th>
                        </tr>
                        </thead>
                        <tbody id="searchResult" style="  max-height: 450px; overflow-y: auto; display:block;">
                        <tr style="width: 500px; display: block;">
                            <td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่พบข้อมูล</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="advertise_editor" style="padding-top: 16px; height: 550px; display: none;width: 500px;">
                    <form action="<?php echo site_url('advertise/update')?>" method="post" id="advertise_form" enctype="multipart/form-data">
                        <table class="table table-bordered">
                            <tr id="re_search">
                                <td colspan="2" class="text-right">
                                    <button type="button" class="btn btn-default" onclick="reset_search_box()">ค้นหาใหม่</button>
                                </td>
                            </tr>
                            <thead>
                            <tr style="background: #ccc;">
                                <th colspan="2" class="text-center" id="header_form_ads">
                                    แบบฟอร์มสำหรับโฆษณา
                                </th>
                            </tr>
                            </thead>
                            <tbody id="searchResult" >
                            <tr  style="width: 100%; display: none;" id="ad_img_show">
                                <td colspan="2" class="text-left" >
                                    <strong>รูปประกอบโฆณา: </strong>
                                    <img src="" style="width: 100%" id="ad_img">
                                </td>
                            </tr>
                            <tr  style="width: 100%;">
                                <td colspan="2" class="text-left" >
                                    <strong>ชื่อถสถานประกอบการ: </strong>
                                    <span class="text-center" id="ad_business_name"></span>
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td colspan="2" class="text-left" id="">
                                    <label for="">ระยะเวลาการเผยแพร่</label>
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td class="text-left" id="">
                                    <label for="start_time">ตั้งแต่</label>
                                </td>
                                <td class="text-left" id="">
                                    <span id="start_time_text"></span>
                                    <input type="text" class="form-control" id="start_time" name="start_time" style="width: 220px;">
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td class="text-left" id="">
                                    <label for="end_time">ถึง</label>
                                </td>
                                <td class="text-left" id="">
                                    <span id="end_time_text"></span>
                                    <input type="text" class="form-control" id="end_time" name="end_time" style="width: 220px;">
                                </td>
                            </tr>
                            <tr style="width: 100%;" id="add_img_show">
                                <td class="text-left" id="">
                                    <label for="end_time">รูป</label>
                                </td>
                                <td class="text-left" id="">
                                    <label>(ใช้รูปขนาด 1200 x 240 เท่านั้น)</label>
                                    <input type="file" id="advertise_cover" name="cover">
                                </td>
                            </tr>

                            <tr style="width: 100%;" id="button_show">
                                <td class="text-left" id="" colspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <button type="reset" class="btn btn-default">ล้างข้อมูล</button>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <input type="hidden" value="" name="business_id" id="ad_business_id">
                                            <input type="hidden" value="" name="id" id="ad_id">
                                            <input type="hidden" value="" name="do" id="do_update">
                                            <input type="hidden" value="" name="image_id" id="image_id">
                                            <button type="button" class="btn btn-primary" onclick="addNewAds()">ตกลง</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr style="width: 100%;" id="history_log">
                                <td class="text-center" colspan="2">
                                    <div style="font-size: 12px; color: gray;">
                                        <strong>สร้างโดย: </strong><span id="created_by"></span> เมื่อ <span id="created_time"></span>
                                    </div>
                                    <div style="font-size: 12px; color: gray;">
                                        <strong>ปรับปรุงโดย: </strong><span id="updated_by"></span> เมื่อ <span id="updated_time"></span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    function searchBusiness() {
        var keyword = $("#s_keyword").val();
        var check_whitespace = keyword.replace(new RegExp(' ', 'g'), '');
        if(keyword.length>0&&check_whitespace.length>0){
            var textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">';
            textHTML += '<img src="<?php echo site_assets_url('images/loading.gif')?>"></td></tr>';
            $("#searchResult").html(textHTML);
            $.ajax({
                url: '<?php echo api_site_url('business/search_business?limit=10&offset=0&keyword='); ?>'+keyword,
                type: 'get',
                data: {'limit':20,'offset':0,'keyword':keyword},
                dataType: 'json',
                success: function (respond) {
                    var textHTML = '';
                    if(respond.total>0){
                        $(respond.data).each(function (index) {
                            var item = respond.data[index];
                            textHTML += '<tr id="'+index+'_res"><td class="text-left" style="width: 460px;">'+item.name+'<br/>';
                            textHTML += '<data data-name="'+item.name+'" data-id="'+item.id+'" data-city="'+item.address_district+', '+item.address_province+'"></data>';
                            textHTML += '<span class="small" style="color: gray;">'+item.address_district+', '+item.address_province+'</span></td>';
                            textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-success" onclick="addBusinessToList('+index+')">';
                            textHTML += '<span class="glyphicon glyphicon-plus"></span>';
                            textHTML += '</button></td></tr>';
                        })
                    }else{
                        textHTML += '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
                    }
                    $("#searchResult").html(textHTML);
                }
            });
        }
    }

    function reset_search_box() {
        $("#search_box").show();
        $("#search_result").show();
        $("#advertise_editor").hide();
        var reset_html = '<tr style="width: 100%; display: block;"> <td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่พบข้อมูล</td></tr>';
        $("#s_keyword").val('');
        $("#searchResult").html(reset_html);
    }

    function addBusinessToList(index) {
        $("#header_form_ads").html('แบบฟอร์มสำหรับโฆษณา');
        $("#start_time_text").hide();
        $("#end_time_text").hide();
        $("#ad_img_show").show();
        $("#button_show").show();
        $("#start_time").show();
        $("#end_time").show();
        $("#add_img_show").show();
        $("#re_search").show();
        $("#ad_img").attr('src',"");
        $("#ad_img_show").hide();
        $("#do_update").val('');
        $("#start_time").val('');
        $("#end_time").val('');
        $("#search_box").hide();
        $("#search_result").hide();
        $("#advertise_editor").show();
        var res = $('#'+index+'_res data');
        var b_name = res.data('name');
        var b_id = res.data('id');
        $("#ad_business_name").html(b_name);
        $("#ad_business_id").val(b_id);
        $("#do_update").val('add');
    }

    function add_data() {
        reset_search_box();
        $("#history_log").hide();
        $.fancybox.open({href : '#markUp'} );
    }
    function view_data(id) {
        $("#ad_img_show").show();

        $("#header_form_ads").html('ข้อมูลของโฆษณา');
        $("#ad_img_show").show();
        $("#button_show").hide();
        $("#start_time").hide();
        $("#end_time").hide();
        $("#add_img_show").hide();
        $("#re_search").hide();
        $("#post_method").val('edit');
        tel = $("#tel_"+id).val();
        title = $("#title_"+id).val();

        $("#do_update").val('');
        $("#start_time").val('');
        $("#end_time").val('');
        $("#start_time_text").html('');
        $("#end_time_text").html('');
        $("#search_box").hide();
        $("#search_result").hide();
        $("#advertise_editor").show();
        var b_name = $("#business_name_"+id).val();
        var b_id = $("#business_id_"+id).val();
        var start_time = $("#start_time_"+id).val();
        var end_time = $("#end_time_"+id).val();
        var image_src = $("#image_src_"+id).val();
        var created_time = $("#created_time_"+id).val();
        var updated_time = $("#updated_time_"+id).val();
        $("#ad_id").val(id);
        $("#ad_business_name").html(b_name);
        $("#ad_business_id").val(b_id);
        $("#start_time_text").html(start_time);
        $("#end_time_text").html(end_time);
//        console.log(image_src);
        if(image_src!=""){
            $("#ad_img").attr('src',image_src);
            $("#ad_img").show();
        }else{
            $("#ad_img").hide();
            $("#ad_img").attr('src',"");
        }

        $("#do_update").val('view');
        $("#history_log").show();
        $.ajax({
            url: '<?php echo site_url('advertise/get_modifier/'); ?>'+id,
            type: 'get',
            dataType: 'json',
            success: function (respond) {
                if(respond.add!=undefined){
                    $("#created_by").html(respond.add.user_name);
                    $("#created_time").html(created_time);
                }else{
                    $("#created_by").html('ไม่ทราบ');
                    $("#created_time").html(created_time);
                }
                if(respond.edit!=undefined){
                    $("#updated_by").html(respond.edit.user_name);
                    $("#updated_time").html(updated_time);
                }else{
                    $("#updated_by").html('ไม่ทราบ');
                    $("#updated_time").html(updated_time);
                }
                $.fancybox.open({href : '#markUp'} );
            }
        });
    }
    function edit_data(id) {
        $("#header_form_ads").html('แบบฟอร์มสำหรับโฆษณา');
        $("#start_time_text").hide();
        $("#end_time_text").hide();
        $("#ad_img_show").show();
        $("#button_show").show();
        $("#start_time").show();
        $("#end_time").show();
        $("#add_img_show").show();
        $("#re_search").hide();
        $("#post_method").val('edit');
        tel = $("#tel_"+id).val();
        title = $("#title_"+id).val();

        $("#do_update").val('');
        $("#start_time").val('');
        $("#end_time").val('');
        $("#search_box").hide();
        $("#search_result").hide();
        $("#advertise_editor").show();
        var b_name = $("#business_name_"+id).val();
        var b_id = $("#business_id_"+id).val();
        var start_time = $("#start_time_"+id).val();
        var end_time = $("#end_time_"+id).val();
        var image_id = $("#image_id_"+id).val();
        var image_src = $("#image_src_"+id).val();
        $("#ad_id").val(id);
        $("#ad_business_name").html(b_name);
        $("#ad_business_id").val(b_id);
        $("#start_time").val(start_time);
        $("#end_time").val(end_time);
        $("#image_id").val(image_id);
        if(image_src!=""){
            $("#ad_img").attr('src',image_src);
            $("#ad_img").show();
        }else{
            $("#ad_img").hide();
            $("#ad_img").attr('src',"");
        }
        $("#history_log").hide();
        $("#do_update").val('edit');
        $.fancybox.open({href : '#markUp'} );
    }

    function del_data(id) {
        if(confirm('คุณต้องการลบรายการโฆษณานี้หรือไม่?')){
            $("#do_update").val('del');
            $("#ad_business_id").val(id);
            $("#advertise_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });
    
    function addNewAds() {
        var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
        if(start_time!=""&&end_time!=""){
//            console.log('test');
            $("#advertise_form").submit();
        }else{
            alert('กรุณากรอกวันเริ่มเผยแพร่ และ วันสิ้นสุดให้ครบถ้วน');
        }
    }

    function handleFileSelect(event) {
        image_to_upload_num = 0;
        var files = event.target.files;
        for(var i = 0; i< files.length; i++)
        {
            var file = files[i];
            if(!file.type.match('image'))
                continue;
            var picReader = new FileReader();

            picReader.addEventListener("load",function(event){
                var picFile = event.target;
                $("#ad_img_show").show();

                $("#ad_img").attr('src',picFile.result);
            });
            picReader.readAsDataURL(file);
        }

    }
    document.getElementById('advertise_cover').addEventListener('change', handleFileSelect, false);
</script>
<style>
    .fancybox-inner{
        overflow-x: hidden !important;
    }
    #history_log span{
        color:#000;
    }
</style>