<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการชุดข้อมูล</strong>
</div>

<div class="col-lg-12" style="clear: both;">
    <div id="main_content" class="">
        <?php if (count($category_list) > 0) {
            $header_title = array('ประเภทสถานประกอบการ', 'ประเภทงานบริการ', 'สิ่งอำนวยความสะดวก');
            foreach ($category_list as $key => $item) {
                ?>
                <div class="row" style="width: 530px;">
                    <div class="col-md-6">
                        <strong style="font-size: 16px;"><?php echo $header_title[$key - 1] ?></strong>
                    </div>
                    <div class="col-md-6 text-right">
                        <button id="add_category_btn" class="btn btn-default" style="color: green;" onclick="add_category(<?php echo $key ?>)">
                            <span class="glyphicon glyphicon-plus"></span> เพิ่ม
                        </button>
                    </div>
                </div>
                <table class="table table-bordered" style="width: 500px;">
                    <thead>
                    <tr>
                        <th class="text-center" style="width: 40px;">#</th>
                        <th class="text-left" >ประเภท</th>
                        <th class="text-center" style="width: 110px;">Tools</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (count($category_list) > 0) {
                        $i = 1;
                    foreach ($item as $category){
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $i;?></td>
                        <td class="text-left">
                            <?php echo $category['title'];?>
                            <input type="hidden" id="cate_data_<?php echo $category['id']?>" value="<?php echo $category['title'];?>">
                        </td>
                        <td class="text-center">
                            <a href="javascript:void(0)" onclick="edit_category(<?php echo $key ?>,<?php echo $category['id']?>)">แก้ไข</a> |
                            <a href="javascript:void(0)" onclick="del_category(<?php echo $category['id']?>)">ลบ</a>
                        </td>
                    </tr>
                    <?php $i++; } }else{ ?>
                        <tr>
                            <td class="text-center" colspan="5">ไม่มีข้อมูล</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            <?php } ?>
        <?php } ?>
    </div>
    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <form action="<?php echo site_url('category/post_data')?>" method="post" id="manage_cate_form">
            <div style="width: 500px;">
                <div class="form-group text-left">
                    <label for="category_title" id="category_title_label">Longitude</label>
                    <input type="text" class="form-control" id="category_title" name="category_title">
                    <input type="hidden" id="category_type" name="category_type" value="">
                    <input type="hidden" id="category_do" name="category_do" value="">
                </div>
                <div class="form-group text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    var category_title = ['ประเภทสถานประกอบการ', 'ประเภทงานบริการ', 'สิ่งอำนวยความสะดวก'];
    function add_category(cate_type) {
        $("#category_title_label").html(category_title[cate_type-1]);
        $("#category_type").val(cate_type);
        $("#category_do").val('add');
        $.fancybox.open({href: '#showMarkUp'});
    }

    function edit_category(cate_type,cate_id) {
        $("#category_title_label").html(category_title[cate_type-1]);
        var cate_data = $("#cate_data_"+cate_id).val();
        $("#category_title").val(cate_data);
        $("#category_type").val(cate_id);
        $("#category_do").val('edit');
        $.fancybox.open({href: '#showMarkUp'});
    }
    function del_category(cate_id) {
        if (confirm('คุณต้องการลบหรือไม่')) {
            $("#category_type").val(cate_id);
            $("#category_do").val('delete');
            $("#manage_cate_form").submit();
        }
    }
</script>