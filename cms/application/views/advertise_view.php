<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการโฆษณา</strong>
</div>

<div class="col-md-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการโฆษณา</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-12 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="javascript:void(0);" onclick="add_data()"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th class="text-center" style="width: 48px;">#</th>
                <th class="text-center" style="width: 100px;">รูป</th>
                <th class="text-center" style="">รายชื่อ</th>
                <th class="text-center" style="width: 150px;">เริ่มเผยแพร่</th>
                <th class="text-center" style="width: 150px;">สิ้นสุด</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($advertise_list)>0){
                $n = 1;
                foreach ($advertise_list as $item){
                    $business_data = $business_list[$item->business_id];
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $n;?></td>
                        <td class="text-left">
                            <a href="<?php echo $item->cover_image_src;?>" data-fancybox="images" data-width="500" data-height="400">
                                <img src="<?php echo $item->cover_image_src;?>" style="width: 100%">
                            </a>
                        </td>
                        <td class="text-left"><a href="<?php echo site_url('business/view/'.$item->business_id)?>" target="_blank"><?php echo $business_data->name ;?></a></td>
                        <td class="text-left"><?php echo date('Y-m-d',strtotime($item->start_time));?></td>
                        <td class="text-left"><?php echo date('Y-m-d',strtotime($item->end_time));?></td>
                        <td class="text-left">
                            <a href="javascript:void(0);" onclick="edit_data(<?php echo $item->id?>)">แก้ไข</a> |
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>)">ลบ</a>
                            <input type="hidden" id="business_name_<?php echo $item->id?>" value="<?php echo $business_data->name?>">
                            <input type="hidden" id="business_id_<?php echo $item->id?>" value="<?php echo $item->business_id?>">
                            <input type="hidden" id="start_time_<?php echo $item->id?>" value="<?php echo date('Y-m-d',strtotime($item->start_time));?>">
                            <input type="hidden" id="end_time_<?php echo $item->id?>" value="<?php echo date('Y-m-d',strtotime($item->end_time));?>">
                            <input type="hidden" id="image_id_<?php echo $item->id?>" value="<?php echo $item->cover_image_id?>">
                            <input type="hidden" id="image_src_<?php echo $item->id?>" value="<?php echo $item->cover_image_src?>">
                        </td>

                    </tr>
                    <?php $n++; }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="6">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>

    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="max-width: 530px; width: 99%">
            <div class="col-md-12">
                <div id="search_box">
                    <label for="exampleInputEmail1">ค้นหาสถานประกอบการ</label>
                    <form class="form-inline">
                        <div class="form-group">

                            <input type="text" class="form-control" id="s_keyword" placeholder="ชื่อสถานประกอบการ / ที่อยู่" style="width: 438px;">
                        </div>
                        <button type="button" class="btn btn-default" onclick="searchBusiness()"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                    </form>
                </div>
                <div id="search_result" style="padding-top: 16px; height: 550px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #ccc;">
                            <th colspan="2" class="text-center">
                                รายการสถานประกอบการ
                            </th>
                        </tr>
                        </thead>
                        <tbody id="searchResult" style="  max-height: 450px; overflow-y: auto; display:block;">
                        <tr style="width: 100%; display: block;">
                            <td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่พบข้อมูล</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div id="advertise_editor" style="padding-top: 16px; height: 550px; display: none;">
                    <form action="<?php echo site_url('advertise/update')?>" method="post" id="advertise_form" enctype="multipart/form-data" id="mainForm">
                        <table class="table table-bordered">
                            <thead>
                            <tr style="background: #ccc;">
                                <th colspan="2" class="text-center">
                                    รายการสถานประกอบการ
                                </th>
                            </tr>
                            </thead>
                            <tbody id="searchResult" >
                            <tr  style="width: 100%;">
                                <td colspan="2" class="text-left" >
                                    <strong>ชื่อถสถานประกอบการ: </strong>
                                    <img src="" style="width: 100%" id="ad_img">
                                </td>
                            </tr>
                            <tr  style="width: 100%;">
                                <td colspan="2" class="text-left" >
                                    <strong>ชื่อถสถานประกอบการ: </strong>
                                    <span class="text-center" id="ad_business_name"></span>
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td colspan="2" class="text-left" id="ad_business_name">
                                    <label for="">ระยะเวลาการเผยแพร่</label>
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td class="text-left" id="ad_business_name">
                                    <label for="start_time">ตั้งแต่</label>
                                </td>
                                <td class="text-left" id="ad_business_name">
                                    <input type="text" class="form-control" id="start_time" name="start_time" style="width: 220px;">
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td class="text-left" id="ad_business_name">
                                    <label for="end_time">ถึง</label>
                                </td>
                                <td class="text-left" id="ad_business_name">
                                    <input type="text" class="form-control" id="end_time" name="end_time" style="width: 220px;">
                                </td>
                            </tr>
                            <tr style="width: 100%;">
                                <td class="text-left" id="ad_business_name">
                                    <label for="advertise_cover">รูป</label>
                                </td>
                                <td class="text-left" id="ad_business_name">
                                    <label>(ใช้รูปขนาด 1200 x 240 เท่านั้น)</label>
                                    <input type="file" id="advertise_cover" name="cover">
                                </td>
                            </tr>

                            <tr style="width: 100%;" class="edit_form">
                                <td class="text-left" id="ad_business_name">
                                    <button type="reset" class="btn btn-default">ล้างข้อมูล</button>
                                </td>
                                <td class="text-right" id="ad_business_name">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <button type="button" class="btn btn-default" onclick="reset_search_box()">ค้นหาใหม่</button>

                                        </div>
                                        <div class="col-md-6 text-right">
                                            <input type="hidden" value="" name="business_id" id="ad_business_id">
                                            <input type="hidden" value="" name="id" id="ad_id">
                                            <input type="hidden" value="" name="do" id="do_update">
                                            <input type="hidden" value="" name="image_id" id="image_id">
                                            <button type="button" class="btn btn-primary" onclick="addNewAds()">ตกลง</button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    function searchBusiness() {
        var keyword = $("#s_keyword").val();
        var check_whitespace = keyword.replace(new RegExp(' ', 'g'), '');
        if(keyword.length>0&&check_whitespace.length>0){
            var textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">';
            textHTML += '<img src="<?php echo site_assets_url('images/loading.gif')?>"></td></tr>';
            $("#searchResult").html(textHTML);
            $.ajax({
                url: '<?php echo api_site_url('business/search_business?limit=10&offset=0&keyword='); ?>'+keyword,
                type: 'get',
                data: {'limit':20,'offset':0,'keyword':keyword},
                dataType: 'json',
                success: function (respond) {
                    var textHTML = '';
                    if(respond.total>0){
                        $(respond.data).each(function (index) {
                            var item = respond.data[index];
                            textHTML += '<tr id="'+index+'_res"><td class="text-left" style="width: 460px;">'+item.name+'<br/>';
                            textHTML += '<data data-name="'+item.name+'" data-id="'+item.id+'" data-city="'+item.address_district+', '+item.address_province+'"></data>';
                            textHTML += '<span class="small" style="color: gray;">'+item.address_district+', '+item.address_province+'</span></td>';
                            textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-success" onclick="addBusinessToList('+index+')">';
                            textHTML += '<span class="glyphicon glyphicon-plus"></span>';
                            textHTML += '</button></td></tr>';
                            textHTML += '<tr style="width: 100%;"><td class="text-center" colspan="2"><div><strong>Created by: </strong>'+item.created_by+''+item.created_time+'</div><div><strong>Update by: </strong>'+item.updated_by+''+item.update_time+'</div></td></tr>';
                        })
                    }else{
                        textHTML += '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
                    }
                    $("#searchResult").html(textHTML);
                }
            });
        }
    }

    function reset_search_box() {
        $("#search_box").show();
        $("#search_result").show();
        $("#advertise_editor").hide();
        var reset_html = '<tr style="width: 100%; display: block;"> <td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่พบข้อมูล</td></tr>';
        $("#s_keyword").val('');
        $("#searchResult").html(reset_html);
    }

    function addBusinessToList(index) {
        $("#do_update").val('');
        $("#start_time").val('');
        $("#end_time").val('');
        $("#search_box").hide();
        $("#search_result").hide();
        $("#advertise_editor").show();
        var res = $('#'+index+'_res data');
        var b_name = res.data('name');
        var b_id = res.data('id');
        $("#ad_business_name").html(b_name);
        $("#ad_business_id").val(b_id);
        $("#do_update").val('add');
    }

    function add_data() {
        reset_search_box();
        $.fancybox.open({href : '#markUp'} );
    }
    function edit_data(id) {
        $("#post_method").val('edit');
        tel = $("#tel_"+id).val();
        title = $("#title_"+id).val();

        $("#do_update").val('');
        $("#start_time").val('');
        $("#end_time").val('');
        $("#search_box").hide();
        $("#search_result").hide();
        $("#advertise_editor").show();
        var b_name = $("#business_name_"+id).val();
        var b_id = $("#business_id_"+id).val();
        var start_time = $("#start_time_"+id).val();
        var end_time = $("#end_time_"+id).val();
        var image_id = $("#image_id_"+id).val();
        var image_src = $("#image_src_"+id).val();
        $("#ad_id").val(id);
        $("#ad_business_name").html(b_name);
        $("#ad_business_id").val(b_id);
        $("#start_time").val(start_time);
        $("#end_time").val(end_time);
        $("#end_time").val(end_time);
        if(image_src!=""){
            $("#ad_img").attr('src',image_src);
            $("#ad_img").show();
        }else{
            $("#ad_img").hide();
            $("#ad_img").attr('src',"");
        }

        $("#do_update").val('edit');
        $.fancybox.open({href : '#markUp'} );
    }

    function del_data(id) {
        if(confirm('คุณต้องการลบชื่อติดต่อนี้หรือไม่?')){
            $("#do_update").val('del');
            $("#ad_business_id").val(id);
            $("#advertise_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });
    
    function addNewAds() {
        var start_time = $("#start_time").val();
        var end_time = $("#end_time").val();
        if(start_time!=""&&end_time!=""){

        }else{
            alert('กรุณากรอกวันเริ่มเผยแพร่ และ วันสิ้นสุดให้ครบถ้วน');
        }
    }
</script>