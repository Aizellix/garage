<?php
$page_title = 'user';
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
$html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i) . '">' . $i . '</a></li>';
} else if ($i == $page_back) {
$html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i) . '">' . $i . '</a></li>';
} else if ($i == $page_next) {
$html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
} else {
$html_page .= "#";
}
}

while (strpos($html_page, "##") > 1) {
$html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next) . '">&raquo;</a></li>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลผู้ใช้งาน</strong>
</div>

<div class="col-md-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการผู้ใช้งาน</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-12">
            <form action="<?php echo site_url('user')?>" method="">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="search_keyword">
                            <input type="text" class="form-control" id="search_keyword" name="keyword" placeholder="ค้นหาจาก ชื่อ หรือ อีเมล์" style="width: 230px;" value="<?php echo ($this->input->get('keyword'))?$this->input->get('keyword'):""; ?>">
                            <label for="searchMoreGender" class="searchMoreForm" style="display:none;">เพศ:</label>
                            <?php $gender = ($this->input->get('gender'))?$this->input->get('gender'):"all"; ?>
                            <select id="searchMoreGender" name="gender" class="searchMoreForm form-control" style="display:none;">
                                <option value="all" <?php echo ($gender=="all")?'selected="selected"':''; ?>>ทั้งหมด</option>
                                <option value="male" <?php echo ($gender=="male")?'selected="selected"':''; ?>>ชาย</option>
                                <option value="female" <?php echo ($gender=="female")?'selected="selected"':''; ?>>หญิง</option>
                                <option value="unknown" <?php echo ($gender=="unknown")?'selected="selected"':''; ?>>ไม่ระบุ</option>
                            </select>
                            <label for="searchMoreRegister" class="searchMoreForm" style="display:none;">สมัครโดย:</label>
                            <?php $gender = ($this->input->get('register'))?$this->input->get('register'):"all"; ?>
                            <select id="searchMoreRegister" name="register" class="searchMoreForm form-control" style="display:none;">
                                <option value="all" <?php echo ($gender=="all")?'selected="selected"':''; ?>>ทั้งหมด</option>
                                <option value="email" <?php echo ($gender=="email")?'selected="selected"':''; ?>>Email</option>
                                <option value="fb" <?php echo ($gender=="fb")?'selected="selected"':''; ?>>Facebook</option>
                            </select>
                            <button id="normalSubmitBtn" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </label>
                    </div>
                    <?php $keyword = $this->input->get('keyword'); if($keyword && isset($keyword)&& $keyword!=''){?>
                        <div class="form-group normalSearch">
                            <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                        </div>
                    <?php }else{ ?>
                        <div class="form-group  normalSearch">
                            <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                        </div>
                    <?php } ?>
                    <div style="float: right; display:none;" class="searchMoreForm">
                        <button id="searchMoreForm" type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        <?php $keyword = $this->input->get('keyword'); if($keyword && isset($keyword)&& $keyword!=''){?>
                            <div class="form-group searchMoreForm">
                                <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                            </div>
                        <?php }else{ ?>
                            <div class="form-group  searchMoreForm">
                                <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                            </div>
                        <?php } ?></div>
                </div>
                <div class="form-inline">
                    <a href="javascript:void(0);" onclick="search_more('on')" id="searchMore">ค้นหาอย่าละเอียด</a>

                    <div class="form-group searchMoreForm" style="display:none;">
                        <label for="searchMoreAgeFrom" class="searchMoreForm" style="display:none;">อายุ ตั้งแต่:</label>
                        <?php $age_from = ($this->input->get('age_from'))?$this->input->get('age_from'):"0"; ?>
                        <input class="form-control text-center" type="text" id="searchMoreAgeFrom" value="<?php echo $age_from; ?>" name="age_from" style="width: 50px;padding: 6px;">
                        <label for="searchMoreAgeTo" class="searchMoreForm" style="display:none;">ถึง:</label>
                        <?php $age_to= ($this->input->get('age_to'))?$this->input->get('age_to'):"100"; ?>
                        <input class="form-control text-center" type="text" id="searchMoreAgeTo" value="<?php echo $age_to; ?>" name="age_to" style="width: 50px;padding: 6px; ">
                        <span> | </span>
                        <label for="searchMoreRegisterFrom" class="searchMoreForm" style="display:none;">สมัคร ตั้งแต่:</label>
                        <?php $register_from = ($this->input->get('register_from'))?$this->input->get('register_from'):""; ?>
                        <input type="text" class="form-control" id="searchMoreRegisterFrom" name="register_from" style="width: 111px;" value="<?php echo $register_from; ?>">
                        <label for="searchMoreRegisterTo">ถึง</label>
                        <?php $register_to = ($this->input->get('register_to'))?$this->input->get('register_to'):""; ?>
                        <input type="text" class="form-control" id="searchMoreRegisterTo" name="register_to" style="width: 111px;" value="<?php echo $register_to; ?>">

                        <input type="hidden" id="is_more" value="no" name="is_more">
                    </div>

                </div>
            </form>
            </form>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <?php
                $order_by = $this->input->get("order_by");
                $sort_by = $this->input->get("sort_by");
                $all_param = str_replace("sort_by=".$sort_by."&order_by=".$order_by,"",$_SERVER['QUERY_STRING']);
                $all_param = str_replace("&","",$all_param);
                $id_order = "DESC";
                $reviewed_order = "DESC";
                $point_order = "DESC";
                $id_sort_icon = "glyphicon-sort-by-attributes-alt";
                $reviewed_sort_icon = "glyphicon-sort";
                $point_sort_icon = "glyphicon-sort";
                if($sort_by=="id"){
                    $id_order = ($order_by=="DESC")?"ASC":"DESC";
                    $id_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                    $reviewed_sort_icon = "glyphicon-sort";
                    $point_order = "glyphicon-sort";
                }else if($sort_by=="reviewed"){
                    $reviewed_order = ($order_by=="DESC")?"ASC":"DESC";
                    $reviewed_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                    $id_sort_icon = "glyphicon-sort";
                    $point_sort_icon = "glyphicon-sort";
                }else if($sort_by=="point"){
                    $point_order = ($order_by=="DESC")?"ASC":"DESC";
                    $point_sort_icon = ($order_by=="DESC")?"glyphicon-sort-by-attributes-alt":"glyphicon-sort-by-attributes";
                    $id_sort_icon = "glyphicon-sort";
                    $reviewed_sort_icon = "glyphicon-sort";
                }
                $id_order.="&".$all_param;
                $reviewed_order.="&".$all_param;
                $point_order.="&".$all_param;
                ?>
                <th class="text-center" style="width: 48px;"><a href="<?php echo site_url('user?sort_by=id&order_by='.$id_order);?>"><span class="glyphicon <?php echo $id_sort_icon ?> small"></span> ID</a></th>
                <th class="text-center" style="width: 72px;">รูป</th>
                <th class="text-center" style="">ชื่อผู้ใช้</th>
                <th class="text-center" style="">อีเมล์</th>
                <th class="text-center" style="">Facebook</th>
                <th class="text-center" style=""><a href="<?php echo site_url('user?sort_by=reviewed&order_by='.$reviewed_order);?>"><span class="glyphicon <?php echo $reviewed_sort_icon ?> small"></span> จำนวนรีวิว</a></th>
                <th class="text-center" style="width: 90px;"><a href="<?php echo site_url('user?sort_by=point&order_by='.$point_order);?>"><span class="glyphicon <?php echo $point_sort_icon ?> small"></span> แต้ม</a></th>
                <th class="text-center" style="width: 150px;">วันที่สมัครสมาชิก</th>
                <th class="text-center" style="width: 50px;">เพศ</th>
                <th class="text-center" style="width: 50px;">อายุ</th>
                <th class="text-center" style="width: 50px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($user_list)>0){
                $n = (($page-1)*$limit)+1;
                $gender_list =array('male'=>"ชาย",'female'=>'หญิง','unknown'=>'ไม่ระบุ');
                foreach ($user_list as $item){
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $item->id;?></td>
                        <td class="text-left" style="padding: 2px !important;">
                            <img src="<?php echo ($item->profile_image!="")?$item->profile_image:site_assets_url('images/no_profile_image.png');?>" style="width: 100%;">
                        </td>
                        <td class="text-left"><a href="javascript:void(0);" onclick="view_data(<?php echo $item->id?>)"><?php echo $item->name ;?></a></td>
                        <td class="text-left"><?php echo $item->email ;?></td>
                        <td class="text-left">
                            <?php if(isset($item->fb_id)&&$item->fb_id!=""){?>
                            <a href="http://facebook.com/<?php echo $item->fb_id ;?>" target="_blank">FB: <?php echo $item->fb_id ;?></a>
                            <?php }else{?>
                                ไม่มี
                            <?php }?>
                        </td>
                        <td class="text-center"><a href="<?php echo site_url('user/review/'.$item->id)?>" target="_blank"><?php echo $item->reviewed;?></a></td>
                        <td class="text-center"><?php echo $item->point;?></td>
                        <td class="text-center"><?php echo date('Y-m-d',strtotime($item->created_time));?></td>
                        <td class="text-center"><?php echo (isset($gender_list[$item->gender]))?$gender_list[$item->gender]:'-';?></td>
                        <td class="text-center"><?php echo date_diff(date_create($item->birthday),date_create(date('Y-m-d')))->y;?></td>
                        <td class="text-center">
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>)">ลบ</a>
                            <input type="hidden" id="user_name_<?php echo $item->id?>" value="<?php echo $item->name?>">
                            <input type="hidden" id="user_email_<?php echo $item->id?>" value="<?php echo $item->email?>">
                            <input type="hidden" id="profile_image_<?php echo $item->id?>" value="<?php echo $item->profile_image;?>">
                            <input type="hidden" id="facebook_id_<?php echo $item->id?>" value="<?php echo $item->fb_id;?>">
                            <input type="hidden" id="created_time_<?php echo $item->id?>" value="<?php echo date('Y-m-d',strtotime($item->created_time));?>">
                            <input type="hidden" id="user_gender<?php echo $item->id?>" value="<?php echo $item->gender?>">
                        </td>

                    </tr>
                    <?php }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="9">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp" style="width: 550px;">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3" style="padding: 0;">
                    <img src="" id="v_profile_image" style="width: 100%; border: 1px solid #666;">
                </div>
                <div class="col-md-9">
                    <table class="table table-bordered">
                        <tr>
                            <td style="width: 100px;">ID</td>
                            <td id="v_user_id"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">ชื่อผู้ใช้</td>
                            <td id="v_user_name"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">อีเมล์</td>
                            <td id="v_user_email"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">Facebook</td>
                            <td id="v_facebook"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">วันเกิด</td>
                            <td id="v_birthday"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">จำนวนรีวิว</td>
                            <td id="v_review"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">เพศ</td>
                            <td id="v_gender"></td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">วันที่สมัครสมาชิก</td>
                            <td id="v_created_time"></td>
                        </tr>
                    </table>
                    <form id="user_form" action="<?php echo site_url('user/update')?>" method="post" style="display: none;">
                        <input type="hidden" value="" name="do" id="do_update">
                        <input type="hidden" value="" name="id" id="user_id">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var is_more = "<?php echo ($this->input->get('is_more'))?$this->input->get('is_more'):"no"; ?>";
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#searchMoreRegisterFrom" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#searchMoreRegisterTo" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    function view_data(id) {
        $.ajax({
            url: '<?php echo api_site_url('user/get_data/'); ?>'+id,
            type: 'GET',
            dataType:'json',
            success: function (data) {
                data = data.data;
//                console.log(data,data.profile_image);
                var gender_list = {male:'ชาย', female:'หญิง', unknown:'ไม่ระบุ'}
                if(data.profile_image==""){

                    $("#v_profile_image").attr('src','<?php echo site_assets_url('images/no_profile_image.png');?>');
                }else{
                    $("#v_profile_image").attr('src',data.profile_image);
                }

                $("#v_user_name").html(data.name);
                $("#v_user_email").html(data.email);
                $("#v_user_id").html(data.id);
                if(data.fb_id==""){
                    $("#v_facebook").html('ไม่มี');
                }else{
                    $("#v_facebook").html('<a href="http://facebook.com/'+data.fb_id+'" target="_blank">Facebook ID: '+data.fb_id+'</a>');
                }

                $("#v_gender").html(gender_list[data.gender]);
                $("#v_birthday").html(data.birthday);
                $("#v_review").html(data.reviewed);
                $("#v_created_time").html(data.created_time);
            }
        });
        $.fancybox.open({href : '#markUp'});
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });

    function del_data(id) {
        if(confirm('คุณต้องการลบผู้ใช้งานนี้หรือไม่?')){
            $("#do_update").val('del');
            $("#user_id").val(id);
            $("#user_form").submit();
        }
    }

    function search_more(state) {
        if(state=='on'){
            $("#searchMore").hide();
            $("#normalSubmitBtn").hide();
            $(".normalSearch").hide();
            $(".searchMoreForm").show();
            $("#is_more").val('yes');
        }else{
            $("#searchMore").show();
            $("#normalSubmitBtn").show();
            $(".normalSearch").show();
            $(".searchMoreForm").hide();
            $("#is_more").val('no');
        }

    }
    $(document).ready(function () {
        if(is_more=="yes"){
            search_more("on");
        }
    })
</script>
<style>
    .fancybox-inner{
        overflow-x: hidden !important;
    }
</style>