<?php
$param_url = '';
$page_title = 'user/review/'.$user_id;
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back.$param_url) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
    if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_back) {
        $html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_next) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
    } else {
        $html_page .= "#";
    }
}

while (strpos($html_page, "##") > 1) {
    $html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next.$param_url) . '">&raquo;</a></li>';

function img_select_box($img_data,$review_id) {
    $html_txt =  '<data data-id="'.$img_data["id"].'" data-src="'.$img_data['url'].'"></data>';
    $html_txt.= '<label style="display:none;" onclick="checked_box_delete(this)" class="select_delete image_overlay_box_unselected select_img_c_'.$review_id.'"><input type="checkbox" name="delete_image[]" value="'.$img_data["id"].'"></label>';
    return $html_txt;
}
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลผู้ใช้งาน</strong>
</div>

<div class="col-md-12" style="clear: both;">
    <strong style="font-size: 16px;">ข้อมูลผู้ใช้งาน</strong>
    <div id="page_operator" class="" style="padding-bottom: 8px;">
        <div class="col-lg-12 text-right">
            <div class="row">
                <div class="col-md-3" style="padding: 0;">
                    <img src="<?php echo ($user_data->profile_image!="")?$user_data->profile_image:site_assets_url('images/no_profile_image.png');?>" style="width: 100%; border: 1px solid #666;">
                </div>
                <div class="col-md-9">
                    <table class="table table-bordered">
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">ID</td>
                            <td class="text-left"><?php echo $user_data->id;?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">ชื่อผู้ใช้</td>
                            <td class="text-left"><?php echo $user_data->name;?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">อีเมล์</td>
                            <td class="text-left"><?php echo $user_data->email;?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">Facebook</td>
                            <?php if(isset($user_data->fb_id)&&$user_data->fb_id!="") { ?>
                                <td class="text-left"><a href="http://facebook.com/<?php echo $user_data->fb_id;?>" target="_blank">Facebook ID: <?php echo $user_data->fb_id;?></a></td>
                            <?php }else {?>
                                <td class="text-left">ไม่มี</td>
                            <?php }?>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">วันเกิด</td>
                            <td class="text-left"><?php echo $user_data->birthday;?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">จำนวนรีวิว</td>
                            <td class="text-left"><?php echo $user_data->reviewed;?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">แต้ม</td>
                            <td class="text-left"><?php echo $user_data->point;?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">ดีลที่เคยใช้</td>
                            <td class="text-left"><?php
                                if($total_used_deal>0){
                                    echo '<a href="javascript:void(0);" onclick="open_log('.$user_data->id.');">'.$total_used_deal.'</a>';
                                }else{
                                    echo $total_used_deal;
                                }
                                ?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">เพศ</td>
                            <td class="text-left"><?php echo $gender_list[$user_data->gender];?></td>
                        </tr>
                        <tr>
                            <td class="text-right" style="width: 150px; font-weight: bold;">วันที่สมัครสมาชิก</td>
                            <td class="text-left"><?php echo $user_data->created_time;?></td>
                        </tr>
                    </table>
                    <form id="review_form" action="<?php echo site_url('user/update')?>" method="post" style="display: none;">
                        <input type="hidden" value="" name="do" id="do_update">
                        <input type="hidden" value="" name="user_id" id="user_id">
                        <input type="hidden" value="" name="review_id" id="review_id">
                        <input type="hidden" value="" name="business_id" id="business_id">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="main_content" class="">
        <strong style="font-size: 16px; padding-top: 20px; display: block;">รายการรีวิวของผู้ใช้งาน</strong>
        <table class="table table-bordered">
            <tbody>
            <?php if(count($review_list)>0){

                foreach ($review_list as $item){
                    $business_data = $business_list[$item->business_id];
                    ?>
                    <tr>
                        <td class="text-left" rowspan="3" style="width: 150px;">
                            <img src="<?php echo $business_data->cover_image_src?>" style="width: 100%">
                        </td>
                        <td class="text-left" style="width: 150px;"><strong>สถานประกอบการ</strong></td>
                        <td class="text-left">
                            <a href="<?php echo site_url('business/view/'.$business_data->id)?>" target="_blank"><?php echo $business_data->name?></a>
                        </td>
                        <td class="text-left" style="width: 150px;"><strong>คะแนน</strong></td>
                        <td class="text-left"><?php echo $item->score?></td>
                        <td rowspan="2" valign="middle" align="center" style="width: 50px;">
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>,<?php echo $user_data->id?>,<?php echo $business_data->id?>)">ลบ</a>
                        </td>

                    </tr>
                    <tr>
                        <td class="text-left" style="width: 100px;"><strong>วันที่สร้าง</strong></td>
                        <td class="text-left"><?php echo $item->created_time?></td>
                        <td class="text-left" style="width: 100px;"><strong>วันที่แก้ไขล่าสุด</strong></td>
                        <td class="text-left"><?php echo $item->updated_time?></td>
                    </tr>
                    <tr>
                        <td class="text-left" colspan="5"><?php echo $item->detail?></td>
                    </tr>
                    <tr>
                        <td class="text-left" colspan="6" style="background: #EFEFEF;"><?php if(count($item->images)>0) {?>
                                <div style="display: inline-block; float: right;">
                                    <button id="show_del_photo_btn_<?php echo $item->id;?>" type="button" class="btn btn-danger show_del_photo_btn" onclick="show_delete_photo(<?php echo $item->id;?>)">เลือกรูปที่ต้องการลบ</button>
                                    <button id="confirm_del_photo_btn_<?php echo $item->id;?>" style="display: none" type="button" class="btn btn-danger set_delete_image_btn" onclick="set_delete_image(<?php echo $item->id;?>)">ตกลง</button>
                                </div>
                                <div style="display: inline-block; float: left;">
                                    <button id="cancel_del_photo_btn_<?php echo $item->id;?>" type="button" style="display: none" class="btn btn-default" onclick="hide_img_sub_tool(<?php echo $item->id;?>)">ยกเลิก</button>
                                </div>
                                <div style="clear: both;" class="review_photo_list">
                                    <?php
                                    for($i=0;$i<count($item->images);$i+=6){
                                        $html_text ='<div class="row"><input type="hidden" name="review_id" class="review_id_'.$item->id.'" value="'.$item->id.'">';
                                        if(isset($item->images[$i])){
                                            $html_text .='<div class="col-md-2"><a href="'.$item->images[$i]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i],$item->id).'</div>';
                                        }
                                        if(isset($item->images[$i+1])){
                                            $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+1]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+1]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+1],$item->id).'</div>';
                                        }else{
                                            $html_text .='<div class="col-md-2"></div>';
                                        }
                                        if(isset($item->images[$i+2])){
                                            $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+2]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+2]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+2],$item->id).'</div>';
                                        }else{
                                            $html_text .='<div class="col-md-2"></div>';
                                        }
                                        if(isset($item->images[$i+3])){
                                            $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+3]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+3]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+3],$item->id).'</div>';
                                        }else{
                                            $html_text .='<div class="col-md-2"></div>';
                                        }
                                        if(isset($item->images[$i+4])){
                                            $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+4]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+4]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+4],$item->id).'</div>';
                                        }else{
                                            $html_text .='<div class="col-md-2"></div>';
                                        }
                                        if(isset($item->images[$i+5])){
                                            $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+5]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+5]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+5],$item->id).'</div>';
                                        }else{
                                            $html_text .='<div class="col-md-2"></div>';
                                        }

                                        echo $html_text .='</div>';
                                    }
                                    ?>
                                </div>
                            <?php }?>
                        </td>
                    </tr>
                    <?php  }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="6">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 550px;">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <thead>
                    <tr style="background: #ccc;">
                        <th class="text-center" style="width: 130px;"> วันที่ </th>
                        <th class="text-center"> ดีล </th>
                        <th class="text-center" style="width: 80px;"> แต้ม </th>
                    </tr>
                    </thead>
                    <tbody id="searchResult">
                    <tr>
                        <td colspan="4" class="text-center">ไม่พบข้อมูล1</td>
                    </tr>
                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <ul class="pagination" id="log_paging">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="display: none;">
        <form action="<?php echo site_url('deal/update/del')?>" method="post" id="article_form">
            <input value="" name="do" id="post_method" type="hidden">
            <input value="" name="id" id="item_id" type="hidden">
        </form>
    </div>
</div>

<script>
    var current_review_id = 0;
    function del_data(review_id,user_id,business_id) {
        if(confirm('คุณต้องการลบรีวิวนี้หรือไม่?')){
            $("#do_update").val('del_review');
            $("#review_id").val(review_id);
            $("#business_id").val(business_id);
            $("#user_id").val(user_id);
            $("#review_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });

    function show_delete_photo(review_id) {
        $('.show_del_photo_btn').prop('disabled',true);
        $('#cancel_del_photo_btn_'+review_id).show();
        $('#confirm_del_photo_btn_'+review_id).show();
        $('#show_del_photo_btn_'+review_id).hide();
        $('.set_delete_image_btn').prop('disabled',true);
        $('.select_delete.select_img_c_'+review_id).show();
        $('.img-thumbnail.img_all').each(function (index) {
//            console.log($( this ).height(),$( this ).parent().parent().find('a').width());
            var w_img = $( this ).parent().parent().find('.img-thumbnail.img_all').width();
            var h_img = $( this ).height();
//            console.log($( this ).parent().parent().find('.select_img_c'),w_img,h_img);
            $( this ).parent().parent().find('.select_img_c_'+review_id).height(h_img);
            $( this ).parent().parent().find('.select_img_c_'+review_id).width(w_img);
            $( this ).parent().parent().find('.select_img_c_'+review_id+' input').css('margin-top',(h_img/2)-8);
            $( this ).parent().parent().find('.select_img_c_'+review_id+' span').css('margin-top',(h_img/2)-12);
        })
        current_review_id = review_id;
    }
    function hide_img_sub_tool(review_id) {
//        $('.select_delete').hide();
        current_review_id = 0;
        $('.select_img_c_'+review_id).removeClass('image_overlay_box_selected');
        $('.select_img_c_'+review_id).addClass('image_overlay_box_unselected');
        $(".select_cover input").prop('checked', false);
        $(".select_delete input").prop('checked', false);
        $('.show_del_photo_btn').prop('disabled',false);
        $('.set_image_cover_btn').hide();
        $('.set_delete_image_btn').hide();
        $('#img_sub_tool').hide();
        $('.select_img_c_'+review_id).hide();
        $('#cancel_del_photo_btn_'+review_id).hide();
        $('#confirm_del_photo_btn_'+review_id).hide();
        $('#show_del_photo_btn_'+review_id).show();
    }
    function checked_box_delete(obj) {
        if($(obj).find('input').prop('checked')==true){
            $(obj).removeClass('image_overlay_box_unselected');
            $(obj).addClass('image_overlay_box_selected');
        }else{
            $(obj).removeClass('image_overlay_box_selected');
            $(obj).addClass('image_overlay_box_unselected');
        }
        if($('.select_delete input:checked').each(function(){}).length>0){
            $('.set_delete_image_btn').prop('disabled',false);
        }else{
            $('.set_delete_image_btn').prop('disabled',true);
        }
    }
    function set_delete_image(review_id) {
        img_num = 0;
        img_id = [];
        $("input[name='delete_image[]']:checked").each(function(index){
            img_num++;
            img_id.push($(this).val());
        });
        if(confirm('คุณต้องการจะลบรูปทั้งหมด '+img_num+' รูป ใช่หรือไม่')){
            show_loading('on');
            var uid = <?php echo $user_id; ?>;
            $.ajax({
                url: '<?php echo site_url('user/remove_review_photo'); ?>',
                type: 'post',
                data: {'uid':uid,'vid':current_review_id,'img_id':img_id},
                dataType: 'json',
                success: function (data) {
                    if(data.status=="SUCCESS"){
                        //reload_album_image();
                        window.location.reload();
                    }else{
                        alert("ระบบมีปัญหา ไม่สามารถลบรูปได้ กรุณาติดต่อฝ่ายเทคนิค.")
                    }
                }
            });
        }
    }
    function show_loading(state) {
        if(state=='on'){
            $("#wait_loading").show();
        }else{
            $("#wait_loading").hide();
        }
    }
    $(document).ready(function () {
        $(window).resize(function () {
            $('.img-thumbnail.img_all').each(function (index) {
                console.log($( this ).height(),$( this ).parent().parent().find('a').width());
                var w_img = $( this ).parent().parent().find('.img-thumbnail.img_all').width();
                var h_img = $( this ).height();
                console.log($( this ).parent().parent().find('.select_img_c'),w_img,h_img);
                $( this ).parent().parent().find('.select_img_c').height(h_img);
                $( this ).parent().parent().find('.select_img_c').width(w_img);
                $( this ).parent().parent().find('.select_img_c input').css('margin-top',(h_img/2)-8);
                $( this ).parent().parent().find('.select_img_c span').css('margin-top',(h_img/2)-12);
            });
        });
    });


    var total_page = 0;
    var page = 1;
    var current_user_id = 0;
    function open_log(user_id) {
        current_user_id = user_id;
        $.fancybox.open({href : '#markUp'} );
        $.ajax({
            url: "<?php echo api_site_url('deal/get_user_deal_log/'); ?>"+user_id,
            type: 'get',
            data: {'limit':20,'page':page},
            dataType: 'json',
            success: function (respond) {
                var textHTML = '';
                total_page = respond.total_page;
                gen_page(total_page);

                var point = "";
                if(respond.total_num>0){
                    $(respond.data).each(function (index) {
                        var item = respond.data[index];
                        if(item.point_type=="use"){
                            point = "<span style='color: red;'>-"+item.point+"</span>";
                        }else{
                            point = "<span style='color: green;'>+"+item.point+"</span>";
                        }
                        textHTML += '<tr><td class="text-left small">'+item.created_time+'</td>';
                        textHTML += '<td class="text-left"><a href="/deal/view/'+item.deal_id+'" target="_blank">'+item.deal_name+'</a></td>';
                        textHTML += '<td class="text-right">'+point+'</td></tr>';
                    });
                }else{
                    textHTML += '<tr><td colspan="4" class="text-center">ไม่มีข้อมูล</td></tr>';
                }
                $("#searchResult").html(textHTML);
            }
        });
    }
    function goto_page(page_id) {
        page = page_id;
        open_log(current_user_id);
    }

    function gen_page() {
        if(total_page>0){
            if ((page - 1) > 0) page_back = page - 1; else page_back = page;
            if ((page + 1) < total_page) page_next = page + 1; else page_next = total_page;
            html_page = '<li><a href="javascript:void(0);" onclick="goto_page(' + page_back + ')">&laquo;</a></li>';
            for (var i = 1; i <= total_page; i++) {
                if (i == page) is_active = ' class="active"'; else is_active = '';
                if (i <= 3 || i >= (total_page - 2) || i == page) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_back) {
                    html_page += '<li class="disabled"><span>...</span></li> <li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_next) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li><li class="disabled"><span>...</span></li>';
                } else {
                    html_page += "#";
                }
            }
            while (html_page.indexOf("##") > 1) {
                html_page = html_page.replace("##", '#');
            }
            html_page = html_page.replace("#", '<li class="disabled"><span>...</span></li>');
            html_page = html_page.replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>');
            html_page += '<li><a href="javascript:void(0);" onclick="goto_page(' + page_next + ')">&raquo;</a></li>';
        }
        $("#log_paging").html(html_page);
    }
</script>
<style>
    .image_overlay_box_selected, .image_overlay_box_unselected{
        top: 5px;
        left: 9px;
    }
    .review_photo_list .col-md-2{
        padding-right: 4px;
        padding-left: 4px;
    }
</style>