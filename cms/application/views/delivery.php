<?php
$all_selected = '';
$normal_selected = '';
$lady_selected = '';
$param_url = '';
$status = $this->input->get('type');
$keyword= $this->input->get('keyword');

if($status=="all"){
    $all_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="normal"){
    $normal_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}else if($status=="lady"){
    $lady_selected = 'selected="selected"';
    $param_url = '&status='.$status;
}
if($keyword && (strlen($keyword)>0)){
    $param_url .= '&keyword='.$keyword;
}
$page_title = 'delivery';
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back.$param_url) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
    if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_back) {
        $html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li>';
    } else if ($i == $page_next) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i.$param_url) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
    } else {
        $html_page .= "#";
    }
}

while (strpos($html_page, "##") > 1) {
    $html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next.$param_url) . '">&raquo;</a></li>';
?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการเดลิเวอรี</strong>
</div>
<div class="col-lg-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการเดลิเวอรี</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-6 text-left">
            <form class="form-inline" action="<?php echo site_url('delivery')?>" method="">
                <div class="form-group">
                    <label for="search_keyword">ประเภท:
                        <select name="type" id="search_status" class="form-control">
                            <option value="all" <?php echo $all_selected;?>>ทั้งหมด</option>
                            <option value="normal" <?php echo $normal_selected;?>>ทั่วไป</option>
                            <option value="lady" <?php echo $lady_selected;?>>Lady</option>
                        </select>
                        <input type="text" class="form-control" value="<?php echo $keyword;?>" id="search_keyword" name="keyword" placeholder="ค้นหา" style="width: 230px;">
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                    </label>
                </div>
                <?php if($status && isset($status)&& $status!=''){?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนที่พบ: </label> <?php echo $total_num; ?>
                    </div>
                <?php }else{ ?>
                    <div class="form-group">
                        <label for="search_keyword">จำนวนทั้งหมด: </label> <?php echo $total_num; ?>
                    </div>
                <?php } ?>
            </form>
        </div>
        <div class="col-lg-6 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="<?php echo site_url('delivery/add')?>"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th class="text-center" style="width: 100px;">ลำดับ</th>
                <th class="text-center" style="width: 100px;" >รูป</th>
                <th class="text-center" style="" >รายชื่อ</th>
                <th class="text-center" style="width: 150px;">Line@</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($delivery_list)>0){
                foreach ($delivery_list as $item){
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $item->order_number;?></td>
                        <td class="text-left">
                            <a href="<?php echo $item->cover_image_src;?>" data-fancybox="images" data-width="2048" data-height="1365">
                                <img src="<?php echo $item->cover_image_src;?>" style=" max-height: 65px;">
                            </a>
                        </td>
                        <td class="text-left"><a href="<?php echo site_url('delivery/view/'.$item->id);?>" target="_blank"><?php echo $item->title ;?></a></td>
                        <td class="text-center"><?php echo $item->line_id ;?></td>
                        <td class="text-center">
                            <a href="<?php echo site_url('delivery/edit/'.$item->id);?>" >แก้ไข</a> |
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>);">ลบ</a>
                        </td>

                    </tr>
                    <?php }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="5">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-sm-12 text-center">
                <ul class="pagination" id="paging">
                    <?php echo $html_page; ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 450px;">
            <div class="col-md-12">
                <form action="<?php echo site_url('delivery/update/del')?>" method="post" id="delivery_form">
                    <input value="" name="do" id="post_method" type="hidden">
                    <input value="" name="id" id="item_id" type="hidden">
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function del_data(id) {
        if(confirm('คุณต้องการลบเดลิเวอรีนี้หรือไม่?')){
            $("#post_method").val('del');
            $("#item_id").val(id);
            $("#delivery_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });
</script>