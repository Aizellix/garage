<?php
$page_title = 'business/review/'.$business_data->id;
if (($page - 1) > 0) $page_back = $page - 1; else $page_back = $page;
if (($page + 1) < $total_page) $page_next = $page + 1; else $page_next = $total_page;

$html_page = '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_back) . '">&laquo;</a></li>';

for ($i = 1; $i <= $total_page; $i++) {
    if ($i == $page) $is_active = ' class="active"'; else $is_active = '';
    if ($i <= 3 || $i >= ($total_page - 2) || $i == $page) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i) . '">' . $i . '</a></li>';
    } else if ($i == $page_back) {
        $html_page .= '<li class="disabled"><span>...</span></li> <li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i) . '">' . $i . '</a></li>';
    } else if ($i == $page_next) {
        $html_page .= '<li' . $is_active . '><a href="' . site_url('/'.$page_title.'?p=' . $i) . '">' . $i . '</a></li><li class="disabled"><span>...</span></li>';
    } else {
        $html_page .= "#";
    }
}

while (strpos($html_page, "##") > 1) {
    $html_page = str_replace("##", '#', $html_page);
}
$html_page = str_replace("#", '<li class="disabled"><span>...</span></li>', $html_page);
$html_page = str_replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>', $html_page);
$html_page .= '<li><a href="' . site_url('/'.$page_title.'?p=' . $page_next) . '">&raquo;</a></li>';

$address = '';
if($business_data->address_no!=""    ) $address = $business_data->address_no;
if($business_data->address_moo!="") $address .= " หมู่ที่ ".$business_data->address_moo;
if($business_data->address_village!="") $address .= " หมู่บ้าน ".$business_data->address_moo;
if($business_data->address_soi!="") $address .= " ซอย ".$business_data->address_soi;
if($business_data->address_road!="") $address .= " ".$business_data->address_road;
if($business_data->address_tambol!="") $address .= " ".$business_data->address_tambol;
if($business_data->address_district!="") $address .= " ".$business_data->address_district;
if($business_data->address_province!="") $address .= " ".$business_data->address_province;
if($business_data->address_postcode!="") $address .= " ".$business_data->address_postcode;

function img_select_box($img_data,$review_id) {
    $html_txt =  '<data data-id="'.$img_data["id"].'" data-src="'.$img_data['url'].'"></data>';
    $html_txt.= '<label style="display:none;" onclick="checked_box_delete(this)" class="select_delete image_overlay_box_unselected select_img_c_'.$review_id.'"><input type="checkbox" name="delete_image[]" value="'.$img_data["id"].'"></label>';
    return $html_txt;
}

?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรีวิวสถานประกอบการ</strong>
</div>

<div class="col-md-12" style="clear: both;">

    <div id="page_operator" class="" style="padding-bottom: 8px;">
        <div class="col-lg-12 text-left">
            <table class="table table-bordered">
                <tr>
                    <td rowspan="3" class="text-right" style="width: 150px; font-weight: bold;">
                        <img src="<?php echo $business_data->cover_image_src;?>" style="width: 100%">
                    </td>
                    <td class="text-right" style="width: 150px; font-weight: bold;">ชื่อสถานประกอบการ</td>
                    <td colspan="3" class="text-left"><?php echo $business_data->name;?></td>
                    <td rowspan="3" valign="middle" class="text-center" style="width: 50px;"><a target="_blank" href="<?php echo site_url('business/view/'.$business_data->id)?>">ดู</a></td>
                </tr>
                <tr>
                    <td class="text-right" style="width: 150px; font-weight: bold;">ที่อยู่</td>
                    <td colspan="3" class="text-left"><?php echo $address;?></td>
                </tr>
                <tr>
                    <td class="text-right" style="width: 150px; font-weight: bold;">จำนวนรีวิว</td>
                    <td class="text-left"><?php echo $business_data->reviewed;?></td>
                    <td class="text-right" style="width: 150px; font-weight: bold;">เรตติ้ง</td>
                    <td class="text-left"><?php echo $business_data->score." (".$business_data->total_score.")";?></td>
                </tr>
            </table>
            <form id="review_form" action="<?php echo site_url('user/update')?>" method="post" style="display: none;">
                <input type="hidden" value="" name="do" id="do_update">
                <input type="hidden" value="" name="user_id" id="user_id">
                <input type="hidden" value="" name="review_id" id="review_id">
                <input type="hidden" value="1" name="is_business" id="is_business">
                <input type="hidden" value="" name="business_id" id="business_id">
            </form>
            <strong style="font-size: 16px; display: block;">รายการรีวิวของสถานประกอบการ</strong>
            <table class="table table-bordered">
                <tbody>
                <?php if(count($review_list)>0){

                    foreach ($review_list as $item){
                        $user_data = $user_list[$item->user_id];
                        ?>
                        <tr>
                            <td class="text-left" rowspan="3" style="width: 150px;">
                                <img src="<?php echo $user_data->profile_image?>" style="width: 100%">
                            </td>
                            <td class="text-left" style="width: 150px;"><strong>ผู้ใช้</strong></td>
                            <td class="text-left">
                                <a href="<?php echo site_url('user/review/'.$user_data->id)?>" target="_blank"><?php echo $user_data->name?></a>
                            </td>
                            <td class="text-left" style="width: 150px;"><strong>คะแนน</strong></td>
                            <td class="text-left"><?php echo $item->score?></td>
                            <td rowspan="2" valign="middle" align="center" style="width: 50px;">
                                <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>,<?php echo $user_data->id?>,<?php echo $business_data->id?>)">ลบ</a>
                            </td>

                        </tr>
                        <tr>
                            <td class="text-left" style="width: 100px;"><strong>วันที่สร้าง</strong></td>
                            <td class="text-left"><?php echo $item->created_time?></td>
                            <td class="text-left" style="width: 100px;"><strong>วันที่แก้ไขล่าสุด</strong></td>
                            <td class="text-left"><?php echo $item->updated_time?></td>
                        </tr>
                        <tr>
                            <td class="text-left" colspan="5"><?php echo $item->detail?></td>
                        </tr>
                        <tr>
                            <td class="text-left" colspan="6" style="background: #EFEFEF;"><?php if(count($item->images)>0) {?>
                                    <div style="display: inline-block; float: right;">
                                        <button id="show_del_photo_btn_<?php echo $item->id;?>" type="button" class="btn btn-danger show_del_photo_btn" onclick="show_delete_photo(<?php echo $item->id;?>)">เลือกรูปที่ต้องการลบ</button>
                                        <button id="confirm_del_photo_btn_<?php echo $item->id;?>" style="display: none" type="button" class="btn btn-danger set_delete_image_btn" onclick="set_delete_image(<?php echo $item->id;?>)">ตกลง</button>
                                    </div>
                                    <div style="display: inline-block; float: left;">
                                        <button id="cancel_del_photo_btn_<?php echo $item->id;?>" type="button" style="display: none" class="btn btn-default" onclick="hide_img_sub_tool(<?php echo $item->id;?>)">ยกเลิก</button>
                                    </div>
                                    <div style="clear: both;" class="review_photo_list col-md-12">
                                        <?php
                                        for($i=0;$i<count($item->images);$i+=6){
                                            $html_text ='<div class="row"><input type="hidden" name="review_id" class="review_id" value="'.$item->id.'">';
                                            if(isset($item->images[$i])){
                                                $html_text .='<div class="col-md-2"><a href="'.$item->images[$i]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i],$item->id).'</div>';
                                            }
                                            if(isset($item->images[$i+1])){
                                                $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+1]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+1]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+1],$item->id).'</div>';
                                            }else{
                                                $html_text .='<div class="col-md-2"></div>';
                                            }
                                            if(isset($item->images[$i+2])){
                                                $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+2]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+2]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+2],$item->id).'</div>';
                                            }else{
                                                $html_text .='<div class="col-md-2"></div>';
                                            }
                                            if(isset($item->images[$i+3])){
                                                $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+3]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+3]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+3],$item->id).'</div>';
                                            }else{
                                                $html_text .='<div class="col-md-2"></div>';
                                            }
                                            if(isset($item->images[$i+4])){
                                                $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+4]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+4]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+4],$item->id).'</div>';
                                            }else{
                                                $html_text .='<div class="col-md-2"></div>';
                                            }
                                            if(isset($item->images[$i+5])){
                                                $html_text .='<div class="col-md-2"><a href="'.$item->images[$i+5]['url'].'" data-fancybox="images" data-width="500" data-height="400"><img class="img-thumbnail img_all" src="'.$item->images[$i+5]['url'].'" style="max-height: 100%; max-width: 100%;"></a>'.img_select_box($item->images[$i+5],$item->id).'</div>';
                                            }else{
                                                $html_text .='<div class="col-md-2"></div>';
                                            }

                                            echo $html_text .='</div>';
                                        }
                                        ?>
                                    </div>
                                <?php }?>
                            </td>
                        </tr>
                    <?php  }
                }else{ ?>
                    <tr>
                        <td class="text-center" colspan="6">ไม่มีข้อมูล</td>
                    </tr>
                <?php }?>
                </tbody>
            </table>
            <div class="row" style="padding-bottom: 100px;">
                <div class="col-sm-12 text-center">
                    <ul class="pagination" id="paging">
                        <?php echo $html_page; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>

<script>

    function del_data(review_id,user_id,business_id) {
        if(confirm('คุณต้องการลบรีวิวนี้หรือไม่?')){
            $("#do_update").val('del_review');
            $("#review_id").val(review_id);
            $("#business_id").val(business_id);
            $("#user_id").val(user_id);
            $("#review_form").submit();
        }
    }
    $("[data-fancybox]").fancybox({
        // Options will go here
    });

    function show_delete_photo(review_id) {
        $('.show_del_photo_btn').prop('disabled',true);
        $('#cancel_del_photo_btn_'+review_id).show();
        $('#confirm_del_photo_btn_'+review_id).show();
        $('#show_del_photo_btn_'+review_id).hide();
        $('.set_delete_image_btn').prop('disabled',true);
        $('.select_delete.select_img_c_'+review_id).show();
        $('.img-thumbnail.img_all').each(function (index) {
//            console.log($( this ).height(),$( this ).parent().parent().find('a').width());
            var w_img = $( this ).parent().parent().find('.img-thumbnail.img_all').width();
            var h_img = $( this ).height();
//            console.log($( this ).parent().parent().find('.select_img_c'),w_img,h_img);
            $( this ).parent().parent().find('.select_img_c_'+review_id).height(h_img);
            $( this ).parent().parent().find('.select_img_c_'+review_id).width(w_img);
            $( this ).parent().parent().find('.select_img_c_'+review_id+' input').css('margin-top',(h_img/2)-8);
            $( this ).parent().parent().find('.select_img_c_'+review_id+' span').css('margin-top',(h_img/2)-12);
        })
        current_review_id = review_id;
    }
    function hide_img_sub_tool(review_id) {
//        $('.select_delete').hide();
        $('.select_img_c_'+review_id).removeClass('image_overlay_box_selected');
        $('.select_img_c_'+review_id).addClass('image_overlay_box_unselected');
        $(".select_cover input").prop('checked', false);
        $(".select_delete input").prop('checked', false);
        $('.show_del_photo_btn').prop('disabled',false);
        $('.set_image_cover_btn').hide();
        $('.set_delete_image_btn').hide();
        $('#img_sub_tool').hide();
        $('.select_img_c_'+review_id).hide();
        $('#cancel_del_photo_btn_'+review_id).hide();
        $('#confirm_del_photo_btn_'+review_id).hide();
        $('#show_del_photo_btn_'+review_id).show();
    }
    function checked_box_delete(obj) {
        if($(obj).find('input').prop('checked')==true){
            $(obj).removeClass('image_overlay_box_unselected');
            $(obj).addClass('image_overlay_box_selected');
        }else{
            $(obj).removeClass('image_overlay_box_selected');
            $(obj).addClass('image_overlay_box_unselected');
        }
        if($('.select_delete input:checked').each(function(){}).length>0){
            $('.set_delete_image_btn').prop('disabled',false);
        }else{
            $('.set_delete_image_btn').prop('disabled',true);
        }
    }
    function set_delete_image() {
        img_num = 0;
        img_id = [];
        $("input[name='delete_image[]']:checked").each(function(index){
            img_num++;
            img_id.push($(this).val());
        });
        if(confirm('คุณต้องการจะลบรูปทั้งหมด '+img_num+' รูป ใช่หรือไม่')){
            show_loading('on');
            var uid = <?php echo $user_id; ?>;
            $.ajax({
                url: '<?php echo site_url('user/remove_review_photo'); ?>',
                type: 'post',
                data: {'uid':uid,'vid':current_review_id,'img_id':img_id},
                dataType: 'json',
                success: function (data) {
                    if(data.status=="SUCCESS"){
                        //reload_album_image();
                        window.location.reload();
                    }else{
                        alert("ระบบมีปัญหา ไม่สามารถลบรูปได้ กรุณาติดต่อฝ่ายเทคนิค.")
                    }
                }
            });
        }
    }
    function show_loading(state) {
        if(state=='on'){
            $("#wait_loading").show();
        }else{
            $("#wait_loading").hide();
        }
    }
    $(document).ready(function () {
        $(window).resize(function () {
            $('.img-thumbnail.img_all').each(function (index) {
                console.log($( this ).height(),$( this ).parent().parent().find('a').width());
                var w_img = $( this ).parent().parent().find('.img-thumbnail.img_all').width();
                var h_img = $( this ).height();
                console.log($( this ).parent().parent().find('.select_img_c'),w_img,h_img);
                $( this ).parent().parent().find('.select_img_c').height(h_img);
                $( this ).parent().parent().find('.select_img_c').width(w_img);
                $( this ).parent().parent().find('.select_img_c input').css('margin-top',(h_img/2)-8);
                $( this ).parent().parent().find('.select_img_c span').css('margin-top',(h_img/2)-12);
            });
        });
    });
</script>
<style>
    .image_overlay_box_selected, .image_overlay_box_unselected{
        top: 5px;
        left: 9px;
    }
    .review_photo_list .col-md-2{
        padding-right: 4px;
        padding-left: 4px;
    }
</style>