<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการติดต่อฉุกเฉิน</strong>
</div>

<div class="col-md-12" style="clear: both;">
    <strong style="font-size: 16px;">รายการติดต่อฉุกเฉิน</strong>
    <div id="page_operator" class="row" style="padding-bottom: 8px;">
        <div class="col-lg-12 text-right">
            <a id="add_staff_btn" class="btn btn-default" style="color: green;" href="javascript:void(0);" onclick="add_data()"><span class="glyphicon glyphicon-plus"></span> เพิ่ม</a>
        </div>
    </div>
    <div id="main_content" class="">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th class="text-center" style="width: 48px;">#</th>
                <th class="text-center" style="">รายชื่อ</th>
                <th class="text-center" style="width: 150px;">ประเภท</th>
                <th class="text-center" style="width: 150px;">หมายเลขโทรศัพท์</th>
                <th class="text-center" style="width: 100px;">ลำดับ</th>
                <th class="text-center" style="width: 110px;">Tools</th>
            </tr>
            </thead>
            <tbody>
            <?php if(count($emergency_list)>0){
                $n = 1;
                foreach ($emergency_list as $item){
                    ?>
                    <tr>
                        <td class="text-left"><?php echo $n;?></td>
                        <td class="text-left"><?php echo $item->title ;?></td>
                        <td class="text-left"><?php echo ($item->type=="emergency")?"เบอร์ฉุกเฉิน":"ประกันภัย"; ;?></td>
                        <td class="text-left"><?php echo $item->tel ;?></td>
                        <td class="text-center"><?php echo $item->order_number ;?></td>
                        <td class="text-left">
                            <a href="javascript:void(0);" onclick="edit_data(<?php echo $item->id?>)">แก้ไข</a> |
                            <a href="javascript:void(0);" onclick="del_data(<?php echo $item->id?>)">ลบ</a>
                            <input type="hidden" id="title_<?php echo $item->id?>" value="<?php echo $item->title?>">
                            <input type="hidden" id="tel_<?php echo $item->id?>" value="<?php echo $item->tel?>">
                            <input type="hidden" id="order_number_<?php echo $item->id?>" value="<?php echo $item->order_number?>">
                            <input type="hidden" id="type_<?php echo $item->id?>" value="<?php echo $item->type?>">
                        </td>

                    </tr>
                <?php $n++; }
            }else{ ?>
                <tr>
                    <td class="text-center" colspan="4">ไม่มีข้อมูล</td>
                </tr>
            <?php }?>
            </tbody>
        </table>

    </div>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 450px;">
            <div class="col-md-12">
                <form action="<?php echo site_url('emergency/update')?>" method="post" id="emergency_form">
                    <div class="form-group">
                        <label for="emergency_title">ชื่อสำหรับการติดต่อ</label>
                        <input type="text" class="form-control" id="emergency_title" placeholder="Title" name="title">
                    </div>
                    <div class="form-group">
                        <label for="emergency_tel">หมายเลขโทรศัพท์</label>
                        <input type="text" class="form-control" id="emergency_tel" placeholder="text" name="tel">
                    </div>
                    <div class="form-group">
                        <label for="emergency_tel">ลำดับ</label>
                        <input type="text" class="form-control" id="emergency_order_number" placeholder="text" name="order_number">
                    </div>
                    <div class="form-group">
                        <label for="emergency_type">ประเภท</label>
                        <select id="emergency_type" class="form-control" name="type">
                            <option value="emergency">เบอร์ฉุกเฉิน</option>
                            <option value="insurance">ประกันภัย</option>
                        </select>
                    </div>
                    <div class="row">
                        <input value="" name="do" id="post_method" type="hidden">
                        <input value="" name="id" id="item_id" type="hidden">
                        <div class="col-md-6 text-left">
                            <button type="reset" class="btn btn-default">Reset</button>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-default" onclick="update_data()">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    function add_data() {
        $("#post_method").val('add');
        $("#item_id").val('');
        $("#emergency_tel").val('');
        $("#emergency_title").val('');
        var selectedOptions = $("#emergency_type").find("option:selected");
        selectedOptions.prop('selected',false);
        $.fancybox.open({href : '#markUp'} );
    }
    function edit_data(id) {
        $("#post_method").val('edit');
        tel = $("#tel_"+id).val();
        order_number = $("#order_number_"+id).val();
        title = $("#title_"+id).val();
        type = $("#type_"+id).val();
        $("#item_id").val(id);
        $("#emergency_tel").val(tel);
        $("#emergency_order_number").val(order_number);
        $("#emergency_title").val(title);
        var selectedOptions = $("#emergency_type").find("option:selected");
        selectedOptions.prop('selected',false);
        $("#emergency_type option[value='"+type+"']").prop('selected',true);
        $.fancybox.open({href : '#markUp'} );
    }
    function update_data() {
        $("#emergency_form").submit();
    }
    function del_data(id) {
        if(confirm('คุณต้องการลบชื่อติดต่อนี้หรือไม่?')){
            $("#post_method").val('del');
            $("#item_id").val(id);
            $("#emergency_form").submit();
        }
    }

    function del_business(id) {
        if(confirm('คุณต้องการลบสถานที่หรือไม่')){
            window.location.assign('<?php echo site_url('business/del/')?>'+id);
        }
    }
</script>