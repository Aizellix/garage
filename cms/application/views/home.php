<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">หน้าแรก</strong>
</div>
<div class="col-lg-12" style="">
    <strong style="font-size: 16px;">ข้อมูลภาพรวม</strong>
    <table class="table table-bordered">
        <tbody>

        <tr>
            <td>จำนวนผู้ประกอบการทั้งหมด</td>
            <td style="width: 100px;" class="text-right"> <?php echo $business_active_total;?> ราย</td>
        </tr>
        <tr>
            <td>คำร้องทั้งหมดที่ยังไม่ได้รับการยืนยัน</td>
            <td style="width: 100px;" class="text-right"> <?php echo $business_pending_total;?> ราย</td>
        </tr>

        <tr style="">
            <td>จำนวนสมาชิกทั้งหมด</td>
            <td style="width: 100px;" class="text-right"> <?php echo $user_total;?> คน</td>
        </tr>

        <tr style="">
            <td>จำนวนการรีวิวทั้งหมด</td>
            <td style="width: 100px;" class="text-right"> <?php echo $review_total;?> รายการ</td>
        </tr>

        </tbody>
    </table>
</div>