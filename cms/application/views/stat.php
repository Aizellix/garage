<?php
$errorMSG = "";
$doneMSG = "";

$day = $this->input->get("date");
$stat_day = strtotime(date("Y-m-d 00:00:00", strtotime("- 1 day")));
$yesterday = strtotime(date("Y-m-d 23:59:59", strtotime("- 1 day")));

if ($day != null) {
    if (strtotime($day) == false) {
        $day = strtotime("- 1 day");
    } else {
        $day = strtotime($day);
    }
    $stat_day = strtotime(date("Y-m-d 00:00:00", $day));
    $yesterday = strtotime(date("Y-m-d 23:59:59", $day));
}

$stat_2day = $stat_day - 86400;
$stat_3day = $stat_day - (86400 * 2);
$stat_4day = $stat_day - (86400 * 3);
$stat_5day = $stat_day - (86400 * 4);
$stat_6day = $stat_day - (86400 * 5);
$stat_7day = $stat_day - (86400 * 6);
$stat_30day = $stat_day - (86400 * 29);

?>
<script language="javascript">
    $(document).ready(function () {
    });
    function showContent(id) {
        var options = {};
        if (id == "all") {
            if ($(".showAll").html() == "[show all]") {
                $(".showContent").html("[hide]");
                $(".showAll").html("[hide all]");
                $(".fromTable").show("blind", options, 400);
            } else {
                $(".showContent").html("[show]");
                $(".showAll").html("[show all]");
                $(".fromTable").hide("blind", options, 400);
            }
        } else {
            if ($("#sC" + id).html() == "[show]") {
                $("#sC" + id).html("[hide]");
            } else {
                $("#sC" + id).html("[show]");
            }
            $("#content" + id).toggle("blind", options, 400);
        }
    }
</script>
<script type="text/javascript" src="<?php echo site_assets_url('js/datepickup.js')?>"></script>
<script type="text/javascript" src="<?php echo site_assets_url('js/jspdf.debug.js')?>"></script>
<script type="text/javascript">
    var init_stat = true;
    $(document).ready(function () {

        if(init_stat){
            get_daily_stat();
            init_stat = false;
        }
    });

    $(function () {
        var dates = $("#from_date, #to_date").datepicker({
            defaultDate:"+1w",
            changeMonth:true,
            numberOfMonths:1,
            maxDate:0,
            showOn:"button",
            buttonImage:"<?php echo site_assets_url('images/datepicker/calendar.gif')?>",
            buttonImageOnly:true,
            onSelect:function (selectedDate) {
                var option = this.id == "from_date" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(
                        instance.settings.dateFormat ||
                        $.datepicker._defaults.dateFormat,
                        selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
            }
        });
    });

    function get_daily_stat() {
        $.ajax({
            url: "<?php echo site_url('stat/get_stat');?>?do=get_daily_stat",
            success: function (result) {
                $("#loading").hide();
                $("#show_stat").html(result);
                <?php if($this->input->get('print')=="1"){?>
                savePDF();
                <?php }?>
                getstat();
            },
            error: function () {
                $(".frm").hide();
                $(".resProcess").show();
                $(".resProcess td").html(msgError)
            },
            complete: function () {
                $(".frm").hide();
                $(".resProcess").after("");
            }

        });
    }

    function change_stat(date_from) {
        var cc = $("#by_country").val();
        $.ajax({
            url:"<?php echo site_url('stat/get_stat');?>?do=getstat&date=" + date_from,
            success:function (result) {
                $("#loading").hide();
                $("#stat_data").html(result);
            },
            error:function () {
                $(".frm").hide();
                $(".resProcess").show();
                $(".resProcess td").html(msgError)
            },
            complete:function () {
                $(".frm").hide();
                $(".resProcess").after("");
            }
        });
    }

    function getstat() {
        if ($("#detail_m").val() != 'null' && $("#detail_y").val() != 'null') {
            stat_date = $("#detail_y").val() + "-" + $("#detail_m").val() + "-" + "01" + " " + "00:00:00";
            change_stat(stat_date);
            $("#error").html("");
            $("#loading").show();
        } else if ($("#detail_y").val() == 'null') {
            $("#error").html("Please choose year.");
        } else {
            stat_date = $("#detail_y").val();
            change_stat(stat_date);
            $("#error").html("");
            $("#loading").show();
        }
    }

</script>
<style type="text/css">

    #labelHeader {
        font-size: 16px;
        color: #3366ff;
        padding-bottom: 10px;
        border-bottom: 1px #ccc solid;
    }

    form, table {
        font-size: 12px;
        color: #666;
    }

    .border_right {
        border-right: 1px #bbb solid !important;
    }

    .statHead {
        font-size: 14px;
    }

    #labelHeader {
        font-size: 16px;
        color: #3366ff;
        padding-bottom: 10px;
        border-bottom: 1px #ccc solid;
    }

    .statHead {
        font-size: 18px;
    }

    .border_right {
        border-right: 1px #CCCCCC solid;
    }
</style>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">ข้อมูลสถิติ</strong>
</div>
<div class="col-md-12" style="clear: both;">

    <div id="main_content" class="">
        <div id="overall_stat">
            <strong style="font-size: 16px;">ข้อมูลภาพรวม <?php echo date('d/m/Y',strtotime('-1 day'))?></strong>
            <div class="row">
                <div class="col-md-12">
                    <div id="show_stat"></div>
                </div>
            </div>
        </div>
        <div class="text-right">
            <button type="button" id="cmd"><span class="glyphicon glyphicon-download-alt"></span> ดาวน์โหลดเป็น PDF</button>
        </div>

        <div>
            <strong style="font-size: 16px;display: none;">ข้อมูลอย่างละเอียด</strong>
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <form class="form-inline">
                        <div class="form-group">
                            <select name="detail_m" id="detail_m" class="form-control">
                                <option value="null">-เลือกเดือน-</option>
                                <option value="01">มกราคม</option>
                                <option value="02">กุมภาพัน</option>
                                <option value="03">มีนาคม</option>
                                <option value="04">เมษายน</option>
                                <option value="05">พฤษพาคม</option>
                                <option value="06">มิถุนายน</option>
                                <option value="07">กรกฎาคม</option>
                                <option value="08">สิงหาคม</option>
                                <option value="09">กันยายน</option>
                                <option value="10">ตุลาคม</option>
                                <option value="11">พฤศจิกายน</option>
                                <option value="12">ธันวาคม</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="detail_y" id='detail_y' class="form-control">
                                <option value="null">-Select Year-</option>
                                <?php
                                $y = (int)date("Y", strtotime("+0 year"));
                                for ($i = 2017; $i <= $y; $i++) {
                                    if ((int)date("Y") == $i) {
                                        $select = 'selected="selected"';
                                    } else {
                                        $select = '';
                                    }
                                    echo '<option ' . $select . ' value="' . $i . '">' . $i . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <button type="button" class="btn btn-default" onclick="getstat()">ดูสถิติ</button>
                        <label generated="true" class="error" id="error"></label>
                        <img src="<?php echo site_assets_url('images/loading.gif')?>" border="0" id="loading" alt="" style="display: none; top:3px; position: relative;">
                    </form>
                    <div>
                        <div id="stat_data"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
</div>


<?php if($this->input->get('print')=="1"){?>
    <script>
        function savePDF() {
            var doc = new jsPDF();
            doc.addHTML($('#mainPage')[0], 15, 15, {
                'background': '#fff',
            }, function() {
                doc.save('<?php echo 'stat_overall_'.date('Y-m-d',strtotime('-1 day'))?>.pdf');
            });
        }
    </script>
<?php }else{?>
    <script>
        $(function () {
            $('#cmd').click(function () {
                window.open('<?php echo site_url('stat?print=1')?>','_blank');
            });
        });
    </script>
<?php }?>