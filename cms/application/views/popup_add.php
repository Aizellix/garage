<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการป๊อปอัพ</strong>
</div>

<div class="col-md-12 col-lg-8" style="clear: both;">
    <strong style="font-size: 16px;">เพิ่มป๊อปอัพ</strong>
    <form action="<?php echo site_url('popup/update/add')?>" method="post" enctype="multipart/form-data" id="mainForm">
        <div id="main_content">
            <div class="form-group">
                <label for="popup_title">ชื่อหัวข้อบทความ</label>
                <input type="text" class="form-control" id="popup_title" placeholder="ชื่อหัวข้อบทความ" name="title">
            </div>
            <div class="form-group">
                <label for="order_number">ลำดับ</label>
                <input type="text" class="form-control" id="order_number" placeholder="1-999" name="order_number" style="width: 80px;" value="999">
            </div>
            <div class="form-group">
                <label for="popup_cover">รูปภาพหัวข้อบทความ (ใช้รูปขนาด 560 x 850 เท่านั้น)</label>
                <div>
                    <img id="simple_cover_image" src="" style="width: 50%">
                </div>
                <input type="file" id="popup_cover" name="cover">
                <p class="help-block">สามารถ Upload ได้เฉพาะไฟล์ jpg, jpeg, png ขนาดไม่เกิน 4MB เท่านั้น</p>
            </div>
            <div class="form-group" >
                <label for="">ระยะเวลาการเผยแพร่
                    <div class="form-inline col-md-12">
                        <label for="start_time">ตั้งแต่</label>
                        <input type="text" class="form-control" id="start_time" name="start_time" style="width: 220px;">
                        <label for="end_time">ถึง</label>
                        <input type="text" class="form-control" id="end_time" name="end_time" style="width: 220px;">
                    </div>
                </label>
            </div>
            <div class="form-group" >
                <label for="">ประเภท
                    <div class="form-inline col-md-12">
                        <label for="type_business">สถานประกอบการ </label>
                        <input type="radio" id="type_business" name="type" value="business" checked style="margin-right: 20px;">
                        <label for="type_article">บทความ </label>
                        <input type="radio" id="type_article" name="type" value="article" style="margin-right: 20px;">
                        <label for="type_promotion">โปรโมชั่น </label>
                        <input type="radio" id="type_promotion" name="type" value="promotion" style="margin-right: 20px;">
                        <label for="type_deal">ดีล </label>
                        <input type="radio" id="type_deal" name="type" value="deal" style="margin-right: 20px;">
                        <label for="type_delivery">เดลิเวอรี </label>
                        <input type="radio" id="type_delivery" name="type" value="delivery" style="margin-right: 20px;">
                        <label for="type_link">Link </label>
                        <input type="radio" id="type_link" name="type" value="link" style="margin-right: 20px;">
                    </div>
                </label>
                <table class="table table-bordered type_business">
                    <thead>
                    <tr style="background: #ccc;" id="search_div">
                        <th colspan="2">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button id="search_btn" class="btn btn-default" onclick="openSearchBox()" type="button">
                                        <span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูล
                                    </button>
                                </div>
                            </div>
                        </th>
                    </tr>
                    <tr style="background: #ccc; display: none;" id="link_div">
                        <th colspan="2">
                            <div class="row">
                                <div class="col-md-12 text-left">
                                    <div class="form-group">
                                        <label for="link_url">URL</label>
                                        <input type="text" class="form-control" id="link_url" placeholder="https://www.google.co.th" name="link_url">
                                    </div>
                                </div>
                            </div>
                        </th>
                    </tr>
                    </thead>
                    <tbody id="businessList">
                    </tbody>
                </table>
            </div>
            <div class="row">
                <div class="col-md-6 text-left">
                    <button type="button" class="btn btn-default" onclick="formReset()">รีเซต</button>
                </div>
                <div class="col-md-6 text-right">
                    <button type="submit" class="btn btn-primary">ยืนยัน</button>
                    <input type="hidden" class="form-control" id="content_id" name="content_id" value="">
                    <input type="hidden" class="form-control" id="selected_type" name="selected_type" value="">
                </div>
            </div>
        </div>

    </form>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 550px;">
            <div class="col-md-12">
                <label for="exampleInputEmail1">ค้นหา</label>
                <form class="form-inline" id="business_search_form">
                    <div class="form-group">
                        <input type="text" class="form-control" id="s_keyword" placeholder="ชื่อสถานประกอบการ / ที่อยู่" style="width: 438px;">
                    </div>
                    <button type="button" class="btn btn-default" onclick="searchBusiness()"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                </form>
                <div style="padding-top: 16px; height: 550px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #ccc;">
                            <th colspan="2" class="text-center"> รายการ  </th>
                        </tr>
                        </thead>
                        <tbody id="searchResult" style="  max-height: 450px; overflow-y: auto; display:block;">
                        <tr style="width: 100%; display: block;">
                            <td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่พบข้อมูล</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    function search_form_initial() {
        $("#s_keyword").val("");
        var textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
        $("#searchResult").html(textHTML);
    }
    function openSearchBox() {
        var type = $(":input[name='type']:checked").val();
        if(type!="business"){
            $("#business_search_form").hide();
            searchBusiness();
        }else{
            $("#business_search_form").show();
        }
        $.fancybox.open({href : '#markUp'} );
    }
    function searchBusiness() {
        var keyword = $("#s_keyword").val();
        var check_whitespace = keyword.replace(new RegExp(' ', 'g'), '');
        var type = $(":input[name='type']:checked").val();
        if(type=="business"){
            api_url = '<?php echo api_site_url('business/search_business?limit=10&offset=0&keyword='); ?>'+keyword;
        }else if(type=="article"){
            api_url = '<?php echo api_site_url('article/get_list?limit=100&offset=0'); ?>';
            keyword = " ";
        }else if(type=="promotion"){
            api_url = '<?php echo api_site_url('promotion/get_list?limit=10&offset=0'); ?>';
            keyword = " ";
        }else if(type=="deal"){
            api_url = '<?php echo api_site_url('deal/get_list?limit=10&offset=0'); ?>';
            keyword = " ";
        }else if(type=="delivery"){
            api_url = '<?php echo api_site_url('delivery/get_list?limit=10&offset=0'); ?>';
            keyword = " ";
        }
        if(type!="business"){
            keyword = " ";
            check_whitespace = " ";
        }
        if(keyword.length>0&&check_whitespace.length>0){
            var textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">';
            textHTML += '<img src="<?php echo site_assets_url('images/loading.gif')?>"></td></tr>';
            $("#searchResult").html(textHTML);
            $.ajax({
                url: api_url,
                type: 'get',
                data: {'limit':200,'offset':0,'keyword':keyword},
                dataType: 'json',
                success: function (respond) {
                    var textHTML = '';
                    if(respond.total>0){

                        if(type!="business"){
                            $(respond.list).each(function (index) {
                                var item = respond.list[index];
                                textHTML += '<tr id="'+index+'_res"><td class="text-left" style="width: 460px;"><img src="'+item.cover_image_src+'" style="width: 120px;">';
                                textHTML += '<data data-img="'+item.cover_image_src+'" data-id="'+item.id+'"></data></td>';
                                textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-success" onclick="addBusinessToList('+index+')">';
                                textHTML += '<span class="glyphicon glyphicon-plus"></span>';
                                textHTML += '</button></td></tr>';
                            });
                        }else{
                            $(respond.data).each(function (index) {
                                var item = respond.data[index];
                                textHTML += '<tr id="'+index+'_res"><td class="text-left" style="width: 460px;">'+item.name+'<br/>';
                                textHTML += '<data data-name="'+item.name+'" data-id="'+item.id+'" data-city="'+item.address_district+', '+item.address_province+'"></data>';
                                textHTML += '<span class="small" style="color: gray;">'+item.address_district+', '+item.address_province+'</span></td>';
                                textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-success" onclick="addBusinessToList('+index+')">';
                                textHTML += '<span class="glyphicon glyphicon-plus"></span>';
                                textHTML += '</button></td></tr>';
                            });
                        }
                    }else{
                        textHTML += '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
                    }
                    $("#searchResult").html(textHTML);
                }
            });
        }
    }
    function addBusinessToList(index) {
        var res = $('#'+index+'_res data');
        var type = $(":input[name='type']:checked").val();
        var textHTML ="";
        if(type!="business"){
            var b_img = res.data('img');
            var b_id = res.data('id');
            textHTML += '<tr id="'+index+'_list"><td class="text-left" style="width:460px;"><img src="'+b_img+'" style="width:120px;">';
            textHTML += '<data data-id="'+b_id+'"></data></td>';
            textHTML += '<td class="text-center" style="width:60px;"><button type="button" class="btn btn-danger" onclick="delBusinessToList('+index+')">';
            textHTML += '<span class="glyphicon glyphicon-minus"></span>';
            textHTML += '</button></td></tr>';
        }else{
            var b_name = res.data('name');
            var b_id = res.data('id');
            var b_city = res.data('city');
            textHTML += '<tr id="'+index+'_list"><td class="text-left">'+b_name+'<br/>';
            textHTML += '<data data-id="'+b_id+'"></data>';
            textHTML += '<span class="small" style="color: gray;">'+b_city+'</span></td>';
            textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-danger" onclick="delBusinessToList('+index+')">';
            textHTML += '<span class="glyphicon glyphicon-minus"></span>';
            textHTML += '</button></td></tr>';
        }
        $('#businessList').append(textHTML);
        $('#'+index+'_res').remove();
        if($('#searchResult tr').length==0){
            textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
            $("#searchResult").html(textHTML);
        }
        $("#b_total").html($('#businessList tr').length);
        $('#search_btn').prop('disabled',true);
        $('#selected_type').val($(":input[name='type']:checked").val());
        $(":input[name='type']").prop('disabled',true);
        genBusinessList();
    }
    function delBusinessToList(index) {
        $('#'+index+'_list').remove();
        $("#b_total").html($('#businessList tr').length);
        $('#search_btn').prop('disabled',false);
        $(":input[name='type']").prop('disabled',false);
        genBusinessList();
    }
    function handleFileSelect(event) {
        image_to_upload_num = 0;
        var files = event.target.files;
        for(var i = 0; i< files.length; i++)
        {
            var file = files[i];
            if(!file.type.match('image'))
                continue;
            var picReader = new FileReader();

            picReader.addEventListener("load",function(event){
                var picFile = event.target;
                $("#simple_cover_image").attr('src',picFile.result);
            });
            picReader.readAsDataURL(file);
        }
    }
    function formReset() {
        $("#mainForm").find("input[type=text], textarea").val("");
    }
    function genBusinessList() {
        var id_list = '';
        $('#businessList tr td data').each(function (index,obj) {
            id_list += $(obj).data('id');
        });
        $('#content_id').val(id_list);
        $.fancybox.close();
    }
    $(document).ready(function () {
        $(":input[name='type']").change(function () {
            if($(this).val()=="link"){
                $("#link_div").show();
                $("#search_div").hide();
                $("#businessList").hide();
            }else{
                $("#link_div").hide();
                $("#search_div").show();
                $("#businessList").show();
            }
            $('#selected_type').val($(":input[name='type']:checked").val());
        });
    });
    document.getElementById('popup_cover').addEventListener('change', handleFileSelect, false);
</script>