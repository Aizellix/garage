<div class="clearfix"></div>

</div>

</div>
</div>

</div>

<div id="footer_bar"  <?php if($this->input->get('print')=="1") echo 'style="display: none;"'; ?>>
    <div class="col-md-12 text-center small">Pueanrod.com Back Office System, &copy 2016 Pueanrod.com</div>
</div>
<div id="wait_loading">
    <img src="<?php echo site_assets_url('images/loading.gif')?>">
</div>
<script>
    $(document).ready(function () {
        $("#wait_loading").height($(window).height());
        $("#wait_loading").width($(window).width());
        $("#wait_loading img").css('margin-top',$(window).height()/2.5);
        var min_h = $(window).height();
        min_h = min_h - $("#footer_bar").height() - $("#header_bar").height();
        $("#contentPage > .row").css('min-height',min_h);
    });
</script>
<?php if($this->input->get('print')=="1"){?>
    <script>
        $(document).ready(function () {
            $("#cmd").hide();
            $("#contentPage").removeClass('col-md-10');
            $("#mainPage").removeClass('container-fluid');
            $("#contentPage").addClass('col-md-12');
            $("#mainPage").addClass('container');
            $("#contentPage > .row").height($(window).height());
            $("#contentPage > .row").css('border','none');
            $("#contentPage > .row").css('margin','0');
            $("#contentPage > .row").css('padding','0 20px 0 0');
            $("body").css('background-color','#fff');
        });
    </script>
<?php }?>
</body>
</html>