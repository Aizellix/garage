<?php
$errorMSG = "";
$doneMSG = "";

?>
<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">ข้อมูลสถิติการเข้าชมโปรโมชั่น</strong>
</div>
<div class="col-md-12" style="clear: both;">

    <div id="main_content" class="">
        <div id="overall_stat">
            <form method="get">
                <div class="form-group" >
                    <div class="row">
                        <div class="form-inline col-md-12">
                            <label for="start_time">ข้อมูลสถิติ ตั้งแต่</label>
                            <input type="text" class="form-control" id="start_time" name="start" style="width: 220px;" value="<?php echo $start?>">
                            <label for="end_time">ถึง</label>
                            <input type="text" class="form-control" id="end_time" name="to" style="width: 220px;" value="<?php echo $to?>">
                            <button class="btn btn-primary" type="submit">ดำเนินการ</button>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <div id="show_stat">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th style="width: 50px;" class="text-center">ลำดับ</th>
                                <th class="text-center">รายชื่อ</th>
                                <th style="width: 100px;" class="text-center">การเข้าชม</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            if($total>0){
                                $i = 1;
                                $class_page = 1;
                                $j = 0;
                                foreach ($data as $item){
                                    echo '<tr class="all_page page_'.$class_page.'" style="display: none;"> <td class="text-center">'.$i.'</td>';
                                    echo '<td class="text-left"><a href="'.site_url('promotion/view/'.$item['id']).'" target="_blank">'.$item['title'].'</a></td>';
                                    echo '<td class="text-center">'.$item['total'].'</td></tr>';
                                    $i++;
                                    $j++;
                                    if($j>=$limit_page){
                                        $j=0;
                                        $class_page++;
                                    }
                                }
                            }else{
                                echo '<tr> <td class="text-center" colspan="3">ไม่พอข้อมูบ</td></tr>';
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <ul class="pagination" id="paging">
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="clearfix"></div>
</div>

<script>
    $( function() {
        var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
        gern_page();
    } );
    var page = 1;
    var total_page = <?php echo $total_page; ?>;

    function goto_page(page_id) {
        page = page_id;
        gern_page();
    }

    function gern_page() {
        var html_page = '';
        if(total_page>0){
            if ((page - 1) > 0) page_back = page - 1; else page_back = page;
            if ((page + 1) < total_page) page_next = page + 1; else page_next = total_page;
            html_page = '<li><a href="javascript:void(0);" onclick="goto_page(' + page_back + ')">&laquo;</a></li>';
            for (var i = 1; i <= total_page; i++) {
                if (i == page) is_active = ' class="active"'; else is_active = '';
                if (i <= 3 || i >= (total_page - 2) || i == page) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_back) {
                    html_page += '<li class="disabled"><span>...</span></li> <li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li>';
                } else if (i == page_next) {
                    html_page += '<li' + is_active + '><a href="javascript:void(0);" onclick="goto_page(' +i+ ')">' + i + '</a></li><li class="disabled"><span>...</span></li>';
                } else {
                    html_page += "#";
                }
            }
            while (html_page.indexOf("##") > 1) {
                html_page = html_page.replace("##", '#');
            }
            html_page = html_page.replace("#", '<li class="disabled"><span>...</span></li>');
            html_page = html_page.replace('<li class="disabled"><span>...</span></li><li class="disabled"><span>...</span></li>', '<li class="disabled"><span>...</span></li>');
            html_page += '<li><a href="javascript:void(0);" onclick="goto_page(' + page_next + ')">&raquo;</a></li>';
        }
        $("#paging").html(html_page);
        $(".all_page").hide();
        $(".page_"+page).show();
    }
</script>
