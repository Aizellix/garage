<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการป๊อปอัพ</strong>
</div>

<div class="col-md-12 col-lg-8" style="clear: both;">
    <strong style="font-size: 16px;">ป๊อปอัพ</strong>
    <form action="<?php echo site_url('popup/update/add')?>" method="post" enctype="multipart/form-data">
        <div id="main_content">
            <div class="form-group">
                <label for="popup_title">ชื่อหัวข้อบทความ</label>
                <p style="font-size: 24px; font-weight: bold;"><?php echo $data_list->title;?></p>
            </div>
            <div class="form-group">
                <label for="order_number">ลำดับ</label>
                <p style="font-size: 24px; font-weight: bold;"><?php echo $data_list->order_number;?></p>
            </div>
            <div class="form-group">
                <label for="popup_cover">รูปภาพหัวข้อบทความ</label>
                <div>
                    <img src="<?php echo $data_list->cover_image_src;?>" style="width: 50%">
                </div>
            </div>
            <div class="form-group" >
                <label for="">ระยะเวลาการเผยแพร่ :
                    <?php echo date('Y-m-d',strtotime($data_list->start_time));?> -
                    <?php echo date('Y-m-d',strtotime($data_list->end_time));?>
                </label>
            </div>
            <div class="form-group">
                <label for="order_number">ประเภท</label>
                <p style="font-size: 24px; font-weight: bold;"><?php echo $popup_type_list[$data_list->type];?></p>
            </div>
            <div class="form-group" >
                <label for="">รายการข้อมูล</label>
                <table class="table table-bordered">
                    <tbody id="businessList">
                    <?php if(count($content_list)>0){
                        $n=0;
                        foreach ($content_list as $item){
                            if($data_list->type=="business"){
                                ?>
                                <tr id="0_list">
                                    <td class="text-center" style="padding: 4px; width: 120px;" valign="middle">
                                        <img src="<?php echo $item->cover_image_src;?>" style="width: 100%">
                                    </td>
                                    <td class="text-left">
                                        <a href="<?php echo site_url('business/view/'.$item->id);?>" target="_blank"><?php echo $item->name;?></a><br>
                                        <span class="small" style="color: gray;"><?php echo $item->address_district;?>, <?php echo $item->address_province;?></span>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr id="0_list">
                                    <td class="text-left" style="width:460px;">
                                        <img src="<?php echo $item->cover_image_src;?>" style="width:120px;">
                                    </td>
                                    <td class="text-left">
                                        <a href="<?php echo site_url($data_list->type.'/view/'.$item->id);?>" target="_blank"><?php echo (isset($item->name)?$item->name:"[ดู]");?></a>
                                    </td>
                                </tr>
                            <?php   }
                        }
                    }else if($data_list->type=="link"){ ?>
                        <tr id=0_list">
                            <td class="text-left" style="width:460px;" colspan="2">
                                <input type="text" class="form-control" id="link_url" readonly value="<?php echo $data_list->link_url;?>">
                            </td>
                        </tr>
                    <?php } else{ ?>
                        <tr id="0_list">
                            <td class="text-center" colspan="2">
                                ไม่พบข้อมูล
                            </td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
            <div class="form-group">
                <?php
                $created_time = $data_list->created_time;
                $updated_time = $data_list->updated_time;
                if(isset($modifier_data['add'])){
                    $created_by = $modifier_data['add']->user_name;
                }else{
                    $created_by = 'ไม่ทราบ';
                }
                if(isset($modifier_data['edit'])){
                    $updated_by = $modifier_data['edit']->user_name;
                }else{
                    $updated_by = 'ไม่ทราบ';
                }
                ?>
                <div style="font-size: 12px; color: gray;">
                    <strong>สร้างโดย: </strong><span id="created_by" style="color: #000;"><?php echo $created_by;?></span> เมื่อ <span id="created_time" style="color: #000;"><?php echo $created_time;?></span>
                </div>
                <div style="font-size: 12px; color: gray;">
                    <strong>ปรับปรุงโดย: </strong><span id="updated_by" style="color: #000;"><?php echo $updated_by;?></span> เมื่อ <span id="updated_time" style="color: #000;"><?php echo $updated_time;?></span>
                </div>
            </div>
            <button type="button" class="btn btn-default" onclick="javascript:location.assign('<?php echo site_url('popup/edit/'.$data_list->id);?>')">Edit</button>

        </div>
    </form>

    <div class="clearfix"></div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 450px;">
            <div class="col-md-12">
                <form action="<?php echo site_url('popup/update')?>" method="post" id="emergency_form">
                    <input value="" name="do" id="post_method" type="hidden">
                    <input value="" name="id" id="item_id" type="hidden">

                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
         var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd 00:00',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd 23:59',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
</script>
<script>
    function del_data(id) {
        if(confirm('คุณต้องการลบบทความนี้หรือไม่?')){
            $("#post_method").val('del');
            $("#item_id").val(id);
            $("#emergency_form").submit();
        }
    }
</script>