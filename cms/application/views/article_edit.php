<div class="col-lg-12" style="height: 48px;">
    <strong style="font-size: 22px;">จัดการข้อมูลรายการบทความ</strong>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-12 col-lg-8" style="clear: both;">
            <strong style="font-size: 16px;">เพิ่มบทความ</strong>
            <form action="<?php echo site_url('article/update/edit')?>" method="post" enctype="multipart/form-data" id="mainForm">
                <div id="main_content">
                    <div class="form-group">
                        <label for="article_title">ชื่อหัวข้อบทความ</label>
                        <input type="text" class="form-control" id="article_title" placeholder="ชื่อหัวข้อบทความ" name="title" value="<?php echo $data_list->title;?>">
                    </div>
                    <div class="form-group">
                        <label for="article_cover">รูปภาพหัวข้อบทความ  (ใช้รูปขนาด 1166 x 500 เท่านั้น)</label>
                        <div>
                            <img id="simple_cover_image" src="<?php echo $data_list->cover_image_src;?>" style="width: 50%">
                        </div>
                        <input type="file" id="article_cover" name="cover">
                        <p class="help-block">สามารถ Upload ได้เฉพาะไฟล์ jpg, jpeg, png ขนาดไม่เกิน 4MB เท่านั้น</p>
                    </div>
                    <div class="form-group">
                        <label for="article_detail">ข้อความบรรยาย</label>
                        <textarea class="form-control" rows="10" id="article_detail" name="detail"><?php echo $data_list->detail;?></textarea>
                    </div>
                    <div class="form-group" >
                        <label for="">ระยะเวลาการเผยแพร่
                            <div class="form-inline col-md-12">
                                <label for="start_time">ตั้งแต่</label>
                                <input type="text" class="form-control" id="start_time" name="start_time" style="width: 220px;" value="<?php echo date('Y-m-d',strtotime($data_list->start_time));?>">
                                <label for="end_time">ถึง</label>
                                <input type="text" class="form-control" id="end_time" name="end_time" style="width: 220px;" value="<?php echo date('Y-m-d',strtotime($data_list->end_time));?>">
                            </div>
                        </label>
                    </div>
                    <div class="form-group" >
                        <label for="">รายชื่อสถานประกอบการ</label>
                        <table class="table table-bordered">
                            <thead>
                            <tr style="background: #ccc;">
                                <th colspan="2">
                                    <div class="row">
                                        <div class="col-md-6 text-left">ทั้งหมด: <span id="b_total"><?php echo count($business_list);?></span></div>
                                        <div class="col-md-6 text-right">
                                            <button class="btn btn-default" onclick="openSearchBox()" type="button">
                                                <span class="glyphicon glyphicon-plus"></span> เพิ่มสถานประกอบการ
                                            </button>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="businessList">

                            <?php if(count($business_list)>0){
                                $n=0;
                                foreach ($business_list as $item){
                                    ?>
                                    <tr id="<?php echo $n;?>_list">
                                        <td class="text-left"><?php echo $item->name;?><br/>
                                            <data data-id="<?php echo $item->id;?>"></data>
                                            <span class="small" style="color: gray;"><?php echo $item->address_district;?>, <?php echo $item->address_province;?></span></td>
                                        <td class="text-center" style="width: 60px;"><button type="button" class="btn btn-danger" onclick="delBusinessToList(<?php echo $n;?>)">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button></td>
                                    </tr>
                                <?php } }else{ ?>
                                <tr id="0_list">
                                    <td class="text-center" colspan="2">
                                        ไม่พบข้อมูล
                                    </td>
                                </tr>
                            <?php }?>

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <input value="<?php echo $data_list->id;?>" name="id" id="item_id" type="hidden">
                            <input value="<?php echo $data_list->cover_image_id;?>" name="image_id" id="item_id" type="hidden">
                            <button type="button" class="btn btn-default" onclick="formReset()">รีเซต</button>
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                            <input type="hidden" class="form-control" id="business_list" name="business_list" value="<?php echo $data_list->business_list;?>">
                        </div>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div id="markUp">
    <div id="showMarkUp">
        <div style="width: 550px;">
            <div class="col-md-12">
                <label for="exampleInputEmail1">ค้นหาสถานประกอบการ</label>
                <form class="form-inline">
                    <div class="form-group">

                        <input type="text" class="form-control" id="s_keyword" placeholder="ชื่อสถานประกอบการ / ที่อยู่" style="width: 438px;">
                    </div>
                    <button type="button" class="btn btn-default" onclick="searchBusiness()"><span class="glyphicon glyphicon-search"></span> ค้นหา</button>
                </form>
                <div style="padding-top: 16px; height: 550px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr style="background: #ccc;">
                            <th colspan="2" class="text-center">
                                รายการสถานประกอบการ
                            </th>
                        </tr>
                        </thead>
                        <tbody id="searchResult" style="  max-height: 450px; overflow-y: auto; display:block;">
                        <tr style="width: 100%; display: block;">
                            <td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่พบข้อมูล</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $( function() {
         var dateFormat = 'yy-mm-dd',
            from = $( "#start_time" )
                .datepicker({
                    dateFormat: 'yy-mm-dd',
                    defaultDate: "+0w",
                    changeMonth: true,
                    numberOfMonths: 3
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
            to = $( "#end_time" ).datepicker({
                dateFormat: 'yy-mm-dd',
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 3
            })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    } );
    function openSearchBox() {
        $.fancybox.open({href : '#markUp'} );
    }
    function searchBusiness() {
        var keyword = $("#s_keyword").val();
        var check_whitespace = keyword.replace(new RegExp(' ', 'g'), '');
        if(keyword.length>0&&check_whitespace.length>0){
            var textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">';
            textHTML += '<img src="<?php echo site_assets_url('images/loading.gif')?>"></td></tr>';
            $("#searchResult").html(textHTML);
            $.ajax({
                url: '<?php echo api_site_url('business/search_business?limit=10&offset=0&keyword='); ?>'+keyword,
                type: 'get',
                data: {'limit':20,'offset':0,'keyword':keyword},
                dataType: 'json',
                success: function (respond) {
                    var textHTML = '';
                    if(respond.total>0){
                        $(respond.data).each(function (index) {
                            var item = respond.data[index];
                            textHTML += '<tr id="'+index+'_res"><td class="text-left" style="width: 460px;">'+item.name+'<br/>';
                            textHTML += '<data data-name="'+item.name+'" data-id="'+item.id+'" data-city="'+item.address_district+', '+item.address_province+'"></data>';
                            textHTML += '<span class="small" style="color: gray;">'+item.address_district+', '+item.address_province+'</span></td>';
                            textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-success" onclick="addBusinessToList('+index+')">';
                            textHTML += '<span class="glyphicon glyphicon-plus"></span>';
                            textHTML += '</button></td></tr>';
                        })
                    }else{
                        textHTML += '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
                    }
                    $("#searchResult").html(textHTML);
                }
            });
        }
    }
    function addBusinessToList(index) {
        var res = $('#'+index+'_res data');
        var b_name = res.data('name');
        var b_id = res.data('id');
        var b_city = res.data('city');
        var textHTML = '<tr id="'+index+'_list"><td class="text-left">'+b_name+'<br/>';
        textHTML += '<data data-id="'+b_id+'"></data>';
        textHTML += '<span class="small" style="color: gray;">'+b_city+'</span></td>';
        textHTML += '<td class="text-center" style="width: 60px;"><button type="button" class="btn btn-danger" onclick="delBusinessToList('+index+')">';
        textHTML += '<span class="glyphicon glyphicon-minus"></span>';
        textHTML += '</button></td></tr>';
        $('#businessList').append(textHTML);
        $('#'+index+'_res').remove();
        if($('#searchResult tr').length==0){
            textHTML = '<tr style="width: 100%; display: block;"><td colspan="2" class="text-center" style="width: 100%; display: block;">ไม่มีข้อมูล</td></tr>';
            $("#searchResult").html(textHTML);
        }
        $("#b_total").html($('#businessList tr').length);
        genBusinessList();
    }
    function delBusinessToList(index) {
//        console.log(index);
        $('#'+index+'_list').remove();
        $("#b_total").html($('#businessList tr').length);
        genBusinessList();
    }
    function handleFileSelect(event) {
        image_to_upload_num = 0;
        var files = event.target.files;
        for(var i = 0; i< files.length; i++)
        {
            var file = files[i];
            if(!file.type.match('image'))
                continue;
            var picReader = new FileReader();

            picReader.addEventListener("load",function(event){
                var picFile = event.target;
                $("#simple_cover_image").attr('src',picFile.result);
            });
            picReader.readAsDataURL(file);
        }

    }

    function formReset() {
        $("#mainForm").find("input[type=text], textarea").val("");
    }
    function genBusinessList() {
        var id_list = '';
        $('#businessList tr td data').each(function (index,obj) {
            id_list += $(obj).data('id')+',';
        })
        $('#business_list').val(id_list);
    }
    document.getElementById('article_cover').addEventListener('change', handleFileSelect, false);
</script>