/*
Navicat MySQL Data Transfer

Source Server         : MySQL
Source Server Version : 50623
Source Host           : localhost:3306
Source Database       : garage

Target Server Type    : MYSQL
Target Server Version : 50623
File Encoding         : 65001

Date: 2017-03-07 10:13:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advertise
-- ----------------------------
DROP TABLE IF EXISTS `advertise`;
CREATE TABLE `advertise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `cover_image_id` int(11) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT NULL,
  `viewed` int(11) unsigned DEFAULT '0',
  `status` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advertise
-- ----------------------------
INSERT INTO `advertise` VALUES ('1', '1', '2017-03-04 00:00:00', '2017-03-31 00:00:00', '269', 'http://depot.garage.dev/images/article_1_20170226032332.jpg', '3', 'active', '2017-03-01 22:56:24', '2017-03-04 02:29:59');
INSERT INTO `advertise` VALUES ('2', '4', '2017-03-01 00:00:00', '2017-03-31 00:00:00', '271', 'http://depot.garage.dev/images/article_2_20170301224808.jpg', '0', 'active', '2017-03-01 22:55:44', '2017-03-01 22:55:44');
INSERT INTO `advertise` VALUES ('3', '4', '2017-03-01 00:00:00', '2017-03-31 00:00:00', '272', 'http://depot.garage.dev/images/article_3_20170301225431.jpg', '0', 'active', '2017-03-01 22:56:18', '2017-03-01 22:56:18');
INSERT INTO `advertise` VALUES ('4', '4', '2017-03-03 00:43:43', '2017-03-03 00:43:43', '273', 'http://depot.garage.dev/images/article_4_20170303004343.jpg', '0', 'active', '2017-03-03 00:43:43', '2017-03-03 00:43:43');
INSERT INTO `advertise` VALUES ('5', '4', '2017-03-03 00:00:00', '2017-03-31 00:00:00', '274', 'http://depot.garage.dev/images/article_5_20170303005729.jpg', '0', 'active', '2017-03-03 00:59:03', '2017-03-03 00:59:03');
INSERT INTO `advertise` VALUES ('6', '4', '2017-03-03 00:00:00', '2017-03-31 00:00:00', '276', 'http://depot.garage.dev/images/article_6_20170303010224.jpg', '0', 'active', '2017-03-03 01:02:24', '2017-03-03 01:02:24');
INSERT INTO `advertise` VALUES ('7', '4', '2017-03-03 01:02:46', '2017-03-03 01:02:46', '277', 'http://depot.garage.dev/images/article_7_20170303010246.jpg', '0', 'active', '2017-03-03 01:02:46', '2017-03-03 01:02:46');
INSERT INTO `advertise` VALUES ('8', '4', '2017-03-03 01:06:00', '2017-03-03 01:06:00', '278', 'http://depot.garage.dev/images/article_8_20170303010600.jpg', '0', 'active', '2017-03-03 01:06:00', '2017-03-03 01:06:00');
INSERT INTO `advertise` VALUES ('9', '4', '2017-03-03 01:06:55', '2017-03-03 01:06:55', '279', 'http://depot.garage.dev/images/article_9_20170303010655.jpg', '0', 'active', '2017-03-03 01:06:55', '2017-03-03 01:06:55');
INSERT INTO `advertise` VALUES ('10', '4', '2017-03-03 01:08:36', '2017-03-03 01:08:36', '280', 'http://depot.garage.dev/images/article_10_20170303010836.jpg', '0', 'deleted', '2017-03-03 01:08:36', '2017-03-03 01:08:36');
INSERT INTO `advertise` VALUES ('11', '4', '2017-03-01 00:00:00', '2017-03-30 00:00:00', '281', 'http://depot.garage.dev/images/article_11_20170303011258.jpg', '0', 'deleted', '2017-03-03 01:00:01', '2017-03-03 01:12:58');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `cover_image_id` int(11) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT '',
  `detail` text,
  `notice` text,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `business_list` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `viewed` int(11) unsigned DEFAULT '0',
  `liked` int(11) unsigned DEFAULT '0',
  `shared` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', 'Test Test', '261', 'http://depot.garage.dev/images/article_1_20161231002024.jpg', '<b>wewetetwet Test</b><br>', null, '2017-02-01 00:00:00', '2017-03-26 00:00:00', '', '2017-03-05 17:14:20', '2017-03-05 17:14:20', 'active', '0', '0', '0');
INSERT INTO `article` VALUES ('2', '', null, null, '<br>', null, '2017-02-25 13:05:42', '2017-02-25 13:05:42', null, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', '0', '0', '0');
INSERT INTO `article` VALUES ('3', '', null, null, '<br>', null, '2017-02-25 13:05:44', '2017-02-25 13:05:44', null, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', '0', '0', '0');
INSERT INTO `article` VALUES ('4', '', null, null, '<br>', null, '2017-02-25 13:05:47', '2017-02-25 13:05:47', null, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', '0', '0', '0');
INSERT INTO `article` VALUES ('5', '', null, null, '<br>', null, '2017-02-25 13:05:52', '2017-02-25 13:05:52', null, '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'deleted', '0', '0', '0');
INSERT INTO `article` VALUES ('6', 'Test 2017', '262', 'http://depot.garage.dev/images/article_6_20170118031416.jpg', 'Test 2017<br>', null, '2017-02-01 00:00:00', '2017-03-31 00:00:00', '1,4,', '2017-03-05 17:14:12', '2017-03-05 17:14:12', 'active', '0', '0', '0');
INSERT INTO `article` VALUES ('7', 'TEST_1 2017', '263', 'http://depot.garage.dev/images/article_7_20170211223803.jpg', 'TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST TEST <br>', null, '2017-02-11 00:00:00', '2017-03-31 00:00:00', '1,4,', '2017-03-05 17:07:18', '2017-03-05 17:07:18', 'active', '0', '0', '0');
INSERT INTO `article` VALUES ('8', 'Test', '284', 'http://depot.garage.dev/images/article_8_20170305171445.jpg', 'test', null, '2017-03-01 00:00:00', '2017-03-24 00:00:00', '4,', '2017-03-05 17:14:45', '2017-03-05 17:14:45', 'active', '0', '0', '0');

-- ----------------------------
-- Table structure for business
-- ----------------------------
DROP TABLE IF EXISTS `business`;
CREATE TABLE `business` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  `address_no` varchar(11) DEFAULT NULL,
  `address_moo` varchar(255) DEFAULT NULL,
  `address_village` varchar(255) DEFAULT NULL,
  `address_soi` varchar(255) DEFAULT NULL,
  `address_road` varchar(255) DEFAULT NULL,
  `address_tambol` varchar(255) DEFAULT NULL,
  `address_district` varchar(255) DEFAULT NULL,
  `address_province` varchar(255) DEFAULT NULL,
  `address_postcode` varchar(255) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `country_code` varchar(3) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `line` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cover_image_id` varchar(255) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT NULL,
  `work_day_sun` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_day_mon` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_day_tue` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_day_wed` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_day_thu` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_day_fri` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_day_sat` tinyint(1) unsigned zerofill DEFAULT NULL,
  `work_time_start` varchar(255) DEFAULT '00:00:00',
  `work_time_close` varchar(255) DEFAULT '00:00:00',
  `work_time_start_2` varchar(255) DEFAULT '00:00:00',
  `work_time_close_2` varchar(255) DEFAULT '00:00:00',
  `work_time_remark` varchar(255) DEFAULT NULL,
  `more_work_time` int(1) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `service_type` varchar(255) DEFAULT NULL,
  `detail` text,
  `facilities` varchar(255) DEFAULT NULL,
  `payment` varchar(255) DEFAULT NULL,
  `award_certification` varchar(255) DEFAULT NULL,
  `privilege` varchar(255) DEFAULT NULL,
  `score` int(11) unsigned DEFAULT '0',
  `viewed` int(11) unsigned DEFAULT '0',
  `liked` int(11) unsigned DEFAULT '0',
  `reviewed` int(11) unsigned DEFAULT '0',
  `shared` int(11) unsigned DEFAULT '0',
  `promoted_time_start` datetime DEFAULT NULL,
  `promoted_time_end` datetime DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_user_type` varchar(255) DEFAULT NULL,
  `created_user_id` int(11) DEFAULT NULL,
  `created_user_name` varchar(255) DEFAULT NULL,
  `status` varchar(15) DEFAULT 'pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of business
-- ----------------------------
INSERT INTO `business` VALUES ('1', 'Test 12345', 'A B', '', '', '', '', 'อังรีดูนังค์', 'ปทุมวัน', 'ปทุมวัน', 'กรุงเทพมหานคร', '10330', '13.731235783576345', '100.53398532450865', 'ประเทศไทย', 'TH', '021234567', 'facebook.com/test', 'test2', 'test@tes.com', '20', 'http://depot.garage.dev/images/business_1_20161122132905.jpg', '0', '1', '1', '1', '1', '1', '1', '09:00', '00:00', '10:30', '00:00', '', '1', '8', '[\"10\",\"11\",\"12\",\"13\"]', 'test test test testsetset', '[\"1\",\"2\",\"3\"]', '[\"cash\",\"credit\"]', '', '', '0', '1', '0', '0', '0', '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-03-04 02:33:34', '2017-03-04 02:33:34', '0', '0', 'FirstGathering', 'vip');
INSERT INTO `business` VALUES ('2', 'abc 123', 'A B', '', '', '', '', 'ถนน เพลินจิต', 'ลุมพินี', 'ปทุมวัน', 'กรุงเทพมหานคร', '10330', '13.7236482960288', '100.52958650172422', 'ประเทศไทย', 'TH', '021234567', 'facebook.com/test', 'test', 'test@tes.com', '219', 'http://depot.garage.dev/images/business_2_20161123032858.jpg', '0', '1', '1', '1', '1', '1', '1', '09:00', '00:00', '10:30', '00:00', '', '1', '9', '[\"10\",\"11\"]', 'test test test testsetset', '[\"1\",\"2\",\"3\"]', '[\"cash\",\"credit\"]', '', '', '0', '0', '0', '0', '0', '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-03-05 06:48:03', '2017-03-05 06:48:03', '0', '0', 'FirstGathering', 'pending');
INSERT INTO `business` VALUES ('3', 'ASDXC', 'asdasdasd', '', '', '', '', 'รัชดาภิเษก', 'วัดท่าพระ', 'บางกอกใหญ่', 'กรุงเทพมหานคร', '10600', '13.729703714522532', '100.53672117770384', 'ประเทศไทย', 'TH', '02-123-4567', '', '', '', '224', 'http://depot.garage.dev/images/business_3_20161129005058.jpg', '1', '1', '1', '0', '0', '0', '0', '00:00', '24:00', '00:00', '24:00', '', '0', '8', '[\"10\",\"11\",\"12\"]', '', '[\"1\",\"2\"]', '[\"cash\"]', '', '', '0', '0', '0', '0', '0', '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-03-05 06:45:29', '2017-03-05 06:45:29', '0', '0', 'FirstGathering', 'active');
INSERT INTO `business` VALUES ('4', 'TEST TEST', 'TEST TEST', '', '', '', '', 'ซอย สีลม 3', 'สีลม', 'บางรัก', 'กรุงเทพมหานคร', '10500', '13.7259203990867', '100.53099197924803', 'ประเทศไทย', 'TH', '02-123-4567', '', '', '', '255', 'http://depot.garage.dev/images/business_4_20161230201815.jpg', '0', '1', '1', '1', '1', '1', '0', '08:30', '17:00', '00:00', '24:00', '', '0', '8', '[\"10\",\"11\"]', '', '[\"1\",\"2\",\"3\"]', '[\"cash\"]', '', '', '0', '19', '0', '0', '0', '2016-01-01 00:00:00', '2016-01-01 00:00:00', '2017-03-05 06:48:08', '2017-03-05 06:48:08', '0', '0', 'FirstGathering', 'active');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'ห้องรับรอง', '3', '2016-11-03 03:15:59', '2016-11-03 03:15:59', 'active');
INSERT INTO `category` VALUES ('2', 'ร้านกาแฟ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active');
INSERT INTO `category` VALUES ('3', 'Wifi', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active');
INSERT INTO `category` VALUES ('4', 'บริการรับ - ส่งรถ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active');
INSERT INTO `category` VALUES ('5', 'ที่จอดรถ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active');
INSERT INTO `category` VALUES ('6', 'นำสัตว์เลี้ยงเข้าได้', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active');
INSERT INTO `category` VALUES ('7', 'รับจองคิว ', '3', '2016-11-03 03:16:04', '2016-11-03 03:16:04', 'active');
INSERT INTO `category` VALUES ('8', 'อู่ซ่อม', '1', '2016-11-03 03:16:34', '2016-11-03 03:16:34', 'active');
INSERT INTO `category` VALUES ('9', 'ศูนย์บริการ', '1', '2016-11-03 03:16:34', '2016-11-03 03:16:34', 'active');
INSERT INTO `category` VALUES ('10', 'ซ่อม', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active');
INSERT INTO `category` VALUES ('11', 'เปลี่ยนอะไหล่', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active');
INSERT INTO `category` VALUES ('12', 'ทำสี', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active');
INSERT INTO `category` VALUES ('13', 'ล้างรถ', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active');
INSERT INTO `category` VALUES ('14', 'ครบวงจร', '2', '2016-11-03 03:17:51', '2016-11-03 03:17:51', 'active');
INSERT INTO `category` VALUES ('15', 'test', '1', '2016-11-03 04:47:39', '2016-11-03 04:47:39', 'deleted');
INSERT INTO `category` VALUES ('16', 'test2', '1', '2016-11-03 20:57:57', '2016-11-03 20:57:57', 'deleted');

-- ----------------------------
-- Table structure for emergency
-- ----------------------------
DROP TABLE IF EXISTS `emergency`;
CREATE TABLE `emergency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of emergency
-- ----------------------------
INSERT INTO `emergency` VALUES ('1', 'ก สายด่วนตำรวจ', '191', 'active', '2016-12-26 15:41:18', '2016-12-26 15:41:18');
INSERT INTO `emergency` VALUES ('2', 'ข สายด่วนดับเพลิง', '199', 'active', '2016-12-26 15:41:25', '2016-12-26 15:41:25');
INSERT INTO `emergency` VALUES ('3', null, null, 'active', '2017-02-25 23:29:56', '2017-02-25 23:29:56');

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `src` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'active',
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=285 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of images
-- ----------------------------
INSERT INTO `images` VALUES ('1', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103171814.jpg', 'business_11_20161103171814.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('2', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103171815.jpg', 'business_11_20161103171815.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('3', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103172055.jpg', 'business_11_20161103172055.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('4', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103172055.jpg', 'business_11_20161103172055.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('5', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103172424.jpg', 'business_11_20161103172424.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('6', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103172424.jpg', 'business_11_20161103172424.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('7', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103173006.jpg', 'business_11_20161103173006.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('8', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103173006.jpg', 'business_11_20161103173006.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('9', 'business', '11', 'http://depot.garage.dev/images/business_11_20161103174712.jpg', 'business_11_20161103174712.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('10', 'business', '13', 'http://depot.garage.dev/images/business_13_20161113000406.jpg', 'business_13_20161113000406.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('11', 'business', '13', 'http://depot.garage.dev/images/business_13_20161113000406.jpg', 'business_13_20161113000406.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('12', 'business', '1', 'http://depot.garage.dev/images/business_1_20161113002703.jpg', 'business_1_20161113002703.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08');
INSERT INTO `images` VALUES ('13', 'business', '1', 'http://depot.garage.dev/images/business_1_20161113002703.jpg', 'business_1_20161113002703.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08');
INSERT INTO `images` VALUES ('14', 'business', '2', 'http://depot.garage.dev/images/business_2_20161114232545.jpg', 'business_2_20161114232545.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('15', 'business', '2', 'http://depot.garage.dev/images/business_2_20161114232546.jpg', 'business_2_20161114232546.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('16', 'business', '1', 'http://depot.garage.dev/images/business_1_20161122132629.png', 'business_1_20161122132629.png', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08');
INSERT INTO `images` VALUES ('17', 'business', '1', 'http://depot.garage.dev/images/business_1_20161122132727.png', 'business_1_20161122132727.png', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08');
INSERT INTO `images` VALUES ('18', 'business', '1', 'http://depot.garage.dev/images/business_1_20161122132814.jpg', 'business_1_20161122132814.jpg', 'deleted', '2016-12-10 04:13:18', '2016-12-10 04:13:18');
INSERT INTO `images` VALUES ('19', 'business', '1', 'http://depot.garage.dev/images/business_1_20161122132814.jpg', 'business_1_20161122132814.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08');
INSERT INTO `images` VALUES ('20', 'business', '1', 'http://depot.garage.dev/images/business_1_20161122132905.jpg', 'business_1_20161122132905.jpg', 'active', null, null);
INSERT INTO `images` VALUES ('21', 'business', '1', 'http://depot.garage.dev/images/business_1_20161122135241.jpg', 'business_1_20161122135241.jpg', 'deleted', '2016-12-10 04:13:08', '2016-12-10 04:13:08');
INSERT INTO `images` VALUES ('186', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032124.jpg', 'business_2_20161123032124.jpg', 'active', '2016-11-23 03:21:24', '2016-11-23 03:21:24');
INSERT INTO `images` VALUES ('187', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032125.jpg', 'business_2_20161123032125.jpg', 'active', '2016-11-23 03:21:25', '2016-11-23 03:21:25');
INSERT INTO `images` VALUES ('188', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032127.jpg', 'business_2_20161123032127.jpg', 'active', '2016-11-23 03:21:27', '2016-11-23 03:21:27');
INSERT INTO `images` VALUES ('189', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032128.jpg', 'business_2_20161123032128.jpg', 'active', '2016-11-23 03:21:28', '2016-11-23 03:21:28');
INSERT INTO `images` VALUES ('190', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032129.jpg', 'business_2_20161123032129.jpg', 'active', '2016-11-23 03:21:29', '2016-11-23 03:21:29');
INSERT INTO `images` VALUES ('191', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032130.jpg', 'business_2_20161123032130.jpg', 'active', '2016-11-23 03:21:30', '2016-11-23 03:21:30');
INSERT INTO `images` VALUES ('192', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032132.jpg', 'business_2_20161123032132.jpg', 'active', '2016-11-23 03:21:32', '2016-11-23 03:21:32');
INSERT INTO `images` VALUES ('193', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032133.jpg', 'business_2_20161123032133.jpg', 'active', '2016-11-23 03:21:33', '2016-11-23 03:21:33');
INSERT INTO `images` VALUES ('194', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032134.jpg', 'business_2_20161123032134.jpg', 'active', '2016-11-23 03:21:34', '2016-11-23 03:21:34');
INSERT INTO `images` VALUES ('195', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032136.jpg', 'business_2_20161123032136.jpg', 'active', '2016-11-23 03:21:36', '2016-11-23 03:21:36');
INSERT INTO `images` VALUES ('196', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032137.jpg', 'business_2_20161123032137.jpg', 'active', '2016-11-23 03:21:37', '2016-11-23 03:21:37');
INSERT INTO `images` VALUES ('197', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032138.jpg', 'business_2_20161123032138.jpg', 'active', '2016-11-23 03:21:38', '2016-11-23 03:21:38');
INSERT INTO `images` VALUES ('198', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032139.jpg', 'business_2_20161123032139.jpg', 'active', '2016-11-23 03:21:39', '2016-11-23 03:21:39');
INSERT INTO `images` VALUES ('199', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032141.jpg', 'business_2_20161123032141.jpg', 'active', '2016-11-23 03:21:41', '2016-11-23 03:21:41');
INSERT INTO `images` VALUES ('200', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032142.jpg', 'business_2_20161123032142.jpg', 'active', '2016-11-23 03:21:42', '2016-11-23 03:21:42');
INSERT INTO `images` VALUES ('201', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032143.jpg', 'business_2_20161123032143.jpg', 'active', '2016-11-23 03:21:43', '2016-11-23 03:21:43');
INSERT INTO `images` VALUES ('202', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032145.jpg', 'business_2_20161123032145.jpg', 'active', '2016-11-23 03:21:45', '2016-11-23 03:21:45');
INSERT INTO `images` VALUES ('203', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032837.jpg', 'business_2_20161123032837.jpg', 'active', '2016-11-23 03:28:37', '2016-11-23 03:28:37');
INSERT INTO `images` VALUES ('204', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032839.jpg', 'business_2_20161123032839.jpg', 'active', '2016-11-23 03:28:39', '2016-11-23 03:28:39');
INSERT INTO `images` VALUES ('205', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032840.jpg', 'business_2_20161123032840.jpg', 'active', '2016-11-23 03:28:40', '2016-11-23 03:28:40');
INSERT INTO `images` VALUES ('206', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032841.jpg', 'business_2_20161123032841.jpg', 'active', '2016-11-23 03:28:41', '2016-11-23 03:28:41');
INSERT INTO `images` VALUES ('207', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032842.jpg', 'business_2_20161123032842.jpg', 'active', '2016-11-23 03:28:42', '2016-11-23 03:28:42');
INSERT INTO `images` VALUES ('208', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032844.jpg', 'business_2_20161123032844.jpg', 'active', '2016-11-23 03:28:44', '2016-11-23 03:28:44');
INSERT INTO `images` VALUES ('209', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032845.jpg', 'business_2_20161123032845.jpg', 'active', '2016-11-23 03:28:45', '2016-11-23 03:28:45');
INSERT INTO `images` VALUES ('210', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032846.jpg', 'business_2_20161123032846.jpg', 'active', '2016-11-23 03:28:46', '2016-11-23 03:28:46');
INSERT INTO `images` VALUES ('211', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032848.jpg', 'business_2_20161123032848.jpg', 'active', '2016-11-23 03:28:48', '2016-11-23 03:28:48');
INSERT INTO `images` VALUES ('212', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032849.jpg', 'business_2_20161123032849.jpg', 'active', '2016-11-23 03:28:49', '2016-11-23 03:28:49');
INSERT INTO `images` VALUES ('213', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032850.jpg', 'business_2_20161123032850.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('214', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032851.jpg', 'business_2_20161123032851.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('215', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032852.jpg', 'business_2_20161123032852.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('216', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032854.jpg', 'business_2_20161123032854.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('217', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032855.jpg', 'business_2_20161123032855.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('218', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032856.jpg', 'business_2_20161123032856.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('219', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032858.jpg', 'business_2_20161123032858.jpg', 'active', '2016-11-23 03:28:58', '2016-11-23 03:28:58');
INSERT INTO `images` VALUES ('220', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032859.jpg', 'business_2_20161123032859.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('221', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032900.jpg', 'business_2_20161123032900.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('222', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032902.jpg', 'business_2_20161123032902.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('223', 'business', '2', 'http://depot.garage.dev/images/business_2_20161123032903.jpg', 'business_2_20161123032903.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('224', 'business', '3', 'http://depot.garage.dev/images/business_3_20161129005058.jpg', 'business_3_20161129005058.jpg', 'active', '2016-11-29 00:50:58', '2016-11-29 00:50:58');
INSERT INTO `images` VALUES ('225', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022318.jpg', 'business_2_20161129022318.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('226', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022319.jpg', 'business_2_20161129022319.jpg', 'active', '2016-12-10 04:08:14', '2016-12-10 04:08:14');
INSERT INTO `images` VALUES ('227', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022320.jpg', 'business_2_20161129022320.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('228', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022322.jpg', 'business_2_20161129022322.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('229', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022323.jpg', 'business_2_20161129022323.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('230', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022324.jpg', 'business_2_20161129022324.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('231', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022325.jpg', 'business_2_20161129022325.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('232', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022327.jpg', 'business_2_20161129022327.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('233', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022328.jpg', 'business_2_20161129022328.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('234', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022329.jpg', 'business_2_20161129022329.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('235', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129022331.jpg', 'business_2_20161129022331.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('236', 'business', '2', 'http://depot.garage.dev/images/business_2_20161129023951.jpg', 'business_2_20161129023951.jpg', 'active', '2016-12-10 04:08:15', '2016-12-10 04:08:15');
INSERT INTO `images` VALUES ('237', 'business', '1', 'http://depot.garage.dev/images/business_1_20161204224334.jpg', 'business_1_20161204224334.jpg', 'deleted', '2016-12-10 04:13:29', '2016-12-10 04:13:29');
INSERT INTO `images` VALUES ('238', 'business', '1', 'http://depot.garage.dev/images/business_1_20161204224357.jpg', 'business_1_20161204224357.jpg', 'deleted', '2016-12-10 04:13:29', '2016-12-10 04:13:29');
INSERT INTO `images` VALUES ('239', 'business', '1', 'http://depot.garage.dev/images/business_1_20161204224523.jpg', 'business_1_20161204224523.jpg', 'deleted', '2016-12-10 04:13:39', '2016-12-10 04:13:39');
INSERT INTO `images` VALUES ('240', 'business', '1', 'http://depot.garage.dev/images/business_1_20161204224814.jpg', 'business_1_20161204224814.jpg', 'deleted', '2016-12-10 04:13:39', '2016-12-10 04:13:39');
INSERT INTO `images` VALUES ('241', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210041418.jpg', 'business_1_20161210041418.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('242', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210042721.jpg', 'business_1_20161210042721.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('243', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210042723.jpg', 'business_1_20161210042723.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('244', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210042725.jpg', 'business_1_20161210042725.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('245', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210042726.jpg', 'business_1_20161210042726.jpg', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('246', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210043411.png', 'business_1_20161210043411.png', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('247', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210043554.png', 'business_1_20161210043554.png', 'deleted', '2016-12-10 04:36:29', '2016-12-10 04:36:29');
INSERT INTO `images` VALUES ('248', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210043643.jpg', 'business_1_20161210043643.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47');
INSERT INTO `images` VALUES ('249', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210043645.jpg', 'business_1_20161210043645.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47');
INSERT INTO `images` VALUES ('250', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210044102.jpg', 'business_1_20161210044102.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47');
INSERT INTO `images` VALUES ('251', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210044138.jpg', 'business_1_20161210044138.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47');
INSERT INTO `images` VALUES ('252', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210045534.jpg', 'business_1_20161210045534.jpg', 'deleted', '2016-12-10 04:55:47', '2016-12-10 04:55:47');
INSERT INTO `images` VALUES ('253', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210045620.jpg', 'business_1_20161210045620.jpg', 'active', '2016-12-10 04:56:20', '2016-12-10 04:56:20');
INSERT INTO `images` VALUES ('254', 'business', '1', 'http://depot.garage.dev/images/business_1_20161210045622.jpg', 'business_1_20161210045622.jpg', 'active', '2016-12-10 04:56:22', '2016-12-10 04:56:22');
INSERT INTO `images` VALUES ('255', 'business', '4', 'http://depot.garage.dev/images/business_4_20161230201815.jpg', 'business_4_20161230201815.jpg', 'active', '2016-12-30 20:18:15', '2016-12-30 20:18:15');
INSERT INTO `images` VALUES ('256', 'article', '9', 'http://depot.garage.dev/images/article_9_20161230225501.jpg', 'article_9_20161230225501.jpg', 'active', '2016-12-30 22:55:01', '2016-12-30 22:55:01');
INSERT INTO `images` VALUES ('257', 'article', '10', 'http://depot.garage.dev/images/article_10_20161230225620.jpg', 'article_10_20161230225620.jpg', 'active', '2016-12-30 22:56:20', '2016-12-30 22:56:20');
INSERT INTO `images` VALUES ('258', 'article', '1', 'http://depot.garage.dev/images/article_1_20161230225702.jpg', 'article_1_20161230225702.jpg', 'active', '2016-12-30 22:57:02', '2016-12-30 22:57:02');
INSERT INTO `images` VALUES ('259', 'article', '1', 'http://depot.garage.dev/images/article_1_20161230225803.jpg', 'article_1_20161230225803.jpg', 'active', '2016-12-30 22:58:03', '2016-12-30 22:58:03');
INSERT INTO `images` VALUES ('260', 'article', '1', 'http://depot.garage.dev/images/article_1_20161230231048.jpg', 'article_1_20161230231048.jpg', 'deleted', '2016-12-31 00:20:24', '2016-12-31 00:20:24');
INSERT INTO `images` VALUES ('261', 'article', '1', 'http://depot.garage.dev/images/article_1_20161231002024.jpg', 'article_1_20161231002024.jpg', 'active', '2016-12-31 00:20:24', '2016-12-31 00:20:24');
INSERT INTO `images` VALUES ('262', 'article', '6', 'http://depot.garage.dev/images/article_6_20170118031416.jpg', 'article_6_20170118031416.jpg', 'active', '2017-01-18 03:14:16', '2017-01-18 03:14:16');
INSERT INTO `images` VALUES ('263', 'article', '7', 'http://depot.garage.dev/images/article_7_20170211223803.jpg', 'article_7_20170211223803.jpg', 'active', '2017-02-11 22:38:03', '2017-02-11 22:38:03');
INSERT INTO `images` VALUES ('264', 'promotion', '1', 'http://depot.garage.dev/images/promotion_1_20170225125553.jpg', 'promotion_1_20170225125553.jpg', 'deleted', '2017-03-01 22:26:01', '2017-03-01 22:26:01');
INSERT INTO `images` VALUES ('265', 'test', '1', 'http://depot.garage.dev/images/test_1_20170226014816.jpg', 'test_1_20170226014816.jpg', 'active', '2017-02-26 01:48:16', '2017-02-26 01:48:16');
INSERT INTO `images` VALUES ('266', 'article', '5', 'http://depot.garage.dev/images/article_5_20170226025741.jpg', 'article_5_20170226025741.jpg', 'active', '2017-02-26 02:57:41', '2017-02-26 02:57:41');
INSERT INTO `images` VALUES ('267', 'article', '6', 'http://depot.garage.dev/images/article_6_20170226025836.jpg', 'article_6_20170226025836.jpg', 'active', '2017-02-26 02:58:36', '2017-02-26 02:58:36');
INSERT INTO `images` VALUES ('268', 'article', '1', 'http://depot.garage.dev/images/article_1_20170226025928.jpg', 'article_1_20170226025928.jpg', 'active', '2017-02-26 02:59:28', '2017-02-26 02:59:28');
INSERT INTO `images` VALUES ('269', 'article', '1', 'http://depot.garage.dev/images/article_1_20170226032332.jpg', 'article_1_20170226032332.jpg', 'active', '2017-02-26 03:23:32', '2017-02-26 03:23:32');
INSERT INTO `images` VALUES ('270', 'promotion', '1', 'http://depot.garage.dev/images/promotion_1_20170301222601.jpg', 'promotion_1_20170301222601.jpg', 'active', '2017-03-01 22:26:01', '2017-03-01 22:26:01');
INSERT INTO `images` VALUES ('271', 'article', '2', 'http://depot.garage.dev/images/article_2_20170301224808.jpg', 'article_2_20170301224808.jpg', 'active', '2017-03-01 22:48:08', '2017-03-01 22:48:08');
INSERT INTO `images` VALUES ('272', 'article', '3', 'http://depot.garage.dev/images/article_3_20170301225431.jpg', 'article_3_20170301225431.jpg', 'active', '2017-03-01 22:54:31', '2017-03-01 22:54:31');
INSERT INTO `images` VALUES ('273', 'article', '4', 'http://depot.garage.dev/images/article_4_20170303004343.jpg', 'article_4_20170303004343.jpg', 'active', '2017-03-03 00:43:43', '2017-03-03 00:43:43');
INSERT INTO `images` VALUES ('274', 'article', '5', 'http://depot.garage.dev/images/article_5_20170303005729.jpg', 'article_5_20170303005729.jpg', 'active', '2017-03-03 00:57:29', '2017-03-03 00:57:29');
INSERT INTO `images` VALUES ('275', 'article', '6', 'http://depot.garage.dev/images/article_6_20170303010008.jpg', 'article_6_20170303010008.jpg', 'active', '2017-03-03 01:00:08', '2017-03-03 01:00:08');
INSERT INTO `images` VALUES ('276', 'article', '6', 'http://depot.garage.dev/images/article_6_20170303010224.jpg', 'article_6_20170303010224.jpg', 'active', '2017-03-03 01:02:24', '2017-03-03 01:02:24');
INSERT INTO `images` VALUES ('277', 'article', '7', 'http://depot.garage.dev/images/article_7_20170303010246.jpg', 'article_7_20170303010246.jpg', 'active', '2017-03-03 01:02:46', '2017-03-03 01:02:46');
INSERT INTO `images` VALUES ('278', 'article', '8', 'http://depot.garage.dev/images/article_8_20170303010600.jpg', 'article_8_20170303010600.jpg', 'active', '2017-03-03 01:06:00', '2017-03-03 01:06:00');
INSERT INTO `images` VALUES ('279', 'article', '9', 'http://depot.garage.dev/images/article_9_20170303010655.jpg', 'article_9_20170303010655.jpg', 'active', '2017-03-03 01:06:55', '2017-03-03 01:06:55');
INSERT INTO `images` VALUES ('280', 'article', '10', 'http://depot.garage.dev/images/article_10_20170303010836.jpg', 'article_10_20170303010836.jpg', 'active', '2017-03-03 01:08:36', '2017-03-03 01:08:36');
INSERT INTO `images` VALUES ('281', 'article', '11', 'http://depot.garage.dev/images/article_11_20170303011258.jpg', 'article_11_20170303011258.jpg', 'active', '2017-03-03 01:12:58', '2017-03-03 01:12:58');
INSERT INTO `images` VALUES ('282', 'promotion', '2', 'http://depot.garage.dev/images/promotion_2_20170303025916.jpg', 'promotion_2_20170303025916.jpg', 'active', '2017-03-03 02:59:16', '2017-03-03 02:59:16');
INSERT INTO `images` VALUES ('283', 'promotion', '3', 'http://depot.garage.dev/images/promotion_3_20170303030023.jpg', 'promotion_3_20170303030023.jpg', 'active', '2017-03-03 03:00:23', '2017-03-03 03:00:23');
INSERT INTO `images` VALUES ('284', 'article', '8', 'http://depot.garage.dev/images/article_8_20170305171445.jpg', 'article_8_20170305171445.jpg', 'active', '2017-03-05 17:14:45', '2017-03-05 17:14:45');

-- ----------------------------
-- Table structure for markers
-- ----------------------------
DROP TABLE IF EXISTS `markers`;
CREATE TABLE `markers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `address` varchar(80) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `lng` float(10,6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of markers
-- ----------------------------
INSERT INTO `markers` VALUES ('1', 'Frankie Johnnie & Luigo Too', '939 W El Camino Real, Mountain View, CA', '37.386337', '-122.085823');
INSERT INTO `markers` VALUES ('2', 'Amici\'s East Coast Pizzeria', '790 Castro St, Mountain View, CA', '37.387138', '-122.083237');
INSERT INTO `markers` VALUES ('3', 'Kapp\'s Pizza Bar & Grill', '191 Castro St, Mountain View, CA', '37.393887', '-122.078918');
INSERT INTO `markers` VALUES ('4', 'Round Table Pizza: Mountain View', '570 N Shoreline Blvd, Mountain View, CA', '37.402653', '-122.079353');
INSERT INTO `markers` VALUES ('5', 'Tony & Alba\'s Pizza & Pasta', '619 Escuela Ave, Mountain View, CA', '37.394012', '-122.095528');
INSERT INTO `markers` VALUES ('6', 'Oregano\'s Wood-Fired Pizza', '4546 El Camino Real, Los Altos, CA', '37.401726', '-122.114647');

-- ----------------------------
-- Table structure for promotion
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `cover_image_id` int(11) DEFAULT NULL,
  `cover_image_src` varchar(255) DEFAULT NULL,
  `detail` text,
  `notice` text,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `business_list` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  `viewed` int(11) unsigned NOT NULL,
  `liked` int(11) unsigned NOT NULL DEFAULT '0',
  `shared` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of promotion
-- ----------------------------
INSERT INTO `promotion` VALUES ('1', 'Test Promotion', '270', 'http://depot.garage.dev/images/promotion_1_20170301222601.jpg', 'Test Promotion<br>', null, '2017-02-26 00:00:00', '2017-03-31 00:00:00', '4,', '2017-03-05 17:08:32', '2017-03-05 17:08:32', 'active', '0', '0', '0');
INSERT INTO `promotion` VALUES ('2', 'Test', '282', 'http://depot.garage.dev/images/promotion_2_20170303025916.jpg', 'test', null, '2017-03-03 02:59:16', '2017-03-03 02:59:16', '', '2017-03-03 02:59:16', '2017-03-03 02:59:16', 'active', '0', '0', '0');
INSERT INTO `promotion` VALUES ('3', 'Test', '283', 'http://depot.garage.dev/images/promotion_3_20170303030023.jpg', 'twetwet', null, '2017-03-18 00:00:00', '2017-03-31 00:00:00', '', '2017-03-03 03:00:23', '2017-03-03 03:00:23', 'active', '0', '0', '0');

-- ----------------------------
-- Table structure for review
-- ----------------------------
DROP TABLE IF EXISTS `review`;
CREATE TABLE `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of review
-- ----------------------------
INSERT INTO `review` VALUES ('1', '2', '4', 'Test', '5', '2017-03-04 02:14:37', '2017-03-04 02:18:11', 'active');
INSERT INTO `review` VALUES ('2', '1', '4', 'AAAAAAA 5555555555 AAAA', '3', '2017-03-04 02:18:27', '2017-03-05 06:48:08', 'active');
INSERT INTO `review` VALUES ('3', '2', '4', 'Test', '5', '2017-03-05 06:11:04', '2017-03-05 06:48:03', 'active');
INSERT INTO `review` VALUES ('4', '2', '4', 'Test', '5', '2017-03-05 06:36:35', '2017-03-05 06:46:17', 'active');

-- ----------------------------
-- Table structure for session
-- ----------------------------
DROP TABLE IF EXISTS `session`;
CREATE TABLE `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `fb_id` varchar(255) DEFAULT NULL,
  `fb_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of session
-- ----------------------------
INSERT INTO `session` VALUES ('1', '2c5b67f774b52f2abded46db5d594784a54e0d4beb8a4a40000243b68b71c2c8d7493767239ed057b4ed1e4cabae5f15a76109ac630d2e18d91cbe5fd205456e', '1', '1000000001', '10205560865053456', 'EAAI7nUxeqMYBABEZA4uZCZBU0hoa1T0D9FY4iymBc8OtaHQhZBKgmAS1IZAuAwRKT8ZA330DyZCZB8idfeCdfMMkMBoxi1J1DY7fbWROxE98ZAoyzWrDeWmGFZBxBxKLs1OW6ZCZA5DzvrEMJgYoxNzj9cHOVMQP8zkPX6po4t1I7xJHM8FHWnOGm8m7');
INSERT INTO `session` VALUES ('2', '40b244112641dd78dd4f93b6c9190dd46e0099194d5a44257b7efad6ef9ff4683da1eda0244448cb343aa688f5d3efd7314dafe580ac0bcbf115aeca9e8dc114', '2', '0', '', '');

-- ----------------------------
-- Table structure for staff
-- ----------------------------
DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of staff
-- ----------------------------
INSERT INTO `staff` VALUES ('1', 'aize', 'b1c1bfaa50e031fb31604b0245840f30ce3fc6c169baa72e65298a98f13b79a36edcd69f3c8ba724f8f58f488c68db213fc4611b6c1e89142a1b532a7dba63d6', 'admin@pueanrod.com', 'Aize', '0987654321', 'admin', 'active', '2017-02-26 08:47:37', '2017-02-26 08:47:37');
INSERT INTO `staff` VALUES ('2', 'Test2', '01fbbd71323dfe904f3565cb488ac2317120af112a69a66d218980c00024eaa46edb7d9cd7007e3b6bc16bea9d7793ae5566d4c3b0bffa203f7f6553ad469672', 'test1@test.com', null, null, 'admin', 'active', '2017-01-03 18:31:37', '2017-01-03 18:31:37');
INSERT INTO `staff` VALUES ('3', 'admin', 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec', 'admin@test.com', 'Admin', '0987654321', 'admin', 'active', '2017-02-26 06:48:17', '2017-02-26 06:48:17');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `reviewed` int(11) NOT NULL DEFAULT '0',
  `created_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '10205560865053456', 'Aize', 'http://graph.facebook.com/10205560865053456/picture?width=500', '1987-03-19', 'male', 'aize@outlook.com', '65ab9f871951a5a0c684280288231fec3f24eeff754f0e3b99be153f9bf54a080f4c203c1545caf57ecc7f1d880a780d6498bdaeb2cb0af06f445698ce3eb097', '0', '2017-03-05 06:48:08', '2017-03-05 06:48:08', 'active');
INSERT INTO `user` VALUES ('2', '', 'TEST TEST', '', '1987-03-19', 'male', 'aize.gamer@gmail.com', 'ee26b0dd4af7e749aa1a8ee3c10ae9923f618980772e473f8819a5d4940e0db27ac185f8a0e1d5f84f88bc887fd67b143732c304cc5fa9ad8e6f57f50028a8ff', '0', '2017-03-05 06:21:01', '2017-03-05 06:21:01', 'active');
